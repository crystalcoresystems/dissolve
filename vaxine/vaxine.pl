#!/usr/bin/perl
use strict;
use warnings;

# Vax-like Instruction Engine

my @memory = get_data();
my $l = 0;
my $q = 0;
my $fh = shift(@memory);
my $ip = 0;

# Main execution loop
while (1) {
    $ip = execute($ip);
    last if $ip == -1;
}
exit;

# Execute the instruction at the given instruction pointer
sub execute {
    my $ip = shift;
    my @code = split //, $memory[$ip];
    my $ins = $code[1];
    $ip++;

    if    ($ins == 0) { mvi(@code) }
    elsif ($ins == 1) { mov(@code) }
    elsif ($ins == 2) { add(@code) }
    elsif ($ins == 3) { min(@code) }
    elsif ($ins == 4) { com(@code) }
    elsif ($ins == 5) { $ip = blt($ip, @code) }
    elsif ($ins == 6) { $ip = beq($ip, @code) }
    elsif ($ins == 7) { rdl(@code) }
    elsif ($ins == 8) { wrl(@code) }
    else              { $ip = -1 }

    return $ip;
}

# Move immediate value to memory
sub mvi {
    my $mode = $_[2] % 4;
    my ($D, $B) = mode_AB($mode, @_);
    my $A = $_[3] * 10 + $_[4];

    $_[0] eq "-" and $A *= -1;
    $memory[$B] = int2str($A);
}

# Move value from source to destination in memory
sub mov {
    my $mode = $_[2] % 4;
    my ($A, $B) = mode_AB($mode, @_);
    $memory[$B] = $memory[$A];
}

# Add values from two memory locations and store the result
sub add {
    my $mode = $_[2] % 4;
    my ($A, $B) = mode_AB($mode, @_);
    my $x = int($memory[$A]) + int($memory[$B]);
    $x < -999999 and $x = ($x % -1_000_000) * -1;
    $x > 999999  and $x = ($x % 1_000_000)  * -1;
    $memory[$B] = int2str($x);
}

# Subtract value at source from value at destination and store the result
sub min {
    my $mode = $_[2] % 4;
    my ($A, $B) = mode_AB($mode, @_);
    my $x = int($memory[$B]) - int($memory[$A]);
    $x < -999999 and $x = ($x % -1_000_000) * -1;
    $x > 999999  and $x = ($x % 1_000_000)  * -1;
    $memory[$B] = int2str($x);
}

# Compare values at two memory locations and set flags
sub com {
    my $mode = $_[2] % 4;
    my ($A, $B) = mode_AB($mode, @_);
    my ($a, $b);
    my (@x, @y);

    $l = int($memory[$A]) < int($memory[$B]) ? 1 : 0;
    $q = int($memory[$A]) == int($memory[$B]) ? 1 : 0;

    if (int($memory[$A]) == 0 && int($memory[$B]) == 0) {
        @x = split //, $memory[$A];
        @y = split //, $memory[$B];
        $a = shift @x;
        $b = shift @y;
        if ($a eq "-" && $b eq "+") {
            $l = 1;
        }
    }
}

# Branch if less-than flag is set
sub blt {
    my $ip = shift;
    my $mode = $_[2] % 4;
    my ($A, $B) = mode_AB($mode, @_);
    return $l ? $B : $ip;
}

# Branch if equal flag is set
sub beq {
    my $ip = shift;
    my $mode = $_[2] % 4;
    my ($A, $B) = mode_AB($mode, @_);
    return $q ? $B : $ip;
}

# Read lines from file into memory
sub rdl {
    my $mode = $_[2] % 4;
    my ($D, $B) = mode_AB($mode, @_);
    my $A = $_[3] * 10 + $_[4];
    return if $A == 0;
    my $oa = $A;

    while (<$fh>) {
        chomp;
        my $data = $_;
        $data = cut_comment($data);
        next if length($data) < 7;
        $memory[ ($B + $oa - $A) % 100 ] = $data;
        $A--;
        return if $A == 0;
    }
}

# Write lines from memory to the output
sub wrl {
    my $mode = $_[2] % 4;
    my ($D, $B) = mode_AB($mode, @_);
    my $A = $_[3] * 10 + $_[4];
    for (0 .. $A - 1) {
        print $memory[ ($_ + $B) % 100 ], "\n";
    }
}

# Convert integer to string with sign and leading zeros
sub int2str {
    my $x = shift;
    my $sign = $x < 0 ? "-" : "+";
    $x = abs($x) % 1_000_000;
    return sprintf("%s%06d", $sign, $x);
}

# Determine source and destination addresses based on addressing mode
sub mode_AB {
    my $mode = shift;
    my ($to_store, $store_at);
    my ($ind_a, $ind_b);
    my @a;

    if ($mode == 0) {
        $to_store = $_[3] * 10 + $_[4];
        $store_at = $_[5] * 10 + $_[6];
        return ($to_store, $store_at);
    }
    elsif ($mode == 1) {
        $to_store = $_[3] * 10 + $_[4];
        $ind_b    = $_[5] * 10 + $_[6];
        @a        = split //, $memory[$ind_b];
        $store_at = $a[5] * 10 + $a[6];
    }
    elsif ($mode == 2) {
        $ind_a    = $_[3] * 10 + $_[4];
        @a        = split //, $memory[$ind_a];
        $to_store = $a[3] * 10 + $a[4];
        $store_at = $_[5] * 10 + $_[6];
    }
    elsif ($mode == 3) {
        $ind_a    = $_[3] * 10 + $_[4];
        @a        = split //, $memory[$ind_a];
        $to_store = $a[3] * 10 + $a[4];
        $ind_b    = $_[5] * 10 + $_[6];
        @a        = split //, $memory[$ind_b];
        $store_at = $a[5] * 10 + $a[6];
    }
    return ($to_store, $store_at);
}

# Remove comments from a line (anything after ';')
sub cut_comment {
    my $string = shift;
    my $i = 0;
    foreach (split //, $string) {
        last if $_ eq ";";
        $i++;
    }
    return substr($string, 0, $i);
}

# Get data from the user-specified file and initialize memory
sub get_data {
    print "\ndatafile: ";
    chomp(my $datafile = <>);
    open my $fh, '<', $datafile or die "File not found\n";
    print "\n\n";
    my @mem = ("+900000") x 100;

    while (<$fh>) {
        chomp;
        $_ = cut_comment($_);
        next if /^\s*$/;
        last if $_ eq '-1';
        next if length($_) < 10;
        my ($loc, $data) = split /\s+/, $_, 2;
        $mem[$loc] = $data;
    }
    return ($fh, @mem);
}
