; Program: Compute Absolute Value
; Description:
; Reads a value and computes its absolute value. If the value is negative, it uses an overflow technique to flip the sign.

; Note:
; - The value is stored in memory address 80
; - Max negative integer (-999999) is stored in memory address 81
; - Constants used:
;   - Memory[60]: Zero constant (0)
;   - Memory[61]: One constant (1)
; - The result (absolute value) is stored in memory address 80 and then output.

; Initialize Section
; ------------------
00 +700280 ; RDL: Read 2 lines into memory[80] (value) and memory[81] (max negative int)
01 +000060 ; MVI: Move immediate value 00 to address 60 (zero constant)
02 +000161 ; MVI: Move immediate value 01 to address 61 (one constant)

; Compare zero with value
03 +406080 ; COM: Compare memory[60] (zero) with memory[80] (value)

; If zero < value (i.e., value > 0), branch to output
04 +500007 ; BLT: If less-than flag is set, branch to address 07 (output result)

; Else, value <= 0, perform overflow trick to flip sign
05 +208180 ; ADD: Add memory[81] (max negative int) to memory[80] (value), store in memory[80]
06 +206180 ; ADD: Add memory[61] (one) to memory[80], store in memory[80]

; Output the absolute value
07 +800180 ; WRL: Write 1 line from memory[80] (absolute value) to output

; End of Program
-1

; Data Section
; ------------
-000007 ; Input value: -7 (memory[80])
-999999 ; Max negative int: -999999 (memory[81])
