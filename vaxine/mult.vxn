; Program: Multiply Two Operands Using Repeated Addition
; Description:
; This program multiplies two operands by repeatedly adding one operand to a sum while decrementing the other operand to zero.

; Note:
; - The operands are stored in memory addresses 80 and 81 after the `-1` end-of-program marker.
; - Memory addresses 80 and 81 start after the program code.

; Initialize Section
; ------------------
; Load constants and initialize sum

00 +700280 ; RDL: Read operands into memory[80] and memory[81] (optional, can be removed if operands are initialized after -1)
01 +000060 ; MVI: Move immediate value 00 to address 60 (zero constant)
02 +000161 ; MVI: Move immediate value 01 to address 61 (increment/decrement constant)
03 +000070 ; MVI: Move immediate value 00 to address 70 (initialize sum to zero)
04 +408060 ; COM: Compare memory[80] (op1) with memory[60] (zero)
05 +600013 ; BEQ: If equal, branch to address 13 (end of loop)

; Begin Loop
; ----------
06 +406060 ; COM: Compare memory[60] with memory[60] (always true)

07 +600020 ; BEQ: Unconditional jump to address 20 (Add Function)
08 +600040 ; BEQ: Unconditional jump to address 40 (Subtract Function) 

09 +406080 ; COM: Compare memory[60] (zero) with memory[80] (op1)
10 +600013 ; BEQ: Branch to address 13 (End Loop) if op1 is zero

11 +406060 ; COM: Compare memory[60] with memory[60] (always true)
12 +600006 ; BEQ: Unconditional jump to address 6 (Loop back to start of loop)

; End Loop
; --------
13 +800170 ; WRL: Write the result from memory[70] (sum) to output


; Add Function
; ------------
; Handles addition based on the signs of the operands
; Entry point: Address 20

20 +408160 ; COM: Compare memory[81] (op2) with memory[60] (zero)
21 +500026 ; BLT: If op2 < 0, branch to address 26
22 +408060 ; COM: Compare memory[80] (op1) with memory[60] (zero)
23 +500033 ; BLT: If op1 < 0, branch to address 33
24 +406060 ; COM: Compare memory[60] with memory[60] (always true)
25 +600030 ; BEQ: Unconditional jump to address 30

26 +408060 ; COM: Compare memory[80] (op1) with memory[60] (zero)
27 +500033 ; BLT: If op1 < 0, branch to address 33
28 +406060 ; COM: Compare memory[60] with memory[60] (always true)
29 +600030 ; BEQ: Unconditional jump to address 30

30 +208170 ; ADD: Add memory[81] (op2) to memory[70] (sum)
31 +406060 ; COM: Compare memory[60] with memory[60] (always true)
32 +600008 ; BEQ: Unconditional jump back to main loop at address 8

33 +308170 ; MIN: Subtract memory[81] (op2) from memory[70] (sum)
34 +406060 ; COM: Compare memory[60] with memory[60] (always true)
35 +600008 ; BEQ: Unconditional jump back to main loop at address 8

; End of Add Function

; Subtract Function
; -----------------
; Adjusts op1 (memory[80]) by incrementing or decrementing based on its sign
; Entry point: Address 40

40 +408060 ; COM: Compare memory[80] (op1) with memory[60] (zero)
41 +500045 ; BLT: If op1 < 0, branch to address 45
42 +306180 ; MIN: Subtract memory[61] (1) from memory[80] (decrement op1)
43 +406060 ; COM: Compare memory[60] with memory[60] (always true)
44 +600009 ; BEQ: Unconditional jump back to main loop at address 9

45 +206180 ; ADD: Add memory[61] (1) to memory[80] (increment op1)
46 +406060 ; COM: Compare memory[60] with memory[60] (always true)
47 +600009 ; BEQ: Unconditional jump back to main loop at address 9

; End of Subtract Function

; Data Section
; ------------
; The operands are initialized here after the `-1` marker.
; They are stored in memory addresses 80 and 81.

-1
-000044 ; Operand 1: -44
-000004 ; Operand 2: -4
