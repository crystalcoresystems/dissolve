#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#define MAX_ARRAY 1751040


    //  2 2 2 2
    // 3 5 7 9 11 13 15 17
    //   ^   x ^     x
    //  0 0 0 0  

    //  2 4  2  4  2  4  2  4  2  4  2  4  2  4  2  4  2
    // 5 7 11 13 17 19 23 25 29 31 35 37 41 43 47 49 53 55
    //   ^                   ^     x                    x
    //  0 1  0  1  0  1  0  1  0  1  0  1

    //  4  2  4  2  4  6  2  6  4  2  4  2  4  6  2  6  4  2  4  2  4  6  2  6  
    // 7 11 13 17 19 23 29 31 37 41 43 47 49 53 59 61 67 71 73 77 79 83 89
    //   ^                                x  ^                          x
    //  0  1  2  3  4  5  6  7  0  1  2  3  4  5  6  7  0  1  2  3  4  5

void loop_increment(int * pointer, int length) {
    *pointer = ((*pointer) + 1) % length;
}

int main() {
    uint8_t pattern[MAX_ARRAY];
    uint8_t new_pattern[MAX_ARRAY];
    int pattern_length;
    int new_len;
    int prime;
    int next_prime;

    int right_pointer;
    int middle_pointer;
    int right_value;
    int middle_value;
    int end_trigger;
    int initial_length;

    pattern[0] = 2;
    pattern[1] = 4;
    pattern_length = 2;

    prime = 5;
    while(1) {
        next_prime = prime + pattern[0];

        right_value = prime;
        right_pointer = 0;
        new_len = 0;

        while(right_value <= prime*prime) {
            end_trigger = right_pointer;
            right_value += pattern[right_pointer];
            loop_increment(&right_pointer, pattern_length);
            new_pattern[new_len] = pattern[right_pointer];
            new_len++;
        }
        new_pattern[new_len - 3] = new_pattern[new_len - 2] + new_pattern[new_len - 3];
        new_pattern[new_len - 2] = new_pattern[new_len - 1];
        new_len--;

        initial_length = new_len;

        middle_pointer = 1;
        middle_value   = next_prime;

        while(1) {

            right_value += pattern[right_pointer];
            loop_increment(&right_pointer, pattern_length);

            new_pattern[new_len] = pattern[right_pointer];
            new_len++;

            if (right_value % prime == 0) {
                if (right_pointer == end_trigger) {
                    break;
                }
                new_pattern[new_len - 2] = new_pattern[new_len - 2] + new_pattern[new_len - 1];
                new_len--;
            }

        }

        pattern_length = new_len - initial_length;
        for (int i = 0; i < pattern_length; i++) {
            printf("%d", new_pattern[i]);
            pattern[i] = new_pattern[i];
        }

        printf("\n\n%d : %d\n", pattern[0], pattern[pattern_length-1]);

        printf("%d : %d\n", pattern[1], pattern[pattern_length-2]);
        printf("%d : %d\n", pattern[2], pattern[pattern_length-3]);
        printf("%d : %d\n", pattern[3], pattern[pattern_length-4]);
        printf("pattern length: %d prime: %d\n\n", pattern_length, prime); 

    int key[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    for (int i = 0; i < pattern_length; i++) {
        prime += pattern[i];
        key[pattern[i]]++;
    }
    for (int i = 0; i < 40; i++) {
        printf("%d ", key[i]);
    }
    printf("\n");

        if (next_prime == 17)
            break;

        prime = next_prime;
    }
}




