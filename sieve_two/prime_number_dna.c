/*
 * find primes starting at 5
 * print (diff + 1) / 3
 */
#include <stdio.h>
#include <math.h>

int isprime(int num, int divisor);
void bounce_inc(int *num, int loop);

int main()
{   
    int last_prime;
    int ind;
    char print[][3] = { {'-',' ',' '},
                        {' ','-',' '},
                        {' ',' ','-'} };
    
    last_prime = 7;
    ind = 0;
    for (int i = 11; i < 100000; i+=2) {
        if (isprime(i, 3))
        {
            int y = ((i - last_prime + 1) / 3);
            if (y == 1) {
                printf("  %c%c%c\n",
                        print[ind][0],
                        print[ind][1],
                        print[ind][2]);
                bounce_inc(&ind, 3);
            } else {
                printf("*");
                for (int j = 0; j < y; j++) {
                    if (j > 0) {
                        printf(" ");
                    }
                    printf(" %c%c%c",
                            print[ind][0],
                            print[ind][1],
                            print[ind][2]);
                    bounce_inc(&ind, 3);
                    if (j < y - 1) {
                        printf("\n");
                    }
                }
               printf(" *\n");
            }
            last_prime = i;
        }
    }
}

int isprime(int num, int divisor)
/* some sort of recursion */
{
	if ( divisor > sqrt(num) )
		return 1;
	if ( num % divisor == 0 )
		return 0;
	if (isprime(num, divisor + 2) == 0 )
		return 0;	
}

void bounce_inc(int *num, int loop)
{
    static int dir = 1;
    int temp;

    temp = *num;
    if (temp == loop - 1) {
       temp = loop - 2;
       dir = -1;
    } else if (temp == 0) {
        temp = 1;
        dir = 1;
    } else {
        temp += dir;
    }
    *num = temp;
}
