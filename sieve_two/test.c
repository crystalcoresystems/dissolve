/*
 *
 *
 * This is an example of how to compress the pattern by
 * storing how the pattern is formed by previous patterns
 *
 * NOTE: The first digit is always added to the last digit
 * base pattern is 2
 * 1 - 2
 * 2 - 2 4
 * 5 - 4 2 4 2 4 6 2 6 [3 concat]
 * 7
 * 2424626
 * 4246626   [1 concat 3]
 * 426468    [2 concat 2, 6]
 * 4242486   [3 concat 5]
 * 4624626   [4 concat 1]
 * 6424626   [5 concat 0]
 * 424210210 [6 concat 4]
 *
* 424626424662642646842424144621026642462102421210
* 24246264666264264684246861024626642462642610210
* 2424684241226426461224248646246261024626424210210
* 2466266466264264684264864624686421026424210210
* 24248642466264846842424864666266424626424210210
* 2646264246684261084242481062486642462646210210
* 24246264246626664684242486484626642468424210210
* 2424621024686426468462486462462664662664210210
* 2424626421062642646842421264624621242486424210210
* 6246264246626421068642486462462666462642421012
* 2421026424662102641442424864624626642462642412212
*
* 4 1 3 0 1 2 1 00 5
*/

#include <stdio.h>

#define LOOP 8


void loop_inc(int *num, int loop);
void bounce_inc(int *num, int loop);
// What I want to find is when we have multiples of seven
//
int main()
{
    int pattern[] = {2,4};
    int pattern2[] = {3,7};
    int pattern3[] = {9,5,9,5,9,14,5,14};
    int pattern4[] = {1,0,0,0,4,1,3,0,1,2,1,0,0,5};

    int circ_ind;
    int circ_ind2;
    int circ_ind3;
    int circ_ind4;
    int circ_ind5;
    int circ_ind6;
    int circ_ind7;
    int index;
    
    circ_ind = 2;
    circ_ind2 = 0;
    circ_ind3 = 0;
    circ_ind4 = 1;
    circ_ind5 = 3;
    circ_ind6 = 0;
    circ_ind7 = 4;

    index = 0;
    int prev_index = 0;
    int counter = -1;
    for (int i = 121; i < 19000; i+=pattern[index%2]) {
        index++;

        if (circ_ind == pattern2[circ_ind2]) {
            loop_inc(&circ_ind2, 2);
            circ_ind = 1;
            loop_inc(&circ_ind4, 15);
            continue;
        }

        loop_inc(&circ_ind, 8);

        if (circ_ind4 == pattern3[circ_ind5]) {
            loop_inc(&circ_ind5, 8);
            circ_ind4 = 1;
            continue;
        }

        counter++;
        loop_inc(&circ_ind4, 15);



        //printf("%d ", i);
//            
        if (i % 11 == 0) {
            //printf("%d : %d : %d : %d : %d : %d\n",
             //       circ_ind, circ_ind2, circ_ind3, circ_ind4, circ_ind5, index);
            printf("%d ", index - prev_index);
            prev_index = index;
        }
    }
    printf("%d ",counter);
}

   
void loop_inc(int *num, int loop)
{
    int temp;
    temp = *num;
    temp = temp == loop - 1 ? 0 : temp+1;
    *num = temp;
}

void bounce_inc(int *num, int loop)
{
    static int dir = 1;
    int temp;
    temp = *num;
    if (temp == loop - 1) {
       temp = loop - 2;
       dir = -1;
    } else if (temp == 0) {
        temp = 1;
        dir = 1;
    } else {
        temp += dir;
    }
    *num = temp;
}
