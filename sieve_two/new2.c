#include <stdio.h>
#include <stdint.h>


void bounce_inc(int *num, int loop, int * dir);
int prime_gap_base();
int prime_gap(int level);

uint8_t main() {
    uint8_t n = 4;
    int acc;
    int ind;

    ind = 3;
    for (uint8_t i = 0; i < 25; i++) {
        printf("%d ", prime_gap(1));
    }
    return 0;
}

int prime_gap(int level) {
    static int      ind = 28;
    static int      dir = 1;
    static int      l   = 29; 

    int acc;

    int index_list[]      = {1,13,20,24};
    int index_length    = 4;

    acc = 0;
    for (int i = 0; i < index_length; i++) {
     while ( ind <= index_list[i]+1 && ind >= index_list[i]) { 
            if (level == 1) {
                acc += prime_gap_base();
            } else {
                acc += prime_gap(level - 1);
            }
            bounce_inc(&ind,l,&dir);
        }
    }

    if (acc == 0) {
        if (level == 1) {
            acc += prime_gap_base();
        } else {
            acc += prime_gap(level - 1);
        }
        bounce_inc(&ind,l,&dir);
    }
    return acc;
}
   

int prime_gap_base() {
    static uint8_t  n   = 4;
    static int      ind = 5;
    static int      dir = 1;
    static int      l   = 6;

    int acc;

    acc = 0;
    while (ind <= 2 && ind >= 1) {
        acc += n;
        n ^= 6;
        bounce_inc(&ind,l,&dir);
    }

    if (acc == 0) {
        acc += n;
        n ^= 6;
        bounce_inc(&ind,l,&dir);
    }
    return acc;
}


void bounce_inc(int *num, int loop, int * dir) {
    int temp;

    temp = *num;
    if (temp == loop - 1) {
       temp = loop - 2;
       *dir = -1;
    } else if (temp == 0) {
        temp = 1;
        *dir = 1;
    } else {
        temp += *dir;
    }
    *num = temp;
}   
