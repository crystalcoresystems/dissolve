/*
 *
 * find primes starting at 5
 * print diff + 1 / 3
 */
#include <stdio.h>
#include <math.h>

int isprime(int num, int divisor);
void bounce_inc(int *num, int loop);

int main()
{   
    int last_prime;
    int sum;
    char print[][3] = { {'-',' ',' '},
                        {' ','-',' '},
                        {' ',' ','-'} };
    int ind;
    
    sum = 0;
    last_prime = 7;
    ind = 0;
    int start_ind = 0;
    int end_ind   = 0;
    for (int i = 11; i < 100000; i++) {
        if (isprime(i, 2))
        {
            int y = ((i - last_prime + 1) / 3);
            if (y == 1) {
                start_ind++;
//                printf("  %c%c%c\n",
//                        print[ind][0],
//                        print[ind][1],
//                        print[ind][2]);
                bounce_inc(&ind, 3);
            } else {
                end_ind = start_ind;
                printf("%d : ", start_ind);
                //printf("*");
                for (int j = 0; j < y; j++) {
//                    if (j > 0) {
//                        printf(" ");
//                    }
//                    printf(" %c%c%c",
//                            print[ind][0],
//                            print[ind][1],
//                            print[ind][2]);
                    bounce_inc(&ind, 3);
                    end_ind++;
//                    if (j < y - 1) {
//                        printf("\n");
//                    }
                }
               // printf(" *\n");
               printf("%d\n", end_ind - 1);
               start_ind = end_ind;
            }

            sum += ((i - last_prime + 1) / 3);
            //printf("%d ", (i - last_prime + 1) / 3);
            if (sum >= 10) {
                //printf("\n");
                sum = 0;
            }
            last_prime = i;
        }
    }
    printf("\n");
}

int isprime(int num, int divisor)
/* some sort of recursion */
{
	if ( divisor > sqrt(num) )
		return 1;
	if ( num % divisor == 0 )
		return 0;
	if ( isprime(num, divisor + 1) == 0 )
		return 0;	
}

void bounce_inc(int *num, int loop)
{
    static int dir = 1;
    int temp;
    temp = *num;
    if (temp == loop - 1) {
       temp = loop - 2;
       dir = -1;
    } else if (temp == 0) {
        temp = 1;
        dir = 1;
    } else {
        temp += dir;
    }
    *num = temp;
}
