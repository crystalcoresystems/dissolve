/* This is a prime number sieve based off the prime number dna.
 * Starting at 5 we can generate all possible primes with the
 * repeating sequence, [2,4]. We generate numbers until we hit
 * our target. The target is a square of a prime number, starting
 * with 25, when we hit the target we add its repeating sequence
 * which is either [2n,4n] or [4n,2n] to a list. Our next target
 * is now the next prime number squared.
 * 
 *                   *
 * 7 11 13 17 19 23 25
 * [2] [6]
 *        *              #
 * 29 31 35 37 41 43 47 49
 * [6] [3] [6] [3] [6] [11] [2] [11]
 *     *        *           #        *     #  *                          *   #   $
 * 53 55 59 61 65 67 71 73 77 79 83 85 89 91 95 97 101 103 107 109 113 115 119 121
 * [4]
 *   *           #           $   *           *       #           !
 * 125 127 131 133 137 139 143 145 149 151 155 157 161 163 167 169 
 *
 *       *           *   $                   #   *   $       *   #   !                   *           *   !       $       #       * 
 * 173 175 179 181 185 187 191 193 197 199 203 205 209 211 215 217 221 223 227 229 233 235 239 241 245 247 251 253 257 259 263 265
 *

