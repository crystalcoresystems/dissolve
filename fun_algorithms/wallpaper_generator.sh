#!/bin/bash

# 1. Set your API Key for DALL-E and GPT-3.5
API_KEY="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"

# 2. Set the Hyperland wallpaper directory
WALLPAPER_DIR="$HOME/wallpaper"

# 3. Ensure the wallpaper directory exists
mkdir -p "$WALLPAPER_DIR"

# 4. Define a Mad Libs-style template
TEMPLATE="Design a wallpaper that showcases a [adjective] [noun] [verb ending with -ing] in a [adjective] [setting/place], illuminated by [natural phenomenon or light source]. The color palette includes [color 1], [color 2], and [color 3], creating a mood of [emotion]. The style is [art style], featuring elements like [noun 2] and [noun 3]. The theme is [noun] and this wallpaper highlights aspects of [noun] such as [noun] and [noun]. (exclude whimsical/mystical themes.) (exclude dreamy adjectives like lush and serene.) (avoid impressionistic or surreal art styles.) take extra steps to remove any artifacts. Use the golden ratio as a base layer for your layout."


# 5. Query GPT-3.5 Turbo to fill in the blanks
GPT_PROMPT="Fill in the following Mad Libs-style template with creative and vivid words: $TEMPLATE"

GPT_RESPONSE=$(curl https://api.openai.com/v1/chat/completions \
  -H "Authorization: Bearer $API_KEY" \
  -H "Content-Type: application/json" \
  -d '{
    "model": "gpt-3.5-turbo",
    "messages": [{"role": "user", "content": "'"$GPT_PROMPT"'"}],
    "max_tokens": 100
  }')

# 6. Extract the filled-in Mad Libs prompt from GPT-3.5 Turbo's response
FILLED_PROMPT=$(echo $GPT_RESPONSE | jq -r '.choices[0].message.content')

# 7. Ask GPT-3.5 to generate a filename based on the filled prompt
FILENAME_PROMPT="Generate a concise and descriptive filename based on the following prompt: $FILLED_PROMPT. Only return the filename with words separated by underscores."

GPT_FILENAME_RESPONSE=$(curl https://api.openai.com/v1/chat/completions \
  -H "Authorization: Bearer $API_KEY" \
  -H "Content-Type: application/json" \
  -d '{
    "model": "gpt-3.5-turbo",
    "messages": [{"role": "user", "content": "'"$FILENAME_PROMPT"'"}],
    "max_tokens": 20
  }')

# 8. Extract the filename from GPT's response
FILENAME=$(echo $GPT_FILENAME_RESPONSE | jq -r '.choices[0].message.content')

# 9. Ensure the filename ends with .png
FILENAME="$FILENAME.png"

# 10. Set the output path
OUTPUT_PATH="$WALLPAPER_DIR/$FILENAME"

# 11. Send request to DALL-E to generate an image with the GPT-3.5 Turbo filled prompt
curl https://api.openai.com/v1/images/generations \
    -H "Authorization: Bearer $API_KEY" \
    -H "Content-Type: application/json" \
    -d '{
        "model": "dall-e-3",
        "prompt": "'"$FILLED_PROMPT"'",
        "n": 1,
        "size": "1792x1024"
    }' > /tmp/dalle_response.json

# 12. Extract Image URL from the JSON response
IMAGE_URL=$(jq -r '.data[0].url' /tmp/dalle_response.json)

# 13. Download the image and save it to the desired output path
curl -o "$OUTPUT_PATH" "$IMAGE_URL"

# 14. Resize the image to 1920x1080 using ImageMagick
magick "$OUTPUT_PATH" -resize 1920x1080^ -gravity center -extent 1920x1080 "$OUTPUT_PATH"
