/*
 * This program finds prime numbers starting from 11 and generates a pattern based on the difference
 * between consecutive prime numbers. For each prime number found, it calculates y = ((current_prime - last_prime + 1) / 3).
 * If y == 1, it prints a single pattern line. If y > 1, it prints multiple lines with the pattern bouncing between
 * different indices. The pattern is generated using a 2D array of characters.
 */

#include <stdio.h>
#include <math.h>

int isprime(int num);
void bounce_inc(int *num, int loop);

int main()
{   
    int last_prime = 7; // Initialize the last prime number found
    int ind = 0;        // Index for pattern selection
    char print[][3] = { {'-',' ',' '},
                        {' ','-',' '},
                        {' ',' ','-'} }; // Pattern array

    // Loop through odd numbers starting from 11 to 99999
    for (int i = 11; i < 100000; i += 2) {
        if (isprime(i)) // Check if the number is prime
        {
            // Calculate the pattern repeat count based on prime difference
            int y = ((i - last_prime + 1) / 3);
            if (y == 1) {
                // Print a single line pattern
                printf("  %c%c%c\n",
                        print[ind][0],
                        print[ind][1],
                        print[ind][2]);
                bounce_inc(&ind, 3); // Update the index for pattern selection
            } else {
                // Print multiple lines with the pattern bouncing between indices
                printf("*");
                for (int j = 0; j < y; j++) {
                    if (j > 0) {
                        printf(" ");
                    }
                    printf(" %c%c%c",
                            print[ind][0],
                            print[ind][1],
                            print[ind][2]);
                    bounce_inc(&ind, 3); // Update the index for pattern selection
                    if (j < y - 1) {
                        printf("\n");
                    }
                }
                printf(" *\n");
            }
            last_prime = i; // Update the last prime number found
        }
    }
    return 0;
}

/*
 * Function to check if a number is prime.
 * num: The number to check.
 * Returns 1 if num is prime, 0 otherwise.
 */
int isprime(int num)
{
    if (num < 2)
        return 0; // Numbers less than 2 are not prime
    if (num == 2)
        return 1; // 2 is prime
    if (num % 2 == 0)
        return 0; // Even numbers greater than 2 are not prime

    int limit = sqrt(num);
    for (int i = 3; i <= limit; i += 2) {
        if (num % i == 0)
            return 0; // Divisor found, number is not prime
    }
    return 1; // No divisors found, number is prime
}

/*
 * Function to update the index in a bouncing manner between 0 and loop-1.
 * num: Pointer to the index variable.
 * loop: The maximum index value.
 */
void bounce_inc(int *num, int loop)
{
    static int dir = 1; // Direction of increment: 1 for forward, -1 for backward
    int temp = *num;

    if (temp == loop - 1) {
        // If at the maximum index, reverse direction
        temp = loop - 2;
        dir = -1;
    } else if (temp == 0) {
        // If at the minimum index, change direction to forward
        temp = 1;
        dir = 1;
    } else {
        // Continue in the current direction
        temp += dir;
    }
    *num = temp; // Update the index variable
}
