// written by K01313S
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>

#define HEIGHT                  96
#define WIDTH                   171
#define MAX_OFFSET_POINTS       50

#define STARS                   999
#define MAX_NODES               STARS * STARS / 2 + 1

#define INITIAL_CHILDREN_SIZE   10 // Initial size for children array
#define MAX_CHILDREN            STARS // Maximum number of children per node

typedef struct {
    int x;
    int y;
} Point;

typedef struct Star_Point {
    int                 x, y;
    int                 num_children;
    int                 capacity;  // Current capaity of the children array
    int                 id;
    struct Star_Point   **children; // Pointer to an array of pointers
    struct Star_Point   *parent;
} Star_Point;

// Define a structure for queue nodes
typedef struct Queue_Node {
    Star_Point          *data;
    struct Queue_Node   *next;
} Queue_Node;

// Define a structure for the queue
typedef struct Queue {
    Queue_Node *front;
    Queue_Node *rear;
} Queue;

// Define a down_stack
typedef struct Stack {
    Star_Point *node;
    struct Stack *next; 
} Stack;

int gx, gy, gd, ox, oy, gc;
int uid;
int queued[HEIGHT][WIDTH];
int visited[MAX_NODES];
Star_Point *pointer[HEIGHT][WIDTH];
int d[8][2] = {
    {1,1},
    {1,0},
    {0,1},
    {-1,0},
    {0,-1},
    {-1,-1},
    {-1,1},
    {1,-1} };

// Terminal visualization stuff../

// fpeek

// procedure to check the next character of the filestream
// this is used for getting keystrokes without pressing enter

void set_raw_mode()
{
    struct termios term;
    tcgetattr(STDIN_FILENO, &term);
    term.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
}

void reset_mode()
{
    struct termios term;
    tcgetattr(STDIN_FILENO, &term);
    term.c_lflag |= ICANON | ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
}

char get_key()
{
    char c;
    read(STDIN_FILENO, &c, 1);
    return c;
}
//..

int fpeek(FILE *stream)
{
    int value = fgetc(stream);
    ungetc(value, stream);
    return value;
}

// Terminal visualization stuff../

// goto_xy

// ANSI escape to move to x, y

void goto_xy(int x, int y)
{
    printf("\033[%d;%dH", x+1, y+1); 
}

// clear_screen

// ANSI escape to clear the screen

void clear_screen()
{
    printf("\033[2J\033[H");
}

//..

// Cluster Node lib ../


// create_star_node

// This is a procedure that creates a node and returns the pointer
// The x, y coordinates associated with the node are passed into the
// function, the rest of the coordinates are set to the default with
// the exception of the global uid which is used to distinguish nodes
// in the visited array when running the dfs.

Star_Point* create_star_node(int x, int y)
{
    Star_Point *new_node = (Star_Point*) calloc(1, sizeof(Star_Point));
    if (new_node != NULL) {
        new_node->x             = x;
        new_node->y             = y;
        new_node->num_children  = 0;
        new_node->id            = uid++;
        new_node->capacity      = INITIAL_CHILDREN_SIZE;

        new_node->children      = (Star_Point**)
            calloc(new_node->capacity, sizeof(Star_Point*));

        new_node->parent        = NULL;
    }
    return new_node;
}

// add_child

// This is a procedure to set a node as the child of another node
// in the tree, the parent pointer is also set for the child node

void add_child(Star_Point *parent, Star_Point *child)
{
    // capacity check
    if (parent->num_children == parent->capacity) {
        // Double the capacity
        int new_capacity = parent->capacity * 2;
        if (new_capacity > MAX_CHILDREN) new_capacity = MAX_CHILDREN;

        Star_Point **new_children = (Star_Point**) realloc(parent->children, new_capacity * sizeof(Star_Point*));
        if(new_children == NULL) {
            // Handle memory allocation failure
            return;
        }

        parent->children = new_children;
        parent->capacity = new_capacity;
    }

    parent->children[parent->num_children++] = child;
    child->parent = parent;
}

// free_star_tree

// This is a procedure to  free the cluster tree after its
// been created

void free_star_tree(Star_Point *root)
{
    if (root != NULL) {
        for (int i = 0; i <  root->num_children; i++) {
            free_star_tree(root->children[i]);
        }
        free(root->children);
        free(root);
    }
}
//..

// Queue lib ../


// create_queue_node

// creates a new node for the queue and returns pointer

Queue_Node* create_queue_node(Star_Point *data)
{
    Queue_Node *new_node = (Queue_Node*) malloc(sizeof(Queue_Node));
    if (new_node != NULL) {
        new_node->data = data;
        new_node->next = NULL;
    }
    return new_node;
}

// create_queue

// creates the initial queue and returns pointer
Queue* create_queue()
{
    Queue *q = (Queue*) malloc(sizeof(Queue));
    if (q != NULL) {
        q->front = q->rear = NULL;
    }
    return q;
}

// is_empty

// checks queue to see if it is empty

int is_empty(Queue *q) {
    return q->front == NULL;
}

// enqueue

// adds a queue node to the back of queue

void enqueue(Queue *q, Star_Point *data) {
    Queue_Node *new_node = create_queue_node(data);
    if (new_node == NULL) {
        printf("Memory allocation failed!");
        return;
    }

    if (is_empty(q)) {
        q->front = q->rear = new_node;
    } else {
        q->rear->next = new_node;
        q->rear = new_node;
    }
}

// dequeue

// removes queue node from the front of queue
// frees the node and returns the data

Star_Point *dequeue(Queue *q) {
    if (is_empty(q)) {
        printf("Queue is empty!\n");
        return NULL;
    }

    Queue_Node *temp = q->front;
    Star_Point *data = temp->data;
    q->front = q->front->next;

    if (q->front == NULL) {
        q->rear = NULL;
    }

    free(temp);
    return data;
}
//..

// Clustering lib../

// inbounds

// simple check if x, y coordinates are in the bounds of matrix

int inbounds(int x, int y)
{
    if (x < 0 || y < 0)
        return 0;
    if (x >= HEIGHT || y >= WIDTH)
        return 0;
    return 1;
}

// create_stars

// randomly creates points in the matrix(stars) ensures
// that each point has unique coordinates

void create_stars(int star_coordinates[STARS][2])
{
    for(int i = 0; i < STARS; i++) {
        star_coordinates[i][0] = rand()%HEIGHT;
        star_coordinates[i][1] = rand()%WIDTH;
        for (int j = 0; j < i; j++) {
            if (star_coordinates[i][0] == star_coordinates[j][0] &&
                    (star_coordinates[i][1] == star_coordinates[j][1]))
                i--; // try again
        }
    }
}

// get_leaves

// This procedure is a bfs that fills the leaf_queue with all leaf nodes
// that are ancesters of the node passed into the procedure

void get_leaves(Queue *leaf_queue, Star_Point *node)
{
    Queue *branch_queue = create_queue();
    int x, y;

    enqueue(branch_queue, node);
    while ( !is_empty(branch_queue) )
    {

        Star_Point *working_node = dequeue(branch_queue);
        x = working_node->x;
        y = working_node->y;

        if (working_node->num_children == 0) {
            enqueue(leaf_queue, working_node);
            queued[x][y] = 1;
        }

        for(int i = 0; i < working_node->num_children; i++)
        {
            enqueue(branch_queue, working_node->children[i]);
        }
    }
}

// update_leaves

// This procedure updates the pointer array after clustering has occured
// it is a bfs from the given node to all leaves

void update_leaves(Star_Point *new_cluster, Star_Point *node)
{
    Queue *branch_queue = create_queue();
    int x, y;

    enqueue(branch_queue, node);
    while ( !is_empty(branch_queue) )
    {

        Star_Point *working_node = dequeue(branch_queue);
        x = working_node->x;
        y = working_node->y;

        if (working_node->num_children == 0) {
            pointer[x][y] = new_cluster;
        }

        for(int i = 0; i < working_node->num_children; i++)
        {
            enqueue(branch_queue, working_node->children[i]);
        }
    }
}

// clear_queued

// this procedure clears the global queued matrix which is used
// for ensure that clustering happens properly (no repeat clustering basically)

void clear_queued(int star_coords[STARS][2])
{
    int i;
    int x;
    int y;

    for(i = 0; i < STARS; i++) {
        x = star_coords[i][0];
        y = star_coords[i][1];
        queued[x][y] = 0;
    }
}

// cluster

// This is the main clustering procedure it takes the coordinate offsets
// that are equidistant and checks around each star for a hit on another star
// when there is a hit a new cluster node is created and both stars are added
// as children to the new cluster, the new star is added to the queue and the
// process repeats

// Note: In addition to skipping points that have been used the queued matrix
//       also skips coordinates that don't contain points
// 

void cluster(int offset[MAX_OFFSET_POINTS][2], int count, int star_coordinates[STARS][2])
{
    Queue *leaf_queue = create_queue();
    int x;
    int y;
    int x1;
    int y1;

    clear_queued(star_coordinates);

    for (int i = 0; i < STARS; i++) {
        x = star_coordinates[i][0];
        y = star_coordinates[i][1];

        if (queued[x][y] == 1)
            continue;

        Star_Point *new_cluster = NULL;

        // Enqueue the leaves star cluster
        get_leaves(leaf_queue, pointer[x][y]);
        while( !is_empty(leaf_queue) )
        {
            Star_Point *working_node = dequeue(leaf_queue);

            for (int i = 0; i < count; i++)
            {
                x  = working_node->x;
                y  = working_node->y;
                x1 = x + offset[i][0];
                y1 = y + offset[i][1];


                if (inbounds(x1, y1)) {
                    goto_xy(x1, y1);
                    printf(".x");
                }

                if ( inbounds(x1, y1) == 1 && queued[x1][y1] == 0 )
                {
                    if (new_cluster == NULL)
                    {
                        new_cluster = create_star_node(-1,-1);
                        add_child(new_cluster, pointer[x][y]);
                        update_leaves(new_cluster, pointer[x][y]);
                    } // else already on a leaf of new_cluster

                    add_child(new_cluster, pointer[x1][y1]);
                    get_leaves(leaf_queue, pointer[x1][y1]);
                    update_leaves(new_cluster, pointer[x1][y1]);
                }
            } 
        }

        fflush(stdout);
    }
}

// fill_queued

// This fills the queued array with ones, which will cause the cluster
// bfs to skip those coordinates

void fill_queued()
{
    int i;
    int j;

    for(i = 0; i < HEIGHT; i++)
        for(j = 0; j < WIDTH; j++)
            queued[i][j] = 1;
}

// fill_pointers

// This procedure fills the pointer array with null then
// creates a node for each star and saves the pointer in matrix

void fill_pointers(int star_coordinates[STARS][2])
{
    int i;
    int j;
    int x;
    int y;

    for(i = 0; i < HEIGHT; i++) {
        for(j = 0; j < WIDTH; j++) {
            pointer[i][j] = NULL;
        }
    }

    for(i = 0; i < STARS; i++) {
        x = star_coordinates[i][0];
        y = star_coordinates[i][1];

        pointer[x][y] = create_star_node(x, y);
    }
}

// print_leaves_dfs

// Tricky dfs that prints all leaf descendants of the node
// one color and all leaves that are not descendants another color
// uses the visited uid and the parent pointer to dfs up and down tree

void print_leaves_dfs(Star_Point *node, int color)
{
    int id;
    int x,y;

    id = node->id;

    if (visited[id] == 1)
        return;

    visited[id] = 1;

    if (node->num_children == 0) {
        x = node->x;
        y = node->y;

        printf("\033[1;%dm", color);
        goto_xy(x, y);
        printf("x");
    }


    for(int i = 0; i < node->num_children; i++)
    {
        print_leaves_dfs(node->children[i], color);
    }

    if(node->parent != NULL)
        print_leaves_dfs(node->parent, 31);
}

// clear_visited

// clears the visited array for print leaves dfs

void clear_visited()
{
    for(int i = 0; i < MAX_NODES; i++)
        visited[i] = 0;
}



// Create Cluster

// This procedure creates random stars and clusters them
// according to distance

Star_Point *create_star_cluster(int star_coordinates[STARS][2])
{

    FILE *fi = fopen("distance_list.bin", "rb");
    if (fi == NULL) {
        perror("Error opening file for reading");
        exit(1);
    }

    srand(time(NULL));

    int x, y;
    int offset[50][2];

    create_stars(star_coordinates);
    fill_queued();


    uid = 0;
    fill_pointers(star_coordinates);

    int next_byte;
    int count = 0;

    system("clear");
    while ((next_byte = fpeek(fi)) != EOF)
    {
        char d;

        for (int i = 0; i < 4; i++) {

            fread(&x, sizeof(int),1,fi);
            fread(&y, sizeof(int),1,fi);
            offset[count][0] = x;
            offset[count][1] = y;

            count++;
        }

        fread(&d, sizeof(char),1,fi);

        if (d == '\n') {
            cluster(offset, count, star_coordinates);
            count = 0;
        }

        // Do a quick test to see if clustering is complete
        x = star_coordinates[0][0];
        y = star_coordinates[0][1];
        Star_Point *ref = pointer[x][y];

        int flag = 0;
        for(int i = 1; i < STARS; i++)
        {
            x = star_coordinates[i][0];
            y = star_coordinates[i][1];

            if (ref != pointer[x][y])
                flag = 1;
        }
        if (flag == 0)
            break;

    }


    fclose(fi);

    Star_Point *root = create_star_node(-1, -1);
    add_child(root, 
                    pointer[star_coordinates[0][0]]
                        [star_coordinates[0][1]]);
    return root;
}

//..


int main()
{
    int branch = 0;
    char input;
    int x, y;
    int star_coordinates[STARS][2];
    Star_Point *root = create_star_cluster(star_coordinates);

    scanf("%c", &input);

    set_raw_mode();
    printf("\e[?25l");

    system("clear");

    for(int i = 0; i < STARS; i++) {
        x = star_coordinates[i][0];
        y = star_coordinates[i][1];
        goto_xy(x, y);
        printf("x");
    }
        
    fflush(stdout);


    Star_Point *node = root;
    Star_Point *temp;

    while(1)
    {
        input = get_key();

        switch (input)
        {
            case 'q':
                free_star_tree(root);
                reset_mode();
                return 0;

            case 'w':
                if(node->parent != NULL) {
                    temp = node;
                    node = node->parent;
                    for(int i = 0; i < node->num_children; i++)
                    {
                        if (temp == node->children[i]) {
                            branch = i;
                            break;
                        }
                    }
                }
                break;
           case 's':
                if(node->children[branch]->num_children != 0)
                    node = node->children[branch];
                branch = 0;
                break;
           case 'a':
                if(branch > 0)
                    branch--;
                break;
           case 'd':
                if(branch < node->num_children-1)
                    branch++;
                 break;

        }
        clear_visited(star_coordinates);
        print_leaves_dfs(node->children[branch], 32);
        fflush(stdout);
    }

    reset_mode();
    printf("\e[?25h");
    return 0;
}
// written by K01313S
