/*
 * Optimal Path: A Practical Solution to the Traveling Salesman Problem (TSP)
 *
 * This program provides a practical approach to the TSP by clustering points (stars)
 * and visiting each cluster recursively. While it may not provide the mathematically
 * optimal solution, it offers a realistic route considering potential delays, such as
 * traffic, making it suitable for real-world applications.
 *
 * The algorithm reduces the cost related to unexpected time delays by visiting each
 * distance cluster only once recursively. If delays occur (e.g., traffic), it minimizes
 * the impact on the overall trip.
 *
 * Usage:
 * - This program works with `radial_distances.c`. Compile and run it to create
 *   `distance_list.bin` before running this program.
 *
 * - Note: This program operates in a finite state space that can be divided into units.
 */

#include <stdio.h>
#include <unistd.h>
#include <termios.h>                                                                                                                                                                          
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>                 

#define HEIGHT                  47      // Height of the coordinate grid
#define WIDTH                   170     // Width of the coordinate grid
#define MAX_OFFSET_POINTS       50      // Maximum number of offset points

#define STARS                   90      // Number of stars (points)
#define MAX_NODES               STARS * STARS / 2 + 1    // Maximum number of nodes

#define INITIAL_CHILDREN_SIZE   10      // Initial capacity for children array in a node
#define MAX_CHILDREN            STARS   // Maximum number of children per node

// Structure representing a point with x and y coordinates
typedef struct {
    int x;
    int y;
} Point;

// Structure representing a node in the star cluster tree
typedef struct Star_Point {
    int                 x, y;           // Coordinates of the point
    int                 num_children;   // Number of child nodes
    int                 capacity;       // Current capacity of the children array
    int                 id;             // Unique identifier for the node
    struct Star_Point   **children;     // Array of pointers to child nodes
    struct Star_Point   *parent;        // Pointer to the parent node
} Star_Point;

// Structure for queue nodes used in BFS
typedef struct Queue_Node {
    Star_Point          *data;          // Pointer to the star point data
    struct Queue_Node   *next;          // Pointer to the next queue node
} Queue_Node;

// Structure for the queue used in BFS
typedef struct Queue {
    Queue_Node *front;  // Pointer to the front of the queue
    Queue_Node *rear;   // Pointer to the rear of the queue
} Queue;

// Structure for stack nodes used in DFS traversal
typedef struct Stack {
    Star_Point *node;       // Pointer to the star point node
    struct Stack *next;     // Pointer to the next stack node
} Stack;

// Global variables
int gx, gy, gd, ox, oy, gc;             // Variables for tracking traversal
int uid;                                // Unique identifier counter
int queued[HEIGHT][WIDTH];              // Matrix for tracking queued points
int visited[MAX_NODES];                 // Array for tracking visited nodes
Star_Point *pointer[HEIGHT][WIDTH];     // Matrix of pointers to star nodes

// Direction vectors for 8-directional movement
int d[8][2] = {
    {1, 1},     // Southeast
    {1, 0},     // East
    {0, 1},     // South
    {-1, 0},    // West
    {0, -1},    // North
    {-1, -1},   // Northwest
    {-1, 1},    // Southwest
    {1, -1}     // Northeast
};

// Function to peek at the next character in a file without consuming it
int fpeek(FILE *stream)
{
    int value = fgetc(stream);
    ungetc(value, stream);
    return value;
}

// Terminal visualization functions

// Moves the cursor to position (x, y) in the terminal
void goto_xy(int x, int y)
{
    printf("\033[%d;%dH", x + 1, y + 1);
}

// Clears the terminal screen
void clear_screen()
{
    printf("\033[2J\033[H");
}

// Star Point (Cluster Node) Functions

// Creates a new Star_Point node with given coordinates
Star_Point* create_star_node(int x, int y)
{
    Star_Point *new_node = (Star_Point*) calloc(1, sizeof(Star_Point));
    if (new_node != NULL) {
        new_node->x             = x;
        new_node->y             = y;
        new_node->num_children  = 0;
        new_node->id            = uid++;
        new_node->capacity      = INITIAL_CHILDREN_SIZE;

        new_node->children      = (Star_Point**)
            calloc(new_node->capacity, sizeof(Star_Point*));

        new_node->parent        = NULL;
    }
    return new_node;
}

// Adds a child node to a parent node in the cluster tree
void add_child(Star_Point *parent, Star_Point *child)
{
    // Check if resizing is needed
    if (parent->num_children == parent->capacity) {
        // Double the capacity
        int new_capacity = parent->capacity * 2;
        if (new_capacity > MAX_CHILDREN) new_capacity = MAX_CHILDREN;

        Star_Point **new_children = (Star_Point**) realloc(parent->children, new_capacity * sizeof(Star_Point*));
        if(new_children == NULL) {
            // Handle memory allocation failure
            fprintf(stderr, "Memory allocation failed in add_child\n");
            exit(EXIT_FAILURE);
        }

        parent->children = new_children;
        parent->capacity = new_capacity;
    }

    parent->children[parent->num_children++] = child;
    child->parent = parent;
}

// Recursively frees the star cluster tree starting from the root
void free_star_tree(Star_Point *root)
{
    if (root != NULL) {
        for (int i = 0; i <  root->num_children; i++) {
            free_star_tree(root->children[i]);
        }
        free(root->children);
        free(root);
    }
}

// Queue Functions for BFS

// Creates a new queue node with given data
Queue_Node* create_queue_node(Star_Point *data)
{
    Queue_Node *new_node = (Queue_Node*) malloc(sizeof(Queue_Node));
    if (new_node != NULL) {
        new_node->data = data;
        new_node->next = NULL;
    }
    return new_node;
}

// Initializes an empty queue
Queue* create_queue()
{
    Queue *q = (Queue*) malloc(sizeof(Queue));
    if (q != NULL) {
        q->front = q->rear = NULL;
    }
    return q;
}

// Checks if the queue is empty
int is_empty(Queue *q) {
    return q->front == NULL;
}

// Enqueues data into the queue
void enqueue(Queue *q, Star_Point *data) {
    Queue_Node *new_node = create_queue_node(data);
    if (new_node == NULL) {
        fprintf(stderr, "Memory allocation failed in enqueue\n");
        exit(EXIT_FAILURE);
    }

    if (is_empty(q)) {
        q->front = q->rear = new_node;
    } else {
        q->rear->next = new_node;
        q->rear = new_node;
    }
}

// Dequeues data from the queue and returns the Star_Point
Star_Point *dequeue(Queue *q) {
    if (is_empty(q)) {
        fprintf(stderr, "Queue is empty!\n");
        return NULL;
    }

    Queue_Node *temp = q->front;
    Star_Point *data = temp->data;
    q->front = q->front->next;

    if (q->front == NULL) {
        q->rear = NULL;
    }

    free(temp);
    return data;
}

// Stack Functions for DFS

// Pushes a node onto the stack
void push(Stack **stack, Star_Point *node) {
    Stack *new_node = (Stack *) malloc(sizeof(Stack));
    if (new_node == NULL) {
        fprintf(stderr, "Memory allocation failed in push\n");
        exit(EXIT_FAILURE);
    }
    new_node->node = node;
    new_node->next = *stack;
    *stack = new_node;
}

// Pops a node from the stack
Star_Point* pop(Stack **stack) {
    if (*stack == NULL) {
        return NULL;
    }
    Stack *temp = *stack;
    Star_Point *node = temp->node;
    *stack = temp->next;
    free(temp);
    return node;
}

// Clustering Functions

// Checks if the coordinates (x, y) are within bounds of the grid
int inbounds(int x, int y)
{
    if (x <= 0 || y <= 0)
        return 0;
    if (x >= HEIGHT || y >= WIDTH)
        return 0;
    return 1;
}

// Randomly generates unique star coordinates within the grid
void create_stars(int star_coordinates[STARS][2])
{
    for(int i = 0; i < STARS; i++) {
        star_coordinates[i][0] = rand() % HEIGHT;
        star_coordinates[i][1] = rand() % WIDTH;
        // Ensure uniqueness of coordinates
        for (int j = 0; j < i; j++) {
            if (star_coordinates[i][0] == star_coordinates[j][0] &&
                star_coordinates[i][1] == star_coordinates[j][1]) {
                i--; // Duplicate found, retry
                break;
            }
        }
    }
}

// Performs BFS to get all leaf nodes and enqueues them into leaf_queue
void get_leaves(Queue *leaf_queue, Star_Point *node)
{
    Queue *branch_queue = create_queue();
    int x, y;

    enqueue(branch_queue, node);
    while (!is_empty(branch_queue))
    {
        Star_Point *working_node = dequeue(branch_queue);
        x = working_node->x;
        y = working_node->y;

        if (working_node->num_children == 0 && queued[x][y] == 0) {
            enqueue(leaf_queue, working_node);
            queued[x][y] = 1;
        }

        for(int i = 0; i < working_node->num_children; i++)
        {
            enqueue(branch_queue, working_node->children[i]);
        }
    }
    free(branch_queue);
}

// Updates the pointer array after clustering by performing BFS from the given node
void update_leaves(Star_Point *new_cluster, Star_Point *node)
{
    Queue *branch_queue = create_queue();
    int x, y;

    enqueue(branch_queue, node);
    while (!is_empty(branch_queue))
    {
        Star_Point *working_node = dequeue(branch_queue);
        x = working_node->x;
        y = working_node->y;

        if (working_node->num_children == 0) {
            pointer[x][y] = new_cluster;
        }

        for(int i = 0; i < working_node->num_children; i++)
        {
            enqueue(branch_queue, working_node->children[i]);
        }
    }
    free(branch_queue);
}

// Clears the queued matrix for the next clustering operation
void clear_queued(int star_coords[STARS][2])
{
    for (int i = 0; i < STARS; i++) {
        int x = star_coords[i][0];
        int y = star_coords[i][1];
        queued[x][y] = 0;
    }
}

// Clusters stars based on the given offsets
void cluster(int offset[MAX_OFFSET_POINTS][2], int count, int star_coordinates[STARS][2])
{
    Queue *leaf_queue = create_queue();
    int x, y, x1, y1;

    clear_queued(star_coordinates);

    for (int i = 0; i < STARS; i++) {
        x = star_coordinates[i][0];
        y = star_coordinates[i][1];

        if (queued[x][y] == 1)
            continue;

        Star_Point *new_cluster = NULL;

        // Enqueue the leaves of the star cluster
        get_leaves(leaf_queue, pointer[x][y]);
        while (!is_empty(leaf_queue))
        {
            Star_Point *working_node = dequeue(leaf_queue);

            for (int j = 0; j < count; j++)
            {
                x  = working_node->x;
                y  = working_node->y;
                x1 = x + offset[j][0];
                y1 = y + offset[j][1];

                // Visualization (optional)
                if (inbounds(x1, y1)) {
                    goto_xy(x1, y1);
                    printf("x|");
                }

                if (inbounds(x1, y1) && queued[x1][y1] == 0 && pointer[x1][y1] != NULL)
                {
                    if (new_cluster == NULL)
                    {
                        new_cluster = create_star_node(-1, -1);
                        add_child(new_cluster, pointer[x][y]);
                        update_leaves(new_cluster, pointer[x][y]);
                    }

                    add_child(new_cluster, pointer[x1][y1]);
                    get_leaves(leaf_queue, pointer[x1][y1]);
                    update_leaves(new_cluster, pointer[x1][y1]);
                }
            }
        }

        usleep(477);

        fflush(stdout);
    }
    free(leaf_queue);
}

// Initializes the queued matrix with ones (used to skip coordinates during clustering)
void fill_queued()
{
    for(int i = 0; i < HEIGHT; i++)
        for(int j = 0; j < WIDTH; j++)
            queued[i][j] = 1;
}

// Initializes the pointer array and creates star nodes for each star coordinate
void fill_pointers(int star_coordinates[STARS][2])
{
    for(int i = 0; i < HEIGHT; i++) {
        for(int j = 0; j < WIDTH; j++) {
            pointer[i][j] = NULL;
        }
    }

    for(int i = 0; i < STARS; i++) {
        int x = star_coordinates[i][0];
        int y = star_coordinates[i][1];

        pointer[x][y] = create_star_node(x, y);
    }
}

// Recursive DFS to print all leaf descendants of the node
void print_leaves_dfs(Star_Point *node, int color, int marker)
{
    int id = node->id;
    int x, y;

    if (visited[id] == 1)
        return;

    visited[id] = 1;

    if (node->num_children == 0) {
        x = node->x;
        y = node->y;

        printf("\033[1;%dm", color);
        goto_xy(x, y);
        printf("x");
        if (marker == 1) {
            for (int i = 0; i < 8; i++) {
                int nx = x + d[i][0];
                int ny = y + d[i][1];
                if (inbounds(nx, ny)) {
                    goto_xy(nx, ny);
                    printf("-");
                }
            }
            // Update traversal tracking variables
            if (gx == -1) {
                ox = x; // Save first coordinates to match last
                oy = y;
                gx = x;
                gy = y;
                gd = 0;
                gc = 1;
            } else {
                gd += abs(x - gx) * abs(x - gx) + abs(y - gy) * abs(y - gy);
                gx = x;
                gy = y;
                gc++;

                if (gc == STARS)
                    gd += abs(x - ox) * abs(x - ox) + abs(y - oy) * abs(y - oy);
            }
        }
    }

    for(int i = 0; i < node->num_children; i++)
    {
        print_leaves_dfs(node->children[i], color, marker);
    }

    if(node->parent != NULL)
        print_leaves_dfs(node->parent, 91, 0);
}

// Resets the visited array before DFS traversal
void clear_visited()
{
    for(int i = 0; i < MAX_NODES; i++)
        visited[i] = 0;
}

// Creates the star cluster by reading distance offsets and performing clustering
Star_Point *create_star_cluster(int star_coordinates[STARS][2])
{
    FILE *fi = fopen("distance_list.bin", "rb");
    if (fi == NULL) {
        perror("Error opening file for reading");
        exit(EXIT_FAILURE);
    }

    srand(time(NULL));

    int x, y;
    int offset[MAX_OFFSET_POINTS][2];

    create_stars(star_coordinates);
    fill_queued();

    uid = 0;
    fill_pointers(star_coordinates);

    int count = 0;

    system("clear");
    while (fpeek(fi) != EOF)
    {
        char d;

        for (int i = 0; i < 4; i++) {

            if (fread(&x, sizeof(int), 1, fi) != 1 ||
                fread(&y, sizeof(int), 1, fi) != 1) {
                fprintf(stderr, "Error reading from file\n");
                fclose(fi);
                exit(EXIT_FAILURE);
            }
            offset[count][0] = x;
            offset[count][1] = y;

            count++;
        }

        if (fread(&d, sizeof(char), 1, fi) != 1) {
            fprintf(stderr, "Error reading from file\n");
            fclose(fi);
            exit(EXIT_FAILURE);
        }

        if (d == '\n') {
            cluster(offset, count, star_coordinates);
            count = 0;
        }

        // Check if clustering is complete
        x = star_coordinates[0][0];
        y = star_coordinates[0][1];
        Star_Point *ref = pointer[x][y];

        int flag = 0;
        for(int i = 1; i < STARS; i++)
        {
            x = star_coordinates[i][0];
            y = star_coordinates[i][1];

            if (ref != pointer[x][y]) {
                flag = 1;
                break;
            }
        }
        if (flag == 0)
            break;
    }

    fclose(fi);

    Star_Point *root = create_star_node(-1, -1);
    add_child(root, pointer[star_coordinates[0][0]][star_coordinates[0][1]]);
    return root;
}

// Euclidean distance squared between two points
int euclidean_distance(Point p1, Point p2) {
    return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y);
}

// Calculates the total distance for a given order of points
int total_distance(int points_order[], Point points[]) {
    int total = 0;
    for (int i = 0; i < STARS - 1; i++) {
        total += euclidean_distance(points[points_order[i]], points[points_order[i + 1]]);
    }
    total += euclidean_distance(points[points_order[STARS - 1]], points[points_order[0]]);
    return total;
}

// Generates the next lexicographical permutation of the order array
int next_permutation(int order[], int n) {
    int i = n - 1;
    while (i > 0 && order[i - 1] >= order[i]) {
        i--;
    }
    if (i <= 0) {
        return 0;
    }
    int j = n - 1;
    while (order[j] <= order[i - 1]) {
        j--;
    }
    int temp = order[i - 1];
    order[i - 1] = order[j];
    order[j] = temp;
    j = n - 1;
    while (i < j) {
        temp = order[i];
        order[i] = order[j];
        order[j] = temp;
        i++;
        j--;
    }
    return 1;
}

// Brute-force solution to find the minimal total distance (for small STARS)
int brute_force_op(Point points[]) {
    int min_order[STARS];
    for (int i = 0; i < STARS; i++) {
        min_order[i] = i;
    }
    int min_distance = total_distance(min_order, points);

    int order[STARS];
    for (int i = 0; i < STARS; i++) {
        order[i] = i;
    }

    while (next_permutation(order, STARS)) {
        int distance = total_distance(order, points);
        if (distance < min_distance) {
            min_distance = distance;
            for (int i = 0; i < STARS; i++) {
                min_order[i] = order[i];
            }
        }
    }

    return min_distance;
}

// Converts the star coordinates into Point structures
void convert_coordinates(int star_coordinates[][2], Point points[]) {
    for (int i = 0; i < STARS; i++) {
        points[i].x = star_coordinates[i][0];
        points[i].y = star_coordinates[i][1];
    }
}

// Main function
int main()
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    // Save old terminal settings
    tcgetattr(STDIN_FILENO, &oldt);

    // Disable canonical mode and echo
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    if(tcsetattr(STDIN_FILENO, TCSANOW, &newt) != 0) {
        perror("tcsetattr");
        exit(EXIT_FAILURE);
    }

    // Set non-blocking input
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    if(fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK) == -1) {
        perror("fcntl");
        exit(EXIT_FAILURE);
    }

    // Hide cursor
    printf("\e[?25l");

    int x, y;
    int star_coordinates[STARS][2];
    Star_Point *root;
    Stack *down_stack = NULL;
    Point points[STARS];

    while(1) {

        gx = -1;

        system("clear");
        printf("\033[0m");

        root = create_star_cluster(star_coordinates);

        system("clear");
        for(int i = 0; i < STARS; i++) {
            x = star_coordinates[i][0];
            y = star_coordinates[i][1];
            goto_xy(x, y);
            printf("x");
        }

        fflush(stdout);

        for(int i = 0; i < 4777; i++) {
            usleep(77);
            ch = getchar();
            if (ch != EOF)
                goto ESCAPE;

        }

        Star_Point *down_current = root;
        while(down_current->num_children != 0 || down_stack != NULL)
        {

            while (1)
            {
                clear_visited();

                if(down_current->num_children == 0) {
                    print_leaves_dfs(down_current, 92, 1);
                } else {
                    print_leaves_dfs(down_current, 92, 0);
                }

                print_leaves_dfs(down_current, 92, 1);
                fflush(stdout);
                for(int i = 0; i < 777; i++) {
                    usleep(77);
                    ch = getchar();
                    if (ch != EOF)
                        goto ESCAPE;

                }

                for (int i = 1; i < down_current->num_children; i++)
                {
                    push(&down_stack, down_current->children[i]);
                }

                if (down_current->num_children > 0)
                    down_current = down_current->children[0];
                else
                    break;
            }

            if (down_stack != NULL) {
                down_current = pop(&down_stack);
            }

        }

        // Last print
        clear_visited();
        print_leaves_dfs(down_current, 32, 1);

        goto_xy(95, 0);
        fflush(stdout);

        // printf("down_current traveled: %d", gd);

        // Brute force comparison (only for small STARS)
        convert_coordinates(star_coordinates, points);
        if (STARS < 10)
        {
            printf(" - brute traveled: %d", brute_force_op(points));
            for(int i = 0; i < 177; i++) {
                usleep(77);
                ch = getchar();
                if (ch != EOF)
                    goto ESCAPE;

            }
        }
        for(int i = 0; i < 11000; i++) {
            usleep(80);
            ch = getchar();
            if (ch != EOF)
                goto ESCAPE;

        }
        free_star_tree(root);
    }
ESCAPE:
    free_star_tree(root);

    while (down_stack != NULL)
    {
        Stack *temp = down_stack;
        down_stack = temp->next;
        free(temp);
    }

    // Restore terminal settings
    if(tcsetattr(STDIN_FILENO, TCSANOW, &oldt) != 0) {
        perror("tcsetattr");
        exit(EXIT_FAILURE);
    }
    // Show cursor
    printf("\e[?25h");

    return 0;
}
