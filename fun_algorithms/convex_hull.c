// written by K01313S (ninja hacker angel)
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>

#define HEIGHT                  52
#define WIDTH                   190

#define STARS                   200

typedef struct {

    int x,y;

} Point;

typedef struct Stack {

    Point           coords;
    struct Stack    *next; 

} Stack;

typedef struct Convex_Hulls {

    int             count;
    Point           hulls[STARS];
    struct Convex_Hulls    *next;
    struct Convex_Hulls    *prev;

} Convex_Hulls;

Point anchor;

// stack push/pop/create procedures../
Stack *new_stack(Point data)
{
    Stack *node  = (Stack *)malloc(sizeof(Stack));
    node->coords = data;
    node->next   = NULL;

    return node;
}

void push(Stack **stack, Point data)
{
    Stack *temp = (Stack *)malloc(sizeof(Stack));

    temp->coords = data;
    temp->next = *stack;
    *stack = temp;
}

Point pop(Stack **stack)
{
    if (*stack == NULL) {
        fprintf(stderr, "Error: Trying to pop from an empty stack.\n");
        exit(EXIT_FAILURE);
    }

    Stack *temp = *stack;
    Point data = temp->coords;

    *stack = temp->next;
    free(temp);

    return data;
}

Point next_to_top(Stack *top)
{
    if (top == NULL || top->next == NULL) {
        fprintf(stderr, "Error: Stack has less than 2 points.\n");
        exit(EXIT_FAILURE);
    }

    return top->next->coords;
}

//.. 

int orientation(Point p, Point q, Point r)
{
    double val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
    if (val == 0) return 0;
    return (val > 0) ? 1 : 2;
}

int scan_orientation(Point p, Point q, Point r)
{
    double val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
    if (val == 0) return 2;
    return (val > 0) ? 1 : 2;
}



int compare(const void *vp1, const void *vp2)
{
    Point *p1 = (Point*)vp1;
    Point *p2 = (Point*)vp2;
    int o = orientation(anchor, *p1, *p2);
    if (o == 0)
        return (p1->x * p1->x + p1->y * p1->y >= 
                p2->x * p2->x + p2->y * p2->y) ? 1 : -1;
    return (o == 2) ? -1 : 1;
}

Stack *convex_stack(Point points[STARS], int n)
{
    int min_y = 0;
    for (int i = 1; i < n; i++) {
        if (points[i].y < points[min_y].y ||
                (points[i].y == points[min_y].y && points[i].x < points[min_y].x))
            min_y = i;
    }

    anchor          = points[min_y];

    Point temp      = points[0];
    points[0]       = points[min_y];
    points[min_y]   = temp;

    qsort(&points[1], n - 1, sizeof(Point), compare);

    Stack *stack = NULL;
    push(&stack, points[0]);
    push(&stack, points[1]);

    int count = 2;
    for (int i = 2; i < n; i++) {
        while (stack != NULL &&
                scan_orientation(next_to_top(stack), stack->coords, points[i]) != 2)
        {
            pop(&stack);
            count--;
        }
        push(&stack, points[i]);
        count++;
    }

    return stack;
}

void add_hull(Convex_Hulls **ch, Stack **points, int visited[HEIGHT][WIDTH])
{
    Convex_Hulls *node = (Convex_Hulls *)malloc(sizeof(Convex_Hulls));

    int i = 0;
    while (*points != NULL)
    {
        Point p;
        p = pop(points);

        node->hulls[i++]    = p;

        visited[p.x][p.y]   = 0;
    }

    node->count = i;
    node->next  = NULL;
    node->next  = *ch;
    node->prev  = NULL;

    if( (*ch) != NULL)
        (*ch)->prev = node;
    *ch         = node;
}

void get_hulls(int star_coordinates[STARS][2], Convex_Hulls **ch)
{
    int visited[HEIGHT][WIDTH];

    for(int i = 0; i < HEIGHT; i++)
        for(int j = 0; j < WIDTH; j++)
            visited[i][j] = 0;

    for (int i = 0; i < STARS; i++) {
        int x = star_coordinates[i][0];
        int y = star_coordinates[i][1];

        visited[x][y] = 1;
    }

    Stack *stack = NULL;

    while(1)
    {
        int count = 0;

        for(int i = 0; i < HEIGHT; i++) {
            for(int j = 0; j < WIDTH; j++) {
                if (visited[i][j] == 1)
                    count++;
            }
        }

        if (count == 0)
            return;

        if (count < 3) {
            for(int i = 0; i < HEIGHT; i++)
                for(int j = 0; j < WIDTH; j++)
                    if (visited[i][j] == 1) {
                        Point z = {i,j};
                        push(&stack, z);
                    }
            add_hull(ch, &stack, visited);
            return;
        }

        Point current_points[STARS];
        count = 0;
        for(int i = 0; i < HEIGHT; i++)
            for(int j = 0; j < WIDTH; j++)
                if (visited[i][j] == 1) {
                    current_points[count].x = i;
                    current_points[count++].y = j;
                }

        stack = convex_stack(current_points, count);
        add_hull(ch, &stack, visited);
    }
}

// create_stars

// randomly creates points in the matrix(stars) ensures
// that each point has unique coordinates

void create_stars(int star_coordinates[STARS][2])
{
    for(int i = 0; i < STARS; i++) {
        star_coordinates[i][0] = rand()%HEIGHT;
        star_coordinates[i][1] = rand()%WIDTH;
        for (int j = 0; j < i; j++) {
            if (star_coordinates[i][0] == star_coordinates[j][0] &&
                    (star_coordinates[i][1] == star_coordinates[j][1]))
                i--; // try again
        }
    }
}

// goto_xy

// ANSI escape to move to x, y

void goto_xy(int x, int y)
{
    printf("\033[%d;%dH", x+1, y+1); 
}


void print_2_hulls(int star_coordinates[STARS][2], Convex_Hulls *ch)
{
    for(int i = 0; i < STARS; i++)
    {
        goto_xy(star_coordinates[i][0], star_coordinates[i][1]);
        printf("x");
    }

    printf("\033[1;34m");
    for (int i = 0; i < ch->count; i++)
    {
        goto_xy(ch->hulls[i].x, ch->hulls[i].y);
        printf("x");
    }

    printf("\033[1;32m");
    for (int i = 0; i < ch->next->count; i++)
    {
        goto_xy(ch->next->hulls[i].x, ch->next->hulls[i].y);
        printf("x");
    }
}

char get_key()
{
    char c;
    read(STDIN_FILENO, &c, 1);
    return c;
}

int main()
{
    Convex_Hulls *hull_struct = NULL;
    int star_coordinates[STARS][2];
    struct termios oldt, newt;
    int ch;
    int oldf;
    char c;

    tcgetattr(STDIN_FILENO, &oldt);

    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    printf("\e[?25l");

    create_stars(star_coordinates);


    system("clear");
    printf("\033[0m");
    for(int i = 0; i < STARS; i++) {
        goto_xy(star_coordinates[i][0], star_coordinates[i][1]);
        printf("x");
    }

    fflush(stdout);
    usleep(500000);

    printf("\033[0m");
    get_hulls(star_coordinates, &hull_struct);

    while(1)
    {
        c = get_key();

        switch (c)
        {
            case 'q':
                while(hull_struct->prev != NULL)
                    hull_struct = hull_struct->prev;
                while(hull_struct != NULL)
                {
                    Convex_Hulls *temp;
                    temp = hull_struct;
                    hull_struct = hull_struct->next;
                    free(temp);
                }

                tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
                printf("\e[?25h");

                return 0;
            case 'a':
                if (hull_struct->next->next != NULL) {

                    hull_struct = hull_struct->next;

                    printf("\033[0m");
                    print_2_hulls(star_coordinates, hull_struct);
                    fflush(stdout);
                }
                break;
            case 'd':
                if (hull_struct->prev != NULL) {

                    hull_struct = hull_struct->prev;

                    printf("\033[0m");
                    print_2_hulls(star_coordinates, hull_struct);
                    fflush(stdout);
                }
        }
    }

    while(hull_struct->prev != NULL)
        hull_struct = hull_struct->prev;
    while(hull_struct != NULL)
    {
        Convex_Hulls *temp;
        temp = hull_struct;
        hull_struct = hull_struct->next;
        free(temp);
    }

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    printf("\e[?25h");


    return 0;
}
// written by K01313S (ninja hacker angel)
