/*
 * decimal_to_fraction.c -- Converts decimal numbers to fractions using three different algorithms:
 *                          - Original algorithm with numerator and denominator iteration.
 *                          - Binary search algorithm using mediants.
 *                          - Improved continued fraction algorithm with mediant examination.
 *
 * The program includes a test suite that generates random decimal numbers and compares the performance
 * and accuracy of the algorithms.
 *
 * Usage:
 *   Compile: gcc -o decimal_to_fraction decimal_to_fraction.c -lm
 *   Run: ./decimal_to_fraction
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <time.h>
#include <sys/time.h>

/* Structure to hold the closest fraction */
struct fraction {
    long double difference;  // Difference between decimal and fraction
    int numerator;           // Numerator of the fraction
    int denominator;         // Denominator of the fraction
};

/* Function prototypes */
void find_fraction_original(long double decimal, struct fraction *closest_frac, int max_denominator);
void find_fraction_binary_search(long double decimal, struct fraction *closest_frac, int max_denominator);
void find_fraction_continued_fraction(long double decimal, struct fraction *closest_frac, int max_denominator);
void run_tests(int num_tests, int max_denominator);
int gcd_euclidean(int a, int b);
void bail(const char *message);

int main()
{
    int num_tests = 1000;        // Number of random test cases
    int max_denominator = 10000; // Maximum denominator for the fractions

    run_tests(num_tests, max_denominator);
    return EXIT_SUCCESS;
}

/*
 * find_fraction_original -- Original algorithm that finds the fraction closest
 *                           to the decimal number with denominators up to max_denominator.
 *
 * decimal: The decimal number to approximate.
 * closest_frac: Pointer to a fraction structure to store the closest fraction.
 * max_denominator: The maximum allowed denominator.
 *
 * This algorithm iterates through possible denominators and calculates the
 * numerator that makes the fraction closest to the decimal. It handles negative
 * numbers and numbers greater than 1 by separating the integer and fractional parts.
 */
void find_fraction_original(long double decimal, struct fraction *closest_frac, int max_denominator)
{
    int sign = 1;

    /* Handle negative numbers */
    if (decimal < 0.0L) {
        sign = -1;
        decimal = -decimal;
    }

    /* Separate integer and fractional parts */
    int integer_part = (int)floorl(decimal);
    long double fractional_part = decimal - integer_part;

    /* Initialize the closest fraction */
    closest_frac->difference = LDBL_MAX;
    closest_frac->numerator = integer_part;
    closest_frac->denominator = 1;

    /* If fractional part is zero, we're done */
    if (fractional_part == 0.0L) {
        closest_frac->numerator *= sign;
        return;
    }

    /* Iterate through possible denominators */
    for (int denominator = 1; denominator <= max_denominator; denominator++) {
        /* Find the numerator that makes the fraction closest to the fractional part */
        int numerator = (int)roundl(fractional_part * denominator);

        /* Check nearby numerators */
        for (int n = numerator - 1; n <= numerator + 1; n++) {
            if (n < 0 || n > denominator) {
                continue;
            }

            /* Calculate the fraction value and difference */
            long double fraction_value = (long double)n / denominator;
            long double difference = fabsl(fractional_part - fraction_value);

            /* Update the closest fraction if this one is closer */
            if (difference < closest_frac->difference) {
                closest_frac->difference = difference;
                closest_frac->numerator = integer_part * denominator + n;
                closest_frac->denominator = denominator;

                /* If exact match is found, exit early */
                if (difference == 0.0L) {
                    closest_frac->numerator *= sign;
                    return;
                }
            }
        }
    }

    /* Apply the sign to the numerator */
    closest_frac->numerator *= sign;
}

/*
 * find_fraction_binary_search -- Binary search algorithm that finds the fraction
 *                                closest to the decimal number with denominators
 *                                up to max_denominator.
 *
 * decimal: The decimal number to approximate.
 * closest_frac: Pointer to a fraction structure to store the closest fraction.
 * max_denominator: The maximum allowed denominator.
 *
 * This algorithm uses the mediant property of fractions to perform a binary search
 * for the best approximation. It handles negative numbers and numbers greater than 1
 * by separating the integer and fractional parts.
 */
void find_fraction_binary_search(long double decimal, struct fraction *closest_frac, int max_denominator)
{
    int sign = 1;

    /* Handle negative numbers */
    if (decimal < 0.0L) {
        sign = -1;
        decimal = -decimal;
    }

    /* Separate integer and fractional parts */
    int integer_part = (int)floorl(decimal);
    long double fractional_part = decimal - integer_part;

    /* Initialize the closest fraction */
    closest_frac->difference = LDBL_MAX;
    closest_frac->numerator = integer_part;
    closest_frac->denominator = 1;

    /* If fractional part is zero, we're done */
    if (fractional_part == 0.0L) {
        closest_frac->numerator *= sign;
        return;
    }

    /* Initialize bounds */
    int low_num = 0, low_den = 1;    // Left bound fraction (0/1)
    int high_num = 1, high_den = 1;  // Right bound fraction (1/1)

    while (1) {
        int mediant_num = low_num + high_num;
        int mediant_den = low_den + high_den;

        /* Break if denominator exceeds maximum allowed */
        if (mediant_den > max_denominator) {
            break;
        }

        /* Calculate mediant value and difference */
        long double mediant_value = (long double)mediant_num / mediant_den;
        long double difference = fabsl(fractional_part - mediant_value);

        /* Update the closest fraction if this one is closer */
        if (difference < closest_frac->difference) {
            closest_frac->difference = difference;
            closest_frac->numerator = integer_part * mediant_den + mediant_num;
            closest_frac->denominator = mediant_den;

            /* If exact match is found, exit early */
            if (difference == 0.0L) {
                closest_frac->numerator *= sign;
                return;
            }
        }

        /* Decide which side to continue searching on */
        if (mediant_value < fractional_part) {
            low_num = mediant_num;
            low_den = mediant_den;
        } else {
            high_num = mediant_num;
            high_den = mediant_den;
        }
    }

    /* After the loop, check fractions with the maximum denominator */
    for (int denom = max_denominator; denom >= 1; denom--) {
        int numer = (int)roundl(fractional_part * denom);

        /* Ensure numerator is within bounds */
        if (numer < 0)
            numer = 0;
        if (numer > denom)
            numer = denom;

        /* Calculate the fraction value and difference */
        long double frac_value = (long double)numer / denom;
        long double difference = fabsl(fractional_part - frac_value);

        /* Update the closest fraction if this one is closer */
        if (difference < closest_frac->difference) {
            closest_frac->difference = difference;
            closest_frac->numerator = integer_part * denom + numer;
            closest_frac->denominator = denom;

            if (difference == 0.0L) {
                closest_frac->numerator *= sign;
                return;
            }
        }
    }

    /* Apply the sign to the numerator */
    closest_frac->numerator *= sign;
}



/*
 * find_fraction_continued_fraction -- Uses the continued fraction method to find the
 *                                     closest fraction to the decimal number within
 *                                     the maximum denominator.
 */
void find_fraction_continued_fraction(long double decimal, struct fraction *closest_frac, int max_denominator)
{
    int a0 = floorl(decimal);
    long double fraction = decimal - a0;

    int num1 = 1, den1 = 0;
    int num = a0, den = 1;

    /* Initialize the closest fraction */
    closest_frac->difference = LDBL_MAX;
    closest_frac->numerator = num;
    closest_frac->denominator = den;

    while (den <= max_denominator) {
        long double current_value = (long double)num / den;
        long double difference = fabsl(decimal - current_value);

        if (difference < closest_frac->difference) {
            closest_frac->difference = difference;
            closest_frac->numerator = num;
            closest_frac->denominator = den;

            if (difference == 0.0) {
                break;
            }
        }

        if (fraction == 0.0) {
            break;
        }

        fraction = 1.0 / fraction;
        int ai = floorl(fraction);

        int num2 = num1;
        int den2 = den1;
        num1 = num;
        den1 = den;
        num = ai * num1 + num2;
        den = ai * den1 + den2;

        fraction = fraction - ai;
    }

    if (closest_frac->denominator > max_denominator) {
        // Use the previous fraction
        closest_frac->numerator = num1;
        closest_frac->denominator = den1;
        closest_frac->difference = fabsl(decimal - ((long double)num1 / den1));
    }
}

/*
 * run_tests -- Runs tests on all algorithms using random decimal numbers
 *              and measures their execution times.
 *
 * num_tests: Number of random test cases to run.
 * max_denominator: Maximum denominator for the fractions.
 *
 * This function generates random decimal numbers and tests all three algorithms.
 * It measures execution times, compares the fractions, and reports any discrepancies.
 */
void run_tests(int num_tests, int max_denominator)
{
    struct fraction frac_original;
    struct fraction frac_binary;
    struct fraction frac_continued;
    struct timeval start, end;
    double total_time_original = 0.0;
    double total_time_binary = 0.0;
    double total_time_continued = 0.0;
    int progress_interval = num_tests / 10; // For progress indication
    int discrepancies = 0; // Count of discrepancies between algorithms

    /* Seed the random number generator */
    srand(time(NULL));

    printf("Running %d tests with maximum denominator %d...\n", num_tests, max_denominator);

    for (int i = 0; i < num_tests; i++) {
        long double decimal;
        double time_original, time_binary, time_continued;

        /* Generate a random decimal number between -10.0 and 10.0 */
        decimal = ((long double)rand() / RAND_MAX) * 20.0L - 10.0L;

        /* Measure time for original algorithm */
        gettimeofday(&start, NULL);
        find_fraction_original(decimal, &frac_original, max_denominator);
        gettimeofday(&end, NULL);
        time_original = (end.tv_sec - start.tv_sec) * 1e6;
        time_original = (time_original + (end.tv_usec - start.tv_usec)) * 1e-6;
        total_time_original += time_original;

        /* Measure time for binary search algorithm */
        gettimeofday(&start, NULL);
        find_fraction_binary_search(decimal, &frac_binary, max_denominator);
        gettimeofday(&end, NULL);
        time_binary = (end.tv_sec - start.tv_sec) * 1e6;
        time_binary = (time_binary + (end.tv_usec - start.tv_usec)) * 1e-6;
        total_time_binary += time_binary;

        /* Measure time for continued fraction algorithm */
        gettimeofday(&start, NULL);
        find_fraction_continued_fraction(decimal, &frac_continued, max_denominator);
        gettimeofday(&end, NULL);
        time_continued = (end.tv_sec - start.tv_sec) * 1e6;
        time_continued = (time_continued + (end.tv_usec - start.tv_usec)) * 1e-6;
        total_time_continued += time_continued;

        /* Verify that the fractions are close enough */
        const long double epsilon = 1e-6L; // Tolerance for considering fractions as equal

        long double diff_orig_cont = fabsl(frac_original.difference - frac_continued.difference);
        long double diff_bin_cont = fabsl(frac_binary.difference - frac_continued.difference);

        if (diff_orig_cont > epsilon || diff_bin_cont > epsilon) {
            discrepancies++;
            printf("Discrepancy found for decimal: %.10Lf\n", decimal);
            printf("Original fraction:      %d/%d (difference: %.10Le)\n",
                   frac_original.numerator, frac_original.denominator, frac_original.difference);
            printf("Binary search fraction: %d/%d (difference: %.10Le)\n",
                   frac_binary.numerator, frac_binary.denominator, frac_binary.difference);
            printf("Continued fraction:     %d/%d (difference: %.10Le)\n",
                   frac_continued.numerator, frac_continued.denominator, frac_continued.difference);
            printf("-------------------------------------------------------------\n");
        }

        /* Print progress */
        if ((i + 1) % progress_interval == 0) {
            printf("Completed %d%% of tests...\n", ((i + 1) * 100) / num_tests);
        }
    }

    /* Calculate average times */
    double avg_time_original = total_time_original / num_tests;
    double avg_time_binary = total_time_binary / num_tests;
    double avg_time_continued = total_time_continued / num_tests;

    /* Print summary */
    printf("\nTest Summary:\n");
    printf("Total Tests Run: %d\n", num_tests);
    printf("Maximum Denominator: %d\n", max_denominator);
    printf("Average Time (Original Algorithm):         %.6f seconds\n", avg_time_original);
    printf("Average Time (Binary Search Algorithm):    %.6f seconds\n", avg_time_binary);
    printf("Average Time (Continued Fraction Method):  %.6f seconds\n", avg_time_continued);
    printf("Total Time (Original Algorithm):           %.6f seconds\n", total_time_original);
    printf("Total Time (Binary Search Algorithm):      %.6f seconds\n", total_time_binary);
    printf("Total Time (Continued Fraction Method):    %.6f seconds\n", total_time_continued);

    /* Calculate speedup */
    if (avg_time_continued > 0.0) {
        double speedup_original = avg_time_original / avg_time_continued;
        double speedup_binary = avg_time_binary / avg_time_continued;
        printf("Speedup (Original / Continued Fraction):        %.2f times\n", speedup_original);
        printf("Speedup (Binary Search / Continued Fraction):   %.2f times\n", speedup_binary);
    }

    printf("Number of discrepancies found: %d\n", discrepancies);
}

/*
 * bail -- Prints an error message and exits the program.
 *
 * message: The error message to display.
 */
void bail(const char *message)
{
    fprintf(stderr, "Error: %s\n", message);
    exit(EXIT_FAILURE);
}


