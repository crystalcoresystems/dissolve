/*
 * domino_sort.c -- Domino Sort algorithm.
 *
 * This program generates an array of random numbers and sorts them using
 * the Domino Sort algorithm, which recursively breaks down numbers and
 * sorts them based on their "fractalized" components.
 *
 * Usage:
 *   Compile: gcc -o domino_sort domino_sort.c -lm
 *   Run: ./domino_sort
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_THRESHOLD 200   // Maximum value for splitting numbers
#define ARRAY_SIZE 20000     // Size of the array to sort
#define FRACTAL_SIZE 64     // Maximum size of the fractal array

/* Structure to hold fractalized number data */
typedef struct {
    int original_number;                   // Original number
    int fractal_size;                      // Size of the fractal array
    int fractal_components[FRACTAL_SIZE];  // Fractalized components of the number
} DominoElement;

/* Function prototypes */
void generate_numbers(int *array, int size);
void domino_sort(DominoElement *elements, int size);
int fractalize_number(int *fractal_array, int number);
void sort_array(DominoElement *elements, int size);
void print_numbers(int *array, int size);
int compare_domino_elements(const void *a, const void *b);

int main()
{
    int numbers[ARRAY_SIZE];
    DominoElement elements[ARRAY_SIZE];

    /* Generate random numbers */
    generate_numbers(numbers, ARRAY_SIZE);

    /* Prepare DominoElements array */
    for (int i = 0; i < ARRAY_SIZE; i++) {
        elements[i].original_number = numbers[i];
        elements[i].fractal_size = fractalize_number(elements[i].fractal_components, numbers[i]);
    }

    /* Sort using optimized Domino Sort */
    clock_t start = clock();
    domino_sort(elements, ARRAY_SIZE);
    clock_t end = clock();
    double domino_sort_time = (double)(end - start) / CLOCKS_PER_SEC;

    /* Sort using qsort for comparison */
    int numbers_qsort[ARRAY_SIZE];
    memcpy(numbers_qsort, numbers, sizeof(numbers));
    start = clock();
    qsort(numbers_qsort, ARRAY_SIZE, sizeof(int), compare_domino_elements);
    end = clock();
    double qsort_time = (double)(end - start) / CLOCKS_PER_SEC;

    /* Compare sorted arrays */
    int mismatch_count = 0;
    for (int i = 0; i < ARRAY_SIZE; i++) {
        if (elements[i].original_number != numbers_qsort[i]) {
            mismatch_count++;
        }
    }

    /* Output performance results */
    printf("Domino Sort Time: %.6f seconds\n", domino_sort_time);
    printf("qsort Time:       %.6f seconds\n", qsort_time);
    printf("Number of mismatches between Domino Sort and qsort: %d\n", mismatch_count);

    return 0;
}

/*
 * generate_numbers -- Generates an array of random numbers.
 *
 * array: The array to fill with random numbers.
 * size: The size of the array.
 */
void generate_numbers(int *array, int size)
{
    srand((unsigned int)time(NULL));
    for (int i = 0; i < size; i++) {
        array[i] = rand() % 100000000; // Increased range for diversity
    }
}

/*
 * fractalize_number -- Fractalizes a number by halving it and retaining remainders.
 *
 * fractal_array: The array to store the fractalized components.
 * number: The number to fractalize.
 *
 * Returns the size of the fractal array.
 */
int fractalize_number(int *fractal_array, int number)
{
    int level = 0;
    int stack[FRACTAL_SIZE];
    int stack_top = -1;

    /* Initialize the stack with the initial number */
    stack[++stack_top] = number;

    while (stack_top >= 0) {
        int num = stack[stack_top--];
        int halves = 0;
        int remainder = 0;

        /* Break up the number by halving and collecting remainders */
        while (num > MAX_THRESHOLD) {
            if (num % 2) {
                remainder += (1 << halves); // Use bit shift instead of pow()
            }
            halves++;
            num /= 2;
        }

        /* Record the halves and number */
        if (halves > MAX_THRESHOLD) {
            stack[++stack_top] = halves;
            fractal_array[level++] = MAX_THRESHOLD + 1; // Indicator for further fractalization
        } else {
            fractal_array[level++] = halves;
        }

        fractal_array[level++] = num;

        /* Handle the remainder */
        if (remainder > MAX_THRESHOLD) {
            stack[++stack_top] = remainder;
            fractal_array[level++] = MAX_THRESHOLD + 1;
        } else if (remainder > 0) {
            fractal_array[level++] = remainder;
        }
    }

    return level;
}

/*
 * domino_sort -- Sorts an array of DominoElements using the optimized Domino Sort algorithm.
 *
 * elements: The array of DominoElements to sort.
 * size: The size of the array.
 */
void domino_sort(DominoElement *elements, int size)
{
    sort_array(elements, size);
}

/*
 * sort_array -- Sorts the array of DominoElements based on their fractal components.
 *
 * elements: The array of DominoElements to sort.
 * size: The size of the array.
 */
void sort_array(DominoElement *elements, int size)
{
    int max_level = 0;

    /* Find the maximum fractal size */
    for (int i = 0; i < size; i++) {
        if (elements[i].fractal_size > max_level) {
            max_level = elements[i].fractal_size;
        }
    }

    /* Perform radix sort based on fractal components */
    for (int level = max_level - 1; level >= 0; level--) {
        /* Counting sort for this level */
        int count[MAX_THRESHOLD + 3] = { 0 }; // +3 to handle MAX_THRESHOLD + 1 and beyond

        /* Count occurrences */
        for (int i = 0; i < size; i++) {
            int key = (level < elements[i].fractal_size) ? elements[i].fractal_components[level] : 0;
            count[key]++;
        }

        /* Compute cumulative counts */
        for (int i = 1; i < MAX_THRESHOLD + 3; i++) {
            count[i] += count[i - 1];
        }

        /* Sort based on the current level */
        DominoElement *output = (DominoElement *)malloc(size * sizeof(DominoElement));
        if (!output) {
            perror("Failed to allocate memory for sorting");
            exit(EXIT_FAILURE);
        }

        for (int i = size - 1; i >= 0; i--) {
            int key = (level < elements[i].fractal_size) ? elements[i].fractal_components[level] : 0;
            output[--count[key]] = elements[i];
        }

        /* Copy back to elements array */
        memcpy(elements, output, size * sizeof(DominoElement));
        free(output);
    }
}

/*
 * compare_domino_elements -- Comparison function for qsort.
 *
 * a: Pointer to the first element.
 * b: Pointer to the second element.
 *
 * Returns negative if a < b, zero if equal, positive if a > b.
 */
int compare_domino_elements(const void *a, const void *b)
{
    int int_a = *(const int *)a;
    int int_b = *(const int *)b;

    if (int_a < int_b)
        return -1;
    else if (int_a > int_b)
        return 1;
    else
        return 0;
}
