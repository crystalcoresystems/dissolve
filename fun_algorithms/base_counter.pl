#!/usr/bin/perl
# base_counter
# This script generates a 2D array containing all possible combinations of digits
# from 0 to ($digits - 1), but for values greater than 9, letters of the alphabet (a, b, c, ...) will be used.
# It then prints the 2D array in a formatted manner.

use strict;
use warnings;

my $depth = 2;    # Length of each combination
my $digits = 26;  # Range of digits (0-9 and then a-f for base 16)

my $result_ref = generate_base_combinations($depth, $digits);
my @result = @{ $result_ref };

print_2d_array(@result);

# Function to generate all base combinations of a certain depth
sub generate_base_combinations {
    my ($depth, $digits) = @_;
    my @list;
    my $total_combinations = $digits ** $depth;  # Total number of combinations

    for (my $i = 0; $i < $total_combinations; $i++) {
        my @combination = ();
        my $number = $i;

        # Convert the number to base $digits and store each digit (use letters for values > 9)
        while ($number > 0) {
            unshift(@combination, base_digit($number % $digits));
            $number = int($number / $digits);
        }

        # Pad the combination with leading zeros (or '0') to reach the desired depth
        while (scalar(@combination) < $depth) {
            unshift(@combination, '0');
        }

        # Add the combination to the list
        push(@list, [ @combination ]);  # Create a copy of @combination
    }
    return \@list;  # Return a reference to the list of combinations
}

# Function to convert a digit to its corresponding character (0-9 or a-f for values 10+)
sub base_digit {
    my ($digit) = @_;
    return $digit < 10 ? $digit : chr($digit - 10 + ord('a'));  # Convert 10+ to a, b, c, etc.
}

# Function to print a 2D array
sub print_2d_array {
    my @array = @_;

    print "-" x 80, "\n";
    foreach my $row (@array) {
        print join('', @$row), "\n";  # Join and print each combination
    }
    print "-" x 40, "\n";
}
