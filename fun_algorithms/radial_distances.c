// written by K01313S (ninja hacker angel)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define HEIGHT                  97
#define WIDTH                   171


// Distance lib../

typedef struct Grid_Distance {
    int                     distance;
    int                     i, j;
    struct Grid_Distance    *next;
} Grid_Distance;


void add_distance(struct Grid_Distance** root, int i, int j)
{
    Grid_Distance* node = calloc(1, sizeof(Grid_Distance));

    if (node == NULL) {
        fprintf(stderr, "Memory allocation failed");
        exit(EXIT_FAILURE);
    }

    node->distance = i*i + j*j;
    node->i = i;
    node->j = j;
    
    // Special case for root
    if(*root == NULL || (*root)->distance >= node->distance) {
        node->next = *root;
        *root = node;
    } else {
        Grid_Distance *current = *root;
        while(current->next != NULL && current->next->distance < node->distance) {
            current = current->next;
        }
        node->next = current->next;
        current->next = node;
    }
}

void add_distances(Grid_Distance** root)
{
    int i;
    int j;
    
    for(i = 0; i < HEIGHT; i++) {
        for (j = 0; j < WIDTH; j++) {
            if (j == 0 && i == 0)
                continue;
            add_distance(root, i, j);
        }
    }
}

void free_distance_list(Grid_Distance **root)
{
    Grid_Distance *temp;

    while(*root != NULL) {
        temp = *root;
        *root = (*root)->next;
        free(temp);
    }
    *root = NULL;
}
//..

int main()
{
    Grid_Distance *distance_list = NULL;
    Grid_Distance *temp = NULL;

    FILE *fo = fopen("distance_list.bin", "wb");
    if (fo == NULL) {
        perror("error opening file for writing");
        return 1;
    }

    add_distances(&distance_list);
    temp = distance_list;

    while (distance_list != NULL)
    {
        int distance = distance_list->distance;
        while (distance_list != NULL &&
                distance_list->distance == distance)
        {
            char delim = ',';
            int x, y;
            char nl = '\n';

            x = distance_list->i;
            y = distance_list->j;

            fwrite(&x, sizeof(int), 1, fo);
            fwrite(&y, sizeof(int), 1, fo);

            x = -1 * distance_list->i;
            y = distance_list->j;

            fwrite(&x, sizeof(int), 1, fo);
            fwrite(&y, sizeof(int), 1, fo);

            x = distance_list->i;
            y = -1 * distance_list->j;

            fwrite(&x, sizeof(int), 1, fo);
            fwrite(&y, sizeof(int), 1, fo);

            x = -1 * distance_list->i;
            y = -1 * distance_list->j;

            fwrite(&x, sizeof(int), 1, fo);
            fwrite(&y, sizeof(int), 1, fo);

            distance_list = distance_list->next;

            if (distance_list != NULL &&
                    distance_list->distance == distance)

                fwrite(&delim, sizeof(char), 1, fo);
            else
                fwrite(&nl, sizeof(char), 1, fo);
        }

    }

    fclose(fo);

    free_distance_list(&temp);
}
// written by K01313S (ninja hacker angel)
