#include <stdio.h>
#include <stdbool.h>
#include "bitwise.h"

#define eq(a,b) a[0] = b[0]; a[1] = b[1]
#define cmp(a,b) (a[0] == b[0] && a[1] == b[1])

bool one[2] = {0,1}; bool zero[2] = {1,1}; bool theta[2] = {1,0};


bool permutate8(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2],
                bool F[2], bool G[2], bool H[2]); 
bool permutate7(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2],
                bool F[2], bool G[2]); 
bool permutate6(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2],
                bool F[2]);
bool permutate5(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2]);
bool permutate4(bool A[2], bool B[2], bool C[2], bool D[2]);
bool permutate3(bool A[2], bool B[2], bool C[2]);
bool permutate2(bool A[2], bool B[2]);
bool permutate1(bool A[2]);

int main(int argc, char ** argv)
{
        struct gen_8 mech;
        bool A[2], B[2], C[2], D[2], E[2], F[2], G[2], H[2];
        bool sA[2], sB[2], sC[2], sD[2], sE[2], sF[2], sG[2], sH[2];
        bool flag = 0;
        int i; 

        eq(A,zero); eq(B,zero); eq(C,zero); eq(D,zero); eq(E,zero); eq(F,zero);
        eq(G,zero); eq(H,zero);

        eq(sA,A); eq(sB,B); eq(sC,C); eq(sD,D); eq(sE,E); eq(sF,F);
        eq(sG,G); eq(sH,H);

        while(1)
        {
                if (flag){break;}

                prop_8(A, B, C, D, E, F, G, H, &mech);
                for(i = 0; i < 50; i++) {
                        print_graph(mech.d);
                        cycle_ig8(&mech);
                }
                printf("    -->");
                print_trinity(A);print_trinity(B);print_trinity(C);
                print_trinity(D);print_trinity(E);print_trinity(F);
                print_trinity(G);print_trinity(H);
                printf("\n");

                flag = permutate8(sA, sB, sC, sD, sE, sF, sG, sH);
                eq(A,sA); eq(B,sB); eq(C,sC); eq(D,sD); eq(E,sE); eq(F,sF);
                eq(G,sG); eq(H,sH);
        } 
} 

bool permutate8(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2],
                bool F[2], bool G[2], bool H[2])
{
        bool flag;

        if (cmp(H,theta) && cmp(G,theta) && cmp(F,theta) && cmp(E,theta) && cmp(D,theta) &&
                        cmp(C,theta) && cmp(B,theta) && cmp(A,theta))
        {
                return 1;
        }
                
        flag = permutate7(A, B, C, D, E, F, G);
        if(flag)
        {
                cycle_gear(theta, H, H);
                eq(A,zero);
                eq(B,zero);
                eq(C,zero);
                eq(D,zero);
                eq(E,zero);
                eq(F,zero);
                eq(G,zero);
        }
        return 0;
}
bool permutate7(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2],
                bool F[2], bool G[2])
{
        bool flag;

        if (cmp(G,theta) && cmp(F,theta) && cmp(E,theta) && cmp(D,theta) &&
                        cmp(C,theta) && cmp(B,theta) && cmp(A,theta))
        {
                return 1;
        }
                
        flag = permutate6(A, B, C, D, E, F);
        if(flag)
        {
                cycle_gear(theta, G, G);
                eq(A,zero);
                eq(B,zero);
                eq(C,zero);
                eq(D,zero);
                eq(E,zero);
                eq(F,zero);
        }
        return 0;
}
bool permutate6(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2],
                bool F[2])
{
        bool flag;

        if (cmp(F,theta) && cmp(E,theta) && cmp(D,theta) &&
                        cmp(C,theta) && cmp(B,theta) && cmp(A,theta))
        {
                return 1;
        }
                
        flag = permutate5(A, B, C, D, E);
        if(flag)
        {
                cycle_gear(theta, F, F);
                eq(A,zero);
                eq(B,zero);
                eq(C,zero);
                eq(D,zero);
                eq(E,zero);
        }
        return 0;
}
bool permutate5(bool A[2], bool B[2], bool C[2], bool D[2], bool E[2])
{
        bool flag;

        if (cmp(E,theta) && cmp(D,theta) &&
                        cmp(C,theta) && cmp(B,theta) && cmp(A,theta))
        {
                return 1;
        }
                
        flag = permutate4(A, B, C, D);
        if(flag)
        {
                cycle_gear(theta, E, E);
                eq(A,zero);
                eq(B,zero);
                eq(C,zero);
                eq(D,zero);
        }
        return 0;
}
bool permutate4(bool A[2], bool B[2], bool C[2], bool D[2])
{
        bool flag;

        if (cmp(D,theta) &&
                        cmp(C,theta) && cmp(B,theta) && cmp(A,theta))
        {
                return 1;
        }
                
        flag = permutate3(A, B, C);
        if(flag)
        {
                cycle_gear(theta, D, D);
                eq(A,zero);
                eq(B,zero);
                eq(C,zero);
        }
        return 0;
}
bool permutate3(bool A[2], bool B[2], bool C[2])
{
        bool flag;

        if (cmp(C,theta) && cmp(B,theta) && cmp(A,theta))
        {
                return 1;
        }
                
        flag = permutate2(A, B);
        if(flag)
        {
                cycle_gear(theta, C, C);
                eq(A,zero);
                eq(B,zero);
        }
        return 0;

}
bool permutate2(bool A[2], bool B[2])
{
        bool flag;

        if (cmp(B,theta) && cmp(A,theta))
        {
                return 1;
        }
                
        flag = permutate1(A);
        if(flag)
        {
                cycle_gear(theta, B, B);
                eq(A,zero);
        }
        return 0;

}
bool permutate1(bool A[2])
{
        if (cmp(A,theta))
        {
                return 1;
        }
        cycle_gear(theta, A, A);
        return 0;
}
