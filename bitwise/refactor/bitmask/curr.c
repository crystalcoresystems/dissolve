#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "bitwise.h"

#define eq(a,b) a[0] = b[0]; a[1] = b[1]

/* trinity types */
extern bool null[2] = {0,0};
extern bool one[2] = {0,1};
extern bool theta[2] = {1,0};
extern bool zero[2] = {1,1};
#define A_SIZE 7
#define B_SIZE 3
#define C_SIZE 3
#define D_SIZE 3
void prop_mechs(struct mech *, struct mech *, struct mech *,
                struct mech *);
void print_output(bool out[2], int n);
        
int main()
{
        int i;
        bool buzz_stream[2], buzz_gate[2];
        bool fizz_stream[2], fizz_gate[2];
        bool output[2];
    /* Declare Mechanisms../                              */
        struct mech mech_a;
        struct mech mech_b;
        struct mech mech_c;
        struct mech mech_d;
    /* -----------------------------------------------/.. */
    /* Propogate Mechanisms */
        prop_mechs(&mech_a, &mech_b, &mech_c, &mech_d);
        
        for (i = 1; i <= 100; i++) {
                buzz_stream[0] = 0; buzz_stream[1] = 1;
                fizz_stream[0] = 1; fizz_stream[1] = 0;
    /* Cycle Buzz Gate../                                        */
                cycle_mech_static(&mech_a);
                cycle_mech_infinity(&mech_b);
                cycle_mech_infinity(&mech_c);
                mech_connect(&mech_a, &mech_b,
                             (mech_a.size)/2, (mech_b.size)/2);
                mech_connect(&mech_b, &mech_c,
                             (mech_b.size)/2, (mech_c.size)/2);
                solve(mech_a.array[(mech_a.size)/2], one, buzz_gate);
    /* ------------------------------------------------------/.. */
    /* Cycle Fizz Gate */
                cycle_mech_infinity(&mech_d);
                eq(fizz_gate, mech_d.array[mech_d.size/2]);
    /* Pass Streams Through Gates */
                gate_solve(buzz_stream, buzz_gate, buzz_stream);
                gate_solve(fizz_stream, fizz_gate, fizz_stream);
    /* get output */
                solve(fizz_stream, buzz_stream, output);
                print_output(output, i);
        }
        printf("\n");
}

void print_output(bool out[2], int n)
{
        if (out[0] == 0 && out[1] == 0) {
                printf("%i\n", n);
                return;
        }
        if (out[0] == 1)
                printf("fizz");
        if (out[1] == 1)
                printf("buzz");
        printf("\n");

}

      /* Propogate Mechanisms../                             */
void prop_mechs(struct mech *mech_a, struct mech *mech_b,
                struct mech *mech_c, struct mech *mech_d) 
{
        bool array[MAX_SIZE][2]; 
        eq(array[0], one);
        eq(array[1], zero);
        eq(array[2], zero);
        eq(array[3], theta);
        eq(array[4], one);
        eq(array[5], zero);
        eq(array[6], zero);
        prop_mech(array, A_SIZE, mech_a);
        eq(array[0], zero);
        eq(array[1], one);
        eq(array[2], zero);
        prop_mech(array, B_SIZE, mech_b);
        eq(array[0], one);
        eq(array[1], one);
        eq(array[2], theta);
        prop_mech(array, C_SIZE, mech_c);
        eq(array[0], theta);
        eq(array[1], zero);
        eq(array[2], zero);
        prop_mech(array, D_SIZE, mech_d);
}
    /* ------------------------------------------------/.. */
