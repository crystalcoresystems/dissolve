#include <stdbool.h>
#include <stdio.h>
#include "bitwise.h"

    /* Switch Solve../                                    */
void switch_solve(struct key_switch *key_switch, bool solve[2])
{
        bool prev[2] = {1,1};
        bool pass[2];
        int i, k;
        solve[0] = 0; solve[1] = 0;

    /* "k" Gets Address of Last Filled Buffer../      */
        for (k = 0; key_switch->buffer[k][0] != 0 ||
                    key_switch->buffer[k][1] != 0; k++){}
    /* ---------------------------------------------------/..    */
    /* return if buffer hasn't filled to size of key      */
        if (k < key_switch->size) { return; }
    /* "k" gets buffer address aligned with start of key  */
        k = k - key_switch->size;
    /*start iteration at 0 if k is less than key otherwise offset*/
        for (i = 0; i < key_switch->size; i++) {
                key_solve(key_switch->key[i], 
                                key_switch->buffer[k++], solve);
                solve[0] = solve[0] || solve[1];
                solve[1] = solve[0];
                prev[0] = prev[0] && solve[0];
                prev[1] = prev[1] && solve[1];
        }
        solve[0] = prev[0];
        solve[1] = prev[1];
        pass[0] = 1;
        pass[1] = !solve[1];
   /* clear buffer if key found */
        for (i = 0; i < MAX_KEY; i++) {
                gate_solve(key_switch->buffer[i], pass,
                                key_switch->buffer[i]);
        }
}
/* --------------------------------------------------------/.. */
/* Fill Solve                                    ../    */
void fill_solve(bool input[2], bool mech[2])
{
        bool tmp[2], tmp2[2];

        tmp[0] = mech[0] || mech[1];
        tmp[1] = tmp[0];
        tmp2[0] = input[0] && tmp[0];
        tmp2[1] = input[1] && tmp[1];

        tmp[0] = !(mech[0] || mech[1]);
        tmp[1] = tmp[0];
        tmp[0] = tmp[0] && input[0];
        tmp[1] = tmp[1] && input[1];
        mech[0] = mech[0] || tmp[0];
        mech[1] = mech[1] || tmp[1];

        input[0] = tmp2[0];
        input[1] = tmp2[1];
}
/* -------------------------------------------------/..*/
/* Key Solve                                        ../    */
void key_solve(bool input[2], bool key[2], bool solve[2])
{
        bool tmp[2];

        tmp[0] = input[0] && key[0];
        tmp[1] = input[1] && key[1];
        solve[0] = input[0] || key[0];
        solve[1] = input[1] || key[1];
        tmp[0] = xor(tmp[0],solve[0]);
        tmp[1] = xor(tmp[1],solve[1]);
        tmp[0] = !(tmp[0] || tmp[1]);
        tmp[1] = tmp[0];
        solve[0] = key[0] && tmp[0];
        solve[1] = key[1] && tmp[1];
}
/* -------------------------------------------------/..  */
/* Gate Solve                                       ../    */
void gate_solve(bool input[2], bool gate[2], bool solve[2])
        /* gate solv*/
{
        bool pass[2];

        pass[0] = xor(gate[0], gate[1]);
        pass[1] = pass[0];

        pass[0] = xor(pass[0], 1);
        pass[1] = pass[0];

        solve[0] = input[0] && pass[0];
        solve[1] = input[1] && pass[1];
}
/* -------------------------------------------------/..  */
/* Null Bypass Solve                                  ../        */
void nbypass_solve(bool input[2], bool mech[2], bool solve[2])
{
        bool tmp[2], tmp2[2], tmp3[2];


        tmp[0] = xor(input[0], mech[0]);
        tmp[1] = xor(input[1], mech[1]);

        tmp3[0] = tmp[0] || tmp[1];
        tmp3[1] = tmp3[0];

        tmp2[0] = xor(tmp[0], tmp3[0]);
        tmp2[1] = xor(tmp[1], tmp3[1]);

        tmp2[0] = xor(tmp3[0], tmp2[0]);
        tmp2[1] = xor(tmp3[1], tmp2[1]);

        tmp[0] = input[0] || input[1];
        tmp[1] = tmp[0];

        solve[0] = !tmp3[0] && input[0];
        solve[1] = !tmp3[1] && input[1];

        solve[0] = xor(solve[0], tmp2[0]);
        solve[1] = xor(solve[1], tmp2[1]);

        solve[0] = solve[0] && tmp[0];
        solve[1] = solve[1] && tmp[1];
}
/* -------------------------------------------------/..*/
/* Origninal Solve                                  ../        */
void solve(bool input[2], bool mech[2], bool solve[2])
{
        bool tmp[2], tmp2[2], tmp3[2];


        tmp[0] = xor(input[0], mech[0]);
        tmp[1] = xor(input[1], mech[1]);

        tmp3[0] = tmp[0] || tmp[1];
        tmp3[1] = tmp3[0];

        tmp2[0] = xor(tmp[0], tmp3[0]);
        tmp2[1] = xor(tmp[1], tmp3[1]);

        tmp2[0] = xor(tmp3[0], tmp2[0]);
        tmp2[1] = xor(tmp3[1], tmp2[1]);

        tmp[0] = !tmp3[0] && input[0];
        tmp[1] = !tmp3[1] && input[1];

        solve[0] = xor(tmp[0], tmp2[0]);
        solve[1] = xor(tmp[1], tmp2[1]);
}
/* -------------------------------------------------/..*/
