$!created on 10-MAR-2021
$!----------------------
$!
$! filename: BWLINK.COM
$!    input: main function to link
$!   output: exe
$!     idea: run command with set objects
$!     form: cmd procedure
$!
$!     __________________________
$! ---| Written by |K|O|13|13|S| |---
$!     ==========================
$
$ if ( p1 .eqs. "" ) then exit
$ link 'p1',cycle,print,solve,prop,fire_intervals
