$!created on 10-MAR-2021
$!----------------------
$!
$! filename: BUILD.COM
$!    input: main function to link
$!   output: exe
$!     idea: run command with set objects
$!     form: cmd procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$
$ cc curr
$ link curr,cycle,print,solve,prop
