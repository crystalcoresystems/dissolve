#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bitwise.h"

/* trinity types */
extern bool null[2] = {0,0};
extern bool one[2] = {0,1};
extern bool theta[2] = {1,0};
extern bool zero[2] = {1,1};
#define A_SIZE 9
#define B_SIZE 4
#define C_SIZE 8
#define A_CENTER 1
#define B_CENTER 2
#define C_CENTER 1
#define D_SIZE 9
#define E_SIZE 8
#define F_SIZE 9
#define D_CENTER 3
#define E_CENTER 2
#define F_CENTER 4

void get_stream(struct mech *mech_a, struct mech * mech_b,
               struct mech * mech_c, bool stream_a[2]);
        
int main()
{
        int i;
        bool array[MAX_SIZE][2]; 
    /* Declare Mechanisms../                              */
        struct mech mech_a;
        struct mech mech_b;
        struct mech mech_c;
        struct mech mech_d;
        struct mech mech_e;
        struct mech mech_f;
    /* -----------------------------------------------/.. */
        bool stream_a[2];
        bool stream_b[2];
        
    /* Propogate Mechanisms../                             */
        for (i = 0; i < MAX_SIZE; i++) {
                array[i][0] = i % 2;
                array[i][1] = (i + 1) % 2;
        }
        prop_mech(array, A_SIZE, &mech_a);
        prop_mech(array, B_SIZE, &mech_b);
        prop_mech(array, C_SIZE, &mech_c);
        prop_mech(array, D_SIZE, &mech_d);
        prop_mech(array, E_SIZE, &mech_e);
        prop_mech(array, F_SIZE, &mech_f);
    /* ------------------------------------------------/.. */
        for (i = 0; i < 100; i++) {
                get_stream(&mech_a, &mech_b, &mech_c, stream_a);
                get_stream(&mech_d, &mech_e, &mech_f, stream_b);
                print_graph(stream_a);
                print_graph(stream_b);
                printf("\n");
        }
}

    /* Get Stream Procedure../                          */
void get_stream(struct mech *mech_a, struct mech * mech_b,
               struct mech * mech_c, bool stream_a[2]) {
        cycle_mech_static(mech_a);
        cycle_mech_infinity(mech_b);
        cycle_mech_infinity(mech_c);
        mech_connect(mech_a, mech_b, A_CENTER, B_CENTER);
        mech_connect(mech_a, mech_c, A_CENTER, C_CENTER);
        stream_a[0] = mech_a->array[A_CENTER][0];
        stream_a[1] = mech_a->array[A_CENTER][1];
}
    /* ---------------------------------------------/.. */
