#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bitwise.h"
/* Print Verbose                                    ../----- 
 * print one zero or eight depending on values in array */
void print_verbose(bool a[2])
{
        if (a[0] == 0 && a[1] == 0)
                printf("null");
        if (a[0] == 1 && a[1] == 1)
                printf("zero");
        if (a[0] == 0 && a[1] == 1)
                printf("one");
        if (a[0] == 1 && a[1] == 0)
                printf("theta");
}
/* ------------------------------------------------/.. */
/* Print Trinity                                    ../----- 
 * print 1 0 or 8 depending on values in array */
void print_trinity(bool a[2])
{
        if (a[0] == 0 && a[1] == 0)
                printf("N");
        if (a[0] == 1 && a[1] == 1)
                printf("O");
        if (a[0] == 0 && a[1] == 1)
                printf("1");
        if (a[0] == 1 && a[1] == 0)
                printf("0");
}
/* -----------------------------------------/..   */
/* Print graph                                      ../----
 * -----v-^------v-^---- */
void print_graph(bool a[2])
{
        if (a[0] == 0 && a[1] == 0)
                printf(" ");
        if (a[0] == 1 && a[1] == 1)
                printf("^");
        if (a[0] == 0 && a[1] == 1)
                printf("-");
        if (a[0] == 1 && a[1] == 0)
                printf("v");
}
/* -----------------------------------------/..   */
/* Print graph2                                      ../----
 * -----O-^------O-^---- */
void print_graph2(bool a[2])
{
        if (a[0] == 0 && a[1] == 0)
                printf(" ");
        if (a[0] == 1 && a[1] == 1)
                printf("^");
        if (a[0] == 0 && a[1] == 1)
                printf("-");
        if (a[0] == 1 && a[1] == 0)
                printf("O");
}
/* -----------------------------------------/..   */
