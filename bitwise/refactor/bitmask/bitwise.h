#define xor(b1, b2) ((b1 && !b2) || (!b1 && b2))
#define MAX_KEY 256
#define MAX_SIZE 10 

extern bool null[2];
extern bool one[2];
extern bool theta[2];
extern bool zero[2];

/*structs../                                              */
/* key switch */
struct key_switch {
        bool key[MAX_KEY][2];
        bool buffer[MAX_KEY][2];
        int size;
};

/* linear or cross generator */
struct mech {
        bool array[MAX_SIZE][2];
        int size;
        int center;
};

/* gate */
struct gate {
        struct mech array[MAX_SIZE];
};
/*-----------------------------------------------------/.. */

/* propogate generators */
void prop_switch(bool key[MAX_KEY][2], int size,
                struct key_switch *);
void prop_mech(bool array[MAX_SIZE][2], int, struct mech *);

/* solve trinities */
void switch_solve(struct key_switch *, bool solve[2]);
void fill_solve(bool input[2], bool mech[2]);
void key_solve(bool input[2], bool key[2], bool solve[2]);
void gate_solve(bool input[2], bool gate[2], bool solve[2]);
void solve(bool input[2], bool mech[2], bool solve[2]);
void nbypass_solve(bool input[2], bool mech[2], bool solve[2]);
void gear_solve(bool input[2], bool gear[2], bool solve[2]);

/* cycle generators */
void cycle_mech_infinity(struct mech *);
void cycle_mech_static(struct mech *);
void mech_connect(struct mech *, struct mech *, int, int);
void cycle_gear(bool a[2], bool b[2], bool c[2]);

/* intervals */
//void fire(bool solved[2], struct gate_12_speed *a12_gate,
/* print functions */
void print_verbose(bool a[2]);
void print_trinity(bool a[2]);
void print_graph(bool a[2]);
void print_graph2(bool a[2]);
