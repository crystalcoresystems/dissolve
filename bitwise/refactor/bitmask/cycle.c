#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bitwise.h"

    /* Static Mechanism Cycle Procedure../                           */
void cycle_mech_static(struct mech *mech)
{
        int i;
        bool solved[MAX_SIZE][2];

        for (i = 1; i < (mech->size) - 1; i++) {
                solve(mech->array[i-1], mech->array[i+1],
                               solved[i]);
        }
        for (i = 1; i < (mech->size) - 1; i++) {
                mech->array[i][0] = solved[i][0]; 
                mech->array[i][1] = solved[i][1];
        }
}
/* ------------------------------------------------/.. */
    /* Infinity Mechanism Cycle Procedure../                     */
void cycle_mech_infinity(struct mech *mech)
{
        int i;
        bool solved[MAX_SIZE][2];

        for (i = 1; i < (mech->size) - 1; i++) {
                solve(mech->array[i-1], 
                      mech->array[i+1], solved[i]);
        }
        solve(mech->array[1],
              mech->array[mech->size - 1], solved[0]);
        solve(mech->array[0],
              mech->array[mech->size - 2], 
              solved[mech->size - 1]);
        for (i = 0; i < mech->size; i++) {
                mech->array[i][0] = solved[i][0]; 
                mech->array[i][1] = solved[i][1];
        }
}
/* ------------------------------------------------/..    */
    /* Mechanism Connector../                           */
void mech_connect(struct mech *mech_a, struct mech *mech_b, 
                                                int a, int b) {
        solve(mech_a->array[a],
                mech_b->array[b],
                mech_a->array[a]);
        mech_b->array[b][0] = mech_a->array[a][0];
        mech_b->array[b][1] = mech_a->array[a][1];
}
/* -------------------------------------------------/.. */
    /* 3 Speed Gear Cycle procedure../                      */ 
void cycle_gear(bool input[2], bool mech[2], bool solve[2]) 
{
        bool tmp[2], tmp2[2], mask_z[2], mask_t[3];

        tmp[0] = mech[0] && mech[1];
        tmp[1] = mech[1];
        tmp[1] = xor(tmp[0], tmp[1]);
        tmp[0] = xor(tmp[0], tmp[1]);

        tmp2[0] = mech[0] && mech[1];
        tmp2[1] = tmp2[0];
        tmp2[0] = xor(tmp2[0], mech[0]);
        tmp2[1] = xor(tmp2[1], mech[1]);
        tmp2[0] = tmp2[0] || tmp2[1];
        tmp2[1] = xor(tmp2[0], tmp2[1]);

        mask_z[0] = input[0] && input[1];
        mask_z[1] = mask_z[0];

        mask_t[1] = xor(input[0], input[1]);
        mask_t[0] = input[0] && mask_t[1];
        mask_t[1] = mask_t[0];

        tmp[0] = tmp[0] && mask_t[0];
        tmp[1] = tmp[1] && mask_t[1];
        tmp2[0] = tmp2[0] && mask_z[0];
        tmp2[1] = tmp2[1] && mask_z[1];
        tmp[0] = xor(tmp[0], tmp2[0]);
        tmp[1] = xor(tmp[1], tmp2[1]);

        solve[0] = xor(tmp[0], mech[0]);
        solve[1] = xor(tmp[1], mech[1]);

        tmp[0] = !(solve[0] || solve[1]);
        tmp[1] = tmp[0];
        tmp[0] = tmp[0] && mech[0];
        tmp[1] = tmp[1] && mech[1];
        solve[0] = tmp[0] || solve[0];
        solve[1] = tmp[1] || solve[1];
}
/* ------------------------------------------------/.. */
    /* Static Mechism Cycle doc../
 * Set Var:     mech(2d array)
 * Synopsis:    Uses solve array to get the sol. of adjacent cells.
 *              Puts into mech. 
-------------------------------------------------------------/.. */
    /* Infinity Mechism Cycle doc../
 * Set Var:     mech(2d array)
 * Synopsis:    Same as static with additional solve at the end 
-------------------------------------------------------------/.. */
    /* Mechanism Connector doc../
* Connects the mechanism at given points using solve and
* direct assignmet
* ------------------------------------------------------/.. */
/*3 Speed Gear Cycle doc../
 * Set Var:     solve
 * Synopsis:    complicated boolean
 *              0 + 0 = 0
 *              0 + 1 = 0
 *              0 + 8 = 1
 *              8 + 8 = 8
 *              8 + 1 = 8
 *              8 + 0 = 1
 *              1 + x = x
--------------------------------------------------------------/.. */
