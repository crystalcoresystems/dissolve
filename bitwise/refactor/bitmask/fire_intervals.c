#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bitwise.h"

/* Fire Based on The 156 Pattern                               ../
 * key passed to function can change fire rate                  */
void fire(bool solved[2], struct gate_12_speed *a12_gate,
     struct gate_6_speed *a6_gate, struct gate_3_speed *a3_gate,
     struct cross_beat_gen *beat_a,
     struct gen_7 *randomizer,struct key_switch *ksa, bool gear[2])
{
        bool pass[2];
        bool stream[2], stream_a[2], stream_b[2], stream_c[2];

        stream[0] = 1; stream[1] = 1;
    /* Send Variable "stream" Through Gates           ../       */
        gate_solve(stream, a3_gate->a.c, stream_a);
        gate_solve(stream, a6_gate->a.c, stream_b);
        gate_solve(stream_b, a6_gate->b.c, stream_b);
        gate_solve(stream, a12_gate->a.c, stream_c);
        gate_solve(stream_c, a12_gate->b.c, stream_c);
        gate_solve(stream_c, a12_gate->c.c, stream_c);
        solve(gear, zero, pass);
        gate_solve(stream_a, pass, stream_a);
        solve(pass, one, pass);
        gate_solve(stream_b, pass, stream_b);
        solve(pass, theta, pass);
        gate_solve(stream_c, pass, stream_c);
        solve(stream_a, stream_b, stream);
        solve(stream, stream_c, stream);
       /*----------------------------------------------/..      */
    /* Solve to Asymmetrical Cross for Beat Pattern             */
        solve(stream, beat_a->c, beat_a->c);
    /* Add Result to "ksa" Buffer                   ../         */ 
        stream[0] = beat_a->c[0];
        stream[1] = beat_a->c[1];
    /* add stream to next empty slot in buffer via fill_solve   */
        for (int j = 0; j < MAX_KEY; j++) {
                fill_solve(stream, ksa->buffer[j]);
        }
   /*-----------------------------------------------/..         */
    /* Check Buffer Against Key (success auto-empties buffer)   */
        switch_solve(ksa, solved);
    /* Cycle Mechanisms                          ../           */
        cycle_gear(gear, randomizer->c, gear);
        cycle_ig7(randomizer);
        cycle_sg5(&a12_gate->a);
        cycle_ig4(&a12_gate->b);
        cycle_ig3(&a12_gate->c);
        cycle_ig4(&a6_gate->a);
        cycle_ig3(&a6_gate->b);
        cycle_ig3(&a3_gate->a);
        cycle_cbg(beat_a);
       /*------------------------------------------/..         */
}
/*---------------------------------------------------------/..  */
