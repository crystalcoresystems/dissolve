#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define eq(a,b) a[0] = b[0]; a[1] = b[1]
#define xor(b1, b2) ((b1 && !b2) || (!b1 && b2))
#define MAX_SIZE 7
#define A_SIZE 7
#define B_SIZE 3
#define C_SIZE 3
#define D_SIZE 3

/* trinity types */
bool null[2] = {0,0};
bool one[2] = {0,1};
bool theta[2] = {1,0};
bool zero[2] = {1,1};

/* linear or cross generator */
struct mech {
        bool array[MAX_SIZE][2];
        int size;
};

void prop_mech(bool a[MAX_SIZE][2], int size, struct mech *mech);
void prop_mechs(struct mech *, struct mech *, struct mech *,
                struct mech *);
void cycle_mech_static(struct mech *);
void cycle_mech_infinity(struct mech *);
void gate_solve(bool input[2], bool gate[2], bool solve[2]);
void solve(bool input[2], bool mech[2], bool solve[2]);
void mech_connect(struct mech *mech_a, struct mech *mech_b, 
                                                int a, int b);
void print_output(bool out[2], int n);
        
int main()
{
        int i;
        bool buzz_stream[2], buzz_gate[2];
        bool fizz_stream[2], fizz_gate[2];
        bool output[2];
    /* Declare Mechanisms../                              */
        struct mech mech_a;
        struct mech mech_b;
        struct mech mech_c;
        struct mech mech_d;
    /* -----------------------------------------------/.. */
    /* Propogate Mechanisms */
        prop_mechs(&mech_a, &mech_b, &mech_c, &mech_d);
        
        for (i = 1; i <= 100; i++) {
                buzz_stream[0] = 0; buzz_stream[1] = 1;
                fizz_stream[0] = 1; fizz_stream[1] = 0;
    /* Cycle Buzz Gate../                                        */
                cycle_mech_static(&mech_a);
                cycle_mech_infinity(&mech_b);
                cycle_mech_infinity(&mech_c);
                mech_connect(&mech_a, &mech_b,
                             mech_a.size/2, mech_b.size/2);
                mech_connect(&mech_b, &mech_c,
                             mech_b.size/2, mech_c.size/2);
                solve(mech_a.array[(mech_a.size)/2], one, buzz_gate);
    /* ------------------------------------------------------/.. */
    /* Cycle Fizz Gate */
                cycle_mech_infinity(&mech_d);
                eq(fizz_gate, mech_d.array[mech_d.size/2]);
    /* Pass Streams Through Gates */
                gate_solve(buzz_stream, buzz_gate, buzz_stream);
                gate_solve(fizz_stream, fizz_gate, fizz_stream);
    /* get output */
                solve(fizz_stream, buzz_stream, output);
                print_output(output, i);
        }
        printf("\n");
}
/* Propogate Mechaninsm../                                  */
void prop_mech(bool a[MAX_SIZE][2], int size, struct mech *mech)
{
        int i;
        
        mech->size = size;
        for(i = 0; i < size; i++) {
                mech->array[i][0] = a[i][0];
                mech->array[i][1] = a[i][1];
        }
}
/* -----------------------------------------------/..  */
    /* Propogate Mechanisms../                             */
void prop_mechs(struct mech *mech_a, struct mech *mech_b,
                struct mech *mech_c, struct mech *mech_d) 
{
        bool array[MAX_SIZE][2]; 
        eq(array[0], one);
        eq(array[1], zero);
        eq(array[2], zero);
        eq(array[3], theta);
        eq(array[4], one);
        eq(array[5], zero);
        eq(array[6], zero);
        prop_mech(array, A_SIZE, mech_a);
        eq(array[0], zero);
        eq(array[1], one);
        eq(array[2], zero);
        prop_mech(array, B_SIZE, mech_b);
        eq(array[0], one);
        eq(array[1], one);
        eq(array[2], theta);
        prop_mech(array, C_SIZE, mech_c);
        eq(array[0], theta);
        eq(array[1], zero);
        eq(array[2], zero);
        prop_mech(array, D_SIZE, mech_d);
}
    /* ------------------------------------------------/.. */
    /* Static Mechanism Cycle Procedure../                           */
void cycle_mech_static(struct mech *mech)
{
        int i;
        bool solved[MAX_SIZE][2];

        for (i = 1; i < (mech->size) - 1; i++) {
                solve(mech->array[i-1], mech->array[i+1],
                               solved[i]);
        }
        for (i = 1; i < (mech->size) - 1; i++) {
                mech->array[i][0] = solved[i][0]; 
                mech->array[i][1] = solved[i][1];
        }
}
/* ------------------------------------------------/.. */
    /* Infinity Mechanism Cycle Procedure../                     */
void cycle_mech_infinity(struct mech *mech)
{
        int i;
        bool solved[MAX_SIZE][2];

        for (i = 1; i < (mech->size) - 1; i++) {
                solve(mech->array[i-1], 
                      mech->array[i+1], solved[i]);
        }
        solve(mech->array[1],
              mech->array[mech->size - 1], solved[0]);
        solve(mech->array[0],
              mech->array[mech->size - 2], 
              solved[mech->size - 1]);
        for (i = 0; i < mech->size; i++) {
                mech->array[i][0] = solved[i][0]; 
                mech->array[i][1] = solved[i][1];
        }
}
/* ------------------------------------------------/..    */
/* Gate Solve../                                           */
void gate_solve(bool input[2], bool gate[2], bool solve[2])
{
        bool pass[2];

        pass[0] = xor(gate[0], gate[1]);
        pass[1] = pass[0];

        pass[0] = xor(pass[0], 1);
        pass[1] = pass[0];

        solve[0] = input[0] && pass[0];
        solve[1] = input[1] && pass[1];
}
/* -------------------------------------------------/..  */
/* Origninal Solve../                                  */
void solve(bool input[2], bool mech[2], bool solve[2])
{
        bool tmp[2], tmp2[2], tmp3[2];


        tmp[0] = xor(input[0], mech[0]);
        tmp[1] = xor(input[1], mech[1]);

        tmp3[0] = tmp[0] || tmp[1];
        tmp3[1] = tmp3[0];

        tmp2[0] = xor(tmp[0], tmp3[0]);
        tmp2[1] = xor(tmp[1], tmp3[1]);

        tmp2[0] = xor(tmp3[0], tmp2[0]);
        tmp2[1] = xor(tmp3[1], tmp2[1]);

        tmp[0] = !tmp3[0] && input[0];
        tmp[1] = !tmp3[1] && input[1];

        solve[0] = xor(tmp[0], tmp2[0]);
        solve[1] = xor(tmp[1], tmp2[1]);
}
/* -------------------------------------------------/..*/
    /* Mechanism Connector../                           */
void mech_connect(struct mech *mech_a, struct mech *mech_b, 
                                                int a, int b) 
{
        solve(mech_a->array[a],
                mech_b->array[b],
                mech_a->array[a]);
        mech_b->array[b][0] = mech_a->array[a][0];
        mech_b->array[b][1] = mech_a->array[a][1];
}
/* -------------------------------------------------/.. */
/* Print Output../                                      */
void print_output(bool out[2], int n)
{
        if (out[0] == 0 && out[1] == 0) {
                printf("%i\n", n);
                return;
        }
        if (out[0] == 1)
                printf("fizz");
        if (out[1] == 1)
                printf("buzz");
        printf("\n");

}
/*-------------------------------------------------------------/.. */
