#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bitwise.h"

/* Propogate Switch                                   ../          */
void prop_switch(bool key[MAX_KEY][2], int size, struct key_switch *key_switch)
{
        int i;

        key_switch->size = size;
        for (i = 0; i < key_switch->size; i++) {
                key_switch->key[i][0] =  key[i][0];
                key_switch->key[i][1] =  key[i][1];
                key_switch->buffer[i][0] = 0;
                key_switch->buffer[i][1] = 0;
        }
        for (i = key_switch->size; i < MAX_KEY; i++) {
                key_switch->buffer[i][0] = 0;
                key_switch->buffer[i][1] = 0;
        }
}
/* --------------------------------------------------------/..  */
/* Propogate mechaninsm                               ../             */
void prop_mech(bool a[MAX_SIZE][2], int size, struct mech *mech)
{
        int i;
        
        mech->size = size;
        mech->center = size/2;
        for(i = 0; i < size; i++) {
                mech->array[i][0] = a[i][0];
                mech->array[i][1] = a[i][1];
        }
}
/* -----------------------------------------------/..  */
