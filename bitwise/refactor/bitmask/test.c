#include <stdio.h>
#include <stdlib.h>

void printBits(unsigned int num);
unsigned int solve(unsigned int input, unsigned int mech,
                unsigned int mask);

#define THETA 0x2
#define ONE 0x1
#define ZERO 0x3
#define N 0x0

int main()
{
        unsigned int i;
        unsigned int a, b, c, mask;

        a = THETA; b = ZERO; mask = 0x3;

        c = solve(a, b, mask);
        printBits(a);
        printf("\n");
        printBits(b);
        printf("\n");
        printBits(c);
        
        b = ONE;
        c = solve(a, b, mask);
        printBits(a);
        printf("\n");
        printBits(b);
        printf("\n");
        printBits(c);
        
        b = N;
        c = solve(a, b, mask);
        printBits(a);
        printf("\n");
        printBits(b);
        printf("\n");
        printBits(c);
        
}

/* PrintBits../                                 */
void printBits(unsigned int num)
{
        int i;
        unsigned int size = sizeof(unsigned int);
        unsigned int maxPow = 1 << (size*8-1);

        for (i = 0; i < size*8; i++) {
                printf("%u", !!(num&maxPow));
                num = num<<1;
        }
}
/* ---------------------------------------------/.. */
/* Origninal Solve                                  ../        */
unsigned int solve(unsigned int input, unsigned int mech,
                unsigned int mask)
{
        unsigned int tmp, tmp2, tmp3;


        tmp = input ^ mech;

        tmp3 = tmp | tmp << 1;
        tmp2 = tmp | tmp >> 1;
        tmp3 = tmp3 | tmp2;
        tmp2 = tmp ^ tmp3;
        tmp2 = tmp3 ^ tmp2;

        return tmp2 & mask;
}
/* -------------------------------------------------/..*/
