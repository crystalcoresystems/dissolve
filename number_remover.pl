foreach my $arg (@ARGV) {
        open(my $fh, $arg) or warn "Can't open $arg: $!";
        open(my $fo, ">output.c") or warn "Can't open output: $!";

        while ( ! eof($fh) ) {
                defined( $_ = readline $fh )
                        or die "readline failed for $arg: $!";
                $_=~ s/^\s+//;
                $_ =~ s/^[0-9][0-9][0-9][0-9]//;
                print $fo $_;  
        }
}
