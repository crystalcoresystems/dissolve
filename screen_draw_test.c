#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void get_bool(bool b1[2], bool b2[2]);
void move(int n[2], bool b[2][2]);

int main()
{
        char esc = 0x1B;
        int n[2] = {10, 40};
        bool b[2][2];
        for (int k = 1; k < 10; k++) {
                get_bool(b[0], b[1]);
                move(n, b);
                printf("%c[2J", esc);
                printf("%c[1;1H", esc);
                for (int i = 0; i < 20; i++) {
                        for (int j = 0; j < 80; j++) {
                                if (i == 0 || i == 19 ||
                                              j == 0 || j == 79)
                                        printf("*");
                                else if (i == n[0] && j == n[1])
                                        printf("*");
                                else
                                        printf(" ");
                        }
                        printf("\n");
                }
                sleep(1);
        }
}

void get_bool(bool b1[2], bool b2[2])
{
        b1[0] = 1; b1[1] = 1;
        b2[0] = 1; b2[1] = 1; 
}

void move(int n[2], bool b[2][2])
{
        if (b[0][0] == 0 && b[0][1] == 1)
                n[0]--;
        if (b[0][0] == 1 && b[0][1] == 1)
                n[0]++;
        if (b[1][0] == 0 && b[1][1] == 1)
                n[1]--;
        if (b[1][0] == 1 && b[1][1] == 1)
                n[1]++;
}
