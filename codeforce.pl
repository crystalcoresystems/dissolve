#array with odd sum
#
# n ints
#
# choose i and j and set i equal to j
# do move any nummber of times
#
# is it possible to obtain array with odd sum of elements?

for(1..<>) {
        # Get input
        chomp (my $n = <>);
        chomp (my $in = <>);
        @a = split / /, $in;
        # Check if all odd or all even
        my $oddfl = 0;
        my $evenfl = 0;
        foreach(@a){
                if ($_%2 == 1) {
                        # Odd then
                        $oddfl = 1;
                } else {
                        $evenfl = 1;
                }
                $oddfl && $evenfl and last;
        }
        $oddfl && $evenfl and print "YES\n" and next;
        $evenfl and print "NO\n" and next;
        print $n%2 ? "YES\n" : "NO\n";
}


exit;
# Polycarp's Favorite sequence...
#
# 1..n ints
#
# a1 left a2 right etc
#
# pop array -> unshift
# pop array -> push
for (1..<>) {
        chomp(my $n = <>);
        chomp(my $in = <>);
        my @a = split / /, $in;
        my @b = ();
        while(@a) {
                my $x = shift @a;
                push @b, $x;
                if(@a) {
                        my $x = pop @a;
                        push @b, $x;
                }
        }
        print join " ", @b;
        print "\n";
}
