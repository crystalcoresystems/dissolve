// Experiment 1 of dice automata program../ 
// // 
// // idea -
//       test to see if it is possible to recover automata dice automata
//       by checking all positions for initial
//
// hypothesis: It works !
//           

// conclusion: It works !

// notes:

// array goes like this: 
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
//   
// rotation goes like this:
// nothing, right, left, down, up, flip
//   0        1     2     3    4    5

// initial dice starting position
//
//       1
//       2
//      536
//       4
//..
#include <iostream>
#include <stdlib.h>
#define SET 6 
#define DATASIZE 3 

// declarations../

// initialize the dice and the data
void initialize_dice(int dice_array[DATASIZE][6]);
void initialize_data(int dice_array[DATASIZE][DATASIZE]);
void initialize_die(int die[6]);

// viz dice and data
void visualize_dice(int dice_array[DATASIZE][6]);
void visualize_die(int die_array[6]);
void visualize_data(int data_array[DATASIZE][DATASIZE]);

// rotate die
void rotate_right(int die[6]);
void rotate_left(int die[6]);
void rotate_up(int die[6]);
void rotate_down(int die[6]);
void spin(int die[6]);
void flip_vertical(int die[6]);
void flip_horizontal(int die[6]);

// solve: rotate based on instruction
void solve_data_right(int data_array[DATASIZE][DATASIZE]);
void solve_data_left(int data_array[DATASIZE][DATASIZE]);
void solve_data_up(int data_array[DATASIZE][DATASIZE]);
void solve_data_down(int data_array[DATASIZE][DATASIZE]);
void mod(int die[6], int);

// interpolate nstff

// unsolve: rotate possible orientations to find previous instructions
void unsolve_data_right(int data_array[DATASIZE][DATASIZE]);
void unsolve_data_left(int data_array[DATASIZE][DATASIZE]);
void unsolve_data_up(int data_array[DATASIZE][DATASIZE]);
void unsolve_data_down(int data_array[DATASIZE][DATASIZE]);

// get all possible orientations
void possible(int orientations[4][6], int);
void orient(int o[4][6], int sides[4], int, int);

// interpolate backwards and check for success
void left_interpolation_die(int die[6], int data[DATASIZE]);
void left_interpolation_instruction(int die[6], int data[DATASIZE]);

void right_interpolation_die(int die[6], int data[DATASIZE]);
void right_interpolation_instruction(int die[6], int data[DATASIZE]);

int find_move(int die[6], int to);
int opposite(int move);
bool interpolation_success(int correct[6], int test[6]);
//..

signed main() {

        int dice_array[DATASIZE][6];
        int data_array[DATASIZE][DATASIZE];
        srand(7);

        initialize_dice(dice_array);
        initialize_data(data_array);

        visualize_data(data_array);

        solve_data_left(data_array);
        solve_data_left(data_array);
        solve_data_right(data_array);

        solve_data_right(data_array);
        solve_data_left(data_array);
        solve_data_right(data_array);

        visualize_data(data_array);

        unsolve_data_right(data_array);
        unsolve_data_left(data_array);
        unsolve_data_right(data_array);

        unsolve_data_right(data_array);
        unsolve_data_left(data_array);
        unsolve_data_left(data_array);


        visualize_data(data_array);
}


// Solve Data Right../
void solve_data_right(int data_array[DATASIZE][DATASIZE]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int i=0;i<DATASIZE;i++) {
                for(int z=0;z<6;z++)
                        die[z] = dummy[z];

                for(int j=0;j<DATASIZE;j++) {
                        mod(die, data_array[i][j]);
                        data_array[i][j] = die[0];
                }
        }
}
//..
// Solve Data Left../
void solve_data_left(int data_array[DATASIZE][DATASIZE]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int i=0;i<DATASIZE;i++) {
                for(int z=0;z<6;z++)
                        die[z] = dummy[z];

                for(int j=DATASIZE-1;j>=0;j--){
                        mod(die, data_array[i][j]);
                        data_array[i][j] = die[0];
                }
        }
}
//..

// Unsolve Data Right../
void unsolve_data_right(int data_array[DATASIZE][DATASIZE]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6];
        int cor = 0;


        for(int i=0;i<DATASIZE;i++){
                initialize_die(correct);
                possible(orientations, data_array[i][DATASIZE-1]);
                for(int j=0;j<4;j++){
                        for(int k=0;k<6;k++)
                                dummy[k] = orientations[j][k];
                        left_interpolation_die(orientations[j], data_array[i]);
                        if( interpolation_success(correct, orientations[j]) ){
                                left_interpolation_instruction(dummy, data_array[i]);
                                break;
                        }
                }
        }
}
//.. 
// Unsolve Data Left../
void unsolve_data_left(int data_array[DATASIZE][DATASIZE]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6];
        int cor = 0;


        for(int i=0;i<DATASIZE;i++){

                initialize_die(correct);
                possible(orientations, data_array[i][0]);
                for(int j=0;j<4;j++){
                        for(int k=0;k<6;k++)
                                dummy[k] = orientations[j][k];
                        right_interpolation_die(orientations[j], data_array[i]);
                        if( interpolation_success(correct, orientations[j]) ){
                                right_interpolation_instruction(dummy, data_array[i]);
                                break;
                        }
                }
        }
}
//.. 

// Left interpolation of die../
void left_interpolation_die(int die[6], int data[DATASIZE]) {
        int from, to;
        int move;
        int dummy[DATASIZE];

        for(int i=0;i<DATASIZE;i++)
                dummy[i] = data[i];

        for(int i=DATASIZE-1;i>0;i--) {
                to = data[i-1];
                move = find_move(die, to);//you
                mod(die, move);
                dummy[i] = die[0];
        }
        move = find_move(die, 3);
        mod(die, move);
        dummy[0] = die[0];
} 
//..
// Left interpolation of for data../
void left_interpolation_instruction(int die[6], int data[DATASIZE]) {
        int from, to;
        int move;


        for(int i=DATASIZE-1;i>0;i--) {
                to = data[i-1];
                move = find_move(die, to);//you
                mod(die, move);
                data[i] = opposite(move);
        }
        move = find_move(die, 3);//you
        data[0] = opposite(move);
} 
//..

// Right interpolation of die../
void right_interpolation_die(int die[6], int data[DATASIZE]) {
        int from, to;
        int move;
        int dummy[DATASIZE];

        for(int i=0;i<DATASIZE;i++)
                dummy[i] = data[i];

        for(int i=0;i<DATASIZE-1;i++) {
                to = data[i+1];
                move = find_move(die, to);//you
                mod(die, move);
                dummy[i] = die[0];
        }
        move = find_move(die, 3);
        mod(die, move);
        dummy[DATASIZE-1] = die[0];
}
//..
// Right interpolation of for data../
void right_interpolation_instruction(int die[6], int data[DATASIZE]) {
        int from, to;
        int move;

        for(int i=0;i<DATASIZE-1;i++) {
                to = data[i+1];
                move = find_move(die, to);//you
                mod(die, move);
                data[i] = opposite(move);
        }
        move = find_move(die, 3);//you
        data[DATASIZE-1] = opposite(move);
}
//..


// if previous was x then move must have been y for orientation z
//
// just brute force the dice then get the direction and flip...
//              if there was a horizontal or vertical flip information loss...
//              therefore no horizontal flip

// Find Move../
int find_move(int die[6], int to) {
        int location;

        for(int i=0;i<6;i++)
                if(die[i] == to) {
                        location = i;
                        break;
                }

        return location;
}
//..
// Find Move Backwards../
// array goes like this: 
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
// rotation goes like this:
// nothing, right, left, down, up, flip
//   0        1     2     3    4    6
int opposite(int move) {
        switch (move) {
                case 0:
                        return 6;
                case 1:
                        return 2;
                case 2:
                        return 1;
                case 3:
                        return 4;
                case 4:
                        return 3;
        }
        return move;
}
//..

//..
// Interpolation Success../
bool interpolation_success(int correct[6], int test[6]) {
        for(int i=0;i<6;i++)
                if(correct[i] != test[i])
                        return false;
        return true;
}
//..

//..
// Possible../
//                  clockwise
//   1 opp 3 -> (3 top) - 2 6 4 5
//   2 opp 4 -> (4 top) - 3 6 1 5
//   5 opp 6 -> (5 top) - 2 3 4 1
//
void possible(int orientations[4][6], int top){
        int three[4] = {2, 6, 4, 5}; 
        int one[4]   = {5, 4, 6, 2}; 

        int four[4]  = {3, 6, 1, 5}; 
        int two[4]   = {5, 1, 6, 3}; 

        int five[4]  = {2, 3, 4, 1}; 
        int six[4]   = {1, 4, 3, 2}; 

        switch (top) {
                case 1:
                        orient(orientations, one, 1, 3);
                        break;
                case 2:
                        orient(orientations, two, 2, 4);
                        break;
                case 3:
                        orient(orientations, three, 3, 1);
                        break;
                case 4:
                        orient(orientations, four, 4, 2);
                        break;
                case 5:
                        orient(orientations, five, 5, 6);
                        break;
                case 6:
                        orient(orientations, six, 6, 5);
                        break;
        }
}
//..
// orient../
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void orient(int o[4][6], int sides[4], int top, int bot) {
        for(int i=0; i<4; i++){
                o[i][0] = top;
                o[i][5] = bot;

                for(int j=1;j<5;j++){
                        o[i][1] = sides[(0+i)%4]; // dumb hack cause yyeah
                        o[i][2] = sides[(2+i)%4];
                        o[i][3] = sides[(1+i)%4];
                        o[i][4] = sides[(3+i)%4];
                }
        }
}
//..        
//.. 

// mod../
// rotate die based on instruction
void mod(int die[6], int instruction) {

        switch (instruction) {
                case 1:
                        rotate_right(die);
                        break;
                case 2:
                        rotate_left(die);
                        break;
                case 3:
                        rotate_down(die);
                        break;
                case 4:
                        rotate_up(die);
                        break;
                case 5:
                        flip_horizontal(die);
                        break;
        }
}
//..

// rotate dice../
// spin die../
//
// initial dice starting position
//
//       1
//       2
//      536
//       4
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
//


void spin(int die[6]) {
        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

       die[1] = dummy[3]; // left  = up
       die[2] = dummy[4]; // right = back
       die[3] = dummy[2]; // up    = right
       die[4] = dummy[1]; // back  = left
}
//..
//  right../
//      1   6
//      2   2
//     536 153
//      4   4
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_right(int die[6]) {

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[1]; // top    = left
        die[1] = dummy[5]; // left   = bottom
        die[2] = dummy[0]; // right  = top
        die[5] = dummy[2]; // bottom = right

}
//..
//  left../
//      1   5
//      2   2
//     536 361
//      4   4
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_left(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[2]; // top    = right
        die[1] = dummy[0]; // left   = top
        die[2] = dummy[5]; // right  = bottom
        die[5] = dummy[1]; // bottom = left

}
//..
//  up../
//      1   2
//      2   3
//     536 546
//      4   1
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_up(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[4]; // top    =  back
        die[3] = dummy[0]; // up     =  top
        die[4] = dummy[5]; // back   =  bottom
        die[5] = dummy[3]; // bottom =  up

}
//..
//  down../
//      1   4
//      2   1
//     536 526
//      4   3
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_down(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[3]; // top    = up
        die[3] = dummy[5]; // up     = bottom
        die[4] = dummy[0]; // back   = top
        die[5] = dummy[4]; // bottom = back

}
//..
//  flip vertical../
//
//      1   3
//      2   4
//     536 516
//      4   2
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5

void flip_vertical(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[5]; // top    = bottom
        die[3] = dummy[4]; // up     = back
        die[4] = dummy[3]; // back   = up
        die[5] = dummy[0]; // bottom = top

}
//..
//  flip horizontal../
//
//      1   3
//      2   2
//     536 615
//      4   4
//
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5

void flip_horizontal(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[5]; // top    = bottom
        die[1] = dummy[2]; // left   = right
        die[2] = dummy[1]; // right  = left
        die[5] = dummy[0]; // bottom = top

}
//..
//..

// Visualize../ 
// Dice../
void visualize_dice(int dice_array[DATASIZE][6]) {
        for(int i=0;i<DATASIZE;i++) {
                visualize_die(dice_array[i]);
        }
}

void visualize_die(int dice_array[6]) {
        cout << "  " << dice_array[5] << endl;
        cout << "  " << dice_array[3] << endl;
        cout << " " << dice_array[1] << dice_array[0] << dice_array[2] << endl;
        cout << "  " << dice_array[4];
        cout << endl << endl;
}
//..
// Data../
void visualize_data(int data_array[DATASIZE][DATASIZE]) {
        for(int i=0;i<DATASIZE;i++) {
                for(int j=0;j<DATASIZE;j++)
                        cout << data_array[i][j];
                cout << endl;
        }
        cout << endl;
}
//..
//..
//..

// Initialize../
// Data../
// just a random 2d array
void initialize_data(int data_array[DATASIZE][DATASIZE]) {
        for(int i=0;i<DATASIZE;i++)
                for(int j=0;j<DATASIZE;j++)
                        data_array[i][j] = rand()%6 + 1;
}
//..
//  Dice../
void initialize_dice(int dice_array[DATASIZE][6]) {

        for(int i=0;i<DATASIZE;i++) {
                dice_array[i][0] = 3;
                dice_array[i][1] = 5;
                dice_array[i][2] = 6;
                dice_array[i][3] = 2;
                dice_array[i][4] = 4;
                dice_array[i][5] = 1;
        }

}
//..
//  Die../
// initial dice starting position
//
//       1
//       2 
//      536
//       4
//
void initialize_die(int die[6]) {

        die[0] = 3;
        die[1] = 5;
        die[2] = 6;
        die[3] = 2;
        die[4] = 4;
        die[5] = 1;

}
//..
//..
