#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint8_t solve(uint8_t linker, uint8_t ofs_a, uint8_t linked, uint8_t ofs_b);
uint8_t boolean_solve(uint8_t input, uint8_t mech);
