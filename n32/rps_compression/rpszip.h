#include <stdio.h>

enum {NORTH, WEST, SOUTH, EAST};
enum {CREATE, ROTATE, MOVE};

struct rps_node {
        int x, y;
        int type;
        struct rps_node *north,
                        *south,
                        *east,
                        *west,
                        *link;
};

struct ins_node {
        int type;
        int value;
        struct ins_node *link;
};


void print_ll(struct rps_node *node);
struct rps_node * make_origin(int type);

struct rps_node * create_node(struct rps_node *, struct rps_node *, int, int);

struct rps_node * find_last(struct rps_node * rps_node, int direction);
struct rps_node * add_node(struct rps_node *creation_point, struct rps_node *current,
                                                                 int direction, int type);
struct ins_node * get_instructions();
struct ins_node * instruction_switch(int, struct ins_node *, FILE *);
struct ins_node * add_instruction(struct ins_node *ins_node, int type, int value);
struct ins_node * popins(struct ins_node * ins_node);


int rotate_right(int direction);
struct rps_node * move_node(struct rps_node * selected, int direction);


void iterate(struct rps_node * current);
void one_adjacent(struct rps_node * current);
struct rps_node * farthest_node(struct rps_node * current, int direction);
int node_direction(struct rps_node * current);
void directional_solve(struct rps_node *current, struct rps_node * node2, int nd);
void two_sym_adjacent(struct rps_node * current);
void two_asym_adjacent(struct rps_node * current);
void three_adjacent(struct rps_node * current);
int null_direction(struct rps_node * current);
int node_direction2(struct rps_node * current, int prev);
void four_adjacent(struct rps_node * current);
int get_case(struct rps_node * current);
int calculate_case(int adjacents);
