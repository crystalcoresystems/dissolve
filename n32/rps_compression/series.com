$!created on 22-AUG-2021
$!----------------------
$!
$! filename: SERIES.COM
$!    input: instructions
$!   output: set of instructions
$!     idea: run the instruction set until desired pattern is matched
$!     form: DCL loop
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    ser__status    = %x11000000
$    ser__success   = ser__status + %x001
$    ser__ctrly     = ser__status + %x00c
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$!
$ iter = -1
$ 10: iter = 1 + iter
$       @patt_match
$       purge instructions.txt
$       if iter .eq. 10 then goto 19
$       goto 10
$ 19:
