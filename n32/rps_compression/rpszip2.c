#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "helper.h"
#include "rpszip.h"

/*
 * instruction key
 * 0 = create
 * 1 = turn right
 * 8 = move up
 */

void print_structure(struct rps_node *node);


/* read instruction from file
 * choose operation based on instruction
 * create node
 * or turn right
 * or move
 */
int main()
{
        
        struct rps_node *current;
        struct rps_node *selected;
        struct ins_node *instructions;
        int direction;

        instructions = get_instructions();
        current = make_origin(instructions->value);
        selected = current;

        instructions = popins(instructions);
        direction = NORTH;
        while (instructions)
        {
                switch (instructions->type)
                {
                        case CREATE: 
                                current = create_node(current, selected, direction,
                                                instructions->value);
                                break;
                        case ROTATE:
                                direction = rotate_right(direction);
                                break;
                        case MOVE:
                                selected = move_node(selected, direction);
                                break;
                }
                instructions = popins(instructions);
        }
//        print_ll(current);
        for (int i = 0; i < 64; i++)
        {
                printf("%i", selected->type);
                iterate(current);
        }
        print_structure(current);
}

/* Iterate../ */
void iterate(struct rps_node * current)
        /* 
         * Cases for iteration if cells:
         *
         * case 1 -> 1 adjacent - Solve with adjacent and furthest in opposite direction
         * case 2 -> 2 symmetric adjacent - Solve adjacents
         * case 3 -> 2 asymmetric adjacent - Solve ( Solve adjacents and furthest in opp. dir.)
         * case 4 -> 3 adjacent - Solve ( Solve adjacent symmetric and asymmetric with furthest)
         * case 5 -> 4 adjacent - Solve ( sovle adjacent symmetrics )
         */
{

        struct rps_node *node;
        uint8_t original;
        uint8_t list[5000];
        int i;

        node = current;
        i = 0;
        while (current)
        {
                original = current->type;
                switch ( get_case(current) )
                {
                        case 1:
                                one_adjacent(current);
                                break;
                        case 2:
                                two_sym_adjacent(current);
                                break;
                        case 3:
                                two_asym_adjacent(current);
                                break;
                        case 4:
                                three_adjacent(current);
                                break;
                        case 5:
                                four_adjacent(current);
                                break;
                        default:
                }

                list[i++] = current->type;
                current->type = original;
                current = current->link;
        }

        i = 0;
        while (node)
        {
                node->type = list[i++];
                node = node->link;
        }
}
/* -----------------------------------------------/.. */

/* One Adjacent../ */
void one_adjacent(struct rps_node * current)
        /*
         * case 1 -> 1 adjacent - Solve with adjacent and furthest in opposite direction
         */
{
        int nd;
        struct rps_node * node2;

        // get node direction and value
        nd = node_direction(current);

        // get value of furthest node in opposite direction
        node2 = farthest_node(current, nd);
        // solve
        directional_solve(current, node2, nd);
}
/* -----------------------------------------------/.. */

/* Directional Solve../ */
void directional_solve(struct rps_node *current, struct rps_node * node2, int nd)
{
        switch (nd)
        {
                case SOUTH:
                        current->type = boolean_solve(current->south->type, node2->type);
                        return;
                case NORTH:
                        current->type = boolean_solve(current->north->type, node2->type);
                        return;
                case EAST:
                        current->type = boolean_solve(current->east->type, node2->type);
                        return;
                case WEST:
                        current->type = boolean_solve(current->west->type, node2->type);
                        return;
        }
}
/* -----------------------------------------------/.. */

/* Node Direction../ */
int node_direction(struct rps_node * current)
{
        if (current->west != NULL)
               return WEST; 
        if (current->east != NULL)
               return EAST; 
        if (current->north != NULL)
               return NORTH; 
        if (current->south != NULL)
               return SOUTH; 
}
/* -------------------------------/.. */

/* Farthest Node../ */
struct rps_node * farthest_node(struct rps_node * rps_node, int direction)
{
        struct rps_node * trav_node;

        while (rps_node)
        {
                trav_node = rps_node;
                switch (direction)
                {
                case NORTH:
                        rps_node = rps_node->north;
                        break;
                case SOUTH:
                        rps_node = rps_node->south;
                        break;
                case EAST:
                        rps_node = rps_node->east;
                        break;
                case WEST:
                        rps_node = rps_node->west;
                        break;
                }
        }
        return trav_node;
}
/* -----------------------------------------------/.. */

/* Two Symmetrical Adjacents../ */
void two_sym_adjacent(struct rps_node * current)
        /*
         * case 2 -> 2 symmetric adjacent - Solve adjacents
         */
{
        // get values of symmetric nodes and solve
        if (current->west == NULL)
                current->type = boolean_solve(current->north->type, current->south->type);
        else
                current->type = boolean_solve(current->east->type, current->west->type);
}
/* -----------------------------------------------/.. */

/* Two Asymmetrical Adjacents../ */
void two_asym_adjacent(struct rps_node * current)
        /*
         * case 3 -> 2 asymmetric adjacent - Solve ( Solve adjacents and furthest in opp. dir.)
         */
{
        int nd;
        struct rps_node * node2;
        uint8_t sol1;

        // get node direction and value
        nd = node_direction(current);

        // get value of furthest node in opposite direction
        node2 = farthest_node(current, nd);
        // solve
        directional_solve(current, node2, nd);
        sol1 = current->type;

        // repeat for other values
        nd = node_direction2(current, nd);
        node2 = farthest_node(current, nd);
        directional_solve(current, node2, nd);

        //solve solutions together
        current->type = boolean_solve(current->type, sol1);
}
/* -----------------------------------------------/.. */

/* Node Direction2../ */
int node_direction2(struct rps_node * current, int prev)
{
        if (current->west != NULL && prev != WEST)
               return WEST; 
        if (current->east != NULL && prev != EAST)
               return EAST; 
        if (current->north != NULL && prev != NORTH)
               return NORTH; 
        if (current->south != NULL && prev != SOUTH)
               return SOUTH; 
}
/* -------------------------------/.. */

/* Three Adjacents../ */
void three_adjacent(struct rps_node * current)
        /*
         * case 4 -> 3 adjacent - Solve ( Solve adjacent symmetric and asymmetric with furthest)
         */
{
        int nd, sol1;
        struct rps_node * node2;

        nd = null_direction(current);
        node2 = farthest_node(current, nd);
        // solve
        directional_solve(current, node2, nd);

        switch(nd)
        {
                case NORTH: case SOUTH:
                        sol1 = boolean_solve(current->east->type, current->west->type);
                        current->type = boolean_solve(current->type, sol1);
                        return;
                case EAST: case WEST:
                        sol1 = boolean_solve(current->north->type, current->south->type);
                        current->type = boolean_solve(current->type, sol1);
                        return;
        }

}
/* -----------------------------------------------/.. */

/* Null Direction../ */
int null_direction(struct rps_node * current)
{
        if (current->west == NULL)
               return EAST; 
        if (current->east == NULL)
               return WEST; 
        if (current->north == NULL)
               return SOUTH; 
        if (current->south == NULL)
               return NORTH; 
}
/* -------------------------------/.. */

/* Four Adjacents../ */
void four_adjacent(struct rps_node * current)
        /*
         * case 5 -> 4 adjacent - Solve ( sovle adjacent symmetrics )
         */
{
        // get values of symmetric nodes and solve
        // get values of other symmetric nodes and solve
        // solve both solutions
        uint8_t sol1;
        uint8_t sol2;
        sol1 = boolean_solve(current->east->type, current->west->type);
        sol2 = boolean_solve(current->north->type, current->south->type);
        current->type = boolean_solve(sol1, sol2);
}
/* -----------------------------------------------/.. */

/* Get Case../ */
int get_case(struct rps_node * current)
        /* 
         * Cases for iteration if cells:
         *
         * case 1 -> 1 adjacent - Solve with adjacent and furthest in opposite direction
         * case 2 -> 2 symmetric adjacent - Solve adjacents
         * case 3 -> 2 asymmetric adjacent - Solve ( Solve adjacents and furthest in opp. dir.)
         * case 4 -> 3 adjacent - Solve ( Solve adjacent symmetric and asymmetric with furthest)
         * case 5 -> 4 adjacent - Solve ( sovle adjacent symmetrics )
         */
{
        // calculate adjacents
        // if 2 adjacents calculate type
        int adjacents;

        adjacents = 4;
        if (current->west != NULL)
                adjacents--;
        if (current->north != NULL)
                adjacents--;
        if (current->south != NULL)
                adjacents--;
        if (current->east != NULL)
                adjacents--;

        adjacents = 4 - adjacents;


        if (adjacents == 0)
        {
                printf("corrupted data\n\n");
                exit(1);
        }

        if (adjacents != 2)
                return calculate_case(adjacents);

        if (current->west == NULL && current->east == NULL)
                return 2;
        if (current->north == NULL && current->south == NULL)
                return 2;
        return 3;

}
/* -----------------------------------------------/.. */

/* Calculate Case../ */
int calculate_case(int adjacents)
{
        switch(adjacents)
        {
                case 1:
                        return 1;
                case 3:
                        return 4;
                case 4:
                        return 5;
        }
}
/* -----------------------------------------------/.. */

/* Move Node../ */
// can movement loop?
// right now no...
struct rps_node * move_node(struct rps_node * selected, int direction)
{
        switch (direction)
        {
                case NORTH:
                        if (selected->north)
                                return selected->north;
                case EAST:
                        if (selected->east)
                                return selected->east;
                case SOUTH:
                        if (selected->south)
                                return selected->south;
                case WEST:
                        if (selected->west)
                                return selected->west;
        }
        return selected;
}
/* -------------------------------------------------------/.. */

/* Rotate Right../ */
int rotate_right(int direction)
{
        return (direction + 1) % 4;
}
/* -----------------------------------/.. */

/* Get Instructions../ */
// This is the next step... how to right and read instructions
//
// instructions should be read from file
//
// right now instructions can be a list of characters 018..
//
struct ins_node * get_instructions()
{
        struct ins_node * instructions;
        FILE * fp;
        int c;

        fp = fopen("instructions.txt", "r");
        instructions = add_instruction(NULL, CREATE, 3);

        while (1)
        {
                c = fgetc(fp);
                if (c == EOF || c == '\n')
                        break;
                instructions = instruction_switch(c, instructions, fp);
        }

        instructions = add_instruction(instructions, CREATE, 3);

        fclose(fp);

        return instructions;
}
/* ------------------------------------------------------------/.. */

/* Instruction switch../ */
struct ins_node * instruction_switch(int c, struct ins_node * instructions, FILE * fp)
{
        struct ins_node * new;
        switch (c)
        {
                case '3':
                        c = fgetc(fp);
                        switch (c)
                        {
                                case '3':
                                        return add_instruction(instructions, CREATE, 3);
                                case '1':
                                        return add_instruction(instructions, CREATE, 1);
                                case '2':
                                        return add_instruction(instructions, CREATE, 2);
                                default:
                                        return add_instruction(instructions, CREATE, 3);
                        }
                        printf("corrupted data: instructions\n");
                        exit(1);
                case'1':
                        return add_instruction(instructions, ROTATE, 0);
                case'2':
                        return add_instruction(instructions, MOVE, 0);
                default:
                        return instructions;
        }
}
/* -----------------------------------------------------------/.. */

/* Add Instruction../ */
struct ins_node * add_instruction(struct ins_node *ins_node, int type, int value)
{

        struct ins_node * node = calloc(1, sizeof(struct ins_node));

        node->type = type;
        node->value = value;
        node->link = ins_node;

        return node;
}
/* ----------------------------------/.. */

/* Pop Instruction Node../ */
struct ins_node * popins(struct ins_node * ins_node)
{
        struct ins_node * node;
        node = ins_node->link;
        free(ins_node);
        return node;
}        
/* -----------------------------------------/.. */  

/* Make Origin../ */
struct rps_node * make_origin(int type)
{
        struct rps_node *origin = calloc(1, sizeof(struct rps_node));
        origin->type = type;
        origin->x = 0;
        origin->y = 0;
        origin->link = NULL;
        origin->north = NULL;
        origin->south = NULL;
        origin->east = NULL;
        origin->west = NULL;
        return origin;
}
/* -----------------------------------------------/.. */

/* Print Linked List../ */
void print_ll(struct rps_node *node)
{
        while(node)
        {
                printf("%i\n", node->type);
                node = node->link;
        }
}
/*-----------------------------/.. */

/* Print Structure../ */
void print_structure(struct rps_node *node)
{
        char array[218][218];
        int i,j;
        int x,y;

        for (i = 0; i < 218; i++)
                for (j = 0; j < 218; j++)
                        array[i][j] = ' ';

        printf("setting array values\n");
        while(node)
        {
                x = node->x + 109;
                y = node->y + 109;
                if (node->x < -109)
                        x = 0;
                if (node->x > 108)
                        x = 217;
                if (node->y < -109)
                        y = 0;
                if (node->y > 108) 
                        y = 217;
                array[x][y] = node->type + 48;
                node = node->link;
        }

        for (i = 0; i < 218; i++)
        {
                for (j = 0; j < 218; j++)
                        putc(array[i][j], stdout);
                printf("\n");
        }
}
/*-----------------------------/.. */

/* Create Node../ */
struct rps_node * create_node(struct rps_node *current, struct rps_node *selected, int direction, int type)
{
        struct rps_node * creation_point;

        creation_point = find_last(selected, direction);

        return add_node(creation_point, current, direction, type);
}
/* -----------------------------------------/.. */

/* Find last../ */
struct rps_node * find_last(struct rps_node * rps_node, int direction)
{
        struct rps_node * trav_node;
        while (rps_node)
        {
                trav_node = rps_node;
                switch (direction)
                {
                case NORTH:
                        rps_node = rps_node->north;
                        break;
                case SOUTH:
                        rps_node = rps_node->south;
                        break;
                case EAST:
                        rps_node = rps_node->east;
                        break;
                case WEST:
                        rps_node = rps_node->west;
                        break;
                }
        }
        return trav_node;
}
/* -----------------------------------------------/.. */

/* Add Node../ */
struct rps_node * add_node(struct rps_node *creation_point, struct rps_node *current,
                                                                 int direction, int type)
{
        struct rps_node *node = calloc(1, sizeof(struct rps_node));
        node->type = type;
        node->link = current;
        node->north = NULL;
        node->south = NULL;
        node->east = NULL;
        node->west = NULL;
        switch(direction)
        {
                case NORTH:
                        creation_point->north = node;
                        node->x = creation_point->x;
                        node->y = creation_point->y+1;
                        break;
                case SOUTH:
                        creation_point->south = node;
                        node->x = creation_point->x;
                        node->y = creation_point->y-1;
                        break;
                case EAST:
                        creation_point->east = node;
                        node->x = creation_point->x+1;
                        node->y = creation_point->y;
                        break;
                case WEST:
                        creation_point->west = node;
                        node->x = creation_point->x-1;
                        node->y = creation_point->y;
                        break;
        }
        while (current)
        {
                if (current->x == node->x && current->y == node->y - 1)
                        node->south = current;
                if (current->x == node->x && current->y == node->y + 1)
                        node->north = current;
                if (current->x == node->x + 1 && current->y == node->y)
                        node->east = current;
                if (current->x == node->x - 1 && current->y == node->y)
                        node->west = current;
                current = current->link;
        }
        return node;
}
/* -------------------------------------------/.. */
