$!created on 19-AUG-2021
$!----------------------
$!
$! filename: PATT_MATCH.COM
$!    input: none
$!   output: intsructions to create a structure with desired pattern
$!     idea: run rpszip until pattern is found
$!     form: output -> input generator for c program
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    ptm__status    = %x11000000
$    ptm__success   = ptm__status + %x001
$    ptm__ctrly     = ptm__status + %x00c
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$ define/user sys$output instructions.txt
$ run rpszip
