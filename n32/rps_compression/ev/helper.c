/* Header../-------------------------------------------------------------*/
#include "helper.h"
#include <stdlib.h>
#include <ssdef.h>
#include <stsdef.h>
#include <libwaitdef.h>
#include <lib$routines.h>
#include <unistd.h>



/*--------------------------------------------------------------------/..*/

/* Solve../--------------------------------------------------------------*/
uint8_t solve(uint8_t linker, uint8_t ofs_a, uint8_t linked, uint8_t ofs_b)
{
        uint8_t a, b, sol, ret;

        a = (linker >> (ofs_a * 2)) & 3;
        b = (linked >> (ofs_b * 2)) & 3;
        
        sol = boolean_solve(a, b);
        sol = sol << (ofs_b*2);
        linked = linked & ~(3 << (ofs_b*2));
        ret = sol | linked;

        return ret;
}/*-------------------------------------------------------------------/..*/

/* Boolean Solve Operations../---------------------------------------------*/
uint8_t boolean_solve(uint8_t input, uint8_t mech)
{
        uint8_t tmp, tmp2, tmp3;

        tmp = input ^ mech;
        tmp3 = (tmp >> 1) | (tmp << 1) | tmp;
        tmp2 = tmp ^ tmp3;
        tmp2 = tmp2 ^ tmp3;
        tmp = ~tmp3 & input;
        return (tmp ^ tmp2) & 3;
}
/* --------------------------------------------------------------------/..*/
