from scrapy import Spider
from scrapy import Request
from scrapy.loader import ItemLoader
from quotes_spider.items import QuotesSpiderItem

class QuotesSpider(Spider):
    name = 'quotes'
    allowed_domains = ['quotes.toscrape.com']
    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        l = ItemLoader(item=QuotesSpiderItem(), response=response)
        quotes = response.xpath('//*[@class="quote"]')
        for quote in quotes:
            text = quote.xpath('.//*[@class="text"]/text()').extract_first()
            author = quote.xpath('.//*[@class="author"]/text()').extract_first()
            tags = quote.xpath('.//*[@class="keywords"]/@content').extract_first()
            l.add_value('Text', text)
            l.add_value('Author', author)
            l.add_value('Tags', tags)
        next_page_url = response.xpath('//*[@class="next"]/a/@href').extract_first()
        absolute_next = response.urljoin(next_page_url)
        return l.load_item()
        #yield Request(absolute_next)
