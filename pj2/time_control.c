#include <stdio.h>
#include "time_control.h"
#include "utility.h"

// Function Prototypes
void speed_control();
void pause_resume();
void reverse_playback();
void stepwise_control();
void keyframe_jump();

// Input Feature Function Pointers
void (*time_control_feature_functions[])() = {
    speed_control,
    pause_resume,
    reverse_playback,
    stepwise_control,
    keyframe_jump
};

void time_control() {
    const char* input_features[] = {
        "Speed Control",
        "Pause/Resume",
        "Reverse Playback",
        "Stepwise Control",
        "Keyframe Jump"
    };

    while (1) {
        display_menu(
            "Time Control: Modify or control the algorithm playback during runtime.",
            input_features, 5
        );
        printf("\nEnter 0 to return to the input subsystem.\n");
        int choice = get_valid_input(0, 5);

        if (choice == 0) {
            printf("\nReturning to the input subsystem.\n");
            break;
        }

        time_control_feature_functions[choice - 1]();
    }
}

// Feature Implementations
void speed_control() {
    printf("\n1. Speed Control: Users can speed up or slowdown the playback speed of the algorithm.\n");
}

void pause_resume() {
    printf("\n2. Pause/Resume: Users can stop and start playback.\n");
}

void reverse_playback() {
    printf("\n3. Reverse Playback: Users can reverse the playback direction.\n");
}

void stepwise_control() {
    printf("\n4. Stepwise Control: Allows single frame stepping forwards and backwards.\n");
}

void keyframe_jump() {
    printf("\n5. Keyframe Jump: Allows the user to jump to certain keyframes in the algorithm.\n");
}
