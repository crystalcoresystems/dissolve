#ifndef INPUT_SUBSYSTEM_H
#define INPUT_SUBSYSTEM_H

void input_subsystem();

// Input Feature Function Pointers
extern void (*input_feature_functions[])();

#endif // INPUT_SUBSYSTEM_H

