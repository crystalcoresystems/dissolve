#include <stdio.h>
#include "input_subsystem.h"
#include "time_control.h"
#include "utility.h"

// Function Prototypes
void parameter_adjustment();
void render_options();
void input_methods();

// Input Feature Function Pointers
void (*input_feature_functions[])() = {
    parameter_adjustment,
    time_control,
    render_options,
    input_methods
};

void input_subsystem() {
    const char* input_features[] = {
        "Parameter Adjustment",
        "Time Control",
        "Render Options",
        "Input Methods"
    };

    while (1) {
        display_menu(
            "Input Subsystem: Capture user input to modify or control the algorithm during runtime.",
            input_features, 4
        );
        printf("\nEnter 0 to return to the main menu.\n");
        int choice = get_valid_input(0, 4);

        if (choice == 0) {
            printf("\nReturning to the main menu.\n");
            break;
        }

        input_feature_functions[choice - 1]();
    }
}

// Feature Implementations
void parameter_adjustment() {
    printf("\n1. Parameter Adjustment: Users should be able to adjust algorithm parameters during runtime.\n");
}

void render_options() {
    printf("\n3. Render Options: Users can choose between different options of how the algorithm is rendered to the screen.\n");
}

void input_methods() {
    printf("\n4. Input Methods: Support various input methods, including keyboard and mouse.\n");
}

