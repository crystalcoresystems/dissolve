#include <stdio.h>
#include "input_subsystem.h"
#include "utility.h"

// Feature Function Pointers
void (*feature_functions[])() = {
    input_subsystem
};

int main() {
    const char* main_features[] = {
        "Input Subsystem"
    };

    printf("\x1B[2J\x1B[3H");
    while (1) {
        display_menu(
            "Problem Definition: Develop an interactive algorithm visualizer.\n"
            "The visualizer should allow users to explore the inner workings of different algorithms by visualizing their state in real-time.",
            main_features, 1
        );
        printf("\nEnter 0 to exit.\n");
        int choice = get_valid_input(0, 1);

        if (choice == 0) {
            printf("\nExiting the program.\n");
            break;
        }

        feature_functions[choice - 1]();
    }

    return 0;
}

