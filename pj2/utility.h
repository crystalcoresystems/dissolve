#ifndef UTILITY_H
#define UTILITY_H

int get_valid_input(int min, int max);
void display_menu(const char* title, const char* options[], int num_options);

#endif // UTILITY_H

