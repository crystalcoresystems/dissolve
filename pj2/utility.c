#include <stdio.h>
#include "utility.h"

int get_valid_input(int min, int max) {
    int choice;
    while (1) {
        printf("Please enter a number (%d-%d): ", min, max);
        if (scanf("%d", &choice) != 1) {
            while (getchar() != '\n'); // Clear invalid input
            printf("Invalid input. Please enter an integer.\n");
            continue;
        }
        if (choice >= min && choice <= max) {
            printf("\x1B[2J\x1B[3H");
            return choice;
        }
        printf("Invalid choice. Try again.\n");
    }
}

void display_menu(const char* title, const char* options[], int num_options) {
    printf("\n%s\n", title);
    for (int i = 0; i < num_options; i++) {
        printf("%d. %s\n", i + 1, options[i]);
    }
}

