$!
$! filename: draw_cards.com
$!   output: 5 playing cards
$!
$!     idea: a random number call grabs 5 cards from a deck
$!     form: 
$!          a card assignment loop using rand to create a file
$!           sort the file and grab the first 5 cards into buffer
$!            then print
$!      _________________________ 
$! --- | Written by |K0|13|13|S| | ---
$!      =========================
$!
$ count = 1
$! tmp_deck = "''f$unique()'.dat"
$! open/write dek 'tmp_deck'
$!
$ cards_tmp = "''f$unique()'.dat"
$ open/write cards 'cards_tmp'
$main_loop:
$ tmp = "''f$unique()'.dat"
$ open/write temp 'tmp'
$ define/user sys$output temp
$ rand 1
$ close temp
$ open/read temp 'tmp'
$ read temp value
$ close/disp=delete temp
$!
$ write cards f$string(value) + " " + f$string(count)
$ count = count + 1
$ if( count .gt. 52 )
$ then
$       goto exit_loop
$ else
$       goto main_loop
$ endif
$exit_loop:
$ close cards
$! sort cards
$ tmp = "''f$unique()'.dat"
$ sort 'cards_tmp' 'tmp'
$ rename 'tmp' 'cards_tmp'
$!
$ count = 1
$ buff1 = ""
$ buff2 = ""
$ buff3 = ""
$ buff4 = ""
$ buff5 = ""
$ buff6 = ""
$ buff7 = ""
$ open/read cards 'cards_tmp'
$print_loop:
$ read cards data
$ cardnum = f$integer( -
        f$extract( f$locate(" ", data), f$locate(".", data), data )  )
$ mod_cardnum = cardnum - (cardnum / 13 ) * 13
$ gosub assign_mod_cardnum
$!
$ buff1 = buff1 + ".-------."
$ if( mod_cardnum .ne. 10 )
$ then
$       buff2 = buff2 + "|" + mod_cardnum + "      |"
$ else
$       buff2 = buff2 + "|" + mod_cardnum + "     |"
$ endif
$ gosub print_suit
$ if( mod_cardnum .ne. 10 )
$ then
$       buff4 = buff4 + "|      " + mod_cardnum + "|"
$ else
$       buff4 = buff4 + "|     " + mod_cardnum + "|"
$ endif
$ buff5 = buff5 + "'-------'"
$ count = count + 1
$ if(count .gt. 5)
$ then
$       goto exit_print
$ else
$       gosub pad_buff
$       goto print_loop
$ endif
$exit_print:
$ close cards
$ gosub print_buff
$ delete 'cards_tmp';*
$ exit
$!
$print_buff:
$ write sys$output buff1
$ write sys$output buff2
$ write sys$output buff6
$ write sys$output buff3
$ write sys$output buff7
$ write sys$output buff4
$ write sys$output buff5
$ return
$!
$pad_buff:
$ buff1 = buff1 + "    "
$ buff2 = buff2 + "    "
$ buff3 = buff3 + "    "
$ buff4 = buff4 + "    "
$ buff5 = buff5 + "    "
$ buff6 = buff6 + "    "
$ buff7 = buff7 + "    "
$ return
$!
$assign_mod_cardnum:
$ if( (mod_cardnum .gt. 10) .or. (mod_cardnum .lt. 2) )
$ then
$       if( mod_cardnum .eq. 0 )
$       then
$               mod_cardnum = "K"
$       endif
$       if( mod_cardnum .eq. 11 )
$       then
$               mod_cardnum = "J"
$       endif
$       if( mod_cardnum .eq. 12 )
$       then
$               mod_cardnum = "Q"
$       endif
$       if( mod_cardnum .eq. 1 )
$       then
$               mod_cardnum = "A"
$       endif
$  else
$        mod_cardnum = f$string(mod_cardnum)
$ endif
$ return
$!
$print_suit:
$ if( cardnum .le. 13 )
$ then
$       buff6 = buff6 + "|   '   |"
$       buff3 = buff3 + "| -:|:- |"
$       buff7 = buff7 + "|   .   |"
$ endif
$ if( (cardnum .le. 26) .and. (cardnum .gt. 13) )
$ then
$       buff6 = buff6 + "|  . .  |"
$       buff3 = buff3 + "|  \'/  |"
$       buff7 = buff7 + "|   '   |"
$ endif
$ if( (cardnum .le. 39) .and. (cardnum .gt. 26) )
$ then
$       buff6 = buff6 + "|   .   |"
$       buff3 = buff3 + "|  /_\  |"
$       buff7 = buff7 + "|   '   |"
$ endif
$ if( (cardnum .le. 52) .and. (cardnum .gt. 39) )
$ then
$       buff6 = buff6 + "|   o   |"
$       buff3 = buff3 + "|  o.o  |"
$       buff7 = buff7 + "|   '   |"
$ endif
$ return
$!
