$!
$! filename: random_numz.com
$!
$!    input: number of random numz to print
$!   output: N random numz
$!
$!     idea: use system clock and basic math to generate random numz
$!     form:
$!           make delicious spaghetti for that nice psuedorandom taste
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$ count = 0
$ prev_num = 200
$!
$loop:
$ gosub random
$ if( num .eq. 0 )
$ then
$       gosub random
$ else
$       digit = num * 10
$       gosub random
$       num = digit + num
$ endif
$ num = num / 10
$!
$ gosub get_diff
$ if( diff .lt 7 )
$ then
$       gosub random
$ endif
$!
$ if( num .ge. 100 )
$ then
$       num = num - 100
$ endif
$!
$ prev_num = num
$ write sys$output num
$ count = count + 1
$ if( count .ge. p1 )
$ then
$       goto bail
$ endif
$ goto loop
$!
$random:
$ num2 = f$cvtime("",,"hundredth")
$ num2 = f$extract(1,2,num2)
$ num = f$unique()
$ num = f$extract(31,32,num)
$ wait 0:0:0.01
$ if( num .eqs. "A" .or. -
      num .eqs. "B" .or. -
      num .eqs. "C" .or. -
      num .eqs. "D" .or. -
      num .eqs. "E" .or. -
      num .eqs. "F")
$ then
$       goto random
$ endif
$ num = num + num2
$!
$ return
$!
$get_diff:
$ diff = prev_num - num
$ if( diff .lt. 0 )
$ then 
$       diff = diff * -1
$ endif
$ return
$!
$bail:
$ exit
