$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_UNZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! Decompress file by parsing metafile../
$ my_dir = f$directory()
$ state = "newfile"
$ 30:
$       open/read input 'my_dir'metafile.dat
$       read/end_of_file=39 input line
$       if state .eqs. "newfile"
$       then
$               state = "contents"
$               create 'line
$               open/append output 'line
$               goto 30
$       endif
$       if line .eqs. "+++++++"
$       then
$               state = "newfile"
$               close output 
$               goto 30
$       endif
$       write output line
$       goto 30
$ 39:
$ close input
$ recy 'my_dir'metafile.dat
$! --------------------------------------------/..
