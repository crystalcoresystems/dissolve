
DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*;*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = f$extract(0, f$locate(";",x), x)
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output 'my_dir'metafile.dat
$       write output "''file'"
$       close output 
$       append 'file 'my_dir'metafile.dat
$       delete 'file
$       open/append output 'my_dir'metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       i = -1
$10:    
$
$       x = f$search("[decuserve_user.kobler.com_scrpts.engine.blocksaver.test]*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create [.test]metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output [.test]metafile.dat
$       write output "''file'"
$       close output 
$       append 'file [.test]metafile.dat
$       delete 'file
$       open/append output [.test]metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM

$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create ['mydir]metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output [my_dir]metafile.dat
$       write output "''file'"
$       close output 
$       append 'file [my_dir]metafile.dat
$       delete 'file
$       open/append output [my_dir]metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM

$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create ['mydir]metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output [my_dir]metafile.dat
$       write output "''file'"
$       close output 
$       append 'file [my_dir]metafile.dat
$       delete 'file
$       open/append output [my_dir]metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM

$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create ['my_dir]metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output [my_dir]metafile.dat
$       write output "''file'"
$       close output 
$       append 'file [my_dir]metafile.dat
$       delete 'file
$       open/append output [my_dir]metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM

$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create ['my_dir']metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output [my_dir]metafile.dat
$       write output "''file'"
$       close output 
$       append 'file [my_dir]metafile.dat
$       delete 'file
$       open/append output [my_dir]metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create [''my_dir']metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output [my_dir]metafile.dat
$       write output "''file'"
$       close output 
$       append 'file [my_dir]metafile.dat
$       delete 'file
$       open/append output [my_dir]metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output [my_dir]metafile.dat
$       write output "''file'"
$       close output 
$       append 'file [my_dir]metafile.dat
$       delete 'file
$       open/append output [my_dir]metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM

$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output 'my_dir'metafile.dat
$       write output "''file'"
$       close output 
$       append 'file 'my_dir'metafile.dat
$       delete 'file
$       open/append output 'my_dir'metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output 'my_dir'metafile.dat
$       write output "''file'"
$       close output 
$       append 'file 'my_dir'metafile.dat
$       delete 'file
$       open/append output 'my_dir'metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*;*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output 'my_dir'metafile.dat
$       write output "''file'"
$       close output 
$       append 'file 'my_dir'metafile.dat
$       delete 'file
$       open/append output 'my_dir'metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*;*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output 'my_dir'metafile.dat
$       write output "''file'"
$       close output 
$       append 'file 'my_dir'metafile.dat
$       delete 'file
$       open/append output 'my_dir'metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*;*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = f$extract(0, f$locate(";",x), x)
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output 'my_dir'metafile.dat
$       write output "''file'"
$       close output 
$       append 'file 'my_dir'metafile.dat
$       delete 'file
$       open/append output 'my_dir'metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM
$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_ZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! get all files from the directory into array../
$       my_dir = f$directory()
$       i = -1
$10:    
$
$       x = f$search("''my_dir'*.*;*")
$       if x .eqs. "" then goto 19
$       spec = f$parse(x,,,"TYPE")
$       if spec .nes. ".DIR" 
$       then
$               i = i + 1
$               fn'i = x
$       endif
$       goto 10
$19:
$ i = i + 1
$!---------------------------------------------/..
$ create 'my_dir'metafile.dat
$! Append files to metafile - first line filename last line +++../
$20:    i = i - 1
$       file = fn'i
$       open/append output 'my_dir'metafile.dat
$       write output f$extract(0, f$locate(";",x), x)
$       close output 
$       append 'file 'my_dir'metafile.dat
$       delete 'file
$       open/append output 'my_dir'metafile.dat
$       write output "+++++++"
$       close output 
$       if i .eq. 0 then goto 29
$       goto 20
$29:
$!-----------------------------------------------/..

+++++++
DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_ZIP.COM

$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_UNZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! Decompress file by parsing metafile../
$ state = "newfile"
$ 30:
$       open/read input [.test]metafile.dat
$       read/end_of_file=39 input line
$       if state .eqs. "newfile"
$       then
$               state = "contents"
$               create 'line
$               open/append output 'line
$               goto 30
$       endif
$       if line .eqs. "+++++++"
$       then
$               state = "newfile"
$               close output 
$               goto 30
$       endif
$       write output line
$       goto 30
$ 39:
$ close input
$ recy [.test]metafile.dat
$! --------------------------------------------/..
+++++++

DSA3:[DECUSERVE_USER.KOBLER.COM_SCRPTS.ENGINE.BLOCKSAVER]BLOCKSAVER_UNZIP.COM

$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BLOCKSAVER_UNZIP.COM
$!    input: All files in the directory (excluding folders)
$!   output: A single file containing the data from all the files
$!            with the symbol +++ between files
$!     idea: Get a list of all files in the directory
$!            Create a new file that is empty
$!            Append all files from the list to the new file
$!            seperate the files with +++ symbol
$!     form: straight forward procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BLS__status    = %x11000000
$    BLS__success   = BLS__status + %x001
$    BLS__ctrly     = BLS__status + %x00c
$!    on control_y then goto ctrl_y
$!    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$! Decompress file by parsing metafile../
$ my_dir = f$directory()
$ state = "newfile"
$ 30:
$       open/read input 'my_dir'metafile.dat
$       read/end_of_file=39 input line
$       if state .eqs. "newfile"
$       then
$               state = "contents"
$               create 'line
$               open/append output 'line
$               goto 30
$       endif
$       if line .eqs. "+++++++"
$       then
$               state = "newfile"
$               close output 
$               goto 30
$       endif
$       write output line
$       goto 30
$ 39:
$ close input
$ recy 'my_dir'metafile.dat
$! --------------------------------------------/..
+++++++
