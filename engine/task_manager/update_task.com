$!created on 24-MAR-2021
$!----------------------
$!
$! filename: UPDATE_TASK.COM
$!    input: Called from main tskmgr procedure, uses tasks.dat
$!   output: rewrites tasks.dat with update
$!     idea: Ask which task to update and what aspect
$!            goto subprocedure based on aspect and get
$!            text. Status and Timeframe overwrite a single line
$!            Progress adds new lines with date
$!     form: Command procedure with basic loops and subprocedures
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$       on control_y then goto ctrl_y
$!Update Task../
$U:
$       @list_tasks num_tasks
$       display ""
$       ask to_update S "What task would you like to update?"
$       if f$integer(to_update) .gt. num_tasks .or. -
           f$integer(to_update) .le. 0 then goto U
$
$       ask aspect S "What aspect [P]rogress [S]tatus [T]imeframe [I]nfo" -
                                                    "" U  
$       if (aspect .nes. "P") .and. (aspect .nes. "S") .and. -
           (aspect .nes. "I") .and. (aspect .nes. "T") then goto U
$       to_update == tasklist'to_update
$       libcall unique_name tmp tmgr_?
$       create 'tmp.dat
$       open/read file tasks.dat
$       open/append tmpfile 'tmp.dat
$       @date date
$u10:   read file task /end_of_file=u19
$       write tmpfile task
$       if f$locate(to_update, task) .ne. f$length(task) -
                                            then goto 'aspect'15
$       goto u10
$!Progress../
$P15:
$       read file task /end_of_file=u19
$       write tmpfile task
$       if f$locate("PROGRESS", task) .eq. f$length(task) -
                                        then goto p15
$       write tmpfile "    ", date
$P16:
$       read sys$command/prompt="Progress Update: " update -
                                                /end_of_file=P17
$       write tmpfile "        ", update
$       goto P16
$P17:   read file task /end_of_file=u19
$       if f$locate(date, task) .eq. f$length(task) -
                                        then write tmpfile task 
$       goto u10
$!/..
$!Info../
$I15:
$       read file task /end_of_file=u19
$       write tmpfile task
$       if f$locate("INFO", task) .eq. f$length(task) -
                                        then goto I15
$       write tmpfile "    ", date
$I16:
$       read sys$command/prompt="Info Update: " update -
                                                /end_of_file=I17
$       write tmpfile "        ", update
$       goto I16
$I17:   read file task /end_of_file=u19
$       if f$locate(date, task) .eq. f$length(task) -
                                        then write tmpfile task 
$       goto u10
$!/..
$!Status../
$S15:
$       read file task /end_of_file=u19
$       if f$locate("STATUS", task) .ne. f$length(task) -
                                        then goto S16
$       write tmpfile task
$       goto S15
$S16:
$       read sys$command/prompt="Status Update: " update -
                                                /end_of_file=u10
$       write tmpfile "STATUS - ", update
$       goto u10
$!/..
$!Timeframe../
$T15:
$       read file task /end_of_file=u19
$       if f$locate("TIMEFRAME", task) .ne. f$length(task) -
                                        then goto T16
$       write tmpfile task
$       goto T15
$T16:
$       read sys$command/prompt="Timeframe Update: " update -
                                                /end_of_file=u10
$       write tmpfile "TIMEFRAME - ", update
$       goto u10
$!/..
$u19:
$       goto exit
$!---------------------------------------------------------------/..
$ctrl_y:
$       display "Starting over..."
$       display ""
$       goto exit
$exit:
$       close file
$       close tmpfile
$       rename 'tmp.dat tasks.dat
$       purge/keep=3 tasks.dat
$       exit
