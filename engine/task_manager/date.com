$ month = f$cvtime(,,"MONTH")
$ goto 'month
$01:    month = "Jan"
$       goto day
$02:    month = "Feb"
$       goto day
$03:    month = "Mar"
$       goto day
$04:    month = "Apr"
$       goto day
$05:    month = "May"
$       goto day
$06:    month = "Jun"
$       goto day
$07:    month = "Jul"
$       goto day
$08:    month = "Aug"
$       goto day
$09:    month = "Sep"
$       goto day
$10:    month = "Oct"
$       goto day
$11:    month = "Nov"
$       goto day
$12:    month = "Dec"
$       goto day
$day:
$ day = f$cvtime(,,"DAY")
$ year = f$cvtime(,,"YEAR")
$ 'p1 == f$string(month + " " + day + ", " + year)
