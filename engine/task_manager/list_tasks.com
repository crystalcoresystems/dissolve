$!created on 25-MAR-2021
$!----------------------
$!
$! filename: LIST_TASK.COM
$!    input: tasks.dat
$!   output: Tasks written to screen with option to display more
$!            variable set for num_tasks
$!     idea: basic loops
$!     form: command procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    ltsk__status    = %x11000000
$    ltsk__success   = ltsk__status + %x001
$    ltsk__ctrly     = ltsk__status + %x00c
$    on control_y then goto ctrl_y
$    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$start:
$       display clear, home
$       gosub list_tasks
$       if p2 .nes. "FULL" then exit
$       ask sel I "Enter Task number for more info"
$       open/read file tasks.dat 
$10:    read file task /end_of_file=19
$       if tasklist'sel .nes. task then goto 10
$15:    display task
$       read file task /end_of_file=19
$       if f$locate("TASK", task) .ne. 0 then goto 15
$19:
$       close file
$       ask sel b "Examine more tasks? " "N"
$       if sel .eq. 1 then goto start
$       exit
$!List Tasks../
$list_tasks:
$       i = 1
$lt10:  
$       open/read file tasks.dat 
$       read file task /end_of_file=lt19
$       if f$locate("TASK",task) .eq. 0 
$       then
$               tasklist'i == task
$               to_display = f$extract(5,999,task)
$               display "''i ", to_display
$               i = i + 1
$       endif
$       goto lt10
$lt19:
$       close file
$       'p1 == i - 1
$       return
$!--------------------------------------------------------------/..
$ctrl_y:
$       close file
$       exit ltsk__ctrly .or. 0x10000000
