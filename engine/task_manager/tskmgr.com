$!created on 20-MAR-2021
$!----------------------
$!
$! filename: TSKMGR.COM
$!    input: String to add to tasks or
$!            None to open application
$!   output: Information about tasks
$!     idea: Application that stores and displays
$!            Data about various tasks
$!     form: Looping application
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$       saved_message   = f$environment("MESSAGE")
$       set message/facility/severity/identification/text
$
$       tmgr__status    = %x11077000
$       tmgr__success   = tmgr__status + %x001
$       tmgr__ctrly     = tmgr__status + %x00c
$       on control_y then goto ctrl_y
$       on warning then goto error
$
$       status          = tmgr__success
$       display         = "write sys$output"
$       libcall         = "@engine:subroutine_library"
$       ask             = libcall + " ask"
$       signal          = libcall + " signal"
$       false           = 0
$       true            = 1
$       undefine        = "deassign"
$!-------------------------------------------------/..
$       display "This is The Task Manager"
$start:
$       display ""
$       display "Options..."
$       ask option S "[A]dd [L]ist [E]xit [U]pdate [R]emove" "" U
$       display clear, home
$       goto 'option
$L:     
$       @list_tasks num_tasks full
$       display clear, home
$       goto start
$U:     
$       @update_task
$       display clear, home
$       goto start
$R:
$!Remove Task../
$       @list_tasks num_tasks
$       display ""
$       ask to_remove S "What task would you like to remove?"
$       if f$integer(to_remove) .gt. num_tasks .or. -
           f$integer(to_remove) .le. 0 then goto R
$
$       to_remove == tasklist'to_remove
$       libcall unique_name tmp tmgr_?
$       create 'tmp.dat
$       open/read file tasks.dat
$       open/append tmpfile 'tmp.dat
$R10:   read file task /end_of_file=R19
$       if f$locate(to_remove, task) .ne. f$length(task)
$       then
$R11:           read file task /end_of_file=R19
$               if f$locate("TIMEFRAME", task) .eq. f$length(task) then goto R11
$       else
$               write tmpfile task
$       endif
$       goto R10
$R19:
$       close file
$       close tmpfile
$       rename 'tmp.dat tasks.dat
$       purge/keep=3 tasks.dat
$       display clear, home
$       goto start
$!---------------------------------------------------------------/..
$A:
$!Add Task../
$       ask new_task S "What is the task?"
$       if f$search("tasks.dat") .eqs. "" then create tasks.dat
$       open/append file tasks.dat
$       write file "TASK - ", new_task
$       write file "PROGRESS - "
$       write file "INFO - "
$       write file "STATUS - New"
$       write file "TIMEFRAME - None"
$       close file
$       display "adding task..."
$       purge/keep=3 tasks.dat
$       goto start 
$!--------------------------------------------------------------/..
$error:
$       display "error"
$ctrl_y:
$       display "Starting over..."
$       display ""
$       goto start
$E:
$       display "Goodbye"
$       exit
$!Add Task Doc../
$! Set Var:     File tasks.dat is updated
$! Synopsis:    Adds appends task.dat with a new task
$!-------------------------------------------------------------/..
$!Update Task Doc../
$! Set Var:     tasks.dat updated
$! Synopsis:    adds "updated" to line beneath selected task
$!-----------------------------------------------------------/..
$!List Task Doc../
$! Set Var:     num_tasks, tasklist (array)
$! Synopsis:    prints all tasks in tasks.dat
$!--------------------------------------------------------------/..
