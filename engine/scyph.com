$!created on  2-JUL-2021
$!----------------------
$!
$! filename: SCYPH.COM
$!    input: passphrase and message
$!   output: Jumble of letters
$!     idea: Combine 3 Cyphers
$!     form: Call 3 perl scripts in this order
$!            ccyph.pl -> rcyph.pl -> pcyph.pl -> rcyph.pl -> ccyph.pl
$!            passphrase is used by each script for key value
$!            first 2 characters of passphrase and last 2 characters
$!  
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    scy__status    = %x11000000
$    scy__success   = scy__status + %x001
$    scy__ctrly     = scy__status + %x00c
$    on control_y then goto ctrl_y
$    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$
$ ask phrase S "What is the message?" x U
$ ask code S "What is the key?" x U
$m10:
$       ask option S "[E]ncrypt or [D]ecrypt?" E U
$       if option .nes. "E" .and. option .nes. "D"
$       then
$               goto m10
$       endif
$m19:
$
$ perl [decuserve_user.kobler.perl]ccyph.pl $phrase $code[0] $option - 'C' >> file
