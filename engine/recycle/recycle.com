$!created on 10-MAR-2021
$!----------------------
$!
$! filename: RECYCLE.COM
$!    input: file to be recycled
$!   output: altered file structure
$!     idea: make a trash in current folder if not already present
$!            rename file to trash
$!     form: command procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$ display = "write sys$output" 
$
$ message = ""
$! check correct usage
$ if ( p1 .eqs. "" .or. p2 .nes. "" )
$ then
$       message = "usage: recycle file"
$       goto exit
$ endif 
$! check for file existance
$ trash = f$search(p1)
$ if ( trash .eqs. "" )
$ then
$       message = "file not found: check extension"
$       goto exit
$ endif
$! check for / create bin
$ workn_dir = f$parse(trash,,,"directory")
$ bin = workn_dir + "trash.dir"
$ bin = f$search(bin)
$ if ( bin .eqs. "" )
$ then
$       trashbin = workn_dir - "]" - ">" + ".trash]"
$       create/dir 'trashbin 
$       bin = trashbin
$ endif
$10: 
$ old = trash - f$parse(trash,,,"device") - -
                f$parse(trash,,,"node")
$ new =  f$parse(trash,,,"directory") - "]" - ">" + -
          ".trash]" + f$string( f$parse(trash,,,"name") + -
                    f$parse(trash,,,"type") )
$ rename 'old 'new
$ trash = f$search(p1)
$ if ( trash .nes. "") then goto 10
19:
$       
$exit:
$       if ( message .nes "") then display message
$       exit
