$!
$! filename: gen_scrpts_engine.com
$!    input: interactive about script to be generated
$!   output: formatted header for script
$!     idea: ask for the info then output it
$!     form: command sequence
$ on control_y then goto ctrl_y
$ ask = "read sys$command/prompt="
$ newline[0,8] = %x0A
$ tab[0,32] = %x20202020
$ input = ""
$ 10:   
$       ask "input: " inp /end_of_file=19
$       if f$length(inp) .gt. 56 then goto 15
$       input=f$string(input + newline + "$!" + tab + tab + tab + inp)
$       goto 10
$ 15:
$       write sys$command "exceeds line length"
$       goto 10
$ 19: input = f$extract(15, 999, input)
$ output = ""
$ 20:   
$       ask "output: " inp /end_of_file=29
$       if f$length(inp) .gt. 56 then goto 25
$       output=f$string(output + newline + "$!" + tab + tab + tab + inp)
$       goto 20
$ 25:
$       write sys$command "exceeds line length"
$       goto 20
$ 29: output = f$extract(15, 999, output)
$ idea = ""
$ 30:   
$       ask "idea: " inp /end_of_file=39
$       if f$length(inp) .gt. 56 then goto 35
$       idea=f$string(idea + newline + "$!" + tab + tab + tab + inp)
$       goto 30
$ 35:
$       write sys$command "exceeds line length"
$       goto 30
$ 39: idea = f$extract(15, 999, idea)
$ form = ""
$ 40:   
$       ask "form: " inp /end_of_file=49
$       if f$length(inp) .gt. 56 then goto 45
$       form=f$string(form + newline + "$!" + tab + tab + tab + inp)
$       goto 40
$ 45:
$       write sys$command "exceeds line length"
$       goto 40
$ 49: form = f$extract(15, 999, form)
$ abrv = ""
$ 50:   
$       ask "abrv: " inp /end_of_file=59
$       if f$length(inp) .gt. 4 then goto 55
$       goto 59
$ 55:
$       write sys$command "exceeds line length"
$       goto 50
$ 59: abrv = inp
$ctrl_y:
$ display f$fao("$!!created on !11%D", 0)
$ display f$fao("$!!!22*-")
$ display "$!"
$ display "$! filename: ''p1'.COM"
$ display "$!    input: ''input'"
$ display "$!   output: ''output'"
$ display "$!     idea: ''idea'"
$ display "$!     form: ''form'"
$ display "$!"
$ display f$fao("$!!!5* !26*_")
$ display "$!    | Written by |K|O|13|13|S| |"
$ display f$fao("$!!!5* !26*=")
$ display "$!"
$ display "$!initialization../"
$ display "$    saved_message   = f$environment(""MESSAGE"")"
$ display "$    set message/facility/severity/identification/text"
$ display "$    ''abrv'__status    = %x11000000"
$ display "$    ''abrv'__success   = ''abrv'__status + %x001"
$ display "$    ''abrv'__ctrly     = ''abrv'__status + %x00c"
$ display "$    on control_y then goto ctrl_y"
$ display "$    on warning then goto error"
$ display "$    display         = ""write sys$output"""
$ display "$    libcall         = ""@engine:subroutine_library"""
$ display "$    ask             = libcall + "" ask"""
$ display "$    signal          = libcall + "" signal"""
$ display "$    false           = 0"
$ display "$    true            = 1"
$ display "$    undefine        = ""deassign"""
$ display "$!-------------------------------------------------/.."
