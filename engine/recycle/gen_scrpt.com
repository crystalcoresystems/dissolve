$!
$! filename: gen_scrpts.com
$!    input: filename
$!   output: runs script and out
$!     idea: validate input is good and use /output mod with engine
$!     form: command sequence
$!
$ libcall = "@engine:subroutine_library"
$ ask = libcall + " ask"
$ signal = libcall + " signal"
$ display clear, home
$start: 
$ ask filename S "filename: "
$ if f$locate(" ", filename) .ne. f$length(filename) -
                        then signal rcy w incfnm "spaces not allowed"
$ if f$locate(" ", filename) .ne. f$length(filename) -
                        then goto start
$ @rcy:rewrite 'filename
$ open/write file 'filename.com
$ @rcy:gen_scrpt_engine/output=file 'filename
$ close file
$ vi 'filename.com
$ exit
