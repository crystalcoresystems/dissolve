$!created on 17-MAR-2021
$!----------------------
$!
$! filename: REWRITE.COM
$!    input: filename 
$!   output: renamed file from recycle directory
$!     idea: f$search(*.*;*, recycle) to get a file
$!            rename that file into current directory
$!     form: command procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$
$ libcall = "@engine:subroutine_library"
$
$ if p1 .eqs. "" then libcall signal rcy e needarg -
                        "procedure requires filename"
$ name = f$search("[decuserve_user.kobler.recycle]*.*;*")
$ old = name - f$parse(name,,,"DEVICE")
$ new = f$directory() + p1
$ rename 'name 'new
