$!created on 17-MAR-2021
$!----------------------
$!
$! filename: OPENVI.COM
$!    input: filename
$!   output: run vi application
$!     idea: Check if the file exists, if it does
$!           open it with vi.
$!           Otherwise use rewrite to create a new
$!           file and open it with vi
$!     form: short conditional command procedure
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$ if p2 .ne. "" then signal rcy e 2MNYARG "Too many arguements"
$ if p1 .eqs. "" then vi
$ if p1 .eqs. "" then exit
$ if f$search(p1) .nes. "" 
$ then
$       vi 'p1
$       exit
$ endif
$ @rcy:rewrite 'p1
$ vi 'p1
$ exit
