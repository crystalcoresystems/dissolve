#include <vector>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <string>
#include <math.h>
#include <iomanip>

#define INF 9999999999
#define int long long
//

#define I i-1
#define J j-1
using namespace std;
//Jatayu's ballanced bracket sequence../
//
// balance bracket sequnce s of length 2
//
// undirected graph of 2n vertices using this sequnce
// for any two distinct vertices i and j 1 <= i < j <= 2n there is an edge
// if subsegment s[i...j] forms a balanced bracket sequence
//
// Determine the nuber of connected components

char a[200000];
int used[200000];

int n;

signed main() {
        int t;

        freopen("input.txt","r",stdin);

        cin >> t;
        while(t--) {
                cin >> n;
                n *= 2;
                for(int i=0; i<n; i++){
                        cin >> a[i];
                        used[i] = 0;
                }
                int ans = 0;
                int ss = 0;
                for(int i=0; i<n; i++) {
                        if(a[i] == ')')
                                ss--;
                        else
                                ss++;
                        if(ss >= 0 && a[i] == ')'){
                                ans++;
                        }
                       if(ss == 0 && i != n-1)
                              ans--; 
                }
                cout << ans << endl;
       }
}

        //..

/*
//Mainak and interesting sequence../
//
// two positive integers n and m
// sequence a1, .. , an n positive integers is interesting if
//   for all integers i 1 <= i <= n the bitwise XOR of all elements in a
//   which are stricly less than ai is 0. Formally if pi is the the bitwise XOR of
//   all elements in a which are strictly less tha ai
//   
//   find any interesting sequence such that the sum of elements is equal to m

int a[100000];
signed main() {
        int t;
        int n;
        int m;
        int max;
        int highest;
        int lowest;

        freopen("input.txt","r",stdin);

        cin >> t;
        while(t--) {
                cin >> n >> m;
                if(n > m) {
                        cout << "No\n";
                        continue;
                }
                if(n%2 == 0 && m%2 == 1) {
                        cout << "No\n";
                        continue;
                }
                cout << "Yes\n";
                if(n%2 == 1) {
                        while(n-- > 1) {
                                cout << 1 << " ";
                                m--;
                        }
                        cout << m << endl;
                } else {
                        while(n-- > 2) {
                                cout << 1 << " ";
                                m--;
                        }
                        cout << m/2 << " " << m/2 << endl;
                }
        }
}
//..
*/

/*
//Mainak and array../
//
//n positive integers
// following operation to the array exactly once
//    Pick a subsegment of the array and cyclically rotate by any amount
//
//
//  he can do the following exactly once
//    pic two integers l and r such that 1 <= l <= r <= n
//    pick any positive integer k
//    repeat k times 
//      set al = al+1 .. al+1 = al+2,...,ar -1 = ar, ar = al
//
//    maximize the value of (an - a1) after one op.

signed main() {
        int t;
        int n;
        int a[2000];
        int max;
        int highest;
        int lowest;

        freopen("input.txt","r",stdin);

        cin >> t;
        while(t--) {
                cin >> n;
                for (int i=0; i<n;i++)
                        cin >> a[i];
                if (n == 1) {
                        cout << 0 << endl;
                        continue;
                }

                max = 0;
                highest = 0;
                lowest = INF;
                if(a[n-1] - a[0] > max)
                        max = a[n-1] - a[0];
                for(int i=0; i<n-1; i++){
                        if (i!=0)
                                if(a[i] > highest)
                                        highest = a[i];
                        if(a[i] < lowest)
                                lowest = a[i];

                        if (a[i] - a[i+1] > max)
                                max = a[i] - a[i+1];
                }
                if(highest - a[0] > max)
                        max = highest - a[0];
                if(a[n-1] - lowest > max)
                        max = a[n-1] - lowest;

                cout << max << endl;
        }
}
//..
*/

/*
// Vector stuff../
struct point {
        double x;
        double y;
};

double mag(point v) {
        return sqrt(v.x*v.x + v.y*v.y);
}
double cross (point a, point b)
{
        return a.x*b.y - a.y*b.x;
}
double angle (point a, point b)
{
        return acos(dot(a, b) / mag(a) / mag(b) );
}

double dist(point p, double A, double B, double C)
{

        point p;
        point t1, t2;

        t1.x = -1*A*C / (A*A + B*B);
        t1.y = -1*B*C / (A*A + B*B);

        point n;
        n.x = A;
        n.y = B;
        point a;
        a.x = n.y * -1;
        a.y = n.x;

        t2 = t1 + a;
        point v1, v2;
        v1 = t1 - p; //
        v2 = t2 - p; //

        return abs(cross(v1, v2)) / mag(t2 - t1);
}
//..

// distance from point to line../
// given - coordinates of the point and of 2 point
// which determine the line
signed main() {
        point t;
        point a, b;
        double A, B, C;

        cin >> t.x >> t.y >> a.x >> a.y >> b.x >> b.y;

        cout << setprecision(12) << dist(t, A, B, C);
}

//..

// Length of vector../
//
// given coordinates x1, x2, y1, y2
// output the length of the vector
signed main() {
        int x1, x2, y1, y2;
        point v;

        cin >> x1 >> y1 >> x2 >> y2;
        v.x = abs(x1 - x2);
        v.y = abs(y1 - y2);
        cout << mag(v);
}
//..
*/
/*
// Monoblock../
// aweesomeness of array is minimum # of blocks of consecutive identical numbers in which the array
// could be splut
// [1, 1, 1] - 1
// [5, 7] 2
// [1, 7, 7, 9, 9] 3
//
// calculate the sum of awesomeness for all sub arrays
// (1, 1 ... 1,5)
// (2, 2 ....2,5)
// .
// .
// .
int a[100000];
int dp[100000];

signed main() {
        int n, t;
        freopen("input.txt","r",stdin);
        cin >> n >> t;
        for(int i=0; i<n; i++) {
                cin >> a[i];
                dp[i] = 0;
        }
        while(t--) {
                int x, y;
                cin >> x >> y;
                x--;
                a[x] = y;

                dp[0] = 1;

                int sum = 1;
                for(int i=1; i < n; i++) {
                        if(a[i-1] != a[i]) {
                                dp[i] = dp[i-1]+1;
                        } else {
                                dp[i] = dp[i-1];
                        }
                                sum += dp[i];
                }
                int addon = sum;
                for(int i=1; i < n; i++) {
                        addon--;
                        if(a[i-1] != a[i])
                                addon -= n-i;
                        sum += addon;
                }
                cout << sum << endl;
        }
}
//..
*/
/*
// Beautiful Array../
// array a of length n kas beauty
// sum of elements divided by k (integer division)
// given integer k find beauty equal to b and sum of elements equal to s
signed main() {
        int t;
        freopen("input.txt","r",stdin);
        cin >> t;
        while(t--) {
                int n, k, b, s;
                int max;
                cin >> n >> k >> b >> s;

                if(s/k < b){
                        cout << -1 << endl;
                        continue;
                }
                max = s;
                for(int i=0;i<n-1;i++) {
                        max -= k-1;
                }
                max = max/k;
                if(b < max) {
                        cout << -1 << endl;
                        continue;
                }
                bool flag = false;
                for(int i=0; i < n; i++) {
                        if (s/k > b) {
                                cout << k-1 << " ";
                                s-=k-1;
                        } else {
                                if(!flag) {
                                        cout << s << " ";
                                        flag = true;
                                } else
                                        cout << 0 << " ";
                        }
                }
                cout << endl;
        }
}
//..
*/
/*
// Crossmarket../
// S and M shop at shop at n x m market
// can move to adjacent cell with 1 unit of power
// portals...
// M places portal at every cell visited
// S or M can travel to any portal cell if in a cell with a portal
// S starts at 1, 1 destination n, m
// M starts at n, 1 destination 1, m
// minimum total energy
signed main() {
        int t;
        int n, m;
        freopen("input.txt","r",stdin);
        cin >> t;

        while(t--) {
                cin >> n >> m;
                if (n == 1 && m ==1) {
                        cout << 0 << endl;
                        continue;
                }
                if (n < m)
                        swap(n, m);
                cout << n + (m-1)*2 << endl;
        }
}
//..
*/
/*
// c../
signed main() {
        int t;
        freopen("input.txt","r",stdin);
        cin >> t;
        while(t--){
                int n, m;
                cin >> n >> m;
                for(int i=0; i<n;i++)
                        cin >> a[i];
                for(int i=0; i<m;i++) {
                        int x, r;
                        cin >> x >> r;
                        x--;
                        int max = 0;
                        int ans = 0;
                        bool flag = false;
                        for(int j=0; j<x; j++)
                                if(a[j] > a[x]) {
                                        flag = true;
                                        break;
                                }
                        if(flag) {
                                cout << 0 << endl;
                                continue;
                        }
                        int count = 0;
                        for(int j=x+1; j<n; j++) {
                                if(a[j] > a[x]) {
                                        flag = true;
                                        break;
                                }
                                count++;
                                if (count == r)
                                        break;
                        }
                        if(flag)
                                cout << count << endl;
                        else
                                if (x < 2)
                                        cout << r << endl;
                                else
                                        cout << r - x + 1 << endl;
                }
        }
}
//..
//Problem B../
signed main() {
        int t;
        //freopen("input.txt","r",stdin);
        cin >> t;
        while(t--) {
                int n, k;
                cin >> n >> k;
                if (k%4 == 0) {
                        cout << "NO\n";
                        continue;
                }
                cout << "YES\n";
                for(int i=0; i < n; i+=2)
                        cout << i+1 << " " << i+2 << endl;
        }
}
//..
*/
/*
// Problem A../
signed main(){
        int t;
        //freopen("input.txt","r",stdin);
        cin >> t;
        while(t--) {
                int n, m;
                cin >> n >> m;
                if (abs(n-m)&1)
                        cout << "Burenka";
                else
                        cout << "Tonya";
                cout << endl;
        }
}
//..
*/
/*
// Levenstein2../
int dp[1001][1001];
string a,b;

signed main() {
        int n, m;
        int i, j;
        int start;

        freopen("input.txt","r",stdin);
        cin >> a >> b;
        n = b.length();
        m = a.length();
        memset(dp,0,sizeof(dp));
        
        for(i=1;i<=n;i++)
                for(j=1;j<=m;j++){
                        dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
                        if(b[I] == a[J])
                                dp[i][j] = max(dp[i][j], dp[i-1][j-1] + 1);
                }

        cout << endl;
        for(i=0;i<=n;i++) {
                for(j=0;j<=m;j++){
                        cout << dp[i][j] << " ";
                }
                cout << endl;
        }
        

        vector<int> ans;
        int last = dp[n][m];
        for(i=n, j=m; i > 0;) {
                if (dp[i-1][j-1] > dp[i-1][j]) {
                        i--; j--;
                } else {
                        i--;
                }
                if(dp[i][j] != last)
                        ans.push_back(b[i]);
                last = dp[i][j];
        }
        reverse(ans.begin(), ans.end());
        
        for(i=0;i<ans.size();i++)
                printf("%c ", ans[i]);
        cout << endl;
        
        int dpa[1000];
        int dpb[2001];
        memset(dpa,0,sizeof(dpa));
        memset(dpb,0,sizeof(dpb));

        for(i=0, j=0; i < n, j < ans.size(); i++) {
                if(b[i] == ans[j]) {
                        dpb[i] = 1;
                        j++;
                }
        }
        for(i=0, j=0; i < m, j < ans.size(); i++) {
                if(a[i] == ans[j]) {
                        dpa[i] = 1;
                        j++;
                }
        }
        cout << endl;
        for(i=0;i<m;i++)
                cout << dpa[i] << " ";
        cout << endl;
        for(i=0;i<n;i++)
                cout << dpb[i] << " ";
       cout << endl; 
}
//..
*/
/*
// Levenstein../
int dp[1001][1001];
string a,b;
void sr(int a[], int x, int n);
void sl(int a[], int x, int n);

signed main() {
        int n, m;
        int i, j;
        int start;

        freopen("input.txt","r",stdin);
        cin >> a >> b;
        n = b.length();
        m = a.length();
        memset(dp,0,sizeof(dp));
        
        for(i=1;i<=n;i++)
                for(j=1;j<=m;j++){
                        dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
                        if(b[I] == a[J])
                                dp[i][j] = max(dp[i][j], dp[i-1][j-1] + 1);
                }

        cout << endl;
        for(i=0;i<=n;i++) {
                for(j=0;j<=m;j++){
                        cout << dp[i][j] << " ";
                }
                cout << endl;
        }
        

        vector<int> ans;
        int last = dp[n][m];
        for(i=n, j=m; i > 0;) {
                if (dp[i-1][j-1] > dp[i-1][j]) {
                        i--; j--;
                } else {
                        i--;
                }
                if(dp[i][j] != last)
                        ans.push_back(b[i]);
                last = dp[i][j];
        }
        reverse(ans.begin(), ans.end());
        
        for(i=0;i<ans.size();i++)
                printf("%c ", ans[i]);
        cout << endl;
        
        int dpa[1000];
        int dpb[2001];
        memset(dpa,0,sizeof(dpa));
        memset(dpb,0,sizeof(dpb));

        for(i=0, j=0; i < n, j < ans.size(); i++) {
                if(b[i] == ans[j]) {
                        dpb[i] = 1;
                        j++;
                }
        }
        for(i=0, j=0; i < m, j < ans.size(); i++) {
                if(a[i] == ans[j]) {
                        dpa[i] = 1;
                        j++;
                }
        }
        cout << endl;
        for(i=0;i<m;i++)
                cout << dpa[i] << " ";
        cout << endl;
        for(i=0;i<n;i++)
                cout << dpb[i] << " ";
       cout << endl; 
        int shift = 0;
        int swap = 0;
        int move = 0;
        for(i=0;i<m;i++) {
                while(dpa[i] == 1 && b[i] != a[i]){
                        sl(dpb, i, 2001);
                        shift--;
                        move++;
                }
                if (dpa[i] == 0 && dpb[i] == 1){
                        sr(dpb, i, 2001);
                        shift++;
                        move++;
                }else if (dpa[i] == 0 && dpb[i] == 0)
                        swap++;
        }
        cout << "shifts = " << move << " swaps = " << swap << endl;
        cout << abs(m - (n+shift)) + swap + move;
}
void sr(int y[], int x, int n) {
        for(int i=n-1; i>x; i--)
                y[i] = y[i-1];
}

void sl(int y[], int x, int n) {
        for(int i=x; i<n; i++)
                y[i] = y[i+1];
}
//..
*/
/*
// NDE../
//
// A sequence is given, it is necessary to find the length
// of its largest increasing subsequence. A sub-sequence is a
// set of its elements, not necessarily standing in a row.
int a[1000];
int dp[1000];
int dpp[1000];

signed main() {
        int i, k;
        int n;
        int last, lasti;
        freopen("input.txt","r",stdin);
        cin >> n;
        memset(dp,0,sizeof(dp));
        for(i=0;i<n;i++)
                cin >> a[i];

        dp[0] = 1;
        for(i=1;i<n;i++) {
                int max = 1;
                int maxi = 0;
                for(k = i-1; k>=0; k--) {
                        if(a[i] == a[k]) 
                                if(dp[k] > max) {
                                        max = dp[k];
                                        maxi = k;
                                }

                        if(a[i] > a[k]) {
                                if(dp[k]+1 > max){
                                        max = dp[k]+1;
                                        maxi = k;
                                }
                        }

                }
                dp[i] = max;
                dpp[i] = maxi;
        }
        int max=0;
        int maxi=0;

        for(i=0;i<n;i++) {
                if(dp[i] > max) {
                        max = dp[i];
                        maxi = i;
                }
        }
        int start;
        vector<int> ans;
        for(start = maxi; dp[start] != 1; start = dpp[start]) 
                if(ans.size() == 0 || a[start] != ans[ans.size()-1])
                        ans.push_back(a[start]);
        ans.push_back(a[start]);
        for(i=ans.size()-1;i>=0;i--)
                cout << ans[i] << " ";

}
//..
*/
/*
// A minimum of items../
// Given N items of mass m1,...,mn they are to fill a backpack that can hold
// no more than M mass. How can you get exactly mass M using the fewest number
// of items
//
// Input:
//   the first line of input contains the natural number n not exceeding 100
//   and natural number M not exceeding 1000. In the second line there are
//   mi natural numbers not exceeding 100
//
// Output:
//   print the smallest number of items or 0 if the weight cannot be achieved.
//      0...............M
//   m0 *****************
//   m1 *****************
//   m2 *****************
//   m3 *****************
//   m4 *****************
//   m5 *****************
//   m6 *****************
//

int dp[101][10001];
int a[101];

signed main()
{
        int n, m;
        int i, j;
        int mn = INF;
//        freopen("input.txt","r",stdin);
        cin >> n >> m;

       for(i=1;i<=n;i++)
              cin >> a[i]; 

       for(i=1;i<=n;i++) {
               for(j=1;j<=m;j++) {
                       int offset = j - a[i];
                       dp[i][j] = dp[i-1][j];
                       if(offset > 0)

                               if(dp[i-1][offset] != 0 &&
                                  (dp[i][j] == 0 || dp[i-1][offset] + 1 < dp[i][j]))
                                       dp[i][j] = dp[i-1][offset] + 1;
                       
                       if(j == a[i])
                               dp[i][j] = 1;
               }
       }

       cout << dp[n][m];
}
//..
*/
/*
// Backpack: the largest weight../
// Given N gold bars of weight m1,..,mn. They are to fill a backpack that can withstand
// no more than M. What is the largest mass you can carry in this backpack?
//
// Input : n <= 100 m <= 10000
//         n lines
//
// Output: max mass
//
int dp[101][10001];
int a[101];

signed main() {
        int n, m;
        int i, j;

//        freopen("input.txt","r",stdin);

        cin >> n >> m;
        for(i=1;i<=n;i++)
                cin >> a[i];

       for(i=1;i<=n;i++) {
               for(j=1;j<=m;j++) {
                       int offset = j - a[i];
                       dp[i][j] = dp[i-1][j];
                       if(offset > 0)

                               if(dp[i-1][offset] != 0 &&
                                  (dp[i][j] == 0 || dp[i-1][offset] + 1 < dp[i][j]))
                                       dp[i][j] = dp[i-1][offset] + 1;
                       
                       if(j == a[i])
                               dp[i][j] = 1;
               }
       }
       for(i=m;i>0;i--)
               if (dp[n][i] != 0) {
                       cout << i;
                       return 0;
               }
       cout << 0;
}
//..
*/
/*
// ATM../
// in some states certain denominations are in circulation
// the National bank wants the ATM to issue any requested amount
// with a minimum number of banknotes, assuming, the number
// of each denomination of banknotes is unlimited.
//
// help the national bank solve this problem
//
// Input:
//   The first line of input contains the natural number N not exceeding 100
//   number of banknotes in circulation. the second line of input contains n
//   different natural numbers. x1, x2,...xn not exceeding 10^6 denominations
//   of banknotes. The third line contains the natural number S not exceeding
//   10^6 
//
// Output:
//   The program must find the number S as a sum of the totals from the set
//   xi, containing the minimum number of components and displaying this view.
//   (as a sequence of numbers seperated by spaces). If there is more than one
//   answer, the program should output any (one) of them. If such a view does
//   not exist, the program should output the string No solution.
//

int dp[1000001];
int dpp[1000001];
int a[101];

signed main()
{
        int n, m;
        int i, j;
        //freopen("input.txt","r",stdin);
        cin >> n;

       for(i=1;i<=n;i++)
              cin >> a[i]; 

       cin >> m;

       for(i=1;i<=n;i++) {
               for(j=1;j<=m;j++) {
                       int offset = j - a[i];

                       if(offset > 0)
                      
                               if(dp[offset] != 0 &&
                                  (dp[j] == 0 || dp[offset] + 1 < dp[j])){
                                       dp[j] = dp[offset] + 1;
                                       dpp[j] = a[i];
                               }

                       if(j == a[i]){
                               dp[j] = 1;
                               dpp[j] = a[i];
                       }
              
               }
       }

       if(dp[m] == 0)
               cout << "No solution";
       else {
               int start = dpp[m];
               int total = m;
               while (total != 0) {
                       cout << start << " ";
                       total -= start;
                       start = dpp[total];
               }
       }
}
//..
*/
/*
// Maze escape../
//
// there is a maze on a grid, adjacent rooms have
// doors, some rooms are closed. traversal is
// from top left corner to bottom right
// can visit no more than n+m-1 rooms 
// should only move right or down
// determine number of routes to the exit
//
int a[1000][1000];
int d[1000][1000];
signed main(){
        int i,j;
        int n,m;

        freopen("input.txt","r",stdin);
        cin >> n >> m;

        for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                        cin >> a[i][j];
                        d[i][j] = 0;
                }
        }

        d[0][0] = 1;

        for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                        if(i>0 && a[i-1][j] != 0)
                                d[i][j] += d[i-1][j];
                        if(j>0 && a[i][j-1] != 0)
                                d[i][j] += d[i][j-1]; 
                }
        }
        if(d[n-1][m-1]==0)
                cout << "Impossible";
        else
                cout << d[n-1][m-1];
}
//..
*/
/*
// Knight's move../
//
// Given a rectangular board n x m n rows and m columns
// In the upper left corner is a horse which you want to move
// to the lower left corner. The horse can only jump diagonally
// down
//
// Determine how many routes are possible
//
// 012345
// 0...... 
// 1..x...
// 2.x....
// 3......
int d[50][50];
        int n, m;

bool jump(){
        // ../
        int i, j;
        bool flag = false;
        for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                        if(d[i][j] != -1)
                                continue;
                        if(
                                ((i > 0 && j > 1) &&
                                d[i-1][j-2]==-1) ||

                                ((i > 1 && j > 0) &&
                                d[i-2][j-1]==-1) ||

                                ((i > 1 && j < n-1) &&
                                d[i-2][j+1]==-1) ||

                                ((i < n-1 && j > 1) &&
                                d[i+1][j-2]==-1)
                          )
                        {
                                flag = true;
                                continue;
                        }

                        d[i][j] = 0;
                        if (i > 0 && j > 1)
                                d[i][j] += d[i-1][j-2];

                        if(i > 1 && j > 0)
                                d[i][j] += d[i-2][j-1];

                        if(i > 1 && j < n-1)
                                d[i][j] += d[i-2][j+1];

                        if(i < n-1 && j > 1)
                                d[i][j] += d[i+1][j-2];

                }
        }
        return flag;
        //..
}
signed main(){
        int i,j;

        cin >> m >> n;

        for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                        d[i][j] = -1;
                }
        }

        d[0][0] = 1;
        while(jump());
        
        cout << d[n-1][m-1];
}
//..
*/
/*
// Pascal's Triangle../
//
// Pascals traingle is constructed as follows
//
// the first line contains a single number 1
// each line contains an additional number which is the
// sum of the previous numbers above it.
//
// given n print pascals triangle
//
int dp[1000][1000];
        int n;

bool isprime(int n) {
        if(n<2)
                return false;
        if(n==2)
                return true;
        for(int i=2; i*i<n;i++)
                if(n%i == 0)
                        return false;
        return true;
}

void out(int n){
                if(isprime(n)) { cout << 1;}
                else cout << 0;
//        if(n%2)
 //               cout << ". ";
  ///      else
    //            cout << "0 ";
}
void down() {
        int i,j;
        for(i=2;i<=n;i++){
                out(dp[i][0]);
                for(j=1;j<i-1;j++){
                        dp[i][j] = dp[i-1][j] + dp[i-1][j-1];
                        out(dp[i][j]);
                }
                out(dp[i][i-1]);
                cout << endl;
        }
}

void up(){
        int i,j;
        for(i=n-1;i>0;i--){
                for(j=0;j<i;j++){
                        dp[i][j] = abs(dp[i+1][j] - dp[i+1][j+1]);
                        out(dp[i][j]);
                }
                cout << endl;
        }
}


signed main() {
        int i,j;
        int m;

        cin >> n >> m;

        for(i=1;i<=n;i++){
                dp[i][0] = 1;
                dp[i][i-1] = 1;
        }
        cout << 1 << endl;
        while(m--){
                down();
                up();
        }
}
//..
*/
/*
//  cute array traversal../
//
// Given a rectangular board n x m n rows and m columns
// In the upper left corner is a horse which you want to move
// to the lower left corner. The horse can only jump diagonally
// to the right
//
// Determine how many routes are possible
//
//   54321
// 8 00000
// 7 12222
// 6 13444
// 5 13566
// 4 13578
// 3 13579
// 2 13579
// 1 13579
signed main(){
        int i,j;
        int n, m;
        int d[50][50];

        cin >> n >> m;


        for(i=1;i<n;i++){
                for(j=0;j<m;j++){
                        d[i][j] = 0;
                }
        }

        d[0][0] = 1;

        int x = 0;
        int y = 0;
        bool toggle = false;
        int k = 0;
        while(x!=n && y!=m){
                if(toggle) {
                        while(x<n){
                                d[x][y] = k;
                                x++;
                        }
                        x = ++y;
                } else {
                        while(y<m){
                                d[x][y] = k;
                                y++;
                        }
                        y = x++;
                }
                toggle = !toggle;
                k++;
        }
        for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                        cout << d[i][j] << " ";
                }
                cout << endl;
        }
}
//..
*/
/*
// Knight's move../
//
// Given a rectangular board n x m n rows and m columns
// In the upper left corner is a horse which you want to move
// to the lower left corner. The horse can only jump diagonally
// down
//
// Determine how many routes are possible
//
signed main(){
        int i,j;
        int n, m;
        int d[50][50];

        cin >> n >> m;

        for(i=1;i<n;i++){
                for(j=0;j<m;j++){
                        d[i][j] = 0;
                }
        }

        d[0][0] = 1;

        for(i=1;i<n;i++){
                for(j=1;j<m;j++){
                        if(j>1) 
                                d[i][j] += d[i-1][j-2];
                        if(i>1)
                                d[i][j] += d[i-2][j-1];
                }
        }

        cout << d[n-1][m-1];
}
//..
*/
/*
// Checkers - to king../
//
// on the chessboard (8x8) is a piece on the white squares
// how may ways can it get to the top. 
//
// (The checker moves diagonally one square up right or up left
// 
// Input:
// checkers origin
//   two numbers between 1 and 8 - column number counting left
//                               - row number counting from bottom
//
// x+1 y-1
//  87654321
// 8xxx0x0x0
// 7xxxx0x0x
// 6xxxxx0xx
// 5xxxxxxxx
// 4xxxxxxxx
// 3xxxxxxxx
// 2xxxxxxxx
// 1xxxxxxxx
signed main() {
        int i, j;
        int x, y;
        int n = 8;
        int d[8][8];

        cin >> y >> x;
        x--;y--;

        for(i=0;i<n;i++)
                for(j=0;j<n;j++)
                        d[i][j] = 0;

        d[x][y] = 1;

        for(i=1;i<n;i++){
                for(j=0;j<n;j++){
                       if(j>0)
                              d[i][j] += d[i-1][j-1];
                       if(j<n-1)
                              d[i][j] += d[i-1][j+1];
                }
        }

        int ans = 0;
        for(i=0;i<n;i++)
                ans += d[n-1][i];
        cout << ans;
}

//..
*/
/*
// Number of routes in a rectangular table../
//
// With a rectangular table of size n x m the player
// starts at the upper left cell in one move he is allowed
// to move right or down
//
// Calculate the number of ways the player has to get to the
// lower right cell
//
// input :
//  1 <= n, m <= 10
//
//
//
int dp[10][10];

signed main() {
        int i, j;
        int n, m;

        //freopen("input.txt","r",stdin);

        cin >> n >> m;

        for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                        dp[i][j] = 0;
                }
        }

        dp[0][0] = 1;

        for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                        if(i>0)
                                dp[i][j] += dp[i-1][j];
                        if(j>0)
                                dp[i][j] += dp[i][j-1];
                }
        }


        cout << dp[n-1][m-1] << endl;
}
//..        
*/
/*
/// Buyin tickets../
//
// for tickets to the new musical there is a line of n people
// each of which wants to buy 1 ticket
// There was only one ticket office, ticket sales were very slow
// and guests were in despair
// The smartest quickly noticed that, as a rule tickets are sold
// faster when they are sold at the same time by one cashier
// So they suggested a few good people in a row give the money to
// the first of them for them to buy tickets for all
// 
// However to combat speculators the cashiers sold no more than
// 3 tickets at a time so only two or three people in a row could
// agree in this way
//
// The time it takes a cashier to sell 3 tickets is given respectively
// by Ai Bi Ci
//
// note a group of united people always buys the first of them
// no extra tickets are purchased
//
// Input:
//   N -  number of buyers in the line 1 <= n <= 5000
//     - N triples of natural numbers the amount of time for the cashier
//       to sell that person 1, 2 or 3 tickets
//
// Output:
//  Minimum amount of time to sell all tickets
//

int a[5000][3];
int dp[5000];
signed main() {
        int i;
        int n;

        //freopen("input.txt","r",stdin);

        cin >> n;

        for(i=0;i<n;i++) {
                cin >> a[i][0] >> a[i][1] >> a[i][2];
                dp[i] = INF;
        }

        dp[0] = a[0][0]; // sale of 1 ticket..
        dp[1] = a[0][1];
        dp[2] = a[0][2];

        for(i=1;i<n;i++){
                if (dp[i-1] + a[i][0] < dp[i])
                        dp[i] = dp[i-1] + a[i][0]; // sale of i + 1 tickets
                if (i < n-1 && dp[i-1] + a[i][1] < dp[i+1])
                        dp[i+1] = dp[i-1] + a[i][1]; // sale of i + 2 tickets
                if (i < n-2 && dp[i-1] + a[i][2] < dp[i+2])
                        dp[i+2] = dp[i-1] + a[i][2]; // sale of i + 3 tickets
        } 

        cout << dp[n-1];
}
//..
*/
/*
// random dynamic problem../
int dp[1000001];
string dp2[1000001];

void dostuff(int n);

signed main() {
        for(int i=242682; i < 1000000;i++) {
                cout << i + 1 << ": ";
                dostuff(i+1);
        }}

void dostuff(int n) {
        // int n;
        // cin >> n;
        //    
        dp[1] = 0;

        string st = "";
        dp2[1] = "";

        for(int i=2; i<=n; i+=1) {
                int a, b, c;

                a = INF;
                b = INF;
                c = dp[i-1] + 1;

                if (i % 3 == 0)
                        a =  dp[i/3] + 1;
                if (i % 2 == 0)
                        b = dp[i/2] + 1;
                if ((c < a) && (c < b)) {
                        dp[i] = c;
                        dp2[i] = dp2[i-1] + '1';
                } else if (a < b) {
                        dp[i] = a;
                        dp2[i] = dp2[i/3] + '3';
                } else {
                        dp[i] = b;
                        dp2[i] = dp2[i/2] + '2';
                }
        }

        cout << dp2[n] << endl;
}
//..
 */       
/*
// Durak../
// The game is played with a deck of 36 cards
// each card is presented as a string of two characters
// the first symbol is rank and the second is suit
//
// Find if a player with a set of N cards will be able to hit M cards
// In order to win the player must cover each of the cards,
// cards can be covered with either a senior card of the same suit
// or a trump card
// If the card is itself a trump card, it can only be covered bt the
// highest trump card
// 
//
struct hand {
        vector<int> s, d, c, h;
} p1, p2;

void player_input(hand &p1, int);
void sort_cards(hand &p1);
void place_trump(vector<int> &p1, vector<int> p2);
void place_card(vector<int> &p1, vector<int> p2, vector<int> &p1t);

signed main() {
        int i, j;
        int n, m;
        char trump_suit;

        freopen("input.txt","r",stdin);

        cin >> n >> m >> trump_suit;

        player_input(p1, n);
        player_input(p2, m);

        sort_cards(p1);
        sort_cards(p2);

        switch (trump_suit) {
                case 'H':
                        place_trump(p1.h, p2.h);
                        place_card(p1.s, p2.s, p1.h);
                        place_card(p1.c, p2.c, p1.h);
                        place_card(p1.d, p2.d, p1.h);
                        break;
                case 'S':
                        place_trump(p1.s, p2.s);
                        place_card(p1.h, p2.h, p1.s);
                        place_card(p1.c, p2.c, p1.s);
                        place_card(p1.d, p2.d, p1.s);
                        break;
                case 'D':
                        place_trump(p1.d, p2.d);
                        place_card(p1.s, p2.s, p1.d);
                        place_card(p1.c, p2.c, p1.d);
                        place_card(p1.h, p2.h, p1.d);
                        break;
                case 'C':
                        place_trump(p1.c, p2.c);
                        place_card(p1.s, p2.s, p1.c);
                        place_card(p1.d, p2.d, p1.c);
                        place_card(p1.h, p2.h, p1.c);
        }
        cout << "YES";
}

        void place_card(vector<int> &p1, vector<int> p2, vector<int> &p1t) {
                vector<int>::iterator it, it2;
                bool flag;
                
                for(it = p2.begin(); it != p2.end(); ++it) {
                        flag = false;
                        for(it2 = p1.begin(); it2 != p1.end(); ++it2) {
                                if(*it2 > *it) {
                                        p1.erase(it2);
                                        flag = true;
                                        break;
                                }
                        }
                        if (!flag) {
                                if(p1t.empty()) {
                                        cout << "NO";
                                        exit(0); 
                                } else {
                                        p1t.pop_back();
                                }
                        }
                }
        }


        void place_trump(vector<int> &p1, vector<int> p2) {
                vector<int>::iterator it, it2;
                bool flag;

                for(it = p2.begin(); it != p2.end(); ++it) {
                        flag = false;
                        for(it2 = p1.begin(); it2 != p1.end(); ++it2) {
                                if(*it2 > *it ){
                                        p1.erase(it2);
                                        flag = true;
                                        break;
                                }
                        }
                        if (!flag) {
                                cout << "NO";
                                exit(0);
                        }
                }              
        }

void sort_cards(hand &p1) {
        sort(p1.s.begin(),p1.s.end());
        sort(p1.h.begin(),p1.h.end());
        sort(p1.c.begin(),p1.c.end());
        sort(p1.c.begin(),p1.c.end());
}

void player_input(hand &p1, int n) {
        int i;
        string input;

        for(i=0;i<n;i++) {
                int num;

                cin >> input;
                switch (input[0]) {
                        case 'A':
                                num = 14;
                                break;
                        case 'K':
                                num = 13;
                                break;
                        case 'Q':
                                num = 12;
                                break;
                        case 'J':
                                num = 11;
                                break;
                        case 'T':
                                num = 10;
                                break;
                        default:
                                num = input[0] - 48;
                }

                switch (input[1] ) {
                        case 'S':
                                p1.s.push_back(num);
                                break;
                        case 'C':
                                p1.c.push_back(num);
                                break;
                        case 'H':
                                p1.h.push_back(num);
                                break;
                        case 'D':
                                p1.d.push_back(num);
                }
        }

}
//..
*/
/*
// Puzzle../
// Petya solves the puzzle which is arranged as follows.
// NxN square size table is given
// Every cell with a Latin letter written in it.
// In addition, there is a list of keywords.
// Petya needs to take a keyword and find it in the table.
// That is, find in the table all the letters of this word arranged
// that the cell containing each subsequent letter is
// adjacent to the cell containing the previous letter
//
// When Petya finds a word, he crosses it off the table
// you can't use already crossed-out letters in other keywords
// After all the keywords were found and deleted,
// there are still a few letters left in the table
//
// what are the letters?
//
// Input:
//
// 1 <= N <= 10 ( size of matrix )
// 1 <= M <= 100 (# of keywords)
// NxN matrix of characters
// M keywords
//
// Output:
// alphabetical order the remaining letters in the table
vector<string> mtrx;
vector<string> keywords;

int n, m;
void xout(char a);

signed main(){
        int i, j;
        freopen("input.txt","r",stdin);
        cin >> n >> m;

        mtrx.resize(n);
        for(i=0;i<n;i++)
                cin >> mtrx[i];

        keywords.resize(m);
        for(i=0;i<m;i++)
                cin >> keywords[i];

        for(i=0;i<m;i++)
                for(j=0;j<keywords[i].size();j++)
                        xout(keywords[i][j]);
        char ans[100];
        for(i=0;i<100;i++)
                ans[i] = '?';

        int k=0;
        for(i=0;i<n;i++)
                for(j=0;j<n;j++)
                        if(mtrx[i][j] != '?')
                                ans[k++] = (mtrx[i][j]);
        
        sort(ans, ans + 100*sizeof(char));

        for(i=0;i<100;i++)
                if(ans[i]!='?')
                        cout << ans[i];
}

void xout(char a) {
        int i, j;
        for(i=0;i<n;i++)
                for(j=0;j<n;j++)
                        if(mtrx[i][j] == a) {
                                mtrx[i][j] = '?';
                                return;
                        }
}
//..
*/
/*
// largest number../
// create largest possible number out of a string of numbers
vector<string> S;

bool cmp(string, string);

signed main() {
        int n;
        int i;

        freopen("input.txt","r",stdin);

        cin >> n;

        S.resize(n);
        for(i=0;i<n;i++)
                cin >> S[i];

        sort(S.begin(), S.end(), cmp);

        for(i=0;i<n;i++)
                cout << S[i];
}

bool cmp(string a, string b) {
        string x, y;
        if (a.size() == b.size())
                return a > b;
        x = a.append(b);
        y = b.append(a);
        return x > y;
}
//..
*/
/*
// Node travel../
vector<string> mtrx;
int delta[8][2] = { {0, 1}, {1, 0}, {0, -1}, {-1, 0},
                    {1, 1}, {1, -1}, {-1, 1}, {-1, -1} };
int n, m;
int coords[1001][2];
double d[75][75];
set< pair< double, pair<int, int> > > q;

void dj(int a, int b);
bool correct(int x1, int y1, int x2, int y2);


signed main() {
        int i, j;
        int k;
        double s;
        int a, b;
        
        freopen("input.txt","r",stdin);

        cout << fixed << setprecision(3);

        cin >> n >> m >> k >> s;
        swap(n, m);

        mtrx.resize(n);
        for(i=0;i<n;i++)
                cin >> mtrx[i];

        cin >> b >> a;
        a--; b--;
        coords[0][0] = a;
        coords[0][1] = b;

        for(i=1;i<=k;i++) {
                cin >> b >> a;
                a--; b--;
                coords[i][0] = a;
                coords[i][1] = b;
        }
        double ans = 0;
        for(i=0;i<k;i++) {
                a = coords[i][0];
                b = coords[i][1];

                dj(a, b);

                a = coords[i+1][0];
                b = coords[i+1][1];

                ans += d[a][b];
        }
        cout << ans / s;
}

void dj(int a, int b) {
        int i, j;
        pair<int, int> v;

        int vx, vy, tox, toy;

        for(i=0;i<n;i++)
                for(j=0;j<m;j++)
                        d[i][j] = INF;
        d[a][b] = 0;

        q.insert(make_pair(0, make_pair(a, b)));

        while(!q.empty()) {
                v = q.begin()->second;
                vx = v.first;
                vy = v.second;
                q.erase(q.begin());

                for(i=0;i<8;i++) {

                        if(!correct(v.first, v.second, delta[i][0], delta[i][1]))
                                continue;

                        tox = vx + delta[i][0];
                        toy = vy + delta[i][1];

                        if(i < 4) {
                                if (d[vx][vy] + 1 < d[tox][toy]) {
                                        q.erase( make_pair( d[tox][toy], make_pair(tox, toy) ));
                                        d[tox][toy] = d[vx][vy] + 1;
                                        q.insert( make_pair( d[tox][toy], make_pair(tox, toy) ));
                                }
                        } else {
                                if (d[vx][vy] + sqrt(2) < d[tox][toy]) {
                                        q.erase( make_pair( d[tox][toy], make_pair(tox, toy) ));
                                        d[tox][toy] = d[vx][vy] + sqrt(2);
                                        q.insert( make_pair( d[tox][toy], make_pair(tox, toy) ));
                                }
                        }
                }
        }
}

bool correct(int x1, int y1, int x2, int y2) {
        int x, y;

        x = x1 + x2;
        y = y1 + y2;

        if (x < 0)
                return false;
        if (x >= n)
                return false;
        if (y < 0)
                return false;
        if (y >= m)
                return false;
        if (mtrx[x][y] == '#')
                return false;

        return true;
}
//..
*/
/*
//Snake../
int a[100][100];
int n;

bool correct(int i) {
        return (i >= 0) && (i < n);
}

signed main() {
        int i, j;
        int x, y, d;

        cin >> n;

        x = 0;
        y = 0;
        d = 1;
        for(i=0;i<n*n;i++) {
                a[x][y] = i+1;
                x += d;
                y -= d;
                if(!correct(x) && !correct(y)) {
                        d*=-1;
                        if(y < 0) {
                                y+=2;
                                x--;
                        } else {
                                x+=2;
                                y--;
                        }
                } else if (!correct(x)) {
                        d*=-1;
                        if(x==n)
                                y+=2;
                        x+=d;
                } else if (!correct(y)) {
                        d*=-1;
                        if(y==n)
                                x+=2;
                        y-=d;
                }
        }

        for(i=0;i<n;i++) {
                for(j=0;j<n;j++)
                        cout << a[i][j] << " ";
                cout << endl;
        }
}
//..
*/
/*
// Computational Biology../
signed main() {
        string a, b;
        int ap;
        int i;

        freopen("input.txt","r",stdin);
        cin >> a >> b;

        ap = 0;
        for(i=0;i<b.length();i++) {
                if(a[ap] == b[i])
                        ap++;
                if (ap == a.length()) {
                        cout << "YES";
                        return 0;
                }
        }
        cout << "NO";
}
//..
*/
/*
// Hairy business../
//
signed main() {
        int i, j;
        int n;
        int a[100], b[100];
        int ans;

//        freopen("input.txt","r",stdin);

        cin >> n;
        for(i=0;i<n;i++) {
                cin >> a[i];
                b[i] = 0;
        }

        for(i=n-1;i>=0;i--)
                for(j=i+1;j<n;j++)
                        if(a[j] > a[i]) {
                                b[i] = 1;
                                break;
                        }

        j=-1;
        ans=0;
        for(i=0;i<n;i++) {
                cout << b[i] << " ";
                if(b[i] == 0) {
                        ans += a[i] * (i-j);
                        j=i;
                }
        }
        cout << endl << ans;
}
//..
*/
