#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/* Example instructions.txt content:
 *
 * 3323332133321332123131121221222
 *
 * This file should be placed in the same directory as the program.
 */

/* Enumerations for directions and instruction types */
enum { NORTH, WEST, SOUTH, EAST };
enum { CREATE, ROTATE, MOVE };

/* Structure representing a node in the Rock-Paper-Scissors (RPS) grid */
struct rps_node {
    int x, y;                    // Coordinates of the node
    int type;                    // Type of the node (Rock=11, Paper=01, Scissors=10)
    struct rps_node *north;      // Pointer to the node to the north
    struct rps_node *south;      // Pointer to the node to the south
    struct rps_node *east;       // Pointer to the node to the east
    struct rps_node *west;       // Pointer to the node to the west
    struct rps_node *next;       // Pointer to the next node in the linked list
};

/* Structure representing an instruction node */
struct ins_node {
    int type;                    // Type of instruction (CREATE, ROTATE, MOVE)
    int value;                   // Value associated with the instruction
    struct ins_node *next;       // Pointer to the next instruction node
};

/* Function Prototypes */
uint8_t solve(uint8_t linker, uint8_t ofs_a, uint8_t linked, uint8_t ofs_b);
uint8_t boolean_solve(uint8_t input, uint8_t mech);

void print_linked_list(struct rps_node *node);
struct rps_node* make_origin(int type);
struct rps_node* create_node(struct rps_node* current_node, struct rps_node* selected_node, int direction, int type);
struct rps_node* find_last(struct rps_node* start_node, int direction);
struct rps_node* add_node(struct rps_node* creation_point, struct rps_node* current_node, int direction, int type);
struct ins_node* get_instructions();
struct ins_node* instruction_switch(int char_read, struct ins_node* instruction_list, FILE* file_ptr);
struct ins_node* add_instruction(struct ins_node* instruction_list, int type, int value);
struct ins_node* pop_instruction(struct ins_node* instruction_list);
int rotate_right(int direction);
struct rps_node* move_node(struct rps_node* selected_node, int direction);

void iterate(struct rps_node* current_node);
void one_adjacent(struct rps_node* current_node);
struct rps_node* farthest_node(struct rps_node* current_node, int direction);
int node_direction(struct rps_node* current_node);
void directional_solve(struct rps_node* current_node, struct rps_node* node2, int direction);
void two_symmetric_adjacent(struct rps_node* current_node);
void two_asymmetric_adjacent(struct rps_node* current_node);
void three_adjacent(struct rps_node* current_node);
int null_direction(struct rps_node* current_node);
int node_direction2(struct rps_node* current_node, int prev_direction);
void four_adjacent(struct rps_node* current_node);
int get_case(struct rps_node* current_node);
int calculate_case(int adjacents);
void print_structure(struct rps_node* node);

/* Main Function */
int main() {
    struct rps_node* current_node;
    struct rps_node* selected_node;
    struct ins_node* instruction_list;
    int direction;
    int i;

    /* Get instructions from the instructions.txt file */
    instruction_list = get_instructions();

    /* Create the origin node with the initial type from instructions */
    current_node = make_origin(instruction_list->value);
    selected_node = current_node;

    /* Remove the first instruction as it's already used */
    instruction_list = pop_instruction(instruction_list);
    direction = NORTH;

    /* Process the instruction list */
    while (instruction_list) {
        switch (instruction_list->type) {
            case CREATE:
                current_node = create_node(current_node, selected_node, direction, instruction_list->value);
                break;
            case ROTATE:
                direction = rotate_right(direction);
                break;
            case MOVE:
                selected_node = move_node(selected_node, direction);
                break;
        }
        instruction_list = pop_instruction(instruction_list);
    }

    /* Print the initial structure */
    print_structure(current_node);

    /* Iterate over the grid and apply the RPS rules */
    for (i = 0; i < 9400; i++) {
        switch (selected_node->type - 1) {
            case CREATE:
                current_node = create_node(current_node, selected_node, direction, selected_node->type);
                break;
            case ROTATE:
                direction = rotate_right(direction);
                break;
            case MOVE:
                selected_node = move_node(selected_node, direction);
                break;
            default:
                printf("Error: Invalid type\n");
        }
        iterate(current_node);
    }

    /* Print the final structure after iterations */
    print_structure(current_node);
    return 0;
}

/* Function Definitions */

/**
 * Solves the interaction between two cells based on their types.
 */
uint8_t solve(uint8_t linker, uint8_t ofs_a, uint8_t linked, uint8_t ofs_b) {
    uint8_t a, b, sol, ret;

    a = (linker >> (ofs_a * 2)) & 3;
    b = (linked >> (ofs_b * 2)) & 3;

    sol = boolean_solve(a, b);
    sol = sol << (ofs_b * 2);
    linked = linked & ~(3 << (ofs_b * 2));
    ret = sol | linked;

    return ret;
}

/**
 * Performs the Rock-Paper-Scissors logic between two cell types.
 */
uint8_t boolean_solve(uint8_t input, uint8_t mech) {
    uint8_t tmp, tmp2, tmp3;

    tmp = input ^ mech;
    tmp3 = (tmp >> 1) | (tmp << 1) | tmp;
    tmp2 = tmp ^ tmp3;
    tmp2 = tmp2 ^ tmp3;
    tmp = ~tmp3 & input;
    return (tmp ^ tmp2) & 3;
}

/**
 * Iterates over the grid and updates each node's type based on the RPS rules.
 */
void iterate(struct rps_node* current_node) {
    struct rps_node* node = current_node;
    uint8_t original_type;
    uint8_t updated_types[5000]; // Adjust size as needed
    int index = 0;

    /* First pass: compute new types and store them */
    while (current_node) {
        original_type = current_node->type;
        switch (get_case(current_node)) {
            case 1:
                one_adjacent(current_node);
                break;
            case 2:
                two_symmetric_adjacent(current_node);
                break;
            case 3:
                two_asymmetric_adjacent(current_node);
                break;
            case 4:
                three_adjacent(current_node);
                break;
            case 5:
                four_adjacent(current_node);
                break;
            default:
                break;
        }
        updated_types[index++] = current_node->type;
        current_node->type = original_type; // Reset to original type
        current_node = current_node->next;
    }

    /* Second pass: update types with the computed new types */
    index = 0;
    while (node) {
        node->type = updated_types[index++];
        node = node->next;
    }
}

/**
 * Updates the node's type when it has only one adjacent node.
 */
void one_adjacent(struct rps_node* current_node) {
    int direction = node_direction(current_node);
    struct rps_node* far_node = farthest_node(current_node, direction);
    directional_solve(current_node, far_node, direction);
}

/**
 * Updates the node's type when it has two symmetric adjacent nodes.
 */
void two_symmetric_adjacent(struct rps_node* current_node) {
    if (current_node->west == NULL)
        current_node->type = boolean_solve(current_node->north->type, current_node->south->type);
    else
        current_node->type = boolean_solve(current_node->east->type, current_node->west->type);
}

/**
 * Updates the node's type when it has two asymmetric adjacent nodes.
 */
void two_asymmetric_adjacent(struct rps_node* current_node) {
    int direction = node_direction(current_node);
    struct rps_node* far_node = farthest_node(current_node, direction);
    directional_solve(current_node, far_node, direction);
    uint8_t temp_solution = current_node->type;

    direction = node_direction2(current_node, direction);
    far_node = farthest_node(current_node, direction);
    directional_solve(current_node, far_node, direction);

    current_node->type = boolean_solve(current_node->type, temp_solution);
}

/**
 * Updates the node's type when it has three adjacent nodes.
 */
void three_adjacent(struct rps_node* current_node) {
    int direction = null_direction(current_node);
    struct rps_node* far_node = farthest_node(current_node, direction);
    directional_solve(current_node, far_node, direction);
    uint8_t temp_solution;

    if (direction == NORTH || direction == SOUTH) {
        temp_solution = boolean_solve(current_node->east->type, current_node->west->type);
    } else {
        temp_solution = boolean_solve(current_node->north->type, current_node->south->type);
    }
    current_node->type = boolean_solve(current_node->type, temp_solution);
}

/**
 * Updates the node's type when it has four adjacent nodes.
 */
void four_adjacent(struct rps_node* current_node) {
    uint8_t solution1 = boolean_solve(current_node->east->type, current_node->west->type);
    uint8_t solution2 = boolean_solve(current_node->north->type, current_node->south->type);
    current_node->type = boolean_solve(solution1, solution2);
}

/**
 * Determines the direction of the first adjacent node found.
 */
int node_direction(struct rps_node* current_node) {
    if (current_node->west != NULL)
        return WEST;
    if (current_node->east != NULL)
        return EAST;
    if (current_node->north != NULL)
        return NORTH;
    if (current_node->south != NULL)
        return SOUTH;
    return -1;
}

/**
 * Determines the direction of the next adjacent node, excluding the previous one.
 */
int node_direction2(struct rps_node* current_node, int prev_direction) {
    if (current_node->west != NULL && prev_direction != WEST)
        return WEST;
    if (current_node->east != NULL && prev_direction != EAST)
        return EAST;
    if (current_node->north != NULL && prev_direction != NORTH)
        return NORTH;
    if (current_node->south != NULL && prev_direction != SOUTH)
        return SOUTH;
    return -1;
}

/**
 * Determines the direction where there is no adjacent node.
 */
int null_direction(struct rps_node* current_node) {
    if (current_node->west == NULL)
        return EAST;
    if (current_node->east == NULL)
        return WEST;
    if (current_node->north == NULL)
        return SOUTH;
    if (current_node->south == NULL)
        return NORTH;
    return -1;
}

/**
 * Finds the farthest node in the given direction from the current node.
 */
struct rps_node* farthest_node(struct rps_node* current_node, int direction) {
    struct rps_node* traversal_node;

    while (current_node) {
        traversal_node = current_node;
        switch (direction) {
            case NORTH:
                current_node = current_node->north;
                break;
            case SOUTH:
                current_node = current_node->south;
                break;
            case EAST:
                current_node = current_node->east;
                break;
            case WEST:
                current_node = current_node->west;
                break;
        }
    }
    return traversal_node;
}

/**
 * Applies the RPS logic between the current node and another node in a specific direction.
 */
void directional_solve(struct rps_node* current_node, struct rps_node* node2, int direction) {
    switch (direction) {
        case SOUTH:
            current_node->type = boolean_solve(current_node->south->type, node2->type);
            break;
        case NORTH:
            current_node->type = boolean_solve(current_node->north->type, node2->type);
            break;
        case EAST:
            current_node->type = boolean_solve(current_node->east->type, node2->type);
            break;
        case WEST:
            current_node->type = boolean_solve(current_node->west->type, node2->type);
            break;
    }
}

/**
 * Determines the case based on the number of adjacent nodes.
 */
int get_case(struct rps_node* current_node) {
    int adjacents = 4;

    if (current_node->west != NULL)
        adjacents--;
    if (current_node->north != NULL)
        adjacents--;
    if (current_node->south != NULL)
        adjacents--;
    if (current_node->east != NULL)
        adjacents--;

    adjacents = 4 - adjacents;

    if (adjacents == 0) {
        printf("Error: Corrupted data\n");
        exit(1);
    }

    if (adjacents != 2)
        return calculate_case(adjacents);

    if (current_node->west == NULL && current_node->east == NULL)
        return 2;
    if (current_node->north == NULL && current_node->south == NULL)
        return 2;
    return 3;
}

/**
 * Calculates the case number based on the number of adjacents.
 */
int calculate_case(int adjacents) {
    switch (adjacents) {
        case 1:
            return 1;
        case 3:
            return 4;
        case 4:
            return 5;
        default:
            return -1;
    }
}

/**
 * Rotates the direction to the right (clockwise).
 */
int rotate_right(int direction) {
    return (direction + 1) % 4;
}

/**
 * Moves the selected node in the specified direction if possible.
 */
struct rps_node* move_node(struct rps_node* selected_node, int direction) {
    switch (direction) {
        case NORTH:
            if (selected_node->north)
                return selected_node->north;
            break;
        case EAST:
            if (selected_node->east)
                return selected_node->east;
            break;
        case SOUTH:
            if (selected_node->south)
                return selected_node->south;
            break;
        case WEST:
            if (selected_node->west)
                return selected_node->west;
            break;
    }
    return selected_node;
}

/**
 * Reads instructions from "instructions.txt" and constructs a linked list of instructions.
 */
struct ins_node* get_instructions() {
    struct ins_node* instruction_list = NULL;
    FILE* file_ptr = fopen("instructions.txt", "r");
    int char_read;

    if (!file_ptr) {
        perror("Failed to open instructions.txt");
        exit(EXIT_FAILURE);
    }

    /* Start with a CREATE instruction with value 3 */
    instruction_list = add_instruction(NULL, CREATE, 3);

    /* Read instructions from file */
    while ((char_read = fgetc(file_ptr)) != EOF && char_read != '\n') {
        instruction_list = instruction_switch(char_read, instruction_list, file_ptr);
    }

    /* Add a final CREATE instruction with value 3 */
    instruction_list = add_instruction(instruction_list, CREATE, 3);
    fclose(file_ptr);
    return instruction_list;
}

/**
 * Processes a character from the instructions file and adds the corresponding instruction to the list.
 */
struct ins_node* instruction_switch(int char_read, struct ins_node* instruction_list, FILE* file_ptr) {
    switch (char_read) {
        case '3':
            char_read = fgetc(file_ptr);
            switch (char_read) {
                case '3':
                    return add_instruction(instruction_list, CREATE, 3);
                case '1':
                    return add_instruction(instruction_list, CREATE, 1);
                case '2':
                    return add_instruction(instruction_list, CREATE, 2);
                default:
                    printf("Error: Corrupted data in instructions\n");
                    exit(1);
            }
        case '1':
            return add_instruction(instruction_list, ROTATE, 0);
        case '2':
            return add_instruction(instruction_list, MOVE, 0);
        default:
            return instruction_list;
    }
}

/**
 * Adds a new instruction to the instruction list.
 */
struct ins_node* add_instruction(struct ins_node* instruction_list, int type, int value) {
    struct ins_node* new_instruction = calloc(1, sizeof(struct ins_node));
    if (!new_instruction) {
        perror("Failed to allocate memory for instruction node");
        exit(EXIT_FAILURE);
    }
    new_instruction->type = type;
    new_instruction->value = value;
    new_instruction->next = instruction_list;
    return new_instruction;
}

/**
 * Removes the head instruction from the instruction list.
 */
struct ins_node* pop_instruction(struct ins_node* instruction_list) {
    struct ins_node* next_instruction = instruction_list->next;
    free(instruction_list);
    return next_instruction;
}

/**
 * Creates the origin node of the RPS grid with the specified type.
 */
struct rps_node* make_origin(int type) {
    struct rps_node* origin_node = calloc(1, sizeof(struct rps_node));
    if (!origin_node) {
        perror("Failed to allocate memory for origin node");
        exit(EXIT_FAILURE);
    }
    origin_node->type = type;
    origin_node->x = 0;
    origin_node->y = 0;
    origin_node->next = NULL;
    origin_node->north = NULL;
    origin_node->south = NULL;
    origin_node->east = NULL;
    origin_node->west = NULL;
    return origin_node;
}

/**
 * Creates a new node in the specified direction from the selected node.
 */
struct rps_node* create_node(struct rps_node* current_node, struct rps_node* selected_node, int direction, int type) {
    struct rps_node* creation_point = find_last(selected_node, direction);
    return add_node(creation_point, current_node, direction, type);
}

/**
 * Finds the farthest node in the given direction from the starting node.
 */
struct rps_node* find_last(struct rps_node* start_node, int direction) {
    struct rps_node* traversal_node = start_node;
    while (start_node) {
        traversal_node = start_node;
        switch (direction) {
            case NORTH:
                start_node = start_node->north;
                break;
            case SOUTH:
                start_node = start_node->south;
                break;
            case EAST:
                start_node = start_node->east;
                break;
            case WEST:
                start_node = start_node->west;
                break;
        }
    }
    return traversal_node;
}

/**
 * Adds a new node in the specified direction from the creation point.
 */
struct rps_node* add_node(struct rps_node* creation_point, struct rps_node* current_node, int direction, int type) {
    struct rps_node* new_node = calloc(1, sizeof(struct rps_node));
    if (!new_node) {
        perror("Failed to allocate memory for rps_node");
        exit(EXIT_FAILURE);
    }
    new_node->type = type;
    new_node->next = current_node;
    new_node->north = NULL;
    new_node->south = NULL;
    new_node->east = NULL;
    new_node->west = NULL;

    /* Set coordinates and link the new node to the creation point */
    switch (direction) {
        case NORTH:
            creation_point->north = new_node;
            new_node->x = creation_point->x;
            new_node->y = creation_point->y + 1;
            break;
        case SOUTH:
            creation_point->south = new_node;
            new_node->x = creation_point->x;
            new_node->y = creation_point->y - 1;
            break;
        case EAST:
            creation_point->east = new_node;
            new_node->x = creation_point->x + 1;
            new_node->y = creation_point->y;
            break;
        case WEST:
            creation_point->west = new_node;
            new_node->x = creation_point->x - 1;
            new_node->y = creation_point->y;
            break;
    }

    /* Link new_node with adjacent nodes */
    struct rps_node* temp_node = current_node;
    while (temp_node) {
        if (temp_node->x == new_node->x && temp_node->y == new_node->y - 1)
            new_node->south = temp_node;
        if (temp_node->x == new_node->x && temp_node->y == new_node->y + 1)
            new_node->north = temp_node;
        if (temp_node->x == new_node->x + 1 && temp_node->y == new_node->y)
            new_node->east = temp_node;
        if (temp_node->x == new_node->x - 1 && temp_node->y == new_node->y)
            new_node->west = temp_node;
        temp_node = temp_node->next;
    }
    return new_node;
}

/**
 * Prints the linked list of nodes.
 */
void print_linked_list(struct rps_node* node) {
    while (node) {
        printf("%i\n", node->type);
        node = node->next;
    }
}

/**
 * Prints the structure of the RPS grid.
 */
void print_structure(struct rps_node* node) {
    #define ARRAY_SIZE 218
    char array[ARRAY_SIZE][ARRAY_SIZE];
    int i, j, x, y;

    /* Initialize the array with spaces */
    for (i = 0; i < ARRAY_SIZE; i++)
        for (j = 0; j < ARRAY_SIZE; j++)
            array[i][j] = ' ';

    /* Set array values based on node positions and types */
    while (node) {
        x = node->x + ARRAY_SIZE / 2;
        y = node->y + ARRAY_SIZE / 2;
        if (x < 0)
            x = 0;
        if (x >= ARRAY_SIZE)
            x = ARRAY_SIZE - 1;
        if (y < 0)
            y = 0;
        if (y >= ARRAY_SIZE)
            y = ARRAY_SIZE - 1;
        array[x][y] = node->type + '0';
        node = node->next;
    }

    /* Print the array to display the grid */
    for (i = 0; i < ARRAY_SIZE; i++) {
        for (j = 0; j < ARRAY_SIZE; j++)
            putchar(array[i][j]);
        putchar('\n');
    }
}
