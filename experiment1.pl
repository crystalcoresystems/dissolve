# Experiment 1 of dice automata program
#
# idea -
#       test to see if it is possible to recover automata dice automata
#       by checking all positions for initial
#

#
#
signed main() {

        int dice_array[3][6]; # array goes like this: top, left, right, top, back, bottom
        for(int i=0;i<3;i++)
        


#

#       1
#       2       365
#      536  =   251
#       4

#       5       612
#     1234  =   345
#       6
# 

#       4      
#      535 =
#       2
#       1
#
# hypothesis: Certain combinations of LDE and RPS encryption will yield
#             greater return on original data.

# conclusion: The hypothesis was correct. The combinations 0101 and 1010
#             return 5/9 the amount of original data passed to the system

# notes: 3/5 of the returned combinations have the original character in
#        the center of the message



# Define cell types used in automata
use constant ONE => 1;
use constant THETA => 8;
use constant ZERO => 0;

# Create an array of all cell types
@a = (ONE, THETA, ZERO);
# Loop through all numbers 0 - 15 and use them as permutations
# by converting to binary "nibble" and passing to solve function
for my $i (0..15) {
        my @b = convert2nib($i);
        print @b, "\n";
        foreach my $trinity (@a) {

                # Solve all combinations of cell types
                solve($trinity, @b, ZERO, ZERO);  #1
                solve($trinity, @b, ZERO, ONE);   #2
                solve($trinity, @b, ZERO, THETA); #3
                solve($trinity, @b, ONE, ZERO);   #4
                solve($trinity, @b, ONE, ONE);    #5
                solve($trinity, @b, ONE, THETA);  #6
                solve($trinity, @b, THETA, ZERO); #7
                solve($trinity, @b, THETA, ONE);  #8
                solve($trinity, @b, THETA, THETA);#9
        }
        print "\n\n";
}

sub solve {#../
        my ($t, $a1, $b1, $a2, $b2, $k1, $k2) = @_;
        print $t, " : ";
        # Use binary permutation to test (a's and b's are either 1 or 0)
        # Pass the trinity through 2 keys in order k1 k2 k1 k2
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        print $t, " : ";
        $t = $b1? LDE($t, $k2) : RPS($t, $k2); 
        print $t, " : ";
        $t = $a2? LDE($t, $k1) : RPS($t, $k1); 
        print $t, " : ";
        $t = $b2? LDE($t, $k2) : RPS($t, $k2); 
        print $t, "\n";
}#/..

sub solve2 {#../
        # This is the same as solve but uses a modified version of LDE
        my ($t, $a1, $b1, $a2, $b2, $k1, $k2) = @_;
        print $t, " : ";
        $t = $a1? LDE2($t, $k1) : RPS($t, $k1); 
        print $t, " : ";
        $t = $b1? LDE2($t, $k2) : RPS($t, $k2); 
        print $t, " : ";
        $t = $a2? LDE2($t, $k1) : RPS($t, $k1); 
        print $t, " : ";
        $t = $b2? LDE2($t, $k2) : RPS($t, $k2); 
        print $t, "\n";
}#/..

sub LDE {#../
# LDE returns the same as RPS except combinations of the same trinity
# return a different one ( Learning creates discovery, experienece creates
# learning, discovery creates experience )
#
        my ($a, $b) = @_;
        $a == ONE && $b == THETA and return ZERO;
        $a == ZERO && $b == THETA and return ONE;
        $a == THETA && $b == ONE and return ZERO;
        $a == THETA && $b == ZERO and return ONE;
        $a == ONE && $b == ZERO and return THETA;
        $a == ZERO && $b == ONE and return THETA;
        $a == ZERO && $b == ZERO  and return THETA;
        $a == THETA && $b == THETA and return ONE;
        $a == ONE && $b == ONE and return ZERO;
}#/..

sub LDE2 {#../
# Same as LDE with different results when combining the same trinities
# (Learning creates experience, experience creates discovery,
# discovery creates learning)
#
        my ($a, $b) = @_;
        $a == ONE && $b == THETA and return ZERO;
        $a == ZERO && $b == THETA and return ONE;
        $a == THETA && $b == ONE and return ZERO;
        $a == THETA && $b == ZERO and return ONE;
        $a == ONE && $b == ZERO and return THETA;
        $a == ZERO && $b == ONE and return THETA;
        $a == ZERO && $b == ZERO  and return ONE;
        $a == THETA && $b == THETA and return ZERO;
        $a == ONE && $b == ONE and return THETA;
}#/..

sub RPS {#../
# "Rock Paper Scissors"
        my ($a, $b) = @_;
        $a == $b and return $a;
        $a == ONE && $b == THETA and return ZERO;
        $a == ZERO && $b == THETA and return ONE;
        $a == THETA && $b == ONE and return ZERO;
        $a == THETA && $b == ZERO and return ONE;
        $a == ONE && $b == ZERO and return THETA;
        $a == ZERO && $b == ONE and return THETA;
}#/..

sub convert2nib{#../
# convert an integer to binary nibble
        $n = shift;
        my @a; 
        my $i = 0;
        while ($n >= 1) {
                $i++;
                unshift(@a, $n%2);
                $n = int($n/2);
        }
        while ($i < 4) {
                unshift (@a, 0);
                $i++;
        }
        return @a;
}#/..
