#include<iostream>
#include<stdlib.h>

#define int long long

using namespace std;

// Problem 9A die roll../
// Yakko Vakko and Dot, world famous musicians, wanted to take a break from
// doing cartoons and have a vacation, for a bit of travel. Yakko dreamed
// of going to Pennsylvania, his homeland and the homeland of his ancesetors
// Vakko leaned towards Tasmania, its beaches, sun and sea. Dot chose
// Transylvania as the most mysterious and upredictable place.
// But, much to their regret, the vaction was short that is sufficient to
// visit only one of the three above. Therefore Yakko, as the most intelligent,
// proposed a truly brilliant idea: Let everyone throw a hexagonal die, and the
// one with the most points - He'll take everyone to his dream place
//
// Yakko threw Y points, Vakko W points. Dot remains. But she didn't hurry.
// She wanted to see how likely it would be that she visits Transylvania.
//
// It is know Yakko And Vakko are true gentlemen, therefore in the event
// of a tie Dot wins.
//
// Input:
// 	in a single line of input are given 2 natural numbers Y and W
// 	Yakko and Vakko's score
// Output:
// 	infer the desired probabilty in the form of an irreducible fraction
// 	in the form A/B if probability is 1 print 1/1
// 	1 - 1/1
// 	2 - 5/6
// 	3 - 2/3
// 	4 - 1/2
// 	5 - 1/3
// 	6 - 1/6

void prob(int x) {
	switch(x){
		case 1:
			cout << "1/1";
			break;
		case 2:
			cout << "5/6";
			break;
		case 3:
			cout << "2/3";
			break;
		case 4:
			cout << "1/2";
			break;
		case 5:
			cout << "1/3";
			break;
		default:
			cout << "1/6";
	}
}

signed main() {
	int y, w;
	cin >> y >> w;

	if (y > w) {
		prob(y);
	} else {
		prob(w);
	}
}
//..
// Problem 9A die roll../
