#include<iostream>
#include<bitset>

using namespace std;

signed main() {
	bitset<54> l;
	bitset<54> m("000000000000000000000000000000000000000000000111111111");
	bitset<54> rez;
	bitset<9> temp;

	l.flip();
	rez = l & (m << 9);
	for(int i=0; i<9; i++) {
		temp[i] = rez[i+3];
	}
	cout << temp << endl << rez;
}
