#include<iostream>

using namespace std;

// Problem 1632C
// Problem 1676A
// Problem 1703A
void print64bit(int a, int binary[32]) {
	int c = a;
	for(int i=31; i>=0; i--) {
		binary[i] = (a & 1);
		a = a >> 1;
	}
	a = c;
	for(int i=0; i<32; i++) {
		cout << binary[i];
	}
	cout << endl;
}
//  equalize numbers with three operations
//  a := a + 1
//  b := b + 1
//  a := a | b
//
//  find the farthest left bit where a is 1 and b is 0
//  truncate b and a at that bit.
//  1: find the distance from b to a
//  2: find the distance from a to the next power of 2
//  lesser of 1 and 2 plus 1 unless already equal
//
//  if 1 and 2 are equal check each case for number equality
//
void trunc(int from[32], int to[32], int index) {
	for(int i=0; i<index; i++)
		to[i] = 0;
	for(int i=index+1; i<32; i++)
		to[i] = from[i];
}
void run_1632C() {
	int a,b,c;
	int x, y;
	int count;
	int atrunc;
	int btrunc;
	int abin[32];
	int bbin[32];
	cin >> count;
	while(count--) {
		cin >> a >> b;
		cout << endl;
		print64bit(a, abin);
		print64bit(b, bbin);
		int flag = 1;
		for(int i=0; i<32; i++) {
			if ( (abin[i] == 1) && (bbin[i] == 0) ){
				int x = 1;
				atrunc = 0;
				btrunc = 0;
				for(int j=0; j<32 - i; j++){
					atrunc = atrunc | (a & x);
					btrunc = btrunc | (b & x);
					x = x << 1;
				}
				x = (1 << (32 - i)) - atrunc;
				y = atrunc - btrunc;
				if(x < y)
					if(x == b)
						cout << x;
					else
						cout << x + 1;
				else
					cout << y;

				flag = 0;
				break;
			}
		}
		if (flag)
			cout << 1;
		cout << endl;
	}
}

// sum of first three digits equal to sum of second three
void run_1676A() {
	int count;
	cin >> count;
	while(count--) {
		int n;
		int a,b;
		cin >> n;
		a = (n/100000)%10 + (n/10000)%10 + (n/1000)%10;
		b = (n/100)%10 + (n/10)%10 + n%10;
		if (a == b)
			cout << "YES";
		else
			cout << "NO";
		cout << endl;
	}
}

// Case insensitive string validation
void run_1703A() {
       int count;
       cin >> count;
      while(count--) {
             string word;
             cin >> word;
            if (
                           (word[0] == 'y' || word[0] == 'Y') && 
                           (word[1] == 'e' || word[1] == 'E') && 
                           (word[2] == 's' || word[2] == 'S') )
                    cout << "YES";
            else
                    cout << "NO";
      }
}
int main() {
	run_1632C();
}

