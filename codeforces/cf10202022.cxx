#include<iostream>

#define int long long
using namespace std;


// Problem 1692A marathon../
// Timur and 3 friends participate in a marathon.
// You are given 4 distinct integers - A, B, C and D
// a corresponds to the distance timur ran, the rest the others
// print the number of friends that ran more than timur
//
signed main() {
	int t;
	cin >> t;
	while(t--){
		int a, b, c, d, ans;
		ans = 0;

		cin >> a >> b >> c >> d;
		if(b>a) ans++;
		if(c>a) ans++;
		if(d>a) ans++;

		cout << ans << endl;
	}
}
