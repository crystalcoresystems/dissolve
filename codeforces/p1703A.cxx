#include<iostream>

using namespace std;

// Problem 1703A
int main() {
       int count;
       cin >> count;
      while(count--) {
             string word;
             cin >> word;
            if (
                           (word[0] == 'y' || word[0] == 'Y') && 
                           (word[1] == 'e' || word[1] == 'E') && 
                           (word[2] == 's' || word[2] == 'S') )
                    cout << "YES";
            else
                    cout << "NO";
      }
}
