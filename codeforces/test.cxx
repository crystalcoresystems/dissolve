#include<iostream>
#include<stdlib.h>
#include<bitset>
#include<unordered_set>

#define int unsigned long long

using namespace std;

signed main() {
	bitset<54> x("111111111111111111111111111111111111111111111111111111");
	hash<bitset<54>> hashfun;
	unordered_set<bitset<54>> store;
	store.insert(x);
	cout << x << endl;
	x.flip(1);
	store.insert(x);
	cout << x << endl;
	x.flip(2);
	cout << x << endl;
	if (store.find(x) != store.end())
	       cout << "contains" << endl;
	cout << store.size() << endl;
}
