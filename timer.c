#include <starlet.h>
#include <stdio.h>
#include <descrip.h>
#include <ssdef.h>

int main(void)
{
        typedef int quadword[2];
        long int status;
        quadword binary_time;
        char now_time[24], filename[64];
#pragma nostandard
        $DESCRIPTOR(ascii_time, "0 0:0:10");
        $DESCRIPTOR(now_time_desc, now_time);
        $DESCRIPTOR(filename_desc, filename);
#pragma standard
        
        puts("\n***** Hibernates for 10 seconds *******\n");
/* convert ascii_time from ascii to binary format.*/
        if (( status=sys$bintim(
                &ascii_time,                    // timbuff
                binary_time                     // timadr
           )) != SS$_NORMAL )
                sys$exit(status);
/* ask system for current time */
        if (( status=sys$asctim(
                0,
                &now_time_desc,
                0,
                0
           )) != SS$_NORMAL )
                sys$exit(status);
/* display current time */
        now_time[23] = '\0';
        printf("The current time is : %s\n", now_time);
/* schedule a wakeup 10 seconds from now */
        if (( status=sys$schdwk(
                0,                              // [pidadr]
                0,                              // [prcnam]
                &binary_time,                   //  daytim
                0                               // [reptim]
           )) != SS$_NORMAL )
                sys$exit(status);
/* go to sleep n the alarm is set */
        if (( status=sys$hiber()) != SS$_NORMAL )
                sys$exit(status);
/* 10 seconds later, continue. Get the current time */
        if (( status=sys$asctim(
                0,
                &now_time_desc,
                0,
                0
           )) != SS$_NORMAL )
                sys$exit(status);
/* display the current time n to check the 10 second interval */
        now_time[23] = '\0';
        printf("The current time is : %s\n", now_time);
/* signal normal status upon return to OpenVMS */
        return SS$_NORMAL;
}
