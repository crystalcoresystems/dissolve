// Experiment 2 of dice automata program../ 
// // 
// // idea -
//              test to see how many iterations of dice it takes to get to
//              to an even number of all digit and different center numbers
//
// hypothesis: hopefully under 10 moves
//           
// conclusion: looks like it might not be possible at least in 8 moves

// notes:

// array goes like this: 
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
//   
// rotation goes like this:
// nothing, right, left, down, up, flip
//   0        1     2     3    4    5

// initial dice starting position
//
//       1
//       2
//      536
//       4
//..
#include <iostream>
#include <stdlib.h>
#define DATASIZE 6 
#define DIM 3 

// declarations../

// initialize the dice and the data
void initialize_cube(int dice_array[DATASIZE][DIM][DIM]);
void initialize_die(int die[6]);

// viz dice and data
void visualize_die(int die_array[6]);
void visualize_cube(int data_array[DATASIZE][DIM][DIM]);

// rotate die
void rotate_right(int die[6]);
void rotate_left(int die[6]);
void rotate_up(int die[6]);
void rotate_down(int die[6]);
void spin(int die[6]);
void flip_vertical(int die[6]);
void flip_horizontal(int die[6]);

// solve: rotate based on instruction
void solve_data_right(int data_array[DATASIZE][DIM][DIM]);
void solve_data_left(int data_array[DATASIZE][DIM][DIM]);
void solve_data_up(int data_array[DATASIZE][DIM][DIM]);
void solve_data_down(int data_array[DATASIZE][DIM][DIM]);
void solve_data_forward(int data_array[DATASIZE][DIM][DIM]);
void solve_data_backward(int data_array[DATASIZE][DIM][DIM]);

void mod(int die[6], int);

// interpolate nstff

// unsolve: rotate possible orientations to find previous instructions
void unsolve_data_right(int data_array[DATASIZE][DIM][DIM]);
void unsolve_data_left(int data_array[DATASIZE][DIM][DIM]);
void unsolve_data_up(int data_array[DATASIZE][DIM][DIM]);
void unsolve_data_down(int data_array[DATASIZE][DIM][DIM]);
void unsolve_data_forward(int data_array[DATASIZE][DIM][DIM]);
void unsolve_data_backward(int data_array[DATASIZE][DIM][DIM]);

// get all possible orientations
void possible(int orientations[4][6], int);
void orient(int o[4][6], int sides[4], int, int);

// interpolate backwards and check for success
void left_interpolation_die(int die[6], int data[DIM]);
void left_interpolation(int die[6], int data[DIM]);
void right_interpolation_die(int die[6], int data[DIM]);
void right_interpolation(int die[6], int data[DIM]);

void forward_interpolation_die(int die[6], int data[DATASIZE]);
void forward_interpolation(int die[6], int data[DATASIZE]);
void interpolation_die(int die[6], int data[DATASIZE]);
void interpolation(int die[6], int data[DATASIZE]);

int find_move(int die[6], int to);
int opposite(int move);
bool interpolation_success(int correct[6], int test[6]);
//..

// declarations2../
void bruteforce_dataform();
void copy_data(int a[DATASIZE][DIM][DIM], int b[DATASIZE][DIM][DIM]);
void roll(int data_array[DATASIZE][DIM][DIM], int dir);
bool equal_numbers(int data_array[DATASIZE][DIM][DIM]);
bool middle_dif(int data_array[DATASIZE][DIM][DIM]);
void zero(int a[6]);
        //..

signed main() {

        int data[DATASIZE][DIM][DIM];
        int c1 = 0, c2 = 0;
        int i=0;

        srand(7);
        int moves = 0;
        for(int x=0; x<100; x++) {
                initialize_cube(data);

                for(i=0;;++i) {
                        roll(data, 1);
                        if(middle_dif(data) && equal_numbers(data))
                                break;
                }
                moves += i;
        }
        cout << moves/100.0;

}

// Bruteforce Dataform../
void bruteforce_dataform(){
        int data_array[DATASIZE][DIM][DIM];
        int dummy_data[DATASIZE][DIM][DIM];
        int d[8]; // direction array
        int i,j;

        initialize_cube(data_array);
        visualize_cube(data_array);

        for(i=0;i<279935;i++){
                int dummy = i;

                for(j=0;j<7;j++){
                        d[6-j] =dummy%6;
                        dummy = dummy / 6;
                }

                copy_data(data_array, dummy_data);

                for(j=0;j<7;j++){
                        roll(dummy_data, d[j]);

                        if(equal_numbers(dummy_data)){
                                cout << "mission accomplished in " << j << " moves" << endl;
                                visualize_cube(data_array);
                                break;
                        }
                }
        }
}
//..

// Roll../
void roll(int data_array[DATASIZE][DIM][DIM], int dir) {
        switch (dir) {
                case 0:
                        solve_data_forward(data_array);
                        break;
                case 1:
                        solve_data_backward(data_array);
                        break;
                case 2:
                        solve_data_up(data_array);
                        break;
                case 3:
                        solve_data_down(data_array);
                        break;
                case 4:
                        solve_data_left(data_array);
                        break;
                case 5:
                        solve_data_right(data_array);
                        break;
        }
}
//..

// Equal Numbers../
bool equal_numbers(int data_array[DATASIZE][DIM][DIM]){
        int count[6];
        int i,j,k;

        zero(count);

        for(i=0;i<DATASIZE;i++)
                for(j=0;j<DIM;j++)
                        for(k=0;k<DIM;k++)
                                if(++count[data_array[i][j][k]-1] > 9)
                                        return false;
        return true;
}
//..

// Middle Different../
bool middle_dif(int data_array[DATASIZE][DIM][DIM]){
        int count[6];
        zero(count);

        for(int i=0;i<DATASIZE;i++)
                if(++count[data_array[i][1][1]-1] > 1)
                        return false;
        return true;
}
//..

// zero array of size 6../
void zero(int a[6]){
        for(int i=0;i<6;i++)
                a[i] = 0;
}
//..

// Copy data../
void copy_data(int a[DATASIZE][DIM][DIM],int b[DATASIZE][DIM][DIM]){
        int i,j,k;
        for(i=0;i<DATASIZE;i++)
                for(j=0;j<DIM;j++)
                        for(k=0;k<DIM;k++)
                                b[i][j][k] = a[i][j][k];
}
//..

// Cube Solving procedures (solve_data_ forward, backward, left, right, up, down../

// Solve Data in 6 directions../

// Solve Data Right../
void solve_data_right(int data_array[DATASIZE][DIM][DIM]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int x=0;x<DATASIZE;x++) {
                for(int i=0;i<DIM;i++) {
                        for(int z=0;z<6;z++)
                                die[z] = dummy[z];

                        for(int j=0;j<DIM;j++) {
                                mod(die, data_array[x][i][j]);
                                data_array[x][i][j] = die[0];
                        }
                }
        }
}
//..
// Solve Data Left../
void solve_data_left(int data_array[DATASIZE][DIM][DIM]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int x=0;x<DATASIZE;x++) {
                for(int i=0;i<DIM;i++) {
                        for(int z=0;z<6;z++)
                                die[z] = dummy[z];

                        for(int j=DIM-1;j>=0;j--){
                                mod(die, data_array[x][i][j]);
                                data_array[x][i][j] = die[0];
                        }
                }
        }
}
//..

// Solve Data down../
void solve_data_down(int data_array[DATASIZE][DIM][DIM]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int x=0;x<DATASIZE;x++) {
                for(int i=0;i<DIM;i++) {
                        for(int z=0;z<6;z++)
                                die[z] = dummy[z];

                        for(int j=0;j<DIM;j++) {
                                mod(die, data_array[x][j][i]);
                                data_array[x][j][i] = die[0];
                        }
                }
        }
}
//..
// Solve Data up../
void solve_data_up(int data_array[DATASIZE][DIM][DIM]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int x=0;x<DATASIZE;x++) {
                for(int i=0;i<DIM;i++) {
                        for(int z=0;z<6;z++)
                                die[z] = dummy[z];

                        for(int j=DIM-1;j>=0;j--){
                                mod(die, data_array[x][j][i]);
                                data_array[x][j][i] = die[0];
                        }
                }
        }
}
//..

// Solve Data forward../
void solve_data_forward(int data_array[DATASIZE][DIM][DIM]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int i=0;i<DIM;i++) {
                for(int j=0;j<DIM;j++) {
                        for(int z=0;z<6;z++)
                                die[z] = dummy[z];
                        for(int x=0;x<DATASIZE;x++) {
                                mod(die, data_array[x][i][j]);
                                data_array[x][i][j] = die[0];
                        }
                }
        }
}
//..
// Solve Data backward../
void solve_data_backward(int data_array[DATASIZE][DIM][DIM]) {

        int dummy[6];
        int die[6];

        initialize_die(dummy);

        for(int i=0;i<DIM;i++) {
                for(int j=0;j<DIM;j++){
                        for(int z=0;z<6;z++)
                                die[z] = dummy[z];
                        for(int x=DATASIZE-1;x>=0;x--) {
                                mod(die, data_array[x][i][j]);
                                data_array[x][i][j] = die[0];
                        }
                }
        }
}
//..

//..

// Unsolve Data in 6 directions../

// Unsolve Data Right../
void unsolve_data_right(int data_array[DATASIZE][DIM][DIM]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6];
        int cor = 0;


        for(int x=0;x<DATASIZE;x++){
                for(int i=0;i<DIM;i++){
                        initialize_die(correct);
                        possible(orientations, data_array[x][i][DIM-1]);
                        for(int j=0;j<4;j++){
                                for(int k=0;k<6;k++)
                                        dummy[k] = orientations[j][k];
                                left_interpolation_die(orientations[j], data_array[x][i]);
                                if( interpolation_success(correct, orientations[j]) ){
                                        left_interpolation(dummy, data_array[x][i]);
                                        break;
                                }
                        }
                }
        }
}
//.. 
// Unsolve Data Left../
void unsolve_data_left(int data_array[DATASIZE][DIM][DIM]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6];
        int cor = 0;


        for(int x=0;x<DATASIZE;x++){
                for(int i=0;i<DIM;i++){
                        initialize_die(correct);
                        possible(orientations, data_array[x][i][0]);
                        for(int j=0;j<4;j++){
                                for(int k=0;k<6;k++)
                                        dummy[k] = orientations[j][k];
                                right_interpolation_die(orientations[j], data_array[x][i]);
                                if( interpolation_success(correct, orientations[j]) ){
                                        right_interpolation(dummy, data_array[x][i]);
                                        break;
                                }
                        }
                }
        }
}
//.. 

// Unsolve Data up../
void unsolve_data_down(int data_array[DATASIZE][DIM][DIM]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6]; // holds original orientaion of dice
        int alternate[DIM]; // holds alternately structured array

        for(int x=0;x<DATASIZE;x++){
                for(int i=0;i<DIM;i++){
                        initialize_die(correct);
                        possible(orientations, data_array[x][DIM-1][i]);

                        for(int k=0;k<DIM;k++) // added for traversing array up and down
                                alternate[k] = data_array[x][k][i];

                        for(int j=0;j<4;j++){
                                for(int k=0;k<6;k++)
                                        dummy[k] = orientations[j][k];

                                left_interpolation_die(orientations[j], alternate);
                                if( interpolation_success(correct, orientations[j]) ){
                                        left_interpolation(dummy, alternate);

                                        for(int k=0;k<DIM;k++)
                                                data_array[x][k][i] = alternate[k];
                                        break;
                                }
                        }
                }
        }
}
//.. 
// Unsolve Data down../
void unsolve_data_up(int data_array[DATASIZE][DIM][DIM]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6];
        int alternate[DIM]; // holds alternately structured array

        for(int x=0;x<DATASIZE;x++){
                for(int i=0;i<DIM;i++){

                        initialize_die(correct);
                        possible(orientations, data_array[x][0][i]);

                        for(int k=0;k<DIM;k++) // added for traversing array up and down
                                alternate[k] = data_array[x][k][i];

                        for(int j=0;j<4;j++){
                                for(int k=0;k<6;k++)
                                        dummy[k] = orientations[j][k];

                                right_interpolation_die(orientations[j], alternate);
                                if( interpolation_success(correct, orientations[j]) ){
                                        right_interpolation(dummy, alternate);
                                        for(int k=0;k<DIM;k++)
                                                data_array[x][k][i] = alternate[k];
                                        break;
                                }
                        }
                }
        }
}
//.. 

// Unsolve Data forward../
void unsolve_data_forward(int data_array[DATASIZE][DIM][DIM]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6]; // holds original orientaion of dice
        int alternate[DATASIZE]; // holds alternately structured array

        initialize_die(correct);

        for(int i=0; i<DIM; i++) {
                for(int j=0; j<DIM; j++){

                        for(int x=0; x<DATASIZE;x++) {
                                alternate[x] = data_array[x][i][j];
                        }

                        possible(orientations, data_array[DATASIZE-1][i][j]);
                        for(int x=0;x<4;x++){
                                for(int k=0;k<6;k++) {
                                        dummy[k] = orientations[x][k];
                                }

                                interpolation_die(orientations[x], alternate);
                                if (interpolation_success(correct, orientations[x])) {
                                        interpolation(dummy, alternate);
                                        for(int k=0;k<DATASIZE;k++)
                                                data_array[k][i][j] = alternate[k];
                                        break;

                               }
                       }

               }
        }
}

//.. 
// Unsolve Data backward../
void unsolve_data_backward(int data_array[DATASIZE][DIM][DIM]) {
        int orientations[4][6];
        int correct[6];
        int dummy[6]; // holds original orientaion of dice
        int alternate[DATASIZE]; // holds alternately structured array

        initialize_die(correct);

        for(int i=0; i<DIM; i++) {
                for(int j=0; j<DIM; j++){

                        for(int x=0; x<DATASIZE;x++) {
                                alternate[x] = data_array[DATASIZE-x-1][i][j];
                        }

                        possible(orientations, data_array[0][i][j]);
                        for(int x=0;x<4;x++){
                                for(int k=0;k<6;k++) {
                                        dummy[k] = orientations[x][k];
                                }

                                interpolation_die(orientations[x], alternate);
                                if (interpolation_success(correct, orientations[x])) {
                                        interpolation(dummy, alternate);
                                        for(int k=0;k<DATASIZE;k++)
                                                data_array[k][i][j] = alternate[DATASIZE-k-1];
                                        break;

                               }
                       }
               }
        }
}

//.. 

// Left interpolation of die../
void left_interpolation_die(int die[6], int data[DIM]) {
        int from, to;
        int move;
        int dummy[DATASIZE];

        for(int i=0;i<DIM;i++)
                dummy[i] = data[i];

        for(int i=DIM-1;i>0;i--) {
                to = data[i-1];
                move = find_move(die, to);//you
                mod(die, move);
                dummy[i] = die[0];
        }
        move = find_move(die, 3); // 3 is initial top position for die
        mod(die, move);
        dummy[0] = die[0];
} 
//..
// Left interpolation of for data../
void left_interpolation(int die[6], int data[DIM]) {
        int from, to;
        int move;


        for(int i=DIM-1;i>0;i--) {
                to = data[i-1];
                move = find_move(die, to);//you
                mod(die, move);
                data[i] = opposite(move);
        }
        move = find_move(die, 3);//you
        data[0] = opposite(move);
} 
//..
// Right interpolation of die../
void right_interpolation_die(int die[6], int data[DIM]) {
        int from, to;
        int move;
        int dummy[DATASIZE];

        for(int i=0;i<DIM;i++)
                dummy[i] = data[i];

        for(int i=0;i<DIM-1;i++) {
                to = data[i+1];
                move = find_move(die, to);//you
                mod(die, move);
                dummy[i] = die[0];
        }
        move = find_move(die, 3);
        mod(die, move);
        dummy[DIM-1] = die[0];
}
//..
// Right interpolation of for data../
void right_interpolation(int die[6], int data[DIM]) {
        int from, to;
        int move;

        for(int i=0;i<DIM-1;i++) {
                to = data[i+1];
                move = find_move(die, to);//you
                mod(die, move);
                data[i] = opposite(move);
        }
        move = find_move(die, 3);//you
        data[DIM-1] = opposite(move);
}
//..
//  interpolation for forward/backward of die../
void interpolation_die(int die[6], int data[DATASIZE]) {
        int to;
        int move;

        for(int i=DATASIZE-1;i>0;i--) {
                to = data[i-1];
                move = find_move(die, to);//you
                mod(die, move);
        }
        move = find_move(die, 3);
        mod(die, move);
}
//..
// interpolation of forward/backward for data../
void interpolation(int die[6], int data[DATASIZE]) {
        int to;
        int move;

        for(int i=DATASIZE-1;i>0;i--) {
                to = data[i-1];
                move = find_move(die, to);//you
                mod(die, move);
                data[i] = opposite(move);
        }
        move = find_move(die, 3);
        data[0] = opposite(move);
}
//..
//..

// Find Move../
// if previous was x then move must have been y for orientation z
//
// just brute force the dice then get the direction and flip...
//              if there was a horizontal or vertical flip information loss...
//              therefore no horizontal flip

int find_move(int die[6], int to) {
        int location;

        for(int i=0;i<6;i++)
                if(die[i] == to) {
                        location = i;
                        break;
                }

        return location;
}
//..
// Find Move Backwards../
// array goes like this: 
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
// rotation goes like this:
// nothing, right, left, down, up, flip
//   0        1     2     3    4    6
int opposite(int move) {
        switch (move) {
                case 0:
                        return 6;
                case 1:
                        return 2;
                case 2:
                        return 1;
                case 3:
                        return 4;
                case 4:
                        return 3;
        }
        return move;
}
//..

// Interpolation Success../
bool interpolation_success(int correct[6], int test[6]) {
        for(int i=0;i<6;i++)
                if(correct[i] != test[i])
                        return false;
        return true;
}
//..

// Possible../
//                  clockwise
//   1 opp 3 -> (3 top) - 2 6 4 5
//   2 opp 4 -> (4 top) - 3 6 1 5
//   5 opp 6 -> (5 top) - 2 3 4 1
//
void possible(int orientations[4][6], int top){
        int three[4] = {2, 6, 4, 5}; 
        int one[4]   = {5, 4, 6, 2}; 

        int four[4]  = {3, 6, 1, 5}; 
        int two[4]   = {5, 1, 6, 3}; 

        int five[4]  = {2, 3, 4, 1}; 
        int six[4]   = {1, 4, 3, 2}; 

        switch (top) {
                case 1:
                        orient(orientations, one, 1, 3);
                        break;
                case 2:
                        orient(orientations, two, 2, 4);
                        break;
                case 3:
                        orient(orientations, three, 3, 1);
                        break;
                case 4:
                        orient(orientations, four, 4, 2);
                        break;
                case 5:
                        orient(orientations, five, 5, 6);
                        break;
                case 6:
                        orient(orientations, six, 6, 5);
                        break;
                default:
                        cout << "Variable Top ERROR" << endl;
                        exit(1);
        }
}
//..
// orient../
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void orient(int o[4][6], int sides[4], int top, int bot) {
        for(int i=0; i<4; i++){
                o[i][0] = top;
                o[i][5] = bot;

                for(int j=1;j<5;j++){
                        o[i][1] = sides[(0+i)%4]; // dumb hack cause yyeah
                        o[i][2] = sides[(2+i)%4];
                        o[i][3] = sides[(1+i)%4];
                        o[i][4] = sides[(3+i)%4];
                }
        }
}
//..        

// mod../
// rotate die based on instruction
void mod(int die[6], int instruction) {

        switch (instruction) {
                case 1:
                        rotate_right(die);
                        break;
                case 2:
                        rotate_left(die);
                        break;
                case 3:
                        rotate_down(die);
                        break;
                case 4:
                        rotate_up(die);
                        break;
                case 5:
                        flip_horizontal(die);
                        break;
        }
}
//..

// rotate dice../
// spin die../
//
// initial dice starting position
//
//       1
//       2
//      536
//       4
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
//


void spin(int die[6]) {
        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[1] = dummy[3]; // left  = up
        die[2] = dummy[4]; // right = back
        die[3] = dummy[2]; // up    = right
        die[4] = dummy[1]; // back  = left
}
//..
//  right../
//      1   6
//      2   2
//     536 153
//      4   4
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_right(int die[6]) {

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[1]; // top    = left
        die[1] = dummy[5]; // left   = bottom
        die[2] = dummy[0]; // right  = top
        die[5] = dummy[2]; // bottom = right

}
//..
//  left../
//      1   5
//      2   2
//     536 361
//      4   4
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_left(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[2]; // top    = right
        die[1] = dummy[0]; // left   = top
        die[2] = dummy[5]; // right  = bottom
        die[5] = dummy[1]; // bottom = left

}
//..
//  up../
//      1   2
//      2   3
//     536 546
//      4   1
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_up(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[4]; // top    =  back
        die[3] = dummy[0]; // up     =  top
        die[4] = dummy[5]; // back   =  bottom
        die[5] = dummy[3]; // bottom =  up

}
//..
//  down../
//      1   4
//      2   1
//     536 526
//      4   3
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5
void rotate_down(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[3]; // top    = up
        die[3] = dummy[5]; // up     = bottom
        die[4] = dummy[0]; // back   = top
        die[5] = dummy[4]; // bottom = back

}
//..
//  flip vertical../
//
//      1   3
//      2   4
//     536 516
//      4   2
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5

void flip_vertical(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[5]; // top    = bottom
        die[3] = dummy[4]; // up     = back
        die[4] = dummy[3]; // back   = up
        die[5] = dummy[0]; // bottom = top

}
//..
//  flip horizontal../
//
//      1   3
//      2   2
//     536 615
//      4   4
//
//  top, left, right, up, back, bottom
//   0    1      2     3    4      5

void flip_horizontal(int die[6]){

        int dummy[6];

        for(int i=0;i<6;i++)
                dummy[i] = die[i];

        die[0] = dummy[5]; // top    = bottom
        die[1] = dummy[2]; // left   = right
        die[2] = dummy[1]; // right  = left
        die[5] = dummy[0]; // bottom = top

}
//..
//..

// Visualize../ 
// Data../
void visualize_cube(int data_array[DATASIZE][DIM][DIM]) {
        for(int i=0; i<DIM; i++) {
                cout << "      ";
                for(int j=0;j<DIM;j++) {
                        cout << data_array[1][i][j];
                }
                cout << endl;
        }

        for (int i=0;i<DIM;i++) {
                for(int j=0;j<DIM;j++)
                        cout << data_array[0][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[4][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[5][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[2][i][j];
                cout << endl;
        }

        for(int i=0; i<DIM; i++) {
                cout << "      ";
                for(int j=0;j<DIM;j++) {
                        cout << data_array[3][i][j];
                }
                cout << endl;
        }
        cout << endl;
}
//..
//..

// Initialize../
// Data../
// just a random 2d array
void initialize_cube(int data_array[DATASIZE][DIM][DIM]) {
        for(int i=0;i<DATASIZE;i++)
                for(int j=0;j<DIM;j++)
                        for(int k=0;k<DIM;k++)
                                data_array[i][j][k] = rand()%6 + 1;
}
//..
//  Die../
// initial dice starting position
//
//       1
//       2 
//      536
//       4
//
void initialize_die(int die[6]) {

        die[0] = 3;
        die[1] = 5;
        die[2] = 6;
        die[3] = 2;
        die[4] = 4;
        die[5] = 1;

}
//..
//..

//..
