# content in data.raw
open(r,"<","data.raw");

# These hold the input array sorted by their respective value
my @xedges, @yedges, @vertices, @area, @perimeter, @eulerchi, @tilt, @sharpness, @white, @black, @class;

# Get Input
my @input;
while (not eof(r)) {
	chomp(my $line = <r>);
	my @in = split(/,/, $line);
	push(@input, \@in);
}
close(r);

@xedges = sort { compare(0, $a, $b) } @input;
@yedges = sort { compare(1, $a, $b) } @input;
@vertices = sort { compare(2, $a, $b) } @input;
@area = sort { compare(3, $a, $b) } @input;
@perimeter = sort { compare(4, $a, $b) } @input;
@eulerchi = sort { compare(5, $a, $b) } @input;
@tilt = sort { compare(6, $a, $b) } @input;
@sharpness = sort { compare(7, $a, $b) } @input;
@white = sort { compare(8, $a, $b) } @input;
@black = sort { compare(9, $a, $b) } @input;
my @data = (\@xedges, \@yedges, \@vertices, \@area, \@perimeter, \@eulerchi, \@tilt, \@sharpness, \@white, \@black);

my @disclist;

print "\@relation image\n\n";

print "\@attribute x-edges {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute y-edges {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute vertices {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute area {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute perimeter {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute euler {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute tilt {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute sharpness {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute white_pixels {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n";
print "\@attribute black_pixels {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}\n\n";
print "\@attribute class {memes, infographic, non-infographics, comics, maps, magazine, propaganda, architecture}\n\n";

print "\@data\n";


# meme, comic, infographic, non-infographic, propoganda, magazine, map
# discretize
foreach my $i (0 .. $#data) {
	my @count = (0, 0, 0, 0, 0, 0, 0, 0);
	my @disc;
	for my $j (0 .. $#{$data[$i]}) {
		my $x = $data[$i][$j][10];
		if ($x eq "memes") { $count[0] += 1;}
		if  ($x eq "comics") { $count[1] += 1;}
		if  ($x eq "infographic") { $count[2] += 1;}
		if  ($x eq "non-infographics") { $count[3] += 1;}
		if  ($x eq "propaganda") { $count[4, 2] += 1;}
		if  ($x eq "magazine") { $count[5] += 1;}
		if  ($x eq "maps") { $count[6] += 1;}
		if  ($x eq "architecture") { $count[7] += 1;}
		for my $y (0 .. $#count) {
			if ($count[$y] > 47) {
				my $name = getname($y);
				while($j <= $#{$data[$i]} and $data[$i][$j+1][10] eq $name) {
					$j++;
				}
				push(@disc, $j);
				@count = (0, 0, 0, 0, 0, 0, 0, 0);
			}
		}
	}
	push(@disclist, \@disc);
}

# go through all 10 categories and replace with nominal value
for (my $i = 0; $i < 10; $i++) {
	@input = sort { compare($i, $a, $b) } @input;
	my $prev = 0;
	for (my $n = 0; $n < scalar(@{ $disclist[$i] }); $n++) {
		for(my $j = $prev; $j < $disclist[$i][$n] ; $j++) {
			$input[$j][$i] = $n + 1;
		}
		$prev = $disclist[$i][$n];
	}
	for(my $j = $prev; $j < 1366; $j++) {
		$input[$j][$i] = 10;
	}
}

@input = sort { compare_string(10, $a, $b) } @input;

foreach my $row (@input) {
	print map("$_,", @$row);	
	print "\n";
}

exit;

sub getname{
	shift;
	if ($_ == 0) { return "memes";}
	if  ($_ == 1) { return "comics";}
	if  ($_ == 2) { return "infographic";}
	if  ($_ == 3) { return "non-infographics";}
	if  ($_ == 4) { return "propaganda";}
	if  ($_ == 5) { return "magazine";}
	if  ($_ == 6) { return "maps";}
}
#compare stuff
sub compare_string{
	my $i = shift;
	if($a->[$i] gt $b->[$i]){
		return -1;
	} elsif($a eq $b){
		return 0;
	}
	else {
		return 1;
	}
}

#compare stuff
sub compare{
	my $i = shift;
	if($a->[$i] < $b->[$i]){
		return -1;
	} elsif($a == $b){
		return 0;
	}
	else {
		return 1;
	}
}

