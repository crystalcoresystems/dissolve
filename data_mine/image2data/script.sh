#!/bin/bash

cd AI\ infographics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | pbmminkowski | sed s/.*:.// | tr '\n' ',' | sed s/[0-9]*,//) 

	# get the tilt and remove endline
	str+=$(pngtopam "$x" | pamtilt | tr '\n' ',')

	# get image sharpness and and convert number
	str+=$(pngtopam "$x" | pamsharpness | sed s/.*=.// | tr '\n' ',')

	# get histogram of white and black pixels
	# some awkard sed but it works
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | ppmhist -noheader | sed s/.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *// | sed s/\ // | tr '\n' ',')
	str+="infographic"
	echo $str >> ../data.raw
done
cd ..
cd AI\ architecture
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | pbmminkowski | sed s/.*:.// | tr '\n' ',' | sed s/[0-9]*,//) 

	# get the tilt and remove endline
	str+=$(pngtopam "$x" | pamtilt | tr '\n' ',')

	# get image sharpness and and convert number
	str+=$(pngtopam "$x" | pamsharpness | sed s/.*=.// | tr '\n' ',')

	# get histogram of white and black pixels
	# some awkard sed but it works
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | ppmhist -noheader | sed s/.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *// | sed s/\ // | tr '\n' ',')
	str+="architecture"
	echo $str >> ../data.raw
done
cd ..
cd AI\ memes
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | pbmminkowski | sed s/.*:.// | tr '\n' ',' | sed s/[0-9]*,//) 

	# get the tilt and remove endline
	str+=$(pngtopam "$x" | pamtilt | tr '\n' ',')

	# get image sharpness and and convert number
	str+=$(pngtopam "$x" | pamsharpness | sed s/.*=.// | tr '\n' ',')

	# get histogram of white and black pixels
	# some awkard sed but it works
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | ppmhist -noheader | sed s/.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *// | sed s/\ // | tr '\n' ',')
	str+="memes"
	echo $str >> ../data.raw
done
cd ..
cd AI\ propaganda\ posters
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | pbmminkowski | sed s/.*:.// | tr '\n' ',' | sed s/[0-9]*,//) 

	# get the tilt and remove endline
	str+=$(pngtopam "$x" | pamtilt | tr '\n' ',')

	# get image sharpness and and convert number
	str+=$(pngtopam "$x" | pamsharpness | sed s/.*=.// | tr '\n' ',')

	# get histogram of white and black pixels
	# some awkard sed but it works
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | ppmhist -noheader | sed s/.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *// | sed s/\ // | tr '\n' ',')
	str+="propaganda"
	echo $str >> ../data.raw
done
cd ..
cd AI\ maps
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | pbmminkowski | sed s/.*:.// | tr '\n' ',' | sed s/[0-9]*,//) 

	# get the tilt and remove endline
	str+=$(pngtopam "$x" | pamtilt | tr '\n' ',')

	# get image sharpness and and convert number
	str+=$(pngtopam "$x" | pamsharpness | sed s/.*=.// | tr '\n' ',')

	# get histogram of white and black pixels
	# some awkard sed but it works
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | ppmhist -noheader | sed s/.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *// | sed s/\ // | tr '\n' ',')
	str+="maps"
	echo $str >> ../data.raw
done
cd ..
cd AI\ Comics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | pbmminkowski | sed s/.*:.// | tr '\n' ',' | sed s/[0-9]*,//) 

	# get the tilt and remove endline
	str+=$(pngtopam "$x" | pamtilt | tr '\n' ',')

	# get image sharpness and and convert number
	str+=$(pngtopam "$x" | pamsharpness | sed s/.*=.// | tr '\n' ',')

	# get histogram of white and black pixels
	# some awkard sed but it works
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | ppmhist -noheader | sed s/.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *// | sed s/\ // | tr '\n' ',')
	str+="comics"
	echo $str >> ../data.raw
done
cd ..
cd AI\ non-infographics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | pbmminkowski | sed s/.*:.// | tr '\n' ',' | sed s/[0-9]*,//) 

	# get the tilt and remove endline
	str+=$(pngtopam "$x" | pamtilt | tr '\n' ',')

	# get image sharpness and and convert number
	str+=$(pngtopam "$x" | pamsharpness | sed s/.*=.// | tr '\n' ',')

	# get histogram of white and black pixels
	# some awkard sed but it works
	str+=$(pngtopam "$x" | ppmtopgm | pgmtopbm | ppmhist -noheader | sed s/.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *[0-9]*.\ *// | sed s/\ // | tr '\n' ',')
	str+="non-infographics"
	echo $str >> ../data.raw
done
cd ..
