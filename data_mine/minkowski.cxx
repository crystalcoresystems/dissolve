#include<iostream>
#include<math.h>
#include<vector>
#define INF 999999999
#define LAST objects.size()-1

// REFERENCE: paper.

using namespace std;

void get_image();
void minko();
void add_point(int, int);
void combine(int a, int b);

struct data {
	int edges;
	int vertices;
	int faces;
	vector< pair<int, int> > c;
};


int bwimage[1024][1024];
vector<data *> objects;
int object_index[1026][1026];
int object_count;
int perimeter;
int size;
int euler;

int main() {
	get_image();
	minko();
	cout << "Objects: " << object_count << endl;
	cout << "Size: " << size << endl;
	cout << "Perimeter: " << perimeter << endl;
	cout << "Euler: " << euler << endl;
	cout << "Average Size: " << size*1.0/object_count << endl;
	cout << "Average Perimeter: " << perimeter*1.0/object_count << endl;
	cout << "Average Euler: " << euler*1.0/object_count << endl;
}

void minko() {
	object_count = 0;
	size = 0;
	perimeter = 0;
	euler = 0;

	for(int i=0;i<1026;i++)
		for(int j=0;j<1026;j++)
			object_index[i][j] = 0;
	for(int i=0;i<1024;i++)
		for(int j=0;j<1024;j++)
			if(bwimage[i][j])
				add_point(i+1, j+1);
	for(int i = 0; i < objects.size(); i++) {
		size += objects[i]->faces;
		perimeter += -4*objects[i]->faces + 2*objects[i]->edges;
		euler += objects[i]->faces - objects[i]->edges + objects[i]->vertices;
	}
}

void add_point(int i, int j) {
	int n_object;
	int a, b, c, d;
// b c d
// a
	a = object_index[i][j-1];
	b = object_index[i-1][j-1];
	c = object_index[i-1][j];
	d = object_index[i-1][j+1];
	if( !(a || b || c || d) ){
		objects.push_back(new data{4, 4, 1});
		objects[LAST]->c.push_back( {i, j} );
		object_index[i][j] = LAST+1;
		object_count++;
		return;
	}

	if (d && !c ) {
		if(a && !(d == a))
			//combine(a-1, d-1);
		a = object_index[i][j-1];
		b = object_index[i-1][j-1];
		if(b && !(d == b))
			//combine(d-1, b-1);
		b = object_index[i-1][j-1];
	}

	if(a)
		n_object = a - 1;
	if(b)
		n_object = b - 1;
	if(c)
		n_object = c - 1;
	if(d)
		n_object = d - 1;

	int upedge = 1;
	int leftedge = 1;
	int uprightvert = 1;
	int upleftvert = 1;
	int downleftvert = 1;

	if (a) {
		leftedge = 0;
		upleftvert = 0;
		downleftvert = 0;
	}
	if (b)
		upleftvert = 0;
	if (c) {
		upedge = 0;
		upleftvert = 0;
		uprightvert = 0;
	}
	if (d)
		uprightvert = 0;

	objects[n_object]->edges += upedge + leftedge + 2;
	objects[n_object]->vertices += uprightvert + upleftvert + downleftvert + 1;
	objects[n_object]->faces += 1;
	objects[n_object]->c.push_back( {i, j} );
	object_index[i][j] = n_object;
}

void combine(int a, int b) {
	objects[a]->edges += objects[b]->edges;	
	objects[a]->vertices += objects[b]->vertices;	
	objects[a]->faces += objects[b]->faces;	
	for(int i=0; i < objects[b]->c.size(); i++) { // cycle object coordinates
		objects[a]->c.push_back( objects[b]->c[i] ); // add to other object
		object_index[objects[b]->c[i].first][objects[b]->c[i].second] = a; // update index map
	}
	objects[b]->faces = 0; 
	objects[b]->vertices = 0; 
	objects[b]->edges = 0;
	object_count--;
}

void get_image() {
	for(int i=0; i<1024; i++)
		for(int j=0; j<1024;j++)
			cin >> bwimage[i][j];
}
