open(r,"<","data3.raw");

# These hold the input array sorted by their respective value
my @size_q1, @size_q2, @size_q3, @size_q4, @perimeter_q1, @perimeter_q2, @perimeter_q3, @perimeter_q4, @euler_q1, @euler_q2, @euler_q3, @euler_q4, @object_count, @solid_object_count, @noise, @total_size, @total_perimeter, @total_euler, @enoise, @edgeless_noise, @horizontal_edges, @horizontal_edge_segments, @horizontal_symmetry,@vertical_edges, @vertical_edge_segments, @vertical_symmetry, @sharpness;

my  @data = (\@size_q1, \@size_q2, \@size_q3, \@size_q4, \@perimeter_q1, \@perimeter_q2, \@perimeter_q3, \@perimeter_q4, \@euler_q1, \@euler_q2, \@euler_q3, \@euler_q4, \@object_count, \@solid_object_count, \@noise, \@total_size, \@total_perimeter, \@total_euler, \@enoise, \@edgeless_noise, \@horizontal_edges, \@horizontal_edge_segments, \@horizontal_symmetry,\@vertical_edges, \@vertical_edge_segments, \@vertical_symmetry, \@sharpness);

my @attribute_names = ( "size_q1", "size_q2", "size_q3", "size_q4", "perimeter_q1", "perimeter_q2", "perimeter_q3", "perimeter_q4", "euler_q1", "euler_q2", "euler_q3", "euler_q4", "object_count", "solid_object_count", "noise", "total_size", "total_perimeter", "total_euler", "enoise", "edgeless_noise", "horizontal_edges", "horizontal_edge_segments", "horizontal_symmetry","vertical_edges", "vertical_edge_segments", "vertical_symmetry", "sharpness");
# Get Input
foreach my $i ( 0 .. $#attribute_names ) {
	print ".*,";
}
