open(r,"<","early_data.raw");
my $DISCRETIZE = 50;

# These hold the input array sorted by their respective value

my @attribute_names = ( "size_q1", "size_q2", "size_q3", "size_q4", "perimeter_q1", "perimeter_q2", "perimeter_q3", "perimeter_q4", "euler_q1", "euler_q2", "euler_q3", "euler_q4", "object_count", "solid_object_count", "noise", "total_size", "total_perimeter", "total_euler", "enoise", "edgeless_noise", "horizontal_edges", "horizontal_edge_segments", "horizontal_symmetry","vertical_edges", "vertical_edge_segments", "vertical_symmetry", "sharpness");
# Get Input
my @input;
while (not eof(r)) {
	chomp(my $line = <r>);
	my @in = split(/,/, $line);
	push(@input, \@in);
}
close(r);


my @data;

foreach my $i (0 .. $#attribute_names) {
	my @x = sort { compare($i, $a, $b) } @input;
	push(@data, \@x);
}

my @disclist;

# meme, comic, infographic, non-infographic, propoganda, magazine, map
# $discretize
foreach my $i (0 .. $#data) {
	my @count = (0, 0, 0, 0, 0, 0, 0, 0);
	my @disc;
	for( my $j = 0; $j <= $#{$data[$i]}; $j++) {
		my $x = $data[$i][$j][$#data+1];
		if ($x eq "memes") { $count[0] += 1;}
		if  ($x eq "comics") { $count[1] += 1;}
		if  ($x eq "infographic") { $count[2] += 1;}
		if  ($x eq "non-infographics") { $count[3] += 1;}
		if  ($x eq "propaganda") { $count[4] += 1;}
		if  ($x eq "magazine") { $count[5] += 1;}
		if  ($x eq "maps") { $count[6] += 1;}
		if  ($x eq "architecture") { $count[7] += 1;}
		for my $y (0 .. $#count) {
			if ($count[$y] > $DISCRETIZE) {
				my $name = getname($y);
				while($j <= $#{$data[$i]} and $data[$i][$j+1][$#data+1] eq $name) {
					$j++;
				}
				push(@disc, $j);
				@count = (0, 0, 0, 0, 0, 0, 0, 0);
			}
		}
	}
	push(@disclist, \@disc);
}

#	for my $i( 0.. $#disclist) {
#		for my $j ( 0 .. $#{ $disclist[$i] }) {
#			print $disclist[$i][$j], " ";
#		}
#		print "\n";
#	}
#	print "\n";


# go through all 10 categories and replace with nominal value
for (my $i = 0; $i <= $#data; $i++) {
	@input = sort { compare($i, $a, $b) } @input;
	my $prev = 0;
	my $n;
	for ($n = 0; $n < scalar(@{ $disclist[$i] }); $n++) {
		for(my $j = $prev; $j < $disclist[$i][$n] ; $j++) {
			$input[$j][$i] = $n + 1;
		}
		$prev = $disclist[$i][$n];
	}
	for(my $j = $prev; $j < 1354; $j++) {
		$input[$j][$i] = $n+1;
	}
}

print "\@relation image\n\n";

foreach my $i (0 .. $#data) {
	print "\@attribute ", $attribute_names[$i], " {";
	for my $j (0 .. $#{ $disclist[$i] }) {
		print $j+1, ",";
	}
	print $#{ $disclist[$i] } + 2;
	print "}\n";
}

print "\@attribute class {memes,comics,infographic,non-infographics,propaganda,architecture,maps}\n";

print "\@data\n";

@input = sort{compare_string($#data+1,$a,$b)} @input;

foreach my $row (@input) {
	print map("$_,", @$row);	
	print "\n";
}

exit;

sub getname{
	shift;
	if ($_ == 0) { return "memes";}
	if  ($_ == 1) { return "comics";}
	if  ($_ == 2) { return "infographic";}
	if  ($_ == 3) { return "non-infographics";}
	if  ($_ == 4) { return "propaganda";}
	if  ($_ == 5) { return "magazine";}
	if  ($_ == 6) { return "maps";}
}
#compare stuff
sub compare_string{
	my $i = shift;
	if($a->[$i] gt $b->[$i]){
		return -1;
	} elsif($a eq $b){
		return 0;
	}
	else {
		return 1;
	}
}

#compare stuff
sub compare{
	my $i = shift;
	if($a->[$i] < $b->[$i]){
		return -1;
	} elsif($a == $b){
		return 0;
	}
	else {
		return 1;
	}
}

