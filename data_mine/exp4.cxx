// Practicing inputing bit map to c++ file
#include <iostream>
#include <string>

using namespace std;

int image[1024][1024];
int bw_img[512][512];
int diag_edge[512][512];
int boolean_solve(int input, int mech);
void get_image();
void gray2bw();
void print_512(int a[512][512]);
void get_diag_edges();
bool inbounds(int a, int b);
int noise_count(int a[512][512]);

int vertical_edge_count(int a[512][512]) ;
int horizontal_edge_count(int a[512][512]) ;
bool horizontal_edge_segments[32][32];
bool vertical_edge_segments[32][32];

int horizontal_noise_distrubution[32];
int vertical_noise_distrubution[32];

int vertz(); // count vertical line segments
int horz(); // count horizontal line segments

int sum_en(int a[512][512]); // Sum edgeless noise
int vertical_symmetry(int a[512][512]);
int horizontal_symmetry(int a[512][512]);


int main() {
        int total_noise;
        int horizontal_edges;
        int vertical_edges;
        int horizontal_edge_segcount;
        int vertical_edge_segcount;
        int edgeless_noise;
        int vertical_sym;
        int horizontal_sym;

    // ** CREATE EDGE MAP ** //
        get_image();
        gray2bw();
        get_diag_edges();

    // ** GET NOISE AND EDGE DATA ** //
        total_noise = noise_count(diag_edge);
        horizontal_edges = horizontal_edge_count(diag_edge);
        vertical_edges = vertical_edge_count(diag_edge);
        horizontal_edge_segcount = horz();
        vertical_edge_segcount = vertz();
        edgeless_noise = sum_en(diag_edge);
        vertical_sym = vertical_symmetry(diag_edge);
        horizontal_sym = horizontal_symmetry(diag_edge);

        cout << total_noise << ",";
        cout << edgeless_noise << ",";
        cout << horizontal_edges << ",";
        cout << horizontal_edge_segcount << ",";
        cout << horizontal_sym << ",";
        cout << vertical_edges << ",";
        cout << vertical_edge_segcount << ",";
        cout << vertical_sym << ",";
}

//cute converter
// (a[i][j] & (a[i][j] << 1)) | 1
// Get vertical symmetry../
int vertical_symmetry(int a[512][512]) {
        int sum = 0;
        for (int i=0; i<512;i++) {
                for(int j=0; j<256;j++) {
                        int x = (a[i][j] & (a[i][j] << 1)) | 1;
                        int y = (a[i][511 - j] & (a[i][511 - j] << 1)) | 1;
                        int z = x & y;
                        sum += z >> 1;
                }
        }
        return sum;
}//..
// Get horizontal symmetry../
int horizontal_symmetry(int a[512][512]) {
        int sum = 0;
        for (int i=0; i<256;i++) {
                for(int j=0; j<512;j++) {
                        int x = (a[i][j] & (a[i][j] << 1)) | 1;
                        int y = (a[511 - i][j] & (a[511 - i][j] << 1)) | 1;
                        int z = x & y;
                        sum += z >> 1;
                }
        }
        return sum;
}//..

// Count horizontal edges../
int horizontal_edge_count(int a[512][512]) {
// cut image into 16*16 squares
// go through each square and determine if there is a line by summing
// if the total of line sums is over threshold don't count as a line
// if the previous was a line dont count (continued line)
        int count = 0;
        for(int i=0; i<512;i+=16) {
                bool previous_line = false;
                for(int j=0; j<512; j+=16) {
                        int total_sum = 0;
                        bool line;
                        for(int k=0; k<16; k++) {
                                int line_sum = 0;
                                for(int l=0; l<16; l++) {
                                        line_sum += a[i+k][j+l] & (a[i+k][j+l] >> 1);
                                }
                                total_sum += line_sum;
                                if (line_sum > 8 )
                                        line = true;
                        }
                        if (total_sum > 50)
                                line = false;
                        horizontal_edge_segments[i/16][j/16] = line;
                        if(line && !previous_line)
                                count++;
                        previous_line = line;
                }
        }
        return count;
}//..
// Count vertical edges../
int vertical_edge_count(int a[512][512]) {
// cut image into 16*16 squares
// go through each square and determine if there is a line by summing
// if the total of line sums is over threshold don't count as a line
// if the previous was a line dont count (continued line)
        int count = 0;
        for(int i=0; i<512;i+=16) {
                bool previous_line = false;
                for(int j=0; j<512; j+=16) {
                        int total_sum = 0;
                        bool line;
                        for(int k=0; k<16; k++) {
                                int line_sum = 0;
                                for(int l=0; l<16; l++) {
                                        line_sum += a[j+l][i+k] & (a[j+l][i+k] >> 1);
                                }
                                total_sum += line_sum;
                                if (line_sum > 8 )
                                        line = true;
                        }
                        if (total_sum > 50)
                                line = false;
                        vertical_edge_segments[i/16][j/16] = line;
                        if(line && !previous_line)
                                count++;
                        previous_line = line;
                }
        }
        return count;
}//..
// Get the noise of an image../
int noise_count(int a[512][512]) {
        int count = 0;
        for (int i =0; i <512; i++)
                for (int j=0; j < 512; j++)
                        count += a[i][j] & (a[i][j] >> 1);
        return count;
}//..
// Get edges travelling diagonally../
void get_diag_edges() {
    //travel matrix diagonally in different directions on different intervals
        for(int j=0; j<512; j++) {
                int i = 1;
                int y = j;
                while(inbounds(i+1,y+1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i+1][y+1]);
                        i+=2;
                        y+=2;
                }
        }
        for(int j=0; j<512; j++) {
                int i = 510;
                int y = j;
                while(inbounds(i-1,y+1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i-1][y+1]);
                        i-=2;
                        y+=2;
                }
        }
        for(int j=0; j<512; j++) {
                int i = 0;
                int y = j;
                while(inbounds(i+1,y+1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i+1][y+1]);
                        i+=2;
                        y+=2;
                }
        }
        for(int j=0; j<512; j++) {
                int i = 511;
                int y = j;
                while(inbounds(i-1,y+1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i-1][y+1]);
                        i-=2;
                        y+=2;
                }
        }
        for(int j=0; j<512; j++) {
                int i = 511;
                int y = j;
                while(inbounds(i-1,y-1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i-1][y-1]);
                        i-=2;
                        y-=2;
                }
        }
        for(int j=0; j<512; j++) {
                int i = 0;
                int y = j;
                while(inbounds(i+1,y-1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i+1][y-1]);
                        i+=2;
                        y-=2;
                }
        }
        for(int j=0; j<512; j++) {
                int i = 510;
                int y = j;
                while(inbounds(i-1,y-1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i-1][y-1]);
                        i-=2;
                        y-=2;
                }
        }
        for(int j=0; j<512; j++) {
                int i = 1;
                int y = j;
                while(inbounds(i+1,y-1)){
                        diag_edge[i][y] = boolean_solve(bw_img[i][y], bw_img[i+1][y-1]);
                        i+=2;
                        y-=2;
                }
        }
}//..
// Check if index is inbounds../
bool inbounds(int a, int b) {
        if(a >= 0 && a < 512)
                if(b >= 0 && b < 512)
                        return true;
        return false;
}//..
// Print a 2d RPS array of 512../
void print_512(int a[512][512]) {
        for(int i=0; i < 512; i++) {
                for (int j=0; j<512; j++) {
                        if (a[i][j] == 0)
                                cout << " ";
                        if (a[i][j] == 1)
                                cout << " ";
                        if (a[i][j] == 2)
                                cout << ".";
                        if (a[i][j] == 3)
                                cout << "+";
                }
                cout << endl;
        }
}//..
        // Get Image as Grayscale from stdin../
void get_image() {
        for(int i=0; i<1024; i++)
                for(int j=0; j<1024;j++)
                        cin >> image[i][j];
}//..
        // Convert gretscale image to black and white (1 and 2)../
void gray2bw() {
        for(int i=0; i<1024; i+=2) {
                for(int j=0;j<1024;j+=2) {
                        int average = (image[i][j] + image[i+1][j] + 
                                image[i][j+1] + image[i+1][j+1])/4;
                        if (average > 195)
                                bw_img[i/2][j/2] = 2;
                        else
                                bw_img[i/2][j/2] = 1;
                }
        }
}//..
// RPS automata solver../
int boolean_solve(int input, int mech)
{
        int tmp, tmp2, tmp3;

        tmp = input ^ mech;
        tmp3 = (tmp >> 1) | (tmp << 1) | tmp;
        tmp2 = tmp ^ tmp3;
        tmp2 = tmp2 ^ tmp3;
        tmp = ~tmp3 & input;
        return (tmp ^ tmp2) & 3;
}//..
// segment countrs../
int vertz() {
        int count = 0;
        for(int i=0;i<32;i++)
                for(int j=0;j<32;j++)
                        if(vertical_edge_segments[i][j])
                                count++;
        return count;
}
int horz() {
        int count = 0;
        for(int i=0;i<32;i++)
                for(int j=0;j<32;j++)
                        if(horizontal_edge_segments[i][j])
                                count++;
        return count;
}//..
// Sum edgelesss noise../
int sum_en(int a[512][512]) {
        int sum =0;
        for(int i=0;i<32;i++) {
                for(int j=0;j<32;j++){
                        if(vertical_edge_segments[i][j] ||
                                        horizontal_edge_segments[i][j])
                                continue;
                        for(int k=0; k<16;k++) {
                                for(int l=0; l<16;l++) {
                                        sum += a[i*16 + k][j*16 + l] & (a[i*16 + k][j*16 + l] >> 1);
                                }
                        }
                }
        }
        return sum;
}//..


// not used -----------------------------------------------------------------------------
/*
// Combine vertical and horizontal edges../
void combine_edges() {
        for(int i=0; i<512; i++){
                for(int j=0; j<512; j++) {
                        edges[i][j] = boolean_solve(vertical_edge[i][j], horizontal_edge[i][j]);
                        edges[i][j] = boolean_solve(edges[i][j], diag_edge[i][j]);
               }
        }
}//...
        // Get the horizontal edges from black and white image../
void get_horizontal_edges() {
        for(int i=0; i<512; i+=2)
                for(int j=0; j<512; j++)
                        horizontal_edge[i][j] = boolean_solve(bw_img[i][j], bw_img[i+1][j]);
        for(int i=1; i<512; i+=2)
                for(int j=0; j<512; j++)
                        horizontal_edge[i][j] = boolean_solve(bw_img[i][j], bw_img[i+1][j]);
}//..
        // Get vertical edges from black and white image../
void get_vertical_edges() {
        for(int i=0; i<512; i++)
                for(int j=0; j<512; j+=2)
                        vertical_edge[i][j] = boolean_solve(bw_img[i][j], bw_img[i][j+1]);
        for(int i=0; i<512; i++)
                for(int j=1; j<512; j+=2)
                        vertical_edge[i][j] = boolean_solve(bw_img[i][j], bw_img[i][j+1]);
}//..
*/
