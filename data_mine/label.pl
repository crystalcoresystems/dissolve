open(r,"<","early_data.raw");
my $DISCRETIZE = 50;

# These hold the input array sorted by their respective value

my @attribute_names = ( "size_q1", "size_q2", "size_q3", "size_q4", "perimeter_q1", "perimeter_q2", "perimeter_q3", "perimeter_q4", "euler_q1", "euler_q2", "euler_q3", "euler_q4", "object_count", "solid_object_count", "noise", "total_size", "total_perimeter", "total_euler", "enoise", "edgeless_noise", "horizontal_edges", "horizontal_edge_segments", "horizontal_symmetry","vertical_edges", "vertical_edge_segments", "vertical_symmetry", "sharpness");
# Get Input
my @input;
while (not eof(r)) {
	chomp(my $line = <r>);
	my @in = split(/,/, $line);
	push(@input, \@in);
}
close(r);


my @data;

print "\@relation image\n\n";

foreach my $i (0 .. $#attribute_names) {
	print "\@attribute ", $attribute_names[$i], " numeric\n";
}

print "\@attribute class {memes,comics,infographic,non-infographics,propaganda,architecture,maps}\n";

print "\@data\n";

@input = sort{compare_string($#data+1,$a,$b)} @input;

foreach my $row (@input) {
	print map("$_,", @$row);	
	print "\n";
}

exit;

sub getname{
	shift;
	if ($_ == 0) { return "memes";}
	if  ($_ == 1) { return "comics";}
	if  ($_ == 2) { return "infographic";}
	if  ($_ == 3) { return "non-infographics";}
	if  ($_ == 4) { return "propaganda";}
	if  ($_ == 5) { return "magazine";}
	if  ($_ == 6) { return "maps";}
}
#compare stuff
sub compare_string{
	my $i = shift;
	if($a->[$i] gt $b->[$i]){
		return -1;
	} elsif($a eq $b){
		return 0;
	}
	else {
		return 1;
	}
}

#compare stuff
sub compare{
	my $i = shift;
	if($a->[$i] < $b->[$i]){
		return -1;
	} elsif($a == $b){
		return 0;
	}
	else {
		return 1;
	}
}

