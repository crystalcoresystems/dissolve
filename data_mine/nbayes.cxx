#include<iostream>
#include<set>

using namespace std;

// For example size, perimeter, euler class
#define RECORDS 5
#define ATTRIBUTES 4
#define MAX_ATTR 3
#define MAX_CLASS 2
#define INF 99999999

using namespace std;

struct rule {
	int clas5 = -1;
	int correct;
	int errors;
};

struct data_cell {
	string attribute;
	int nominal;
	int numeric = 0;
	string nominal_val;
} a1a {"size", 0}, a2a{"perimeter", 1}, a3a{"euler",2}, ca{"class",0,0,"Infographic"},
  a1b {"size", 0}, a2b{"perimeter", 2}, a3b{"euler",0}, cb{"class",1,0,"Comic"},
  a1c {"size", 0}, a2c{"perimeter", 1}, a3c{"euler",1}, cc{"class",0,0,"Infographic"},
  a1d {"size", 1}, a2d{"perimeter", 0}, a3d{"euler",1}, cd{"class",2,0,"Poster"},
  a1e {"size", 1}, a2e{"perimeter", 0}, a3e{"euler",0}, ce{"class",1,0,"Comic"};

data_cell input_data[RECORDS][ATTRIBUTES] = { {a1a, a2a, a3a, ca},
			     		{a1b, a2b, a3b, cb},
			     		{a1c, a2c, a3c, cc},
			     		{a1d, a2d, a3d, cd},
			     		{a1e, a2e, a3e, ce}};

float probability_array[100][200][100];
int class_hist[100];
int class_size[MAX_CLASS];
int attribute_size[ATTRIBUTES];
set<int> attribute_set[ATTRIBUTES];

// i = Attribute
// j = Class value
int histogram [100][100];

int main() {
	int attributes = ATTRIBUTES - 1;
	int clas5 = ATTRIBUTES - 1;
	int records = RECORDS;
	// Get the size of each class
	for(int i=0; i<RECORDS; i++) {
		for(int j=0; j<ATTRIBUTES; j++) {
			attribute_set[j].insert(input_data[i][j].nominal);
		}
	}
	// Get a count of the number of values for each attribute;
	//    -> Actually should be the max nominal value... i guess
	for (int i=0; i<ATTRIBUTES; i++) {
		attribute_size[i] = attribute_set[i].size();
	}
	for(int i=0; i<records;i++) 
		class_hist[ input_data[i][clas5].nominal ]++;
	for(int i=0;i<attribute_size[clas5];i++)
		cout << class_hist[i]<< " ";
	cout << endl;

	for(int i=0; i<attributes;i++) {
		for(int j=0; j<100; j++)
			for(int k=0; k<100; k++)
				histogram[j][k] = 0;
		
		// make a histogram of the amount of times each attribute value
		// belongs to a class
		for(int j=0; j<records; j++) {	
			int this_attribute = input_data[j][i].nominal;
			int this_class = input_data[j][clas5].nominal;
			histogram[this_class][this_attribute]++;
		}
		bool zeroval = false;
		for(int j=0; j<attributes; j++)
			for(int k=0; k<attributes; k++)
				if (histogram[j][k] == 0)
					zeroval = true;
	
		// Build probability matrix for attribute from histogram
		if (zeroval) {
			for(int j=0; j<attribute_size[clas5]; j++)
				for(int k=0; k<attribute_size[i]; k++)
					probability_array[i][j][k] = (histogram[j][k] + 1.0) / (attribute_size[i] + class_hist[j]);
		} else {
			for(int j=0; j<attribute_size[i]; j++)
				for(int k=0; k<attributes; k++)
					probability_array[i][j][k] = (histogram[j][k] + 1.0) / class_hist[j];
		}

	}
	// Finish probability array with class probabilities../
	for(int i=0; i < attribute_size[clas5]; i++)
		probability_array[clas5][0][i] = 1.0*class_hist[i] / records;
	
	for(int i=0; i<attributes; i++){
		cout << "Attribute: " << i << endl;
		for(int j=0;j<attribute_size[clas5];j++){
			cout << "Class " << j << ":";
			for(int k=0;k<attribute_size[i];k++){
				cout << " - " << probability_array[i][j][k];
			}
			cout << endl;
		}
	}

}
