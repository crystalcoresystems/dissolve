#include<iostream>
#include<math.h>
#define INF 999999999

// REFERENCE: paper.

using namespace std;

void get_image();
void grey_convert(int, int);
void convert2bw(int);

int greyscale_image[1024][1024];
int main() {
	int bin = 8; // Species trait
	bin = pow(2,bin);
	get_image();
	convert2bw(bin);
	for(int i=0;i<1024;i++) {
		for(int j=0;j<1024;j++) {
			cout << greyscale_image[i][j] + bin/2 - 1 << " ";
		}
		cout << endl;
	}
}

void convert2bw(int bin) {
	int long long histogram[256];
	for(int i=0;i<256;i++)
		histogram[i] = 0;
	for(int i=0;i<1024;i++)
		for(int j=0;j<1024;j++)
			histogram[ greyscale_image[i][j]  ]++;

	int d = 2; // the distance at which to find a neighbor (double for cute reasons)
	while ( d < bin ) {
		int m = INF; //  m for minimum
		int mi;
		for(int i=0; i<256; i++) { // Find smallest value in histogram
			if ( histogram[ d * (i/d) ] == 0 ) { continue; } 	//skip if empty
			if ( histogram[ d * (i/d) + d/2 ] == 0 ) { continue; }	//skip if neighbor empty
			if ( histogram[ d * (i/d) ] < m ) { m = histogram[ d * (i/d) ]; mi = i;}
		}
		if ( m == INF ) { d*=2; }
		else {
			histogram[ d * (mi/d) ] += histogram[ d * (mi/d) + d/2 ];
			histogram[ d * (mi/d) + d/2 ] = 0;
			grey_convert( d * (mi/d), d * (mi/d) + d/2 );
		}
	}
}

void grey_convert(int to, int from) {
	for(int i=0; i<1024; i++)
		for(int j=0; j<1024; j++)
			if (greyscale_image[i][j] == from)
				greyscale_image[i][j] = to;
}
			
void get_image() {
	for(int i=0; i<1024; i++)
		for(int j=0; j<1024;j++)
			cin >> greyscale_image[i][j];
}
