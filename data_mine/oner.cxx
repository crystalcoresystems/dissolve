#include<iostream>
#include<string>

// For example size, perimeter, euler class
#define RECORDS 5
#define ATTRIBUTES 4
#define MAX_ATTR 3
#define MAX_CLASS 2
#define INF 99999999

using namespace std;

struct rule {
	int clas5 = -1;
	int correct;
	int errors;
};

struct data_cell {
	string attribute;
	int nominal;
	int numeric = 0;
	string nominal_val;
} a1a {"size", 1}, a2a{"perimeter", 2}, a3a{"euler",3}, ca{"class",0,0,"Infographic"},
  a1b {"size", 1}, a2b{"perimeter", 3}, a3b{"euler",1}, cb{"class",1,0,"Comic"},
  a1c {"size", 1}, a2c{"perimeter", 2}, a3c{"euler",3}, cc{"class",0,0,"Infographic"},
  a1d {"size", 2}, a2d{"perimeter", 1}, a3d{"euler",2}, cd{"class",2,0,"Poster"},
  a1e {"size", 2}, a2e{"perimeter", 1}, a3e{"euler",1}, ce{"class",1,0,"Comic"};

data_cell input_data[RECORDS][ATTRIBUTES] = { {a1a, a2a, a3a, ca},
			     		{a1b, a2b, a3b, cb},
			     		{a1c, a2c, a3c, cc},
			     		{a1d, a2d, a3d, cd},
			     		{a1e, a2e, a3e, ce}};

// For Each Attribute calculate the amount of times it matches each 
//
// for each column build a histogram
//    for example size 1 = 0 2 times;
//    for example size 1 = 1 1 times;

// i = Attribute
// j = Nominal int
// k = attributed Class
int histogram [100][200][100];

int main() {
	int attributes = ATTRIBUTES - 1;
	int records = RECORDS;
	int classes     = 3;
	int majority_hist[100];
	int majority_max = 0;
	int majority;
	for(int i=0; i<RECORDS; i++) {
		majority_hist[ input_data[i][attributes].nominal ] ++;
		if (majority_hist[ input_data[i][attributes].nominal ] > majority_max) {
			majority_max = majority_hist[ input_data[i][attributes].nominal ];
			majority = input_data[i][attributes].nominal;
		}
	}
	// make a histogram of the relationship of attributes to class
	for(int i=0; i<attributes;i++) {
		for(int j=0; j<records; j++) {	
			int this_attribute = input_data[j][i].nominal;
			int this_class = input_data[j][attributes].nominal;
			histogram[i][this_attribute][this_class]++;
		}
	}
	// use histogram to find the best attribute (lowest error rate)
	int min_errors = INF;
	int best_attribute = 0;
	for(int i=0; i<attributes;i++) {
		int errors = 0;
		for(int j=0; j<=MAX_ATTR; j++)  {
			int max = 0;
			int count = 0;
			for(int k=0; k<=MAX_CLASS; k++) {
				if( histogram[i][j][k] > max )
					max = histogram[i][j][k];
				count += histogram[i][j][k];
			}
			errors += count - max;
		}
		if (errors < min_errors) {
			min_errors = errors;
			best_attribute = i;
		}
	}
	cout << " Best Attribute: " << best_attribute << endl;
	// create rules based on best attribute
	int rule[100];
	for(int i=0; i<=MAX_ATTR; i++) {
		int max = 0;
		int max_i = 0;
		for(int j=0; j<=MAX_CLASS;j++) {
			if (histogram[best_attribute][i][j] > max) {
				max = histogram[best_attribute][i][j];
				max_i = j;
			}
		}
		if (max_i == 0) {
			rule[i] = majority;
		} else {
			rule[i] = max_i;
		}
		cout << "Rule " << i << ": " << rule[i] << endl;
	}
	
}
