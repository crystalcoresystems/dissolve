#!/bin/bash

cd AI\ infographics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+="infographic"
	echo $str >> ../data2.raw
done
cd ..
cd AI\ architecture
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+="architecture"
	echo $str >> ../data2.raw
done
cd ..
cd AI\ memes
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+="memes"
	echo $str >> ../data2.raw
done
cd ..
cd AI\ propaganda\ posters
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+="propaganda"
	echo $str >> ../data2.raw
done
cd ..
cd AI\ maps
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+="maps"
	echo $str >> ../data2.raw
done
cd ..
cd AI\ Comics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+="comics"
	echo $str >> ../data2.raw
done
cd ..
cd AI\ non-infographics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+="non-infographics"
	echo $str >> ../data2.raw
done
cd ..
