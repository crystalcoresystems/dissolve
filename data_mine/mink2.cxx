#include<iostream>
#include<math.h>
#include<vector>
#define INF 999999999

// REFERENCE: paper.

using namespace std;

void get_image();
void minko();
void add_point(int, int);
int bwimage[1026][1026];
int vertices;
int edges;
int faces;

int main() {
	get_image();
	minko();

	cout << "Size: " << faces << endl;
	cout << "Perimeter: " << -4*faces + 2*edges << endl;
	cout << "Euler: " << faces - edges + vertices << endl;
}

void minko() {
	vertices = 0;
	edges = 0;
	faces = 0;

	for(int i=0;i<1026;i++)
		for(int j=0;j<1026;j++)
			if(bwimage[i][j])
				add_point(i, j);
}

void add_point(int i, int j) {
	int a, b, c, d;

	int upedge = 1;
	int leftedge = 1;
	int uprightvert = 1;
	int upleftvert = 1;
	int downleftvert = 1;
// cell representation
// b c d
// a X
	a = bwimage[i][j-1];
	b = bwimage[i-1][j-1];
	c = bwimage[i-1][j];
	d = bwimage[i-1][j+1];

// Remove vertices and edges based on existing neighbors
	leftedge     *= (a - 1)*-1;      // (x - 1) * -1 -> cute 1 0 switch
	upleftvert   *= (a - 1)*-1;      // sets edge or vertex to 0 if corresponding cell is 1
	downleftvert *= (a - 1)*-1;
	upleftvert   *= (b - 1)*-1;
	upedge       *= (c - 1)*-1;
	upleftvert   *= (c - 1)*-1;
	uprightvert  *= (c - 1)*-1;
	uprightvert  *= (d - 1)*-1;

	edges += upedge + leftedge + 2;
	vertices += uprightvert + upleftvert + downleftvert + 1;
	faces += 1;
}

void get_image() {
	for(int i=1; i<1025; i++) {
		bwimage[0][i]    = 0;  // Set edges to zero to pad
		bwimage[1025][i] = 0;  // 1024x1024 array
		bwimage[i][0]    = 0;
		bwimage[i][1025] = 0;
		for(int j=1; j<1025;j++)
			cin >> bwimage[i][j];
	}
}
