#!/bin/bash

cd AI\ infographics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../edges.o)
	str+=$(pngtopnm "$x" | pamsharpness | tr -d [a-z],S,=," ")
	str+=","
	str+="infographic"
	echo $str >> ../data4.raw
done
cd ..
cd AI\ architecture
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../edges.o)
	str+=$(pngtopnm "$x" | pamsharpness | tr -d [a-z],S,=," ")
	str+=","
	str+="architecture"
	echo $str >> ../data4.raw
done
cd ..
cd AI\ memes
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../edges.o)
	str+=$(pngtopnm "$x" | pamsharpness | tr -d [a-z],S,=," ")
	str+=","
	str+="memes"
	echo $str >> ../data4.raw
done
cd ..
cd AI\ propaganda\ posters
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../edges.o)
	str+=$(pngtopnm "$x" | pamsharpness | tr -d [a-z],S,=," ")
	str+=","
	str+="propaganda"
	echo $str >> ../data4.raw
done
cd ..
cd AI\ maps
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../edges.o)
	str+=$(pngtopnm "$x" | pamsharpness | tr -d [a-z],S,=," ")
	str+=","
	str+="maps"
	echo $str >> ../data4.raw
done
cd ..
cd AI\ Comics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../edges.o)
	str+=$(pngtopnm "$x" | pamsharpness | tr -d [a-z],S,=," ")
	str+=","
	str+="comics"
	echo $str >> ../data4.raw
done
cd ..
cd AI\ non-infographics
for x in *
do
	str=""
	# get the minkowski numbers and convert with sed
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../grey2bw_bins.o 8| ./../objects.o)
	str+=$(pngtopnm "$x" | ppmtopgm -plain | tail -n +4 | ./../edges.o)
	str+=$(pngtopnm "$x" | pamsharpness | tr -d [a-z],S,=," ")
	str+=","
	str+="non-infographics"
	echo $str >> ../data4.raw
done
cd ..
