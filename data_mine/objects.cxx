#include<iostream>
#include<algorithm>

using namespace std;

void get_image();
void dfs(int i, int j);
int bwimage[1026][1026];
int object_data[1026][1026];

void minko();
void add_point(int, int, int);
struct data {
int vertices;
int edges;
int faces;
}object_mdata[10000];
int vertices;
int edges;
int faces;
int objects;

int compare_faces(data a, data b);
int compare_faces2(data a, data b);
int compare_perimeter(data a, data b);
int compare_euler(data a, data b);
int compare_euler2(data a, data b);
int main() {
	objects = 0;
	for(int i=0;i<1026;i++)
		for(int j=0;j<1026;j++)
			object_data[i][j] = 0;
	get_image();
	for(int i=1; i<1025; i++)
		for(int j=1; j<1025; j++)
			if(bwimage[i][j]){
				dfs(i, j);
				objects++;
				object_mdata[objects] = {0,0,0};
			}
	minko();
	sort(object_mdata, object_mdata+objects, compare_faces2);
	int pixels = 0;
	while(object_mdata[pixels].faces == 1) {
		objects--;
		pixels++;
	}
	sort(object_mdata, object_mdata+objects+pixels, compare_faces);
	for(int i=0; i < objects; i+= objects/4+1) {
		cout << object_mdata[i].faces << ",";
	}
	sort(object_mdata, object_mdata+objects, compare_perimeter);
	for(int i=0; i < objects; i+= objects/4+1) {
		cout << object_mdata[i].faces*-4 + object_mdata[i].edges*2 << ",";
	}
	sort(object_mdata, object_mdata+objects, compare_euler2);
	int solid_objects = 0;
	while(object_mdata[solid_objects].faces - object_mdata[solid_objects].edges + object_mdata[solid_objects].vertices == 1)
		solid_objects++;
	sort(object_mdata, object_mdata+objects, compare_euler);
	int eco = 0;
	for(int i=0; i < (objects - solid_objects); i+= ((objects - solid_objects)/4)+1) {
		cout << object_mdata[i].faces - object_mdata[i].edges + object_mdata[i].vertices << ",";
		eco++;
	}
	while (eco++ < 4)
		cout << "0,";
		
	cout << objects << ",";
	cout << solid_objects << ",";
	cout << pixels << ",";
	cout << faces << ",";
	cout << -4*faces + 2*edges << ",";
	cout << faces - edges + vertices << ",";
}

int compare_faces(data a, data b) {
	return a.faces > b.faces;
}
int compare_faces2(data a, data b) {
	return a.faces < b.faces;
}
int compare_perimeter(data a, data b) {
	return a.faces*-4 + a.edges*2 > b.faces*-4 + b.edges*2;
}
int compare_euler2(data a, data b) {
	return a.faces - a.edges + a.vertices > b.faces - b.edges + b.vertices;
}
int compare_euler(data a, data b) {
	return a.faces - a.edges + a.vertices < b.faces - b.edges + b.vertices;
}

void dfs(int i, int j) {
	if(bwimage[i][j] == 0)
		return;
	object_data[i][j] = objects+1;
	bwimage[i][j] = 0;
	dfs(i+1,j);
	dfs(i-1,j);
	dfs(i,j+1);
	dfs(i,j-1);
	dfs(i+1,j+1);
	dfs(i-1,j-1);
	dfs(i+1,j-1);
	dfs(i-1,j+1);
}

void get_image() {
	for(int i=1; i<1025; i++) {
		bwimage[0][i]    = 0;  // Set edges to zero to pad
		bwimage[1025][i] = 0;  // 1024x1024 array
		bwimage[i][0]    = 0;
		bwimage[i][1025] = 0;
		for(int j=1; j<1025;j++)
			cin >> bwimage[i][j];
	}
}

void minko() {
	vertices = 0;
	edges = 0;
	faces = 0;

	for(int i=0;i<1026;i++)
		for(int j=0;j<1026;j++)
			if(object_data[i][j])
				add_point(i, j, object_data[i][j]-1);
}

void add_point(int i, int j, int obj) {
	int a, b, c, d;

	int upedge = 1;
	int leftedge = 1;
	int uprightvert = 1;
	int upleftvert = 1;
	int downleftvert = 1;
// cell representation where x is the current i,j cell
// b c d
// a X
	a = object_data[i][j-1];
	b = object_data[i-1][j-1];
	c = object_data[i-1][j];
	d = object_data[i-1][j+1];

	// convert 0 to 1 and everything else to zero
	a = (a+1) / (a*2+1);
	b = (b+1) / (b*2+1);
	c = (c+1) / (c*2+1);
	d = (d+1) / (d*2+1);

// Remove vertices and edges based on existing neighbors
// (existing neighbors == 0 which zeros the edge or vertex when multiplying)
	leftedge     *= a; 
	upleftvert   *= a;
	downleftvert *= a;
	upleftvert   *= b;
	upedge       *= c;
	upleftvert   *= c;
	uprightvert  *= c;
	uprightvert  *= d;

	object_mdata[obj].edges += upedge + leftedge + 2;
	object_mdata[obj].vertices += uprightvert + upleftvert + downleftvert + 1;
	object_mdata[obj].faces += 1;

	edges += upedge + leftedge + 2;
	vertices += uprightvert + upleftvert + downleftvert + 1;
	faces += 1;
}
