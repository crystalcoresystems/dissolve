$!
$! filename: spansquatch.com
$! square all digits spanning input
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$ usage = "@spansquatch num1 num2 (adds squares of all numbers in range and including)
$main:
$ gosub process_args
$ acc = 0
$ num = f$integer(p1)
$loop:
$ acc = acc + (num * num)
$ num = num + 1
$ if( num .gt. p2 )
$ then
$       write sys$output acc
$       goto bail
$ endif
$ goto loop
$
$process_args:
$ if( p1 .nes. "0" )
$ then
$       if( p1 .eq. "" )
$       then
$               write sys$output usage
$               goto bail
$       endif
$ endif
$!
$ if( p2 .nes. "0" )
$ then
$       if( p2 .eq. "" )
$       then
$               write sys$output usage
$               goto bail
$       endif
$ endif
$!
$ if( p1 .gt. p2 )
$ then
$       temp = p1
$       p1 = p2
$       p2 = temp
$ endif
$ return
$!
$bail:
$ exit
