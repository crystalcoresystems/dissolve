$!
$! filename: move_to_new_dir.com
$!    input: name of subdirectory
$!   output: empties directory and fills subdirectory with copies
$!     idea: ask for subdirectory and verify presence
$!              copy all files to subdirectoy and delete from present dir.
$!     form: command sequence with read loop for filenames


$! this is a script that will move all files of a directory
$! into a subdirectory
$!
$main:
$ purge
$ gosub get_directory
$ display tmp
$ type 'tmp
$ gosub relocate
$ goto bail
$
$relocate:
$ open/read lexit 'tmp
$10:
$       read/end_of_file=19 lexit line
$       display line
$       if( f$locate(".DIR;", line) .ne. f$length(line) .or. -
            f$locate(";", line) .eq f$length(line) .or. -
            line .eqs. tmp )
$       then
$               goto 10
$       endif
$       tocopy = "[.''dname']''line'"
$       copy 'line 'tocopy
$       delet 'line
$       goto 10
$19:
$ close lexit
$ return
$
$get_directory:
$ flag = 0
$ ask "folder: " dname
$ if( dname .eqs. "exit" )
$ then 
$       goto bail
$ endif
$ dname = f$edit(dname, "upcase")
$ dname_test = f$string(dname + ".DIR")
$
$! read input of folder
$! needs to be implemented
$! write dir to temp file
$ tmp = "''f$unique()'.DAT;1"
$ define/user sys$output 'tmp
$ dir/col=1
$! write complete
$!
$! read and output temp file
$ open/read lexit 'tmp
$read_loop:
$ read/end_of_file=endit lexit line
$ if( f$locate(dname_test, line) .ne. f$length(line) .and. -
        f$locate(dname_test, line) .eq. 0 )
$ then
$       flag = 1
$ endif
$ goto read_loop
$endit:
$ close lexit
$! if flag not triggered get input again
$ if( flag .eq. 0 )
$ then
$       display "folder not found (exit to exit)"
$       dir *.dir
$       goto get_directory
$ else
$       return
$ endif
$
$bail:
$ display "goodbye"
$ delet 'tmp
$ exit
