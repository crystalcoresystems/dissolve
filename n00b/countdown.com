$!
$! filename: countdown.com
$!
$!     idea: countdown from 9 to 0 in a fun way
$!     form:
$!           loop multiplying number by 8 adding itself and modulo 10
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$ cls
$ z = 1
$loop:
$ if( z .eq 10)
$ then
$       goto exit_program
$ endif
$ x = z * 8 + z
$ write sys$output x - (x / 10) * 10
$ z = z + 1
$ wait 0:0:0.50
$ goto loop
$!
$exit_program:
$ write sys$output "BLAST OFF!"
$ exit
