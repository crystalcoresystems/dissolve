$! filename: fizzbuzz.com
$! just another (simpler? ugly) fizzbuzz -- writtin in DCL
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$ num = 1
$loop:
$       flag = 0
$       buffer = num
$       if( num/3 * 3 - num .eq. 0 )
$       then
$               buffer = "fizz"
$               flag = 1
$       endif
$       if( num/5 * 5 - num .eq. 0 )
$       then
$               if( flag .eq. 0 )
$               then
$                      buffer = "buzz"
$               else
$                      buffer = buffer + "buzz"
$               endif
$       endif
$       write sys$output buffer
$       num = num + 1
$       if( num .gt. 100)
$       then
$               goto endloop
$       endif
$       goto loop
$endloop:
$       exit
