$
$
$simplify_fraction:
$ if( compound_numer8r .gt. compound_divisor )
$ then
$       flag = 0
$       a = compound_numer8r
$       b = compound_divisor
$ else
$       flag = 1
$       b = compound_numer8r
$       a = compound_divisor
$ endif
$! check if high is divisable by low
$ if(  ( a - (a / b) * b ) .eq. 0  )
$ then
$       a = a / b
$       b = 1
$       goto exit_simplify
$ endif
$!
c=""
$high_simplify_loop:
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ sqrt 'b'
$ open/read temp 'tmp'
$ read temp c
$ close/disp=delete temp
$simplify_loop:
$!
$ if( c .eq. 1 )
$ then 
$       goto exit_simplify
$ endif
$! check if low is simplifiable
$ if(  ( b - (b / c) * c ) .eq. 0  )
$ then
$! check if high is simplifiable
$       if(  ( a - (a / c) * c) .eq. 0  )
$       then
$               a = a / c
$               b = b / c
$               goto high_simplify_loop
$       endif
$ endif
c = c - 1
$ goto simplify_loop
$exit_simplify:
$ if( flag .eq. 0 )
$ then
$       compound_numer8r = a
$       compound_divisor = b
$ else
$       compound_numer8r = b
$       compound_divisor = a
$ endif      
$ return
