$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ divide 'value_numer8r' 'value_divisor'
$ open/read temp 'tmp'
$ read temp value
$ close/disp=delete temp
$!
$! filename: draw_cards.com
$!   output: 5 playing cards
$!
$!     idea: a random number call grabs 5 cards from a deck
$!     form: 
$!          a card assignment loop using rand to create a file
$!           sort the file and grab the first 5 cards into buffer
$!            then print
$!      _________________________ 
$! --- | Written by |K0|13|13|S| | ---
$!      =========================
$!
$ count = 1
$! tmp_deck = "''f$unique()'.dat"
$! open/write dek 'tmp_deck'
$!
$main_loop:
$ buffer = "''f$unique()'.dat"
$ define/user sys$output 'buffer'
$ rand
$ open/read temp 'buffer''
$ read temp towrit
$ close/disp=delete temp
$!
$! towrite = 'towrite' + f$string('count')
$! write dek 'towrite'
$!
$ count = count + 1
$ if( count .gt. 52 )
$ then
$       goto exit_loop
$ else
$       goto main_loop
$ endif
$exit_loop:
$! close dek
$! type 'temp_deck'
$! delete f$string('temp_deck' + ";*")
$! sort cards
