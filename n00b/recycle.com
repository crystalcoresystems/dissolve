$!
$! filename: recycle.com
$!
$! input: filename
$! output: success else throw error
$! procedure: moves file versions to recycle.bin
$!      _______________________
$! --- |Written by |K|0|13|13|S| ---
$!      =======================
$!
$ if p1 .eqs. ""
$ then
$       write sys$output "enter file to recycle"
$       goto def_exit
$ endif
$!
$ if p2 .nes. ""
$ then
$       write sys$output "please recycle one file at a time"
$       goto def_exit
$ endif
$!
$ write sys$output "arguement was " + p1
$ wildcard = p1 + ";*"
$ copy/log 'p1' dsa3:[decuserve_user.kobler.recycle]'p1'
$ delete/confirm 'wildcard'
$!
$ goto def_exit
$
$def_exit:
$       exit
