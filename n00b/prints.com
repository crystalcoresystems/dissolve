$! filename: prints.com
$!    input: ??
$!   output: ???
$!     idea: print out different things as an experiment
$!     form: free
$!
$!     __________________________
$! ---| Written by |K|0|13|13|S| | ---
$!     ==========================
$
$ esc[0,8] = %x1B !Prepare a 7-bit CSI string:
$ csi = esc + "["
$ display csi,"2J"              !clear 
$ display csi,"1;1H"            !home
$ display csi,"0;1m", -         !bold
               "A Bold Heading", -
               csi,"0m"         !normal
$!
$ bold   = csi + "0;1m"
$ clear  = csi + "2J"
$ home   = csi + "1;1H"
$ normal = csi + "0m"
$!
$ display bold, "test", normal
