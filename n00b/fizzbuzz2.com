$! filename: fizzbuzz.com
$! just another (less? ugly) fizzbuzz -- writtin in DCL
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$ num = 1
$ datafile = "''f$unique()'.dat"
$loop:
$       open/write temp_file 'datafile
$       if( num/15 * 15 - num .eq. 0 )
$       then
$               write temp_file "fizzbuzz"
$       else
$               if( num/5 * 5 - num .eq. 0 )
$               then
$                       write temp_file "buzz"
$               else
$                       if( num/3 * 3 - num .eq. 0 )
$                       then
$                               write temp_file "fizz"
$                       else
$                               write temp_file num
$                       endif
$               endif
$       endif
$       close temp_file
$       type 'datafile
$       num = num + 1
$       if( num .gt. 100)
$       then
$               goto endloop
$       endif
$       goto loop
$endloop:
$       delete 'datafile;*
$       exit
