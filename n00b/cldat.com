$ ! filename: cldat.com
$ !
$ ! precondition: .dat file in dir
$ ! postcondition: confirmed .dat files deleted
$ !
$ ! action: delete .dat files with confimation
$ !
$ define sys$output nl:
$ delete/confirm *.dat;*
$ if .not. $severity then $ goto doorway
$ goto clean_exit
$ ! send error msg and exit
$doorway:
$       deassign sys$output
$       write sys$output "no files of type dat"
$       goto def_exit
$ ! send success msg and exit
$clean_exit:
$       deassign sys$output
$       write sys$output "deleted dat files"
$       write sys$output f$fao("completed at: ")
$       show time
$       goto def_exit
$def_exit:
$ exit
