$!
$! filename: interest.com
$!
$!     idea: just a n00bscript
$! .--------------------.
$! | rate= 8.125%       | <-- rate = 8.125
$! |--------------------|
$! | per. | bal.        |
$! |--------------------|
$! | 0    | 1.00        |
$! |--------------------|
$! | 2    | 1.08        | <-- increment = 2 (from 0 -> 1 & 1 -> 2)
$! |--------------------|
$! | 4    | 1.16        | <-- periods = 4
$! '--------------------'
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$ value = "1/1"
$ gosub get_input
$ gosub get_rate_fraction
$! extract integer values from current value fraction
$main_loop:
$ value_numer8r = f$integer(  f$extract( 0, f$locate("/", value), value )  )
$ value_divisor = f$integer(  f$extract( f$locate("/", value) + 1, -
                                f$locate(".", value),  value )  )
$ value_whole = f$string(value_numer8r / value_divisor)
$ value_numer8r = value_numer8r - (value_numer8r / value_divisor) -
                         * value_divisor
$ write sys$output f$string(value_whole) 
$ write sys$output f$string(value_numer8r) + "/" + f$string(value_divisor)
$! 
$ numer8r_product = value_numer8r * rate_numer8r
$ divisor_product = value_divisor * rate_divisor
$!
$ compound_numer8r = value_numer8r * divisor_product + -
                        numer8r_product * value_divisor
$ compound_divisor = value_divisor * divisor_product
$ compound_whole = value_whole * rate_numer8r
$ compound_whole = divide compound_whole rate_divisor
$!
$ write sys$output f$string(compound_numer8r) + "/" + -
                         f$string(compound_divisor)
$! gosub simplify_fraction
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ divide 'compound_numer8r' 'compound_divisor'
$ open/read temp 'tmp'
$ read temp value
$ close/disp=delete temp
$ write sys$output value
$ value = f$string(value_whole) + f$extract(1, 10, value)
$!
$ per = f$integer(per) - f$integer(inc)
$ write sys$output 'per'
$ if( per .le. 0 )
$ then
$       goto ex_program
$ endif
$ write sys$output "|--------------------|"
$ write sys$output "| " + value + " |"
$ write sys$output "|--------------------|"
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ tofract 'value' 1000
$ open/read temp 'tmp'
$ read temp value
$ close/disp=delete temp
$ write sys$output "|--------------------|"
$ write sys$output "| " + value + " |"
$ write sys$output "|--------------------|"
$ goto main_loop
$ex_program:
$ write sys$output "|--------------------|"
$ write sys$output "| " + value + " |"
$ write sys$output "|--------------------|"
$ exit
$!
$simplify_fraction:
$ if( compound_numer8r .gt. compound_divisor )
$ then
$       flag = 0
$       a = compound_numer8r
$       b = compound_divisor
$ else
$       flag = 1
$       b = compound_numer8r
$       a = compound_divisor
$ endif
$! check if high is divisable by low
$ if(  ( a - (a / b) * b ) .eq. 0  )
$ then
$       a = a / b
$       b = 1
$       goto exit_simplify
$ endif
$!
c=""
$high_simplify_loop:
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ sqrt 'b'
$ open/read temp 'tmp'
$ read temp c
$ close/disp=delete temp
$simplify_loop:
$!
$ if( c .eq. 1 )
$ then 
$       goto exit_simplify
$ endif
$! check if low is simplifiable
$ if(  ( b - (b / c) * c ) .eq. 0  )
$ then
$! check if high is simplifiable
$       if(  ( a - (a / c) * c) .eq. 0  )
$       then
$               a = a / c
$               b = b / c
$               goto high_simplify_loop
$       endif
$ endif
c = c - 1
$ goto simplify_loop
$exit_simplify:
$ if( flag .eq. 0 )
$ then
$       compound_numer8r = a
$       compound_divisor = b
$ else
$       compound_numer8r = b
$       compound_divisor = a
$ endif      
$ return
$!
$get_rate_fraction:
$ rate = "0." + rate
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ tofract 'rate' 1000000
$ open/read temp 'tmp'
$ read temp rate
$ close/disp=delete temp
$! extract integer values from rate fraction
$ rate_numer8r = f$integer(  f$extract( 0, f$locate("/", rate), rate )  )
$ rate_divisor = f$integer(  f$extract( f$locate("/", rate) + 1, -
                                f$locate(".", rate), rate )  )
$ return
$!
$get_input:
$input_loop_a:
$ inquire rate "what is the interest?(8.125% = 08125, 0.5% = 005)"
$ if( rate .eqs. "" )
$ then
$       write sys$output "please enter a number"
$       goto input_loop_a
$ else
$       if( rate .eq. 0 )
$       then
$               write sys$output "please enter a number"
$               goto input_loop_a
$       endif
$ endif
$!
$input_loop_b:
$ inquire per "for how many periods?"
$ if( per .eqs. "" )
$ then
$       write sys$output "please enter a number"
$       goto input_loop_b
$ else
$       if( per .eq. 0 )
$       then
$               write sys$output "please enter a number"
$               goto input_loop_b
$       endif
$ endif
$!
$input_loop_c:
$ inquire inc "what increment do you want to see?"
$ if( inc .eqs. "" )
$ then
$       write sys$output "please enter a number"
$       goto input_loop_c
$ else
$       if( inc .eq. 0 )
$       then
$               write sys$output "please enter a number"
$               goto input_loop_c
$       endif
$ endif
$!
$ return
