$!
$! filename: product_even.com
$!
$!    input: a set of numbers
$!   output: product of all even digits contained within input
$!     idea: use modulo and divide to split up numbers and see
$!            if each digit is even
$!     form:
$!           while loop with if conditions
$!      _________________________
$! --- | Written by K|0|13|13|S| | ---
$!      =========================
$!
$ buffer = 1
$ count = 1
$loop:
$ if( p'count' .eqs. "" )
$ then
$       goto bail
$ endif
$innerloop:
$ mod = p'count' - p'count' / 10 * 10
$ if( (mod - mod / 2 * 2) .eq. 0)
$ then
$       buffer = buffer * mod
$ endif
$ if( p'count' / 10 .eq. 0 )
$ then
$       count = count + 1
$       goto loop
$ else
$       p'count' = p'count' / 10
$       goto innerloop
$ endif
$bail:
$ if( buffer .eq. 1)
$ then
$       buffer = 0
$ endif    
$ write sys$output buffer
$ exit
