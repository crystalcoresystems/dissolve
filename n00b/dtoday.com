$ ! dtoday.com
$ !
$ ! assign f$time to a symbol
$ !
$ now = f$time()
$ !
$ ! open a new file for writing
$ ! and write current time and a heading
$ !
$ open/write infile dir.dat
$ write infile now
$ write infile ""
$ write infile "Files created today:"
$ close infile
$ ! 
$ ! open dir.dat and append the list
$ ! of new files created today
$ ! 
$ open/append infile dir.dat
$ directory/since/output = infile
$ close infile
$ exit
