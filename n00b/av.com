$ ! av.com
$ ! version 1.1
$ !
$ ! action: compute aritmetic average
$ !
$ count = 1
$ sum =0
$ set verify
$
$ while:
$       inquire term "term (enter positive integer, or 0 to stop)"
$       if term .eq. 0 then goto doorway
$       sum = sum + term
$       count = count + 1
$ goto while
$
$ doorway:
$       set noverify
$       result = sum / count
$       write sys$output f$fao("count!_sum!_average")
$       write sys$output f$fao("!SL!_!SL!_!SL", count, sum, result)
$       exit
