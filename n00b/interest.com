$!
$! filename: interest.com
$!
$!     idea: just a n00bscript
$! .--------------------.
$! | rate= 8.125%       | <-- rate = 8.125
$! |--------------------|
$! | per. | bal.        |
$! |--------------------|
$! | 0    | 1.00        |
$! |--------------------|
$! | 2    | 1.08        | <-- increment = 2 (from 0 -> 1 & 1 -> 2)
$! |--------------------|
$! | 4    | 1.16        | <-- periods = 4
$! '--------------------'
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$!
$ new_value = ""
$ gosub get_input
$ gosub get_rate
$ count = 1
$ value = "1/1"
$mainloop:
$ value_numer8r = f$integer(  f$extract( 0, f$locate("/", value), value )  )
$ value_divisor = f$integer(  f$extract( f$locate("/", value) + 1, -
                                f$locate(".", value), value )  )
$ inc_numer8r = rate_numer8r * value_numer8r 
$ inc_divisor = rate_divisor * value_divisor
$!
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ divide 'inc_numer8r' 'inc_divisor'
$ open/read temp 'tmp'
$ read temp inc
$ close/disp=delete temp
$!
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ divide 'value_numer8r' 'value_divisor'
$ open/read temp 'tmp'
$ read temp value
$ close/disp=delete temp
$!add value and increment - seperately whole number and fraction
$ inc_whole = f$extract( 0, f$locate(".", inc), inc )
$ inc_fract = f$extract( f$locate(".", inc) + 1, 9, inc )
$ value_whole = f$extract( 0, f$locate(".", value), value )
$ value_fract = f$extract( f$locate(".", value) + 1, 9, value )
$ sum_whole = f$integer(value_whole) + f$integer(inc_whole)
$ sum_fract = f$string (f$integer(value_fract) + f$integer(inc_fract) )
$! condition where sum_fract exceeds length 9
$ if( f$length(sum_fract) .gt. 9)
$ then
$       sum_whole = sum_whole + 1
$       sum_fract = f$extract(1, 9, sum_fract)
$ endif
$! get leading zeros for sum of fractions
$miniloop:
$ if( f$length(sum_fract) .eq. 9 )
$ then
$       goto jumpout
$ endif
$ sum_fract = "0" + sum_fract
$ goto miniloop
$jumpout:
$ new_value = f$string(sum_whole) + "." + sum_fract
$!  test
$ write sys$output new_value
$ define/user sys$output 'tmp'
$ tofract 'new_value' 500
$ open/read temp 'tmp'
$ read temp value
$ close/disp=delete temp
$ write sys$output value
$ if (count .eq. per)
$ then
$       exit
$ endif
$ count = count + 1
$ goto mainloop

$get_rate:
$ rate = "0." + rate
$ write sys$output rate
$ tmp = "''f$unique()'.dat"
$ define/user sys$output 'tmp'
$ tofract 'rate' 1000000
$ open/read temp 'tmp'
$ read temp rate
$ close/disp=delete temp
$! extract integer values from rate fraction
$ rate_numer8r = f$integer(  f$extract( 0, f$locate("/", rate), rate )  )
$ rate_divisor = f$integer(  f$extract( f$locate("/", rate) + 1, -
                                f$locate(".", rate), rate )  )
$ return
$!
$get_input:
$input_loop_a:
$ inquire rate "what is the interest?(8.125% = 08125, 0.5% = 005)"
$ if( rate .eqs. "" )
$ then
$       write sys$output "please enter a number"
$       goto input_loop_a
$ else
$       if( rate .eq. 0 )
$       then
$               write sys$output "please enter a number"
$               goto input_loop_a
$       endif
$ endif
$!
$input_loop_b:
$ inquire per "for how many periods?"
$ if( per .eqs. "" )
$ then
$       write sys$output "please enter a number"
$       goto input_loop_b
$ else
$       if( per .eq. 0 )
$       then
$               write sys$output "please enter a number"
$               goto input_loop_b
$       endif
$ endif
$!
$input_loop_c:
$ inquire inc "what increment do you want to see?"
$ if( inc .eqs. "" )
$ then
$       write sys$output "please enter a number"
$       goto input_loop_c
$ else
$       if( inc .eq. 0 )
$       then
$               write sys$output "please enter a number"
$               goto input_loop_c
$       endif
$ endif
$ inc = f$integer(inc)
$ per = f$integer(per)
$!
$ return
