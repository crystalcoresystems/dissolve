$!
$! filename: sort.com
$!    input: filename to sort
$!   output: sorted file filename_sorted
$!
$!     idea: bubble sort
$!     form:
$!           fookin mess aight mate??
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$!
$! setup toggle and names for writing data + copy of original data
$ name = f$unique() + ".dat"
$ data = f$unique() + ".dat"
$ copy dsa3:[decuserve_user.kobler.data]'p1' -
        dsa3:[decuserve_user.kobler.com_scrpts]'data'
$! m = a + b; k = a; while(1){...k = m-k;} (its a toggle thing)
$ switch = 3
$ toggle = 1
$! need count for toggle; open files and set variables null
$begin_read:
$ count = 0
$ open/read infile 'data'
$ open/write sorted 'name'
$ num_3 = "nan"
$ num_2 = "nan"
$ num_1 = "nan"
$! read old data and bubble sort
$read_data:
$ read/end_of_file=end_read infile num_curr
$ if( toggle .eq. 1 )
$ then
$       gosub write_nums
$ endif
$ num_3 = num_2
$ num_2 = num_1
$ num_1 = num_curr
$ count = count + 1
$ toggle = switch - toggle
$ goto read_data
$! sort last elements depending on toggle
$end_read: 
$ if( toggle .eq. 1 )
$ then
$       gosub sort_last3
$ else
$       num_3 = num_2
$       num_2 = num_1
$       gosub write_nums
$ endif
$!
$ close infile
$ close sorted
$ if( flag .eq. 1 )
$ then
$       flag = 0
$! <-- this is the trick here            -->
$! <-- toggle if even number of numbers -->>
$! <-- tricky                          -->>>
$       if( (count - count / 2 * 2) .eq. 0 )
$       then
$               toggle = switch - toggle
$!               write sys$output "triggered"
$       endif
$       temp = name
$       name = data
$       data = temp
$!       
$       goto begin_read
$ endif
$ type 'name
$ delete/noconfirm 'name;*
$ delete/noconfirm 'data;*
$ exit
$!
$write_nums:
$ if( num_3 .eqs. "nan")
$ then
$       if( num_2 .eqs. "nan")
$       then
$               return
$       else
$               write sorted num_2
$               return
$       endif
$ endif
$!
$ if( num_3 .gt. num_2 )
$ then 
$        flag = 1
$        write sorted num_2
$        write sorted num_3
$ else
$        write sorted num_3
$        write sorted num_2
$ endif
$ return
$!

$sort_last3:
$ high = 0
$ mid = 0
$ low = 0
$       
$ if( num_1 .gt. num_2 )
$ then
$       high = num_1
$       low = num_2
$ else
$       flag = 1
$       high = num_2
$       low = num_1
$ endif
$ if( num_3 .gt. high )
$ then
$       flag = 1
$       mid = high
$       high = num_3
$ else
$       if( num_3 .lt. low )
$       then
$               mid = low
$               low = num_3
$       else
$               mid = num_3
$       endif
$ endif
$ write sorted low
$ write sorted mid
$ write sorted high
$ return
