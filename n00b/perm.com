$!
$! filename: permutations.com
$!
$!    input: interactive option to input up to 10 items
$!   output: all permutations i.e. 1.2.3 1.3.2 2.1.3 2.3.1 3.2.1 3.1.2
$!
$!     idea: inner function of permutation of three items, every 
$!            successive item is rotated in as a layer
$!                 |          ...             |                 
$!                 | Rotate in 5th 5 times    |    8-0-O-*-X
$!                 | Rotate in 4th 4 times    |    X-8-0-O-*
$!                      __________________
$!                     | permutation of 3 |        O-X-8-0-*
$!                      ------------------
$!     form: loops with call to succesive loops down to the
$!           permutation of three
$!
$ global = 1
$ gosub dec_items
$ max = 9
$ gosub extend_max
$ iteration = 1
$ top_orientation = ""
$build_loop:
$ top_orientation = top_orientation + item'iteration'
$ iteration = iteration + 1
$ if( iteration .gt. max )
$ then
$       goto finish_build
$ else
$       goto build_loop
$ endif
$finish_build:
$ ori'max' = top_orientation 
$ count'max' = max
$ iteration = 0
$ gosub 'ext_max'_loop
$ exit
$!
$nine_loop:
$ count8 = count9 - 1
$ if( count8 .eq. 0 )
$ then
$       count8 = max
$ endif
$!
$ ori8 = ori9 - item'count9'
$ if( ori8 .eqs. ori9 )
$ then
$       goto skip9
$ endif
$ temp9 = iteration
$ iteration = 0
$ gosub eight_loop
$ iteration = temp9
$ ori9 = item'count9' + ori8
$ iteration = iteration + 1
$ if( iteration .eq. 9 )
$ then
$       return
$ endif
$!
$skip9:
$ count9 = count9 - 1
$ if( count9 .eq. 0 )
$ then
$       count9 = max
$ endif
$ goto nine_loop
$!
$eight_loop:
$ count7 = count8 - 1
$ if( count7 .eq. 0 )
$ then
$       count7 = max
$ endif
$!
$ ori7 = ori8 - item'count8'
$ if( ori7 .eqs. ori8 )
$ then
$       goto skip8
$ endif
$ temp8 = iteration
$ iteration = 0
$ gosub seven_loop
$ iteration = temp8
$ ori8 = item'count8' + ori7
$ iteration = iteration + 1
$ if( iteration .eq. 8 )
$ then
$       return
$ endif
$!
$skip8:
$ count8 = count8 - 1
$ if( count8 .eq. 0 )
$ then
$       count8 = max
$ endif
$ goto eight_loop
$!
$seven_loop:
$ count6 = count7 - 1
$ if( count6 .eq. 0 )
$ then
$       count6 = max
$ endif
$!
$ ori6 = ori7 - item'count7'
$ if( ori6 .eqs. ori7 )
$ then
$       goto skip7
$ endif
$ temp7 = iteration
$ iteration = 0
$ gosub six_loop
$ iteration = temp7
$ ori7 = item'count7' + ori6
$ iteration = iteration + 1
$ if( iteration .eq. 7 )
$ then
$       return
$ endif
$!
$skip7:
$ count7 = count7 - 1
$ if( count7 .eq. 0 )
$ then
$       count7 = max
$ endif
$ goto seven_loop
$!
$six_loop:
$ count5 = count6 - 1
$ if( count5 .eq. 0 )
$ then
$       count5 = max
$ endif
$!
$ ori5 = ori6 - item'count6'
$ if( ori5 .eqs. ori6 )
$ then
$       goto skip6
$ endif
$ temp6 = iteration
$ iteration = 0
$ gosub five_loop
$ iteration = temp6
$ ori6 = item'count6' + ori5
$ iteration = iteration + 1
$ if( iteration .eq. 6 )
$ then
$       return
$ endif
$!
$skip6:
$ count6 = count6 - 1
$ if( count6 .eq. 0 )
$ then
$       count6 = max
$ endif
$ goto six_loop
$!
$five_loop:
$ count4 = count5 - 1
$ if( count4 .eq. 0 )
$ then
$       count4 = max
$ endif
$!
$ ori4 = ori5 - item'count5'
$ if( ori5 .eqs. ori4 )
$ then
$       goto skip5
$ endif
$ temp5 = iteration
$ iteration = 0
$ gosub four_loop
$ iteration = temp5
$ ori5 = item'count5' + ori4
$ iteration = iteration + 1
$ if( iteration .eq. 5 )
$ then
$       return
$ endif
$skip5:
$ count5 = count5 - 1
$ if( count5 .eq. 0 )
$ then
$       count5 = 6
$ endif
$ goto five_loop
$!
$four_loop:
$!
$! write sys$output ori4
$! write sys$output item'count4'
$! write sys$output 'ext_max'_orientation
$ ori3 = ori4 - item'count4'
$ if( ori3 .eqs. ori4 )
$ then
$       goto skip4
$ endif
$ temp4 = iteration
$ iteration = 0
$ gosub three_permutation
$ iteration = temp4
$ ori4 = item'count4' + ori3
$ iteration = iteration + 1
$ if( iteration .eq. 4 )
$ then
$       return
$ endif
$skip4:
$ count4 = count4 - 1
$ if( count4 .eq. 0 )
$ then
$       count4 = max
$ endif
$ goto four_loop
$!
$three_permutation:
$ last1 = ""
$ last2 = ""
$ last3 = ""
$ remainder = ""
$ count3 = 1
$print_loop:
$ temp = f$integer(max - iteration)
$ hold = ori3 - item'temp'
$ if( hold .nes. ori3 )
$ then
$       last'count3' = item'temp' 
$       count3 = count3 + 1
$ endif
$ iteration = iteration + 1
$ if( iteration .eq. max )
$ then
$       goto finish_print
$ else
$       goto print_loop
$ endif
$!
$finish_print:
$ buff = ""
$ iteration = f$integer(4)
$buff_loop:
$ if( iteration .gt. max )
$ then
$       goto exit_buff
$ endif
$ count = count'iteration'
$ buff = buff + item'count'
$ iteration = iteration + 1
$ goto buff_loop
$exit_buff:
$!
$  write sys$output last3 + last2 + last1 + buff
$  write sys$output last3 + last1 + last2 + buff
$  write sys$output last2 + last3 + last1 + buff
$  write sys$output last2 + last1 + last3 + buff
$  write sys$output last1 + last3 + last2 + buff
$  write sys$output last1 + last2 + last3 + buff 
$ global = global + 1
$ return
$!
$extend_max:
$ if( max .eq. 1 )
$ then
$       ext_max = "one"
$ endif
$ if( max .eq. 2 )
$ then
$       ext_max = "two"
$ endif
$ if( max .eq. 3 )
$ then
$       ext_max = "three"
$ endif
$ if( max .eq. 4 )
$ then
$       ext_max = "four"
$ endif
$ if( max .eq. 5 )
$ then
$       ext_max = "five"
$ endif
$ if( max .eq. 6 )
$ then
$       ext_max = "six"
$ endif
$ if( max .eq. 7 )
$ then
$       ext_max = "seven"
$ endif
$ if( max .eq. 8 )
$ then
$       ext_max = "eight"
$ endif
$ if( max .eq. 9 )
$ then
$       ext_max = "nine"
$ endif
$ if( max .eq. 10 )
$ then
$       ext_max = "ten"
$ endif
$ return
$!
$dec_items:
$ item1 = "one"
$ item2 = "two"
$ item3 = "three"
$ item4 = "four"
$ item5 = "five"
$ item6 = "six"
$ item7 = "seven"
$ item8 = "eight"
$ item9 = "nine"
$ item10 = "ten"
$ return
$!
