$! filename: fizzbuzz.com
$! just another (ugly) fizzbuzz -- writtin in DCL
$!      __________________________
$! --- | Written by |K|0|13|13|S| | ---
$!      ==========================
$!
$ num = 1
$loop:
$       if( num/3 * 3 - num .eq. 0 )
$       then
$               goto fizzbuzz
$       endif
$       if( num/5 * 5 - num .eq. 0 )
$       then
$               write sys$output "buzz"
$               goto newloop
$       endif
$       write sys$output num
$       goto newloop
$newloop:
$       num = num + 1
$       if( num .gt. 100)
$       then
$               goto endloop
$       endif
$       goto loop
$fizzbuzz:
$       if( num/5 * 5 - num .eq. 0 )
$       then
$               write sys$output "fizzbuzz"
$               goto newloop
$       endif
$       write sys$output "fizz"
$       goto newloop
$
$endloop:
$       exit
