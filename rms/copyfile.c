/*
 * COPYFILE.C   This program copies the input file to the output
 * file. It is made to resemble the MACRO example in the RMS 
 * reference manual
 */
#include <rms>
#include <stdio>
#include <starlet>
#include <stdlib>

#define REC_SIZE 132
#define INPUT_NAME "personnel"
#define OUTPUT_NAME "out_file"
#define DEFAULT_NAME ".dat"

int copyfile();

int main(){int stat; stat = copyfile(); exit(stat); }

int copyfile()
{
        struct FAB      infab, outfab, *fab;
        struct RAB      inrab, outrab, *rab;
        int             lib$signal();
        int             stat;
        char            rec_buff[REC_SIZE]; /* max record size */

        infab = cc$rms_fab;
        infab.fab$l_fna = (char *) &INPUT_NAME;
        infab.fab$b_fns = sizeof INPUT_NAME -1;
        infab.fab$l_dna = (char *) &DEFAULT_NAME;
        infab.fab$b_dns = sizeof DEFAULT_NAME;

        inrab = cc$rms_rab;
        inrab.rab$l_fab = &infab;
        inrab.rab$v_rah = 1;
        inrab.rab$l_ubf = rec_buff;
        inrab.rab$w_usz = REC_SIZE;
        
        outfab = cc$rms_fab;
        outfab.fab$v_ctg = 1;
        outfab.fab$v_put = 1;
        outfab.fab$v_nil = 1;
        outfab.fab$b_rat = FAB$M_CR;
        outfab.fab$w_mrs = REC_SIZE;
        outfab.fab$l_fna = (char *) &OUTPUT_NAME;
        outfab.fab$b_fns = sizeof OUTPUT_NAME -1;
        outfab.fab$l_dna = (char *) &DEFAULT_NAME;
        outfab.fab$b_dns = sizeof DEFAULT_NAME;

        outrab = cc$rms_rab;
        outrab.rab$l_fab = &outfab;
        outrab.rab$v_wbh = 1;
        outrab.rab$l_rbf = rec_buff;

        fab = &infab;
        stat = sys$open (fab);
        if (stat & 1)
        {
                outfab.fab$l_alq = infab.fab$l_alq;
                fab = &outfab;
                stat = sys$create(fab);
        }
        if (stat & 1)
        {
                rab = &outrab;
                stat = sys$connect(rab);
                if (stat & 1)
                {
                        rab = &inrab;
                        stat = sys$connect(rab);
                }
                if(stat & 1)
                        stat = sys$get(rab);

                while (stat & 1)
                {
                        /*set correct size*/
                        outrab.rab$w_rsz = inrab.rab$w_rsz;
                        rab = &outrab;
                        stat = sys$put(rab);
                        if (stat & 1)
                        {
                                rab = &inrab;
                                stat = sys$get(rab);
                        }
                } /* while */

                if (stat != RMS$_EOF)
                        stat = lib$signal( stat, rab->rab$l_stv );
                stat = sys$close( &infab );
                stat = sys$close( &outfab );
        }
        else
        {
                stat = lib$signal(stat, fab->fab$l_stv);
        }
        return stat;
}
