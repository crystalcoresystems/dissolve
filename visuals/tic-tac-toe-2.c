#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>

#define SIZE 3
#define WINDOW_WIDTH 300
#define WINDOW_HEIGHT 300
#define CELL_SIZE (WINDOW_WIDTH / SIZE)

// Function declarations
void renderBoard(SDL_Renderer* renderer, char board[SIZE][SIZE]);
bool checkWinner(char board[SIZE][SIZE], char player);
bool isBoardFull(char board[SIZE][SIZE]);

int main() {
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("Tic-Tac-Toe",
                                          SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          WINDOW_WIDTH, WINDOW_HEIGHT,
                                          SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    char board[SIZE][SIZE] = { {' ', ' ', ' '},
                               {' ', ' ', ' '},
                               {' ', ' ', ' '} };
    char player = 'X';
    bool gameWon = false;
    bool running = true;

    while (running) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = false;
            } else if (event.type == SDL_MOUSEBUTTONDOWN) {
                int x, y;
                SDL_GetMouseState(&x, &y);
                int row = y / CELL_SIZE;
                int col = x / CELL_SIZE;

                if (row < SIZE && col < SIZE && board[row][col] == ' ') {
                    board[row][col] = player;

                    if (checkWinner(board, player)) {
                        printf("Player %c wins!\n", player);
                        gameWon = true;
                        running = false;
                    } else {
                        player = (player == 'X') ? 'O' : 'X';
                    }
                }
            }
        }

        // Clear the window
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // White background
        SDL_RenderClear(renderer);

        // Render the Tic-Tac-Toe board
        renderBoard(renderer, board);

        // Present the rendered frame
        SDL_RenderPresent(renderer);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

// Function to render the Tic-Tac-Toe board
void renderBoard(SDL_Renderer* renderer, char board[SIZE][SIZE]) {
    // Draw grid lines
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Black color

    for (int i = 1; i < SIZE; i++) {
        // Vertical lines
        SDL_RenderDrawLine(renderer, i * CELL_SIZE, 0, i * CELL_SIZE, WINDOW_HEIGHT);
        // Horizontal lines
        SDL_RenderDrawLine(renderer, 0, i * CELL_SIZE, WINDOW_WIDTH, i * CELL_SIZE);
    }

    // Draw X's and O's
    for (int row = 0; row < SIZE; row++) {
        for (int col = 0; col < SIZE; col++) {
            if (board[row][col] == 'X') {
                // Draw an X
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red for X
                SDL_RenderDrawLine(renderer, col * CELL_SIZE, row * CELL_SIZE,
                                   (col + 1) * CELL_SIZE, (row + 1) * CELL_SIZE);
                SDL_RenderDrawLine(renderer, (col + 1) * CELL_SIZE, row * CELL_SIZE,
                                   col * CELL_SIZE, (row + 1) * CELL_SIZE);
            } else if (board[row][col] == 'O') {
                // Draw an O
                SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); // Blue for O
                int centerX = col * CELL_SIZE + CELL_SIZE / 2;
                int centerY = row * CELL_SIZE + CELL_SIZE / 2;
                int radius = CELL_SIZE / 2 - 10;

                for (int w = 0; w < 360; w++) {
                    int x = centerX + radius * SDL_cos(w * M_PI / 180);
                    int y = centerY + radius * SDL_sin(w * M_PI / 180);
                    SDL_RenderDrawPoint(renderer, x, y);
                }
            }
        }
    }
}

// Function to check if a player has won
bool checkWinner(char board[SIZE][SIZE], char player) {
    for (int i = 0; i < SIZE; i++) {
        if ((board[i][0] == player && board[i][1] == player && board[i][2] == player) ||
            (board[0][i] == player && board[1][i] == player && board[2][i] == player)) {
            return true;
        }
    }
    if ((board[0][0] == player && board[1][1] == player && board[2][2] == player) ||
        (board[0][2] == player && board[1][1] == player && board[2][0] == player)) {
        return true;
    }
    return false;
}
