#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define BOARD_SIZE 3
#define INITIAL_WINDOW_WIDTH 400
#define INITIAL_WINDOW_HEIGHT 400

// Function declarations
void render_board(SDL_Renderer* renderer, char board[BOARD_SIZE][BOARD_SIZE], int window_width, int window_height, int hover_row, int hover_col, float scale[BOARD_SIZE][BOARD_SIZE]);
bool check_winner(char board[BOARD_SIZE][BOARD_SIZE], char player);
bool is_board_full(char board[BOARD_SIZE][BOARD_SIZE]);
void draw_gradient_background(SDL_Renderer* renderer, int window_width, int window_height);
void render_x(SDL_Renderer* renderer, int x, int y, int width, int height, float scale);
void render_o(SDL_Renderer* renderer, int x, int y, int width, int height, float scale);

int main() {
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("Tic-Tac-Toe on Hyperland",
                                          SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT,
                                          SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    char board[BOARD_SIZE][BOARD_SIZE] = { {' ', ' ', ' '},
                                           {' ', ' ', ' '},
                                           {' ', ' ', ' '} };
    char player = 'X';
    bool game_won = false;
    bool running = true;
    bool is_fullscreen = false;
    int window_width = INITIAL_WINDOW_WIDTH;
    int window_height = INITIAL_WINDOW_HEIGHT;
    int hover_row = -1, hover_col = -1; // Track the cell being hovered
    float scale[BOARD_SIZE][BOARD_SIZE] = {0}; // Scale effect for X and O animations

    while (running) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = false;
            } else if (event.type == SDL_KEYDOWN) {
                // Toggle fullscreen mode with 'F' key
                if (event.key.keysym.sym == SDLK_f) {
                    is_fullscreen = !is_fullscreen;
                    SDL_SetWindowFullscreen(window, is_fullscreen ? SDL_WINDOW_FULLSCREEN : 0);
                }
            } else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED) {
                // Handle window resizing
                SDL_GetWindowSize(window, &window_width, &window_height);
            } else if (event.type == SDL_MOUSEMOTION) {
                // Detect mouse hovering
                int x, y;
                SDL_GetMouseState(&x, &y);
                hover_row = y / (window_height / BOARD_SIZE);
                hover_col = x / (window_width / BOARD_SIZE);
            } else if (event.type == SDL_MOUSEBUTTONDOWN) {
                // Get mouse click coordinates
                int x, y;
                SDL_GetMouseState(&x, &y);
                int row = y / (window_height / BOARD_SIZE);
                int col = x / (window_width / BOARD_SIZE);

                // Place player's mark if the cell is empty
                if (row < BOARD_SIZE && col < BOARD_SIZE && board[row][col] == ' ') {
                    board[row][col] = player;
                    scale[row][col] = 0.0f; // Reset scale to animate the new mark

                    // Check for a winner
                    if (check_winner(board, player)) {
                        printf("Player %c wins!\n", player);
                        game_won = true;
                        running = false;
                    } else {
                        // Switch player
                        player = (player == 'X') ? 'O' : 'X';
                    }
                }
            }
        }

        // Update scale for animation
        for (int row = 0; row < BOARD_SIZE; row++) {
            for (int col = 0; col < BOARD_SIZE; col++) {
                if (scale[row][col] < 1.0f) {
                    scale[row][col] += 0.05f; // Increase scale for animation effect
                }
            }
        }

        // Clear screen and draw gradient background
        draw_gradient_background(renderer, window_width, window_height);

        // Render the Tic-Tac-Toe board
        render_board(renderer, board, window_width, window_height, hover_row, hover_col, scale);
        SDL_RenderPresent(renderer);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

// Function to draw a simple vertical gradient background
void draw_gradient_background(SDL_Renderer* renderer, int window_width, int window_height) {
    for (int y = 0; y < window_height; y++) {
        // Interpolate between two colors
        int r = (255 * y) / window_height;
        int g = (128 * y) / window_height;
        int b = (64 * y) / window_height;

        SDL_SetRenderDrawColor(renderer, r, g, b, 255);
        SDL_RenderDrawLine(renderer, 0, y, window_width, y);
    }
}

// Function to render the Tic-Tac-Toe board
void render_board(SDL_Renderer* renderer, char board[BOARD_SIZE][BOARD_SIZE], int window_width, int window_height, int hover_row, int hover_col, float scale[BOARD_SIZE][BOARD_SIZE]) {
    int cell_width = window_width / BOARD_SIZE;
    int cell_height = window_height / BOARD_SIZE;

    // Draw grid lines with rounded corners
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);  // Black for grid

    for (int i = 1; i < BOARD_SIZE; i++) {
        SDL_RenderDrawLine(renderer, i * cell_width, 0, i * cell_width, window_height);  // Vertical lines
        SDL_RenderDrawLine(renderer, 0, i * cell_height, window_width, i * cell_height); // Horizontal lines
    }

    // Highlight the hovered cell
    if (hover_row >= 0 && hover_col >= 0 && board[hover_row][hover_col] == ' ') {
        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 128); // Semi-transparent green
        SDL_Rect hover_rect = { hover_col * cell_width, hover_row * cell_height, cell_width, cell_height };
        SDL_RenderFillRect(renderer, &hover_rect);
    }

    // Draw X's and O's with a scaling animation
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            if (board[row][col] == 'X') {
                render_x(renderer, col * cell_width, row * cell_height, cell_width, cell_height, scale[row][col]);
            } else if (board[row][col] == 'O') {
                render_o(renderer, col * cell_width, row * cell_height, cell_width, cell_height, scale[row][col]);
            }
        }
    }
}

// Function to render a scaled 'X'
void render_x(SDL_Renderer* renderer, int x, int y, int width, int height, float scale) {
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red for X
    int thickness = 5;
    int scaled_width = width * scale;
    int scaled_height = height * scale;

    for (int t = 0; t < thickness; t++) {
        SDL_RenderDrawLine(renderer, x + t, y,
                           x + scaled_width + t, y + scaled_height);
        SDL_RenderDrawLine(renderer, x + scaled_width - t, y,
                           x - t, y + scaled_height);
    }
}

// Function to render a scaled 'O' as an ellipse
void render_o(SDL_Renderer* renderer, int x, int y, int width, int height, float scale) {
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); // Blue for O
    int center_x = x + width / 2;
    int center_y = y + height / 2;
    int radius_x = (width / 2 - 15) * scale;
    int radius_y = (height / 2 - 15) * scale;

    // Draw an ellipse with a thicker outline
    int thickness = 5;
    for (int t = 0; t < thickness; t++) {
        for (int angle = 0; angle < 360; angle++) {
            double theta = angle * M_PI / 180.0;  // Convert to radians
            int draw_x = center_x + (radius_x + t) * cos(theta);
            int draw_y = center_y + (radius_y + t) * sin(theta);
            SDL_RenderDrawPoint(renderer, draw_x, draw_y);
        }
    }
}

// Function to check if a player has won
bool check_winner(char board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Check rows and columns
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0] == player && board[i][1] == player && board[i][2] == player) ||
            (board[0][i] == player && board[1][i] == player && board[2][i] == player)) {
            return true;
        }
    }

    // Check diagonals
    if ((board[0][0] == player && board[1][1] == player && board[2][2] == player) ||
        (board[0][2] == player && board[1][1] == player && board[2][0] == player)) {
        return true;
    }

    return false;
}

// Function to check if the board is full
bool is_board_full(char board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (board[i][j] != 'X' && board[i][j] != 'O') {
                return false;
            }
        }
    }
    return true;
}
