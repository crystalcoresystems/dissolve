#include <stdio.h>
#include <stdbool.h>

// Constants
#define SIZE 3

// Function declarations
void printBoard(char board[SIZE][SIZE]);
bool checkWinner(char board[SIZE][SIZE], char player);
bool isBoardFull(char board[SIZE][SIZE]);

int main() {
    char board[SIZE][SIZE] = { {'1', '2', '3'},
                               {'4', '5', '6'},
                               {'7', '8', '9'} };
    char player = 'X';
    int choice;
    bool gameWon = false;

    // Main game loop
    while (!gameWon && !isBoardFull(board)) {
        printBoard(board);
        printf("Player %c, enter a number: ", player);
        scanf("%d", &choice);

        // Map the user input to the board
        int row = (choice - 1) / SIZE;
        int col = (choice - 1) % SIZE;

        // Check if the chosen position is valid
        if (choice >= 1 && choice <= 9 && board[row][col] == '0' + choice) {
            board[row][col] = player;

            // Check if the current player won
            if (checkWinner(board, player)) {
                printBoard(board);
                printf("Player %c wins!\n", player);
                gameWon = true;
            } else {
                // Switch player
                player = (player == 'X') ? 'O' : 'X';
            }
        } else {
            printf("Invalid move, try again.\n");
        }
    }

    if (!gameWon && isBoardFull(board)) {
        printBoard(board);
        printf("It's a draw!\n");
    }

    return 0;
}

// Function to print the Tic-Tac-Toe board
void printBoard(char board[SIZE][SIZE]) {
    printf("\n");
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            printf(" %c ", board[i][j]);
            if (j < SIZE - 1) printf("|");
        }
        printf("\n");
        if (i < SIZE - 1) printf("---|---|---\n");
    }
    printf("\n");
}

// Function to check if a player has won
bool checkWinner(char board[SIZE][SIZE], char player) {
    // Check rows and columns
    for (int i = 0; i < SIZE; i++) {
        if ((board[i][0] == player && board[i][1] == player && board[i][2] == player) ||
            (board[0][i] == player && board[1][i] == player && board[2][i] == player)) {
            return true;
        }
    }

    // Check diagonals
    if ((board[0][0] == player && board[1][1] == player && board[2][2] == player) ||
        (board[0][2] == player && board[1][1] == player && board[2][0] == player)) {
        return true;
    }

    return false;
}

// Function to check if the board is full
bool isBoardFull(char board[SIZE][SIZE]) {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (board[i][j] != 'X' && board[i][j] != 'O') {
                return false;
            }
        }
    }
    return true;
}
