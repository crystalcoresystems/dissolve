#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define BOARD_SIZE 3
#define INITIAL_WINDOW_WIDTH 300
#define INITIAL_WINDOW_HEIGHT 300

// Function declarations
void render_board(SDL_Renderer* renderer, char board[BOARD_SIZE][BOARD_SIZE], int window_width, int window_height);
bool check_winner(char board[BOARD_SIZE][BOARD_SIZE], char player);
bool is_board_full(char board[BOARD_SIZE][BOARD_SIZE]);

int main() {
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("Tic-Tac-Toe on Hyperland",
                                          SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT,
                                          SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    char board[BOARD_SIZE][BOARD_SIZE] = { {' ', ' ', ' '},
                                           {' ', ' ', ' '},
                                           {' ', ' ', ' '} };
    char player = 'X';
    bool game_won = false;
    bool running = true;
    bool is_fullscreen = false;
    int window_width = INITIAL_WINDOW_WIDTH;
    int window_height = INITIAL_WINDOW_HEIGHT;

    while (running) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = false;
            } else if (event.type == SDL_KEYDOWN) {
                // Toggle fullscreen mode with 'F' key
                if (event.key.keysym.sym == SDLK_f) {
                    is_fullscreen = !is_fullscreen;
                    SDL_SetWindowFullscreen(window, is_fullscreen ? SDL_WINDOW_FULLSCREEN : 0);
                }
            } else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED) {
                // Handle window resizing
                SDL_GetWindowSize(window, &window_width, &window_height);
            } else if (event.type == SDL_MOUSEBUTTONDOWN) {
                // Get mouse click coordinates
                int x, y;
                SDL_GetMouseState(&x, &y);
                int row = y / (window_height / BOARD_SIZE);
                int col = x / (window_width / BOARD_SIZE);

                // Place player's mark if the cell is empty
                if (row < BOARD_SIZE && col < BOARD_SIZE && board[row][col] == ' ') {
                    board[row][col] = player;

                    // Check for a winner
                    if (check_winner(board, player)) {
                        printf("Player %c wins!\n", player);
                        game_won = true;
                        running = false;
                    } else {
                        // Switch player
                        player = (player == 'X') ? 'O' : 'X';
                    }
                }
            }
        }

        // Clear screen and render board
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);  // White background
        SDL_RenderClear(renderer);
        render_board(renderer, board, window_width, window_height);
        SDL_RenderPresent(renderer);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

// Function to render the Tic-Tac-Toe board
void render_board(SDL_Renderer* renderer, char board[BOARD_SIZE][BOARD_SIZE], int window_width, int window_height) {
    int cell_width = window_width / BOARD_SIZE;
    int cell_height = window_height / BOARD_SIZE;

    // Draw grid lines
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);  // Black for grid

    for (int i = 1; i < BOARD_SIZE; i++) {
        SDL_RenderDrawLine(renderer, i * cell_width, 0, i * cell_width, window_height);  // Vertical lines
        SDL_RenderDrawLine(renderer, 0, i * cell_height, window_width, i * cell_height); // Horizontal lines
    }

    // Draw X's and O's
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            if (board[row][col] == 'X') {
                // Draw an X
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red for X
                SDL_RenderDrawLine(renderer, col * cell_width, row * cell_height,
                                   (col + 1) * cell_width, (row + 1) * cell_height);
                SDL_RenderDrawLine(renderer, (col + 1) * cell_width, row * cell_height,
                                   col * cell_width, (row + 1) * cell_height);
            } else if (board[row][col] == 'O') {
                // Draw an O as an ellipse
                SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); // Blue for O
                int center_x = col * cell_width + cell_width / 2;
                int center_y = row * cell_height + cell_height / 2;
                int radius_x = (cell_width / 2) - 10;
                int radius_y = (cell_height / 2) - 10;

                // Use trigonometry to draw an ellipse by calculating points
                for (int angle = 0; angle < 360; angle++) {
                    double theta = angle * M_PI / 180.0;  // Convert to radians
                    int x = center_x + radius_x * cos(theta);
                    int y = center_y + radius_y * sin(theta);
                    SDL_RenderDrawPoint(renderer, x, y);
                }
            }
        }
    }
}

// Function to check if a player has won
bool check_winner(char board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Check rows and columns
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0] == player && board[i][1] == player && board[i][2] == player) ||
            (board[0][i] == player && board[1][i] == player && board[2][i] == player)) {
            return true;
        }
    }

    // Check diagonals
    if ((board[0][0] == player && board[1][1] == player && board[2][2] == player) ||
        (board[0][2] == player && board[1][1] == player && board[2][0] == player)) {
        return true;
    }

    return false;
}

// Function to check if the board is full
bool is_board_full(char board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (board[i][j] != 'X' && board[i][j] != 'O') {
                return false;
            }
        }
    }
    return true;
}
