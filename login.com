$! Template login.com procedure for DECUServe users.
$ define engine dsa3:[decuserve_user.kobler.com_scrpts.engine]
$ define rcy dsa3:[decuserve_user.kobler.com_scrpts.engine.recycle]
$ define/job home$r 'f$str(f$trnlnm("sys$login")- ".]["-"]" + ".]") /trans=conceal
$ isprime :== $dsa3:[decuserve_user.kobler.exable]isprime.exe
$ tofract :== $dsa3:[decuserve_user.kobler.exable]2fract.exe
$ sqrt :== $dsa3:[decuserve_user.kobler.exable]sqrt.exe
$ divide :== $dsa3:[decuserve_user.kobler.exable]divide.exe
$ can32 :== $dsa3:[decuserve_user.kobler.c_progs.n32]can32.exe
$ know :== type/page [decuserve_user.kobler]knowledge.base
$ up :== set def [-]
$ morse :== type morse.code
$ sir :== write sys$output """YES SIR"""
$! recycle application../
$ recy*cle :== @rcy:recycle.com
$ script :== @rcy:gen_scrpt.com
$ vir :== @rcy:openvi.com
$ @rcy:trash_collector
$!--------------------------------/..
$ rand*om :== @engine:random_numz
$ card*s :== @engine:cards
$ task*s :== @engine:[task_manager]tskmgr
$ mv :== @dsa3:[decuserve_user.kobler.com_scrpts.engine]mv
$ display :== write sys$output
$ esc[0,8] = %x1B
$ csi = esc + "["
$ bold == csi + "0;1m"
$ clear == csi + "2J"
$ home == csi + "1;1H"
$ normal == csi + "0m"
$ set noon
$ ! Remove the following line if you want to use DECWindows Notes
$	NO*TES :== NOTES/INTERFACE=CHARACTER_CELL
$ if f$mode() .nes. "INTERACTIVE" then goto end_interactive
$ ! Place commands to be executed only in interactive mode here:
$ set terminal/inquire/noeightbit
$ update_notebook  ! Spawned update of the MAIN class in your Notebook.
$end_interactive:
$ ! Place commands to be executed in all modes here:
$ show quota  
$ exit
