#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

/* tofract -- converts a decimal to the nearest fraction possible
 *            with max divisor
 *
 *  input: a decimal and a maximum divisor value
 * output: a fraction that is as close to the decimal as possible
 *         without exceeding the max divisor
 *
 *   idea: progress down the number line towards the decimal until
 *         it is reached or passed over, in which case start
 *         progressing towards it again in the opposite direction.
 *   form:
 *         a recursive function that iterates the divisor to move
 *         left and iterates the numerator to move right. A global
 *         struct holds the value of the fraction closest to the 
 *         decimal.
 *      __________________________
 * --- | Written by |K|0|13|13|S| | ---
 *      ==========================
 */

struct fraction {
        long double diff;
        int numer8r;
        int divisor;
}closest_fract;

int max_div;

void find_fract(long double x, int numer8r, int divisor);
int process_args(int argc, char ** argv);
void bail(char * str);

int main(int argc, char **argv)
{
        long double deci;

        max_div = process_args(argc, argv);
        deci = strtod(argv[1], NULL);

        closest_fract.diff = 1.0;
        find_fract(deci, 1, 2);
        //print_fract();
        printf("%i/%i", closest_fract.numer8r, closest_fract.divisor);

        return 0;
}

void find_fract(long double x, int numer8r, int divisor)
{
        long double z, diff;

        if( divisor > max_div )
                return;

        z = (long double)numer8r/divisor;
        diff = fabs(x-z);
      //  printf("%Le\n", diff); uncomment to check path
        if( diff < closest_fract.diff ){
                closest_fract.diff = diff;
                closest_fract.numer8r = numer8r;
                closest_fract.divisor = divisor;
        }
        if( diff == 0.0 )
                return;
        if( x < z )
                find_fract(x, numer8r, divisor + 1);
        else
                find_fract(x, numer8r + 1, divisor);
}

int process_args(int argc, char **argv)
{
        int z;
        char str[100];

        if( argc != 3 )
                bail("usage: 0.5[decimal fraction] 10[max divisor]");
        if( argv[1][0] != '.' && argv[1][1] != '.' )
                bail("usage: 0.5[decimal fraction] 10[max divisor]");
        z = atoi(argv[2]);
        sprintf(str, "%d", z);
        if( strcmp(argv[2], str) != 0 )
                bail("usage: 0.5[decimal fraction] 10[max divisor]");
        return z;
}

void bail(char *str)
{
        fprintf(stderr, "error: %s\n", str);
        exit(1);
}
