/*
 *  U N B R E A K A B L E
 *  N                   L
 *  B                   B
 *  R                   A
 *  E                   K
 *  A        :^)        A
 *  K                   E
 *  A                   R
 *  B                   B
 *  L                   N
 *  E L B A K A E R B N U 
 */ 

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "encryption.h"

#define MAX_I 2000

int main()
/*
 * idea - get the message and password from user
 *        generate 9001 keys based on the password
 *        solve the message into a key and scramble
 *        or
 *        unscramble and solve the message into keys
 *        in reverse order to decrypt
 */
{
        char * password = malloc(MAX_I * sizeof(char));
        char * message = malloc(MAX_I * sizeof(char));
        char keys[9001][9];
        char ** square_key;
        int rows;
        int option;
        int i;

        option = get_user_input(password, message);

        message = pad_message(message); // pads message to length divisable by 9
        rows = strlen(message) / 9;

        generate_keys(keys, password);
        if (!option)
        {
                for (i = 0; i < 9001; i++)
                {
                        square_key = create_square_key(rows, keys[i]);
                        message = solve_with_key(rows, message, square_key);
                        s_scramble(message);
                }
                printf("%s\n", message);
        } else {
                for (i = 9000; i >= 0; i--)
                {
                        message = s_unscramble(message);
                        square_key = create_square_key(rows, keys[i]);
                        message = solve_with_key(rows, message, square_key);
                }
                printf("%s\n", message);
        }
}

// Get User Input../
int get_user_input(char * password, char *message)
/* 
 * idea - get password and message from user
 *        returns option of decrypt or encrypt
 */
{
        char option = 'a';

        printf("Hello. What's your message?\n");
        fgets(message,MAX_I,stdin);
        strtok(message, "\n");
        for ( ; *message; ++message) *message = tolower(*message);

        printf("What's your password?\n");
        fgets(password,MAX_I,stdin);
        strtok(password, "\n");
        for ( ; *password; ++password) *password = tolower(*password);


        while (option != 'D' && option != 'E')
        {
                printf("[E]ncrypt or [D]ecrypt?\n");
                option = fgetc(stdin);
                option = toupper(option);
        }

        return  'E' - option;
}//..

// Generate Keys../
void generate_keys(char keys[9001][9], char * pswd)
/*
 * idea - create 9001 keys by randomly selecting characters from the password
 *        uses a set seed so decryption can work
 */
{
        int i, j;
        srand(7);

        for (i = 0; i < 9001; i++)
        {
                for (j = 0; j < 9; j++)
                {
                        keys[i][j] = pswd[rand() % strlen(pswd)];
                }
        }
}//.. 

// Solve With Key../
char * solve_with_key(int rows, char *message, char **square_key)
/*
 * solves message and key characters together in their corresponding grid locations
 */
{
        int row, index;
        char * new_message = malloc((strlen(message) + 1)*sizeof(char));

        for (row = 0; row < rows; row++)
        {
                for (index = 0; index < 9; index++)
                {
                        new_message[index + row*9] = solve(message[index + row*9], square_key[row][index]);
                }
        }
        free(message);
        return new_message; 
}//..

// Straight Scramble../
void s_scramble(char * message)
/*
 * idea - scrambles a message by solving a character together
 *        with the next character in the message
 */
{
        int i, j;

        int toggle = 0;
        int direction = 1;
        char * new_message = malloc( (strlen(message) + 1) * sizeof(char) );

        i = 0;
        j = 1;
        while (j < strlen(message))
        {
                message[j] = solve(message[i], message[j]);
                i++;
                j++;
        }
}//..

// Straight Unscramble../
char * s_unscramble(char * message)
/*
 * idea - unscrambles a message by solving a character together
 *        with the next character in the message starting from the
 *        second to last character and working backwords
 */
{
        int i, j;
        char * new_message = malloc( (strlen(message) + 1) * sizeof(char) );

        i = 0;
        j = 1;
        new_message[0] = message[0];
        while (j < strlen(message))
        {
                new_message[j] = solve(message[i], message[j]);
                i++;
                j++;
        }
        return new_message;
}//..

// Pad Message../
char * pad_message(char * message)
/*
 * idea - add ls to a message in order to make its length a multiple of 9
 */
{
        int i;
        int pad_size;
        int message_size;
        char *pad, *new_message;

        message_size = strlen(message);
        pad_size = 9 - message_size % 9;
        if (pad_size == 9)
                return message;

        pad = malloc(9 * sizeof(char));
        sprintf(pad, "%.*s", pad_size, "llllllllll"); 

        new_message = malloc((message_size + pad_size + 1)*sizeof(char));
        strcpy(new_message, message);
        strcat(new_message, pad);

        free(pad);
        free(message);
        return new_message;
}//..

// Create Square Key../
char ** create_square_key(int rows, char * key)
/* 
 * idea - create a rectangular key
 *        intilitize with the first row as the key
 *        and the rest of the rows filled with l
 *        then solve with cellular automata diagonally
 *        to the left then right
 */
{
        char ** square_key;
        int i;

        square_key = initialize_key(rows, key);
        for (i = 0; i < 9; i++)
        {
                solve_diagonally_right(i, rows, square_key);
                solve_diagonally_left(i, rows, square_key);
        }
        return square_key;
}//..

// Initialize Key../
char ** initialize_key(int rows, char * key)
/*
 * idea - adds the key to the top of a rectangular key
 *        and fills the rest with ls
 */ 
{
        int i, j;
        char ** square_key;

        square_key = malloc( rows * sizeof(*square_key) );
        for (i = 0; i < rows; i++)
                square_key[i] = malloc( 9 * sizeof(char) );

        for (i = 0; i < 9; i++)
                square_key[0][i] = key[i];

        for (i = 1; i < rows; i++)
                for (j = 0; j < 9; j++)
                        square_key[i][j] = 'l';
        return square_key;

}//.. 

// Solve Diagonally right../
void solve_diagonally_right(int index, int rows, char ** sk)
/*
 * idea -  0 0 0 0 0 0 0
 *                \
 *         0 0 0 0 0 0 0
 *                  \
 *         0 0 0 0 0 0 0
 *                    \
 *         0 0 0 0 0 0 0 
 *        \             \
 *         0 0 0 0 0 0 0
 *          \
 *         0 0 0 0 0 0 0
 *            \
 *         0 0 0 0 0 0 0
 */
{
        int row;

        for (row = 0; row < rows - 1; row++)
        {
                if (index == 8)
                {
                        sk[row+1][0] = solve(sk[row][index], sk[row+1][0]);
                        index = 0;
                } else {
                        sk[row+1][index+1] = solve(sk[row][index], sk[row+1][index+1]);
                        index++;
                }
        }
}//..

// Solve Diagonally left../
void solve_diagonally_left(int index, int rows, char ** sk)
/*
 * idea -  0 0 0 0 0 0 0
 *                /
 *         0 0 0 0 0 0 0
 *              /
 *         0 0 0 0 0 0 0
 *            /
 *         0 0 0 0 0 0 0 
 *          /
 *         0 0 0 0 0 0 0
 *        /
 *         0 0 0 0 0 0 0
 *                      /
 *         0 0 0 0 0 0 0
 */
{
        int row;

        for (row = 0; row < rows - 1; row++)
        {
                if (index == 0)
                {
                        sk[row+1][8] = solve(sk[row][index], sk[row+1][8]);
                        index = 8;
                } else {
                        sk[row+1][index-1] = solve(sk[row][index], sk[row+1][index-1]);
                        index--;
                }
        }
}//..

// Solve../
char solve(char a, char b)
/*
 * idea - converts character to 6 bit value then
 *        shifts 2 bits to boolean solve
 */
{
        char a1, b1;
        char solve, solution;
        int offset;
        char mask;

        a1 = char_to_triune(a);
        b1 = char_to_triune(b);

        solution = 0;
        for (offset = 0; offset < 3; offset++)
        {
                mask = 3 << offset*2;
                solve = boolean_solve(( (a1 & mask) >> offset*2 ),
                                ( (b1 & mask) >> offset*2 ));
                solution = ( (solution & ~mask) | (solve << offset*2) );
        }

        return triune_to_char(solution);
}//..

// Boolean Solve Operations../---------------------------------------------*/
char boolean_solve(char input, char mech)
        /*
         * 10 and 01 = 11
         * 11 and 01 = 10
         * 11 and 10 = 01
         * x and x = x
         *
         * xor except identical values stay the same
         *
         * must be right 2 bits only
         */
{
        char tmp, tmp2, tmp3;

        tmp = input ^ mech;
        tmp3 = (tmp >> 1) | (tmp << 1) | tmp;
        tmp2 = tmp ^ tmp3;
        tmp2 = tmp2 ^ tmp3;
        tmp = ~tmp3 & input;
        return (tmp ^ tmp2) & 3;
}
/* --------------------------------------------------------------------/..*/

// Convert to triune../
char char_to_triune(char c)
{
        switch (c)
        {
                case 'a':
                        return 0x15;
                case 'b':
                        return 0x16;
                case 'c':
                        return 0x17;
                case 'd':
                        return 0x19;
                case 'e':
                        return 0x1a;
                case 'f':
                        return 0x1b;
                case 'g':
                        return 0x1d;
                case 'h':
                        return 0x1e;
                case 'i':
                        return 0x1f;
                case 'j':
                        return 0x25;
                case 'k':
                        return 0x26;
                case 'l':
                        return 0x27;
                case 'm':
                        return 0x29;
                case 'n':
                        return 0x2a;
                case 'o':
                        return 0x2b;
                case 'p':
                        return 0x2d;
                case 'q':
                        return 0x2e;
                case 'r':
                        return 0x2f;
                case 's':
                        return 0x35;
                case 't':
                        return 0x36;
                case 'u':
                        return 0x37;
                case 'v':
                        return 0x39;
                case 'w':
                        return 0x3a;
                case 'x':
                        return 0x3b;
                case 'y':
                        return 0x3d;
                case 'z':
                        return 0x3e;
                case ' ':
                        return 0x3f;
                default:
                        return 0x3b;
        }
}//..

// Convert to char../
char triune_to_char(char c)
{
        switch (c)
        {
                case 0x15:
                        return 'a';
                case 0x16:
                        return 'b';
                case 0x17:
                        return 'c';
                case 0x19:
                        return 'd';
                case 0x1a:
                        return 'e';
                case 0x1b:
                        return 'f';
                case 0x1d:
                        return 'g';
                case 0x1e:
                        return 'h';
                case 0x1f:
                        return 'i';
                case 0x25:
                        return 'j';
                case 0x26:
                        return 'k';
                case 0x27:
                        return 'l';
                case 0x29:
                        return 'm';
                case 0x2a:
                        return 'n';
                case 0x2b:
                        return 'o';
                case 0x2d:
                        return 'p';
                case 0x2e:
                        return 'q';
                case 0x2f:
                        return 'r';
                case 0x35:
                        return 's';
                case 0x36:
                        return 't';
                case 0x37:
                        return 'u';
                case 0x39:
                        return 'v';
                case 0x3a:
                        return 'w';
                case 0x3b:
                        return 'x';
                case 0x3d:
                        return 'y';
                case 0x3e:
                        return 'z';
                case 0x3f:
                        return ' ';
                default:
                        return 'l';
        }
}//..

// Print Square Key../
void print_square_key(int rows, char ** sk)
{
        int i, j;

        for (i = 0; i < rows; i++)
        {
                for (j = 0; j < 9; j++)
                {
                        printf("%c", sk[i][j]);
                }
                printf("\n");
        }
}//..
