use Class::Struct;
use Data::Dumper;
use POSIX;

struct( Data => {
                p1 => '$',  #probability 1
                v1 => '$',  #value 1
                p2 => '$',  #probabilty 2
                v2 => '$',  #value 2
        });

my $line1 = <STDIN>;
my @input1 = split / /, $line1;

my $max1; my $max2;
my $sdata = Data->new();

for (1..$input1[0]) {
        my $linei = <STDIN>;
        my @inputi = split / /, $linei;
        if (@inputi[0] * @inputi[2] > $max1) {
                $max1 = @inputi[0] * @inputi[2];
                $sdata->p1(@inputi[2]);
                $sdata->v1(@inputi[0]);
        }
        if (@inputi[1] * @inputi[2] > $max2) {
                $max2 = @inputi[1] * @inputi[2];
                $sdata->p2(@inputi[2]);
                $sdata->v2(@inputi[1]);
        }
}

my $flag=1;
my $probability=1;
my $sum=0;
my $steps=@input1[1];
if ($steps <3) {
        print pathsum($sdata, $flag, $probability, $sum, $steps);
        exit;
}

my $prev = 0;
my $prev2 = 0;
my $prev3 = 0;
my $inc;
my $val;
my $i;
for (1..15) {
        $val = pathsum($sdata, $flag, $probability, $sum, $_);
        print $val, ":";
        $prev and print $val - $prev, ":";
        $prev2 and print (($val - $prev - $prev2), ":");
        $prev3 and print ((($val - $prev - $prev2) / $prev3), ";");
        print "\n";
        $prev3 and $inc = ($val - $prev - $prev2) / $prev3;#sprintf("%.12f", $prev3);
        $prev2 and $prev3 = $val - $prev - $prev2;
        $prev and $prev2 = $val - $prev;
        $prev = $val;
}
#$inc = sprintf("%.122f", $inc);
#$inc = 1 - .0000001;

my $increment; 
for (15..$steps) {
        $increment = $prev2 + ($prev3 * $inc);
        $val += $increment;
        $prev2 = $increment;
        $prev3 = $prev3 * $inc;
}
$val = sprintf("%.8f", $val);
print $val, "\n";

sub pathsum {
        if (@_[4] == 0) {
                if (@_[1] == 1) {
                        return @_[2] * @_[3];
                }else{
                        return @_[2] * @_[3];
                }
        }
        if (@_[1] == 1) {
                return pathsum(@_[0], 2, @_[2] * @_[0]->p1, @_[0]->v1, @_[4] - 1) +
                pathsum(@_[0], 1, @_[2] * (1 - @_[0]->p1), 0, @_[4] - 1)
        } else {
                return pathsum(@_[0], 2, @_[2] * @_[0]->p2, @_[3] + @_[0]->v2, @_[4] - 1) +
                pathsum(@_[0], 2, @_[2] * (1 - @_[0]->p2), @_[3], @_[4] - 1)
        }
}
