/*
** Copyright (C) 2023 by Kevin Kobler
**
** Permission to use, copy, modify, and distribute this software and its
** documentation for any purpose and without fee is hereby granted, provided
** that the above copyright notice appear in all copies and that both that
** copyright notice and this permission notice appear in supporting
** documentation.  This software is provided "as is" without express or
** implied warranty.

In this study, a unique variant of Pascal's Triangle incorporating
subtraction to form diamond shapes is introduced. The asymmetric pattern
generated is then subjected to RPS automata, bringing symmetry to the chaos
in an intriguing way. The resulting pattern showcases a mesmerizing
interplay of order and asymmetry. This new method opens up the possibility
of discovering new and exciting patterns by connecting the diamond "wings."
The results of this study provide insights into the creation of asymmetric
patterns and the role of RPS automata in inducing symmetry.

*/

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <unistd.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <climits>
#include <math.h>


#define WING_ITERATIONS 60
#define HEIGHT          660
#define WIDTH           1320 
#define DIAMOND_SIZE    330
#define TRIANGLE_SIZE   5 
#define int int long long

using namespace std;

Display* display;
int screen;
Window window;
GC gc;
XColor blue;
// decs..
// function declarations../
// 
void draw_diamonds(const vector<pair<int, int>> &diamond_coords,
    vector<vector<pair<int, int>>> inner_triangle_coords,
    vector<vector<vector<int>>> d);
void calculate_diamond_coords(vector<pair<int, int>> &diamond_coords);
void init_display();
void draw_triangles(vector<pair<int, int>> triangle_coords,vector<vector<int>>);
void calculate_inner_triangle_coords(vector<pair<int, int>> &diamond_coords,
    vector<vector<pair<int, int>>> &inner_triangle_coords);
void add_inverted_triangle_coords(vector<pair<int, int>> &curr_diamond_coords,
                int curr_x, int curr_y);
void add_triangle_coords(vector<pair<int, int>> &curr_diamond_coords,
                int curr_x, int curr_y);
int rps_solve(int a, int b);
void apply_rps_automata(vector<vector<vector<int>>>& diamond1);
void simplify_diamond(vector<vector<int>>& diamond);
void subtract_values(vector<vector<int>>& pascal_triangle);
vector<vector<int>> generate_pascals_triangle(int iterations);
void initialize_angel_wings(
                vector<vector<vector<int>>> &diamonds, 
                int s);
        //..
// main()
// ../
// ../
// The main function of the code creates a number of 2D arrays of integers
// to represent the state of the diamonds, inner triangles and their
// coordinates on the screen.

// The function calculate_diamond_coords calculates the coordinates of the
// diamonds to be drawn on the screen, while the function
// calculate_inner_triangle_coords calculates the coordinates of the inner
// triangles of the diamonds.

// The function initialize_angel_wings initializes the diamond arrays
// with a default state. After a 10 second sleep, the inner triangles are
// drawn to the screen using the function draw_triangles.

// The diamonds are then iterated over, with each iteration applying a set
// of rules defined in the function apply_rps_automata to change the state
// of the diamonds. The inner triangles are redrawn in each iteration, with
// a 50 millisecond sleep between each iteration.

// Finally, after another 10 second sleep, the X11 window is closed using
// XCloseDisplay.
//..
signed main() {
        vector<vector<vector<int>>> diamonds;
        vector<pair<int, int>> diamond_coords;
        vector<vector<pair<int, int>>> inner_triangle_coords;

        // Initialize X11 window and display
        init_display();
        // Calculate the polygon coordinates for diamonds
        calculate_diamond_coords(diamond_coords);
        // Calculate the coordinates for inner triangles
        calculate_inner_triangle_coords(diamond_coords, inner_triangle_coords);
        initialize_angel_wings(diamonds, diamond_coords.size());
        usleep(10000000);
        for(int i = 0; i < diamonds.size(); i++) {
                draw_triangles(inner_triangle_coords[i], diamonds[i]);
        }
        XFlush(display);
        usleep(10000000);
        // Draw the diamonds to the screen
        for(int x = 0; x < diamonds[0].size()/2; x++) {
                apply_rps_automata(diamonds);
                for(int i = 0; i < diamonds.size(); i++) {
                        draw_triangles(inner_triangle_coords[i], diamonds[i]);
                }
                XFlush(display);
                usleep(50000);
        }
        usleep(10000000);
        // Destroy the screen
        XCloseDisplay(display);
        return 0;
}
//..
// initialize_angel_wings()
// ../
// ../
// This function initializes the creation of Angel Wings. The function
// takes in a 3D vector "diamonds" and an integer "count" as parameters.

// The function first calculates the number of rows the diamond will have
// by dividing the size of the diamond by the size of the triangle.

// The function then enters a loop to generate the desired number of
// diamonds. Inside the loop, it generates Pascal's triangle and saves it
// in a 2D vector "pascal_triangle".

// Next, the function subtracts the values in the triangle to create
// Kevin's Diamond. It then simplifies the diamond to only 3 different
// values by using the modulus operator with 3.

// Finally, the function simplifies the diamond further by only taking
// every other row starting from the first row to the middle row. The
// the middle row is taken again and every other row to the end.

// The final simplified diamond is then saved in the 3D vector "diamonds".
//..
void initialize_angel_wings(
                vector<vector<vector<int>>> &diamonds,
                int count)
{
        int num_rows = DIAMOND_SIZE / TRIANGLE_SIZE - 1;

        for(int i=0; i < count; i++) {
                // Generate Pascal's triangle to a certain iteration
                vector<vector<int>> pascal_triangle = generate_pascals_triangle(num_rows);
                vector<vector<int>> diamond;
                // Subtract values instead of adding them to create Kevin's diamond
                subtract_values(pascal_triangle);
                // Simplify the diamonds to only 3 different values by using %3
                simplify_diamond(pascal_triangle);
                for(int j = 0; j <= pascal_triangle.size()/2; j+=2)
                        diamond.push_back(pascal_triangle[j]);
                for(int j = pascal_triangle.size()/2;
                                j < pascal_triangle.size(); j+=2)
                        diamond.push_back(pascal_triangle[j]);
                diamonds.push_back(diamond);
        }
}
//..
// generate_pascals_triangle()
// ../
// ../
// This function generates Pascal's triangle given a number of iterations.
// It initializes the first row with 1, then generates the rest of the rows
// by adding the elements of the previous row and storing the result in the
// current row. The function returns the final Pascal's triangle as a 2D
// vector.
//..
vector<vector<int>> generate_pascals_triangle(int iterations)
{
        vector<vector<int>> pascal_triangle;

        // Initialize first row
        vector<int> first_row = {1};
        pascal_triangle.push_back(first_row);

        // Generate the rest of the rows
        for (int i = 1; i < iterations; ++i) {
                vector<int> current_row;
                current_row.push_back(1);
                for (int j = 1; j < i; ++j) {
                        current_row.push_back(abs(pascal_triangle[i-1][j-1] 
                                        + pascal_triangle[i-1][j]));
                }
                current_row.push_back(1);
                pascal_triangle.push_back(current_row);
        }

        return pascal_triangle;
}
//..
// subtract_values()
// ../
// ../
// 'subtract_values' subtracts values in a given row of Pascal's triangle to
// form a new row. The last row of the triangle is added to the vector
// and new rows are created by subtracting values of adjacent elements in
// the previous row until only one value is left.
//..
void subtract_values(vector<vector<int>>& pascal_triangle)
{
        int x = pascal_triangle[pascal_triangle.size()-1].size();

        vector<int> row;
        for (int i = 0; i < x; i++) {
                int value = pascal_triangle[pascal_triangle.size()-1][i];
                row.push_back(value);
        }
        pascal_triangle.push_back(row);
        while(pascal_triangle[pascal_triangle.size()-1].size() > 1) {
                vector<int> row;
                for(int i=0; 
                   i < pascal_triangle[pascal_triangle.size()-1].size()-1; i++) {
                    int z = pascal_triangle.size()-1;
                  int value = abs(pascal_triangle[z][i] - pascal_triangle[z][i+1]);
                        row.push_back(value);
                }
                pascal_triangle.push_back(row);
        }
}
//..
// simplify_diamond()
// ../
// Simplify the values in the diamond by taking the remainder of each value with 3
void simplify_diamond(vector<vector<int>>& diamond)
{
        for (int i = 0; i < diamond.size(); ++i) {
                for (int j = 0; j < diamond[i].size(); ++j) {
                        diamond[i][j] = diamond[i][j] % 3;
                }
        }
}
//..
// apply_rps_automata(
// ../
// ../
// 'apply_rps_automata()' applies the Rock-Paper-Scissors (RPS) automata to
// the diamonds 2D matrix. It loops through each diamond, first processing
// the top half and then processing the bottom half. For each iteration, it
// updates the value of the current position in the matrix by computing the
// result of the RPS game between the two neighbors in the previous row.
// The result of the RPS game is computed by the rps_solve() function.
//..
void apply_rps_automata( vector<vector<vector<int>>>& diamonds)
{
        // Go through each diamond and apply automata between eachother and down
        // two loops, one starts in the middle and goes up, the other starts in
        // the middle and goes down
        vector<vector<int>> temp;

        for(int i = 0; i < diamonds.size(); i++) {
                for(int j = 0; j < diamonds[i].size()/2-1; j++) {
                        for(int k = 0; k < diamonds[i][j+1].size()-2; k++) {
                                int x = rps_solve(diamonds[i][j+1][k],
                                                diamonds[i][j+1][k+2]);
                                diamonds[i][j][k] = x;
                        }
                }
                for(int j = diamonds[i].size()-1; j > diamonds[i].size()/2; j--) {
                        for(int k = 0; k < diamonds[i][j-1].size()-2; k++) {
                                int x = rps_solve(diamonds[i][j-1][k],
                                                diamonds[i][j-1][k+2]);
                                diamonds[i][j][k] = x;
                        }
                }
        }
}
//..
// rps_solve()
// ../
// rps_solve() - returns the odd value out from the input values a and b,
// if they are the same then it returns that value.
int rps_solve(int a, int b)
{
        if(a == b)
                return a;
        if(a == 1)
                if(b == 0)
                        return 2;
                else
                        return 0;
        if(a == 2)
                if(b == 0)
                        return 1;
                else
                        return 0;
        if(a == 0)
                if(b == 1)
                        return 2;
                else
                        return 1;
        exit(1);
}
//..
// initialize_x11_display()
// ../
void init_display()
{
        display = XOpenDisplay(NULL);
        if (display == NULL) {
                cerr << "Failed to open X display\n";
                exit(1);
        }

        blue.red = 0;
        blue.green = 0;
        blue.blue = 0xffff;
        XAllocColor(display, DefaultColormap(display, screen), &blue);

        screen = DefaultScreen(display);
        window = XCreateSimpleWindow(display, RootWindow(display, screen),
                        100, 100, WIDTH, HEIGHT, 0,
                        BlackPixel(display, screen),
                        blue.pixel);

        XStoreName(display, window, "Angel Wings");
        XSelectInput(display, window, ExposureMask | KeyPressMask);
        XMapWindow(display, window);

        gc = XCreateGC(display, window, 0, NULL);
        XSetForeground(display, gc, BlackPixel(display, screen));
        XFlush(display);
}
//..
// calculate_diamond_coords(vector<pair<int, int>> &diamond_coords)
// ../
// ../
// This function is used to calculate the center coordinates of each
// diamond that can fit on the screen. The screen dimensions and diamond
// size are defined by global constants (WIDTH and HEIGHT, DIAMOND_SIZE).
// The calculated coordinates are stored in the 'diamond_coords' vector of
// pairs. 

// The coordinates are calculated by looping
// through each row and column of diamonds, and using the formula
// (column * DIAMOND_SIZE + DIAMOND_SIZE/2, row * DIAMOND_SIZE +
// DIAMOND_SIZE/2) for the center coordinate of each diamond.
//..
void calculate_diamond_coords(vector<pair<int, int>> &diamond_coords) {
        // Clear the previous diamond coordinates
        diamond_coords.clear();

        // Calculate the number of diamonds that can fit horizontally on the screen 
        int num_diamonds_x = WIDTH / DIAMOND_SIZE;

        // Calculate the number of diamonds that can fit vertically on the screen
        int num_diamonds_y = HEIGHT / DIAMOND_SIZE;

        // Loop through each row of diamonds
        for (int row = 0; row < num_diamonds_y; ++row) {
                // Loop through each column of diamonds
                for (int col = 0; col < num_diamonds_x; ++col) {
                        // Calculate the top-left coordinate for the current diamond
                        int x = col * DIAMOND_SIZE + DIAMOND_SIZE/2;
                        int y = row * DIAMOND_SIZE + DIAMOND_SIZE/2;

                        // Add the current diamond's coordinates to the list
                        diamond_coords.push_back(make_pair(x, y));
                }
        }
}
//..
// add_triangle_coords()
// ../
// ../
// This function is used to add the coordinates of a triangle to the
// curr_diamond_coords vector.

// The triangle is defined by three points, (x1, y1), (x2, y2), and (x3, y3),
// and it is specified by the curr_x and curr_y inputs.
//..
void add_triangle_coords(vector<pair<int, int>> &curr_diamond_coords,
                int curr_x, int curr_y)
{
        curr_diamond_coords.push_back(make_pair(curr_x + TRIANGLE_SIZE/2, curr_y));
        curr_diamond_coords.push_back(make_pair(curr_x + TRIANGLE_SIZE*3/2,
                                curr_y + TRIANGLE_SIZE));
        curr_diamond_coords.push_back(make_pair(curr_x - TRIANGLE_SIZE/2, curr_y + TRIANGLE_SIZE));
}
//..
// add_inverted_triangle_coords()
// ../
// ../
// The function creates an inverted triangle by adding three pairs of x,y
// coordinates to the curr_diamond_coords list. The coordinates are
// calculated as follows:

// 1. The top left point is located at (curr_x - TRIANGLE_SIZE/2, curr_y)
// 2. The bottom point of the triangle is located at 
//      (curr_x + TRIANGLE_SIZE/2, curr_y + TRIANGLE_SIZE)
// 3. The top right point of the triangle is located at 
//      (curr_x + TRIANGLE_SIZE*3/2, curr_y)
//..
void add_inverted_triangle_coords(vector<pair<int, int>> &curr_diamond_coords,
                int curr_x, int curr_y)
{
        curr_diamond_coords.push_back(make_pair(curr_x - TRIANGLE_SIZE/2, curr_y));
        curr_diamond_coords.push_back(make_pair(curr_x + TRIANGLE_SIZE/2,
                                curr_y + TRIANGLE_SIZE));
        curr_diamond_coords.push_back(make_pair(curr_x + TRIANGLE_SIZE*3/2,
                                curr_y));
}
//..
// add_diamond_triangles2()
// ../
// ../

// This function adds the bottom half of the diamond, which is composed of
// inverted triangles and regular triangles. It starts from the bottom row
// and works its way up to the middle row, following the same pattern as
// add_diamond_triangles. The starting x and y coordinates are calculated
// based on the number of rows in the diamond and the size of the
// triangles. The triangle pattern is determined by checking if the current
// triangle's index in the row is even or odd. If it's odd, a regular
// triangle is added, and if it's even, an inverted triangle is added.

//..
void add_diamond_triangles2(vector<pair<int, int>> &curr_diamond_coords,
                const pair<int, int> &diamond_coords)
{

        // Calculate the number of rows in the current diamond
        int num_rows = DIAMOND_SIZE / (TRIANGLE_SIZE * 2);

        int x = diamond_coords.first - TRIANGLE_SIZE/2,
            y = diamond_coords.second  - DIAMOND_SIZE/2 + TRIANGLE_SIZE*(num_rows-1);

        for (int row = num_rows - 1; row >= 0; row--) {
                int num_triangles_in_row = 2 * row + 1;
                int start_x = (x - row*TRIANGLE_SIZE);

                // Calculate the y-coordinate for the current row
                int curr_y = y + (num_rows - row) * TRIANGLE_SIZE;

                for (int triangle = 0; triangle < num_triangles_in_row; triangle++) {
                        int curr_x = start_x + triangle * TRIANGLE_SIZE;

                        if (triangle % 2 == 1) {
                                add_triangle_coords(curr_diamond_coords,
                                                curr_x, curr_y);
                        } else {
                                add_inverted_triangle_coords(curr_diamond_coords,
                                                curr_x, curr_y);
                        }
                }
        }
}
//..
// add_diamond_triangles()
// ../
// ../

// This function add_diamond_triangles adds right-side-up and upside-down
// triangles to the top half of a diamond shape. It calculates the number
// of rows in the diamond shape and then adds the necessary triangles for
// each row by alternating between right-side-up and upside-down triangles.
// The x and y coordinates for each triangle are calculated based on the
// starting x and y coordinates of the diamond, the number of rows in the
// diamond, the size of each triangle, and the current row being processed.

//..
void add_diamond_triangles(vector<pair<int, int>> &curr_diamond_coords,
                const pair<int, int> &diamond_coords)
{
        int x = diamond_coords.first - TRIANGLE_SIZE/2, y = diamond_coords.second;

        // Calculate the number of rows in the current diamond
        int num_rows = DIAMOND_SIZE / (TRIANGLE_SIZE * 2);

        for (int row = 0; row < num_rows; row++) {
                int num_triangles_in_row = 2 * row + 1;
                int start_x = (x - row*TRIANGLE_SIZE);

                // Calculate the y-coordinate for the current row
                int curr_y = y - DIAMOND_SIZE/2 + row * TRIANGLE_SIZE;

                for (int triangle = 0; triangle < num_triangles_in_row; triangle++) {
                        int curr_x = start_x + triangle * TRIANGLE_SIZE;

                        if (triangle % 2 == 0) {
                                add_triangle_coords(curr_diamond_coords,
                                                curr_x, curr_y);
                        } else {
                                add_inverted_triangle_coords(curr_diamond_coords,
                                                curr_x, curr_y);
                        }
                }
        }
}
//..
// calculate_inner_triangle_coords()
// ../
// ../

// This function calculates the coordinates of the triangles
// inside a diamond shape.

// The diamond coordinates are passed as the first argument diamond_coords,
// and the calculated inner triangle coordinates are stored in the second
// argument inner_triangle_coords.

// The function first clears any previous inner triangle coordinates stored
// in inner_triangle_coords.

// Then, it loops through each diamond in diamond_coords and calculates the
// coordinates of the triangles inside it.

// This is done by first calling add_diamond_triangles which adds the
// top half of the diamond then calling add_diamond_triangles2 which
// adds the bottom half of the diamond

// The calculated inner triangle coordinates for each diamond are then
// added to inner_triangle_coords as a vector of pairs.

//..
void calculate_inner_triangle_coords(vector<pair<int, int>> &diamond_coords,
                vector<vector<pair<int, int>>> &inner_triangle_coords)
{
        inner_triangle_coords.clear();

        // Loop through each diamond
        for (int i = 0; i < diamond_coords.size(); i++) {
                vector<pair<int, int>> curr_diamond_coords;
                add_diamond_triangles(curr_diamond_coords,diamond_coords[i]);
                add_diamond_triangles2(curr_diamond_coords,diamond_coords[i]);
                inner_triangle_coords.push_back(curr_diamond_coords);
        }
}
//..
// draw_triangles() 
// ../
// ../

// This is a function to draw triangles in a window using the X Window
// System. The triangles are defined by their vertices stored in
// triangle_coords, which is a vector of pairs of integers. The type
// parameter is a 2D vector of integers representing the type of the
// triangle. If type[i][j] is 0, the triangle is filled with white color,
// if type[i][j] is 1, it is filled with blue color, and if type[i][j] is
// 2, it is filled with black color.

// The function uses the Xlib library to draw the triangles. It loops
// through type, and for each triangle in type, it retrieves the
// corresponding vertices from triangle_coords and sets the color based on
// the value of type[i][j]. The XFillPolygon function is used to draw a
// filled polygon with the vertices defined by triangle.

//..
void draw_triangles(vector<pair<int, int>> triangle_coords, vector<vector<int>> type) 
{
        int x = 0;
        for (int i = 0; i < type.size(); i++) {
                for (int j = 0; j < type[i].size(); j++) {
                        int x1 = triangle_coords[x].first,
                            y1 = triangle_coords[x++].second;
                        int x2 = triangle_coords[x].first,
                            y2 = triangle_coords[x++].second;
                        int x3 = triangle_coords[x].first,
                            y3 = triangle_coords[x++].second;

                        XPoint triangle[3];
                        triangle[0].x = x1;
                        triangle[0].y = y1;
                        triangle[1].x = x2;
                        triangle[1].y = y2;
                        triangle[2].x = x3;
                        triangle[2].y = y3;
                        if (type[i][j] == 2)
                                XSetForeground(display, gc, BlackPixel(display, screen));
                        if (type[i][j] == 0)
                                XSetForeground(display, gc, WhitePixel(display, screen));
                        if (type[i][j] == 1)
                                XSetForeground(display, gc, blue.pixel);
                        XFillPolygon(display, window, gc, triangle, 3,
                                        Convex, CoordModeOrigin);
                }
        }
}
//..
