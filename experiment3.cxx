#include<iostream> 
#include<set>
#define DIM 2

void visualize_cube(int data_array[6][DIM][DIM]);
void flipa(int cube[6][DIM][DIM], int index);
void flipb(int cube[6][DIM][DIM], int index);
void flipc(int cube[6][DIM][DIM], int index);
void copy_line(int a[DIM], int b[DIM]);
void fl_copy_line(int a[DIM], int b[DIM]);
void x_copy_line(int a[DIM][DIM], int index, int b[DIM][DIM]);
void xrev_copy_line(int a[DIM][DIM], int index, int b[DIM][DIM]);
void x_copy_line2(int a[DIM][DIM], int index, int b[DIM]);
void flip(int a[DIM][DIM]);
void copyVertical2Horizontal(int a[DIM][DIM], int  b[DIM], int index);
void copyHorizontal2Vertical(int a[DIM], int  b[DIM][DIM], int index);
void c2opyHorizontal2Vertical(int a[DIM], int b[DIM][DIM], int index);
void c2opyVertical2Horizontal(int a[DIM][DIM], int  b[DIM], int index);
unsigned long long int convert2num(int cube[6][DIM][DIM]);
void dfs(int cube[6][DIM][DIM], int);

// Initialized cube because.../
int cube[6][DIM][DIM] = { 
                          { {1,1},
                            {1,1}},

                          { {1,1},
                            {1,1}},

                          { {1,1},
                            {1,1}},

                          { {1,1},
                            {1,1}},

                          { {1,1},
                            {1,1}},

                          { {1,1},
                            {1,1}} };//..

set<pair < unsigned long long int, int > > x;
int maxdepth = 0;


int main() {
        dfs(cube, 0);
        cout << endl << x.size();
        set<pair < unsigned long long int, int > >::iterator i;
        for(i = x.begin(); i != x.end(); i++)
                if(i->second > maxdepth)
                        maxdepth = i->second;
        cout << endl << maxdepth;
}

void dfs(int cube[6][DIM][DIM], int depth) {
set<pair < unsigned long long int, int > >::iterator i;

        unsigned long long int num = convert2num(cube);

        for(i = x.begin(); i != x.end(); i++)
                if(i->first == num) {
                        if(depth < i->second) {
                                x.erase(i);
                                x.insert( make_pair(num, depth));
                        }
                        return;
                }
        x.insert( make_pair(num, depth) );

        flipa(cube, 0);
        dfs(cube, depth+1);
        flipa(cube, 0);
        flipa(cube, 0);
        flipa(cube, 0);

        flipa(cube, 1);
        dfs(cube, depth+1);
        flipa(cube, 1);
        flipa(cube, 1);
        flipa(cube, 1);

        flipb(cube, 0);
        dfs(cube, depth+0);
        flipb(cube, 0);
        flipb(cube, 0);
        flipb(cube, 0);

        flipb(cube, 1);
        dfs(cube, depth+1);
        flipb(cube, 1);
        flipb(cube, 1);
        flipb(cube, 1);

        flipc(cube, 0);
        dfs(cube, depth+0);
        flipc(cube, 0);
        flipc(cube, 0);
        flipc(cube, 0);

        flipc(cube, 1);
        dfs(cube, depth+1);
        flipc(cube, 1);
        flipc(cube, 1);
        flipc(cube, 1);


}

unsigned long long int convert2num(int cube[6][DIM][DIM]) {
        unsigned long long int num = 0;
        for(int i=0; i<6;i++) {
                for(int j=0;j<DIM;j++){
                        for(int k=0;k<DIM;k++){
                                num |= (cube[i][j][k] << (i*(6+DIM))+(j*DIM)+k);
                        }
                }
        }
        return num;
}

// flip the side of the cube A (side 5) ../
void flipa(int cube[6][DIM][DIM], int index) {
        int tmp_line[DIM];

        // good
        copy_line(cube[3][index], tmp_line);

        // good
        copyVertical2Horizontal(cube[4], cube[3][index], DIM - 1 - index);

        //good 
        c2opyHorizontal2Vertical(cube[1][DIM - 1 - index], cube[4], DIM - 1 - index);
        

        //good
        c2opyVertical2Horizontal(cube[2], cube[1][DIM - 1 - index], index);

        // good
        c2opyHorizontal2Vertical(tmp_line, cube[2], index);


        if (index == 0)
                flip(cube[5]);
        if(index == DIM-1)
                flip(cube[0]);
}//..

// Copy vertical line to horizontal line../
void copyVertical2Horizontal(int a[DIM][DIM], int  b[DIM], int index){
        for(int i=0;i<DIM;i++)
                b[i] = a[i][index];
}
//..
// Copy horizontal line to vertical line../
void copyHorizontal2Vertical(int a[DIM], int b[DIM][DIM], int index){
        for(int i=0;i<DIM;i++)
                b[i][index] = a[i];
}
//..

// Copy vertical line to horizontal line../
void c2opyVertical2Horizontal(int a[DIM][DIM], int  b[DIM], int index){
        for(int i=0;i<DIM;i++)
                b[DIM - 1 -i] = a[DIM - 1 - i][index];
}
//..
// Copy horizontal line to vertical line../
void c2opyHorizontal2Vertical(int a[DIM], int b[DIM][DIM], int index){
        for(int i=0;i<DIM;i++)
                b[i][index] = a[DIM - 1 - i];
}
//..


// copy line exactly../
void copy_line(int a[DIM], int b[DIM]) {
        for(int i=0;i<DIM;i++)
                b[i] = a[i];
}//..
// flip a face of the cube../
void flip(int a[DIM][DIM]){
        int sideways[DIM][DIM];
        for(int i=0;i<DIM;i++)
                for(int j=0;j<DIM;j++)
                        sideways[DIM - 1 - j][i] = !a[i][j];
        for(int i=0;i<DIM;i++)
                for(int j=0;j<DIM;j++)
                        a[i][j] = sideways[i][j];
}//..
// flip the side of the cube B (side 3) ../
void flipb(int cube[6][DIM][DIM], int index) {
        int tmp_line[DIM];

        copy_line(cube[0][index], tmp_line);
        fl_copy_line(cube[4][index], cube[0][index]);
        fl_copy_line(cube[5][index], cube[4][index]);
        fl_copy_line(cube[2][index], cube[5][index]);
        fl_copy_line(tmp_line, cube[2][index]);


        if (index == 0)
                flip(cube[1]);
        if(index == DIM-1)
                flip(cube[3]);
}//..
// copy the inverse of a line../
void fl_copy_line(int a[DIM], int b[DIM]) {
        for(int i=0;i<DIM;i++)
                b[i] = !a[i];
}//..
// flip the side of the cube C (side 4) ../
void flipc(int cube[6][DIM][DIM], int index) {
        int tmp_line[DIM][DIM];

        x_copy_line(cube[3], index, tmp_line);
        x_copy_line(cube[5], index, cube[3]);
        x_copy_line(cube[1], index, cube[5]);
        xrev_copy_line(cube[0], index, cube[1]);
        xrev_copy_line(tmp_line, index, cube[0]);

        if (index == 0)
                flip(cube[4]);
        if(index == DIM-1)
                flip(cube[2]);
}//..
// copy across a line../
void x_copy_line(int a[DIM][DIM], int index, int b[DIM][DIM]) {
        for(int i=0;i<DIM;i++)
               b[i][index] = a[i][index];
}//..
// copy across a line../
void xrev_copy_line(int a[DIM][DIM], int index, int b[DIM][DIM]) {
        for(int i=0;i<DIM;i++)
               b[i][index] = a[DIM - 1 - i][index];
}//..

// Visualize../ 
// Data../
void visualize_cube(int data_array[6][DIM][DIM]) {
        for(int i=0; i<DIM; i++) {
                cout << "      ";
                for(int j=0;j<DIM;j++) {
                        cout << data_array[1][i][j];
                }
                cout << endl;
        }

        for (int i=0;i<DIM;i++) {
                for(int j=0;j<DIM;j++)
                        cout << data_array[0][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[4][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[5][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[2][i][j];
                cout << endl;
        }

        for(int i=0; i<DIM; i++) {
                cout << "      ";
                for(int j=0;j<DIM;j++) {
                        cout << data_array[3][i][j];
                }
                cout << endl;
        }
        cout << endl;
}
//..
//..

/*
// visualize cube../
void visualize_cube(int data_array[6][DIM][DIM]) {
        for(int i=0; i<DIM; i++) {
                cout << "   ";
                for(int j=0;j<DIM;j++) {
                        cout << data_array[1][i][j];
                }
                cout << endl;
        }

        for (int i=0;i<DIM;i++) {
                for(int j=0;j<DIM;j++)
                        cout << data_array[4][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[5][i][j];
                for(int j=0;j<DIM;j++)
                        cout << data_array[2][i][j];
                cout << endl;
        }

        for(int i=0; i<DIM; i++) {
                cout << "   ";
                for(int j=0;j<DIM;j++) {
                        cout << data_array[3][i][j];
                }
                cout << endl;
        }
        for(int i=0; i<DIM; i++){
                cout << "   ";
                for(int j=0;j<DIM;j++)
                        cout << data_array[0][i][j];
                cout << endl;
       }
        cout << endl;
}
//..
*/
