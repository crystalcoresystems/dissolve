#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_I 2000

char *solve_with_key(int rows, char *message, char **square_key);
void generate_keys(char keys[9001][9], const char *pswd);
char *pad_message(char *message);
void s_scramble(char *message);
char *s_unscramble(const char *message);
int get_user_input(char *password, char *message);
char **initialize_key(int layers, const char *key);
char **create_square_key(int layers, const char *key);
char solve(char a, char b);
char boolean_solve(char input, char mech);
char char_to_triune(char c);
char triune_to_char(char c);
void solve_diagonally_right(int x, int layers, char **sk);
void solve_diagonally_left(int x, int layers, char **sk);

/* Convert a character to its triune representation */
char char_to_triune(char c) {
    switch (c) {
        case 'a': return 0x15;
        case 'b': return 0x16;
        case 'c': return 0x17;
        case 'd': return 0x19;
        case 'e': return 0x1A;
        case 'f': return 0x1B;
        case 'g': return 0x1D;
        case 'h': return 0x1E;
        case 'i': return 0x1F;
        case 'j': return 0x25;
        case 'k': return 0x26;
        case 'l': return 0x27;
        case 'm': return 0x29;
        case 'n': return 0x2A;
        case 'o': return 0x2B;
        case 'p': return 0x2D;
        case 'q': return 0x2E;
        case 'r': return 0x2F;
        case 's': return 0x35;
        case 't': return 0x36;
        case 'u': return 0x37;
        case 'v': return 0x39;
        case 'w': return 0x3A;
        case 'x': return 0x3B;
        case 'y': return 0x3D;
        case 'z': return 0x3E;
        case ' ': return 0x3F;
        default:  return 0x3B; // Default to 'x'
    }
}

/* Convert a triune representation back to a character */
char triune_to_char(char c) {
    switch (c) {
        case 0x15: return 'a';
        case 0x16: return 'b';
        case 0x17: return 'c';
        case 0x19: return 'd';
        case 0x1A: return 'e';
        case 0x1B: return 'f';
        case 0x1D: return 'g';
        case 0x1E: return 'h';
        case 0x1F: return 'i';
        case 0x25: return 'j';
        case 0x26: return 'k';
        case 0x27: return 'l';
        case 0x29: return 'm';
        case 0x2A: return 'n';
        case 0x2B: return 'o';
        case 0x2D: return 'p';
        case 0x2E: return 'q';
        case 0x2F: return 'r';
        case 0x35: return 's';
        case 0x36: return 't';
        case 0x37: return 'u';
        case 0x39: return 'v';
        case 0x3A: return 'w';
        case 0x3B: return 'x';
        case 0x3D: return 'y';
        case 0x3E: return 'z';
        case 0x3F: return ' ';
        default:   return 'l'; // Default to 'l'
    }
}

/* Perform boolean solve operation */
char boolean_solve(char input, char mech) {
    char tmp = input ^ mech;
    char tmp3 = (tmp >> 1) | (tmp << 1) | tmp;
    char tmp2 = tmp ^ tmp3;
    tmp2 = tmp2 ^ tmp3;
    tmp = ~tmp3 & input;
    return (tmp ^ tmp2) & 3;
}

/* Solve two characters using boolean_solve */
char solve(char a, char b) {
    char a1 = char_to_triune(a);
    char b1 = char_to_triune(b);
    char solution = 0;

    for (int offset = 0; offset < 3; offset++) {
        char mask = 3 << (offset * 2);
        char s = boolean_solve(
            (a1 & mask) >> (offset * 2),
            (b1 & mask) >> (offset * 2)
        );
        solution = (solution & ~mask) | (s << (offset * 2));
    }

    return triune_to_char(solution);
}

/* Get user input for message and password */
int get_user_input(char *password, char *message) {
    char option = 'a';
    char *p;

    printf("Hello. What's your message?\n");
    if (fgets(message, MAX_I, stdin) == NULL) exit(1);
    message[strcspn(message, "\n")] = '\0';
    for (p = message; *p; ++p) *p = tolower(*p);

    printf("What's your password?\n");
    if (fgets(password, MAX_I, stdin) == NULL) exit(1);
    password[strcspn(password, "\n")] = '\0';
    for (p = password; *p; ++p) *p = tolower(*p);

    while (option != 'D' && option != 'E') {
        printf("[E]ncrypt or [D]ecrypt?\n");
        option = toupper(fgetc(stdin));
        while (fgetc(stdin) != '\n'); // Clear input buffer
    }

    return (option == 'E') ? 0 : 1;
}

/* Generate keys based on the password */
void generate_keys(char keys[9001][9], const char *pswd) {
    int pswd_len = strlen(pswd);
    srand(7); // Fixed seed for consistent key generation

    for (int i = 0; i < 9001; i++) {
        for (int j = 0; j < 9; j++) {
            keys[i][j] = pswd[rand() % pswd_len];
        }
    }
}

/* Pad message to make its length a multiple of 9 */
char *pad_message(char *message) {
    int message_size = strlen(message);
    int pad_size = (9 - (message_size % 9)) % 9;
    char *new_message = malloc(message_size + pad_size + 1);

    strcpy(new_message, message);
    for (int i = 0; i < pad_size; i++) {
        new_message[message_size + i] = 'l';
    }
    new_message[message_size + pad_size] = '\0';

    free(message);
    return new_message;
}

/* Scramble the message */
void s_scramble(char *message) {
    int length = strlen(message);

    for (int i = 0, j = 1; j < length; i++, j++) {
        message[j] = solve(message[i], message[j]);
    }
}

/* Unscramble the message */
char *s_unscramble(const char *message) {
    int length = strlen(message);
    char *new_message = malloc(length + 1);

    new_message[0] = message[0];
    for (int i = 0, j = 1; j < length; i++, j++) {
        new_message[j] = solve(message[i], message[j]);
    }
    new_message[length] = '\0';

    return new_message;
}

/* Initialize the key grid */
char **initialize_key(int layers, const char *key) {
    char **square_key = malloc(layers * sizeof(char *));
    for (int i = 0; i < layers; i++) {
        square_key[i] = malloc(9);
    }

    memcpy(square_key[0], key, 9);
    for (int i = 1; i < layers; i++) {
        memset(square_key[i], 'l', 9);
    }

    return square_key;
}

/* Solve diagonally to the right */
void solve_diagonally_right(int x, int layers, char **sk) {
    for (int row = 0; row < layers - 1; row++) {
        if (x == 8) {
            sk[row + 1][0] = solve(sk[row][x], sk[row + 1][0]);
            x = 0;
        } else {
            sk[row + 1][x + 1] = solve(sk[row][x], sk[row + 1][x + 1]);
            x++;
        }
    }
}

/* Solve diagonally to the left */
void solve_diagonally_left(int x, int layers, char **sk) {
    for (int row = 0; row < layers - 1; row++) {
        if (x == 0) {
            sk[row + 1][8] = solve(sk[row][x], sk[row + 1][8]);
            x = 8;
        } else {
            sk[row + 1][x - 1] = solve(sk[row][x], sk[row + 1][x - 1]);
            x--;
        }
    }
}

/* Create the square key grid */
char **create_square_key(int layers, const char *key) {
    char **square_key = initialize_key(layers, key);

    for (int i = 0; i < 9; i++) {
        solve_diagonally_right(i, layers, square_key);
        solve_diagonally_left(i, layers, square_key);
    }

    return square_key;
}

/* Solve message with the key grid */
char *solve_with_key(int rows, char *message, char **square_key) {
    int length = rows * 9;
    char *new_message = malloc(length + 1);

    for (int row = 0; row < rows; row++) {
        for (int index = 0; index < 9; index++) {
            int idx = index + row * 9;
            new_message[idx] = solve(message[idx], square_key[row][index]);
        }
    }
    new_message[length] = '\0';

    free(message);
    return new_message;
}

/* Main function */
int main() {
    char *password = malloc(MAX_I);
    char *message = malloc(MAX_I);
    char keys[9001][9];
    int option;

    option = get_user_input(password, message);
    message = pad_message(message);
    int rows = strlen(message) / 9;

    generate_keys(keys, password);

    if (option == 0) { // Encrypt
        for (int i = 0; i < 9001; i++) {
            char **square_key = create_square_key(rows, keys[i]);
            message = solve_with_key(rows, message, square_key);
            s_scramble(message);

            for (int r = 0; r < rows; r++) free(square_key[r]);
            free(square_key);
        }
        printf("Encrypted message:\n%s\n", message);
    } else { // Decrypt
        for (int i = 9000; i >= 0; i--) {
            char *unscrambled_message = s_unscramble(message);
            free(message);
            message = unscrambled_message;

            char **square_key = create_square_key(rows, keys[i]);
            message = solve_with_key(rows, message, square_key);

            for (int r = 0; r < rows; r++) free(square_key[r]);
            free(square_key);
        }
        printf("Decrypted message:\n%s\n", message);
    }

    free(message);
    free(password);
    return 0;
}
