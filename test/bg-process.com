$!created on  2-AUG-2021
$!----------------------
$!
$! filename: BG-PROCESS.COM
$!    input: current directory
$!   output: print something if current directory changed
$!     idea: Testing out how to make a background process in openVMS
$!     form: test script
$!
$!     __________________________
$!    | Written by |K|O|13|13|S| |
$!     ==========================
$!
$!initialization../
$    saved_message   = f$environment("MESSAGE")
$    set message/facility/severity/identification/text
$    BGP__status    = %x11000000
$    BGP__success   = BGP__status + %x001
$    BGP__ctrly     = BGP__status + %x00c
$    on control_y then goto ctrl_y
$    on warning then goto error
$    display         = "write sys$output"
$    libcall         = "@engine:subroutine_library"
$    ask             = libcall + " ask"
$    signal          = libcall + " signal"
$    false           = 0
$    true            = 1
$    undefine        = "deassign"
$!-------------------------------------------------/..
$ create test.dat
