#include "playback_controller.h"
#include <pthread.h>


void playback_init(PlaybackController *controller, Buffer *buffer) {
    controller->paused = 0;
    controller->at_boundary = 0;
    controller->exit_flag = 0; // Initialize exit flag to 0
    controller->speed = 1.0;
    controller->direction = PLAYBACK_FORWARD;
    controller->position = 0;
    controller->mode = PLAYBACK_MODE_NORMAL;
    controller->buffer = buffer;

    pthread_mutex_init(&controller->mutex, NULL);
    pthread_cond_init(&controller->cond, NULL);
    pthread_cond_init(&controller->boundary_cond, NULL);
}

void playback_pause(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    controller->paused = 1;
    pthread_mutex_unlock(&controller->mutex);
}

void playback_resume(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    controller->paused = 0;
    pthread_cond_signal(&controller->cond);
    pthread_mutex_unlock(&controller->mutex);
}

int playback_get_paused(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    int paused = controller->paused;
    pthread_mutex_unlock(&controller->mutex);
    return paused;
}

void playback_signal_boundary(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    pthread_cond_signal(&controller->boundary_cond);
    pthread_mutex_unlock(&controller->mutex);
}

void playback_set_exit_flag(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    controller->exit_flag = 1;
    pthread_mutex_unlock(&controller->mutex);
}

int playback_get_exit_flag(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    int flag = controller->exit_flag;
    pthread_mutex_unlock(&controller->mutex);
    return flag;
}

void playback_signal_all(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    pthread_cond_broadcast(&controller->cond);
    pthread_cond_broadcast(&controller->boundary_cond);
    pthread_mutex_unlock(&controller->mutex);
}

void playback_set_mode(PlaybackController *controller, PlaybackMode mode) {
    pthread_mutex_lock(&controller->mutex);
    controller->mode = mode;
    pthread_mutex_unlock(&controller->mutex);
}

void playback_set_speed(PlaybackController *controller, double speed) {
    pthread_mutex_lock(&controller->mutex);
    controller->speed = speed;
    pthread_mutex_unlock(&controller->mutex);
}

void playback_reverse(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    if (controller->direction == PLAYBACK_FORWARD)
        controller->direction = PLAYBACK_REVERSE;
    else
        controller->direction = PLAYBACK_FORWARD;

    // Signal the rendering thread if it's waiting at the boundary
    if (controller->at_boundary) {
        pthread_cond_signal(&controller->boundary_cond);
    }

    pthread_mutex_unlock(&controller->mutex);
}

void playback_seek(PlaybackController *controller, size_t position) {
    pthread_mutex_lock(&controller->mutex);
    controller->position = position;
    pthread_mutex_unlock(&controller->mutex);
}

void playback_seek_to_keyframe(PlaybackController *controller, size_t keyframe_index) {
    pthread_mutex_lock(&controller->mutex);
    size_t position = buffer_get_keyframe_position(controller->buffer, keyframe_index);
    if (position != (size_t)-1) {
        controller->position = position;
    } else {
        // Handle invalid keyframe index (optional: log an error)
    }
    pthread_mutex_unlock(&controller->mutex);
}

size_t playback_get_position(PlaybackController *controller) {
    pthread_mutex_lock(&controller->mutex);
    size_t pos = controller->position;
    pthread_mutex_unlock(&controller->mutex);
    return pos;
}
