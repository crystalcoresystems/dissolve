#ifndef PLAYBACK_CONTROLLER_H
#define PLAYBACK_CONTROLLER_H

#include <stddef.h>
#include <pthread.h>
#include "buffer.h"

typedef enum {
    PLAYBACK_FORWARD = 1,
    PLAYBACK_REVERSE = -1
} PlaybackDirection;

typedef enum {
    PLAYBACK_MODE_NORMAL,
    PLAYBACK_MODE_KEYFRAME
} PlaybackMode;

typedef struct {
    int paused;
    int at_boundary;
    int exit_flag; // New exit flag
    double speed;
    PlaybackDirection direction;
    size_t position;
    PlaybackMode mode;

    pthread_mutex_t mutex;
    pthread_cond_t cond;
    pthread_cond_t boundary_cond;

    Buffer *buffer;
} PlaybackController;

void playback_init(PlaybackController *controller, Buffer *buffer);
void playback_pause(PlaybackController *controller);
void playback_resume(PlaybackController *controller);
void playback_set_speed(PlaybackController *controller, double speed);
void playback_reverse(PlaybackController *controller);
void playback_seek(PlaybackController *controller, size_t position);
size_t playback_get_position(PlaybackController *controller);
void playback_set_mode(PlaybackController *controller, PlaybackMode mode);
void playback_seek_to_keyframe(PlaybackController *controller, size_t keyframe_index);
int playback_get_paused(PlaybackController *controller);
void playback_signal_boundary(PlaybackController *controller);
void playback_set_exit_flag(PlaybackController *controller);
int playback_get_exit_flag(PlaybackController *controller);
void playback_signal_all(PlaybackController *controller);

#endif // PLAYBACK_CONTROLLER_H
