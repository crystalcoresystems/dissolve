#ifndef BUFFER_H
#define BUFFER_H

#include <stddef.h>
#include <pthread.h>

#define CHUNK_SIZE 500
#define MAX_CHUNKS 20000

typedef enum {
    DATA_TYPE_INT,
} DataType;

typedef struct Buffer Buffer; // Opaque type

// Function declarations
Buffer *buffer_create(DataType data_type);
void buffer_destroy(Buffer *buffer);
void buffer_write_data(Buffer *buffer, const void *data, size_t size);
int buffer_read_data(Buffer *buffer, void *data, size_t position, size_t size);
size_t buffer_get_total_elements(Buffer *buffer);
void buffer_signal_algorithm_finished(Buffer *buffer);

// Keyframe-related functions
void buffer_mark_keyframe(Buffer *buffer, size_t position);
int buffer_is_keyframe(Buffer *buffer, size_t position);
size_t buffer_get_keyframe_count(Buffer *buffer);
size_t buffer_get_keyframe_position(Buffer *buffer, size_t index);

// Shutdown functions
void buffer_set_shutdown(Buffer *buffer);
int buffer_get_shutdown(Buffer *buffer);
void buffer_signal_all(Buffer *buffer);

#endif // BUFFER_H
