#include "buffer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>

#define SLOT_INDEX(base, offset) (((base) + (offset) + 4) % 4)

typedef enum {
    IO_READ,
    IO_WRITE
} IOOperation;

typedef struct IORequest {
    IOOperation operation;
    void *buffer;
    size_t offset;
    size_t size; // in elements
    int slot;    // For read operations, the slot index
    struct IORequest *next;
} IORequest;

struct Buffer {
    void *read_data[4];              // Holds 4 chunks for reading in memory
    void *write_data;                // Holds 1 chunk for writing in memory
    size_t element_size;             // Size of each element

    // Index tracking
    size_t total_written_chunks;     // Total number of full chunks written to disk
    size_t write_index;              // Current write index in the buffer
    size_t current_read_chunks[4];   // Tracks the chunks currently loaded in memory

    int current_chunk_slot;          // Tracks which of the 4 slots is the current chunk

    // Keyframe bitmap
    unsigned char *keyframe_bitmap;
    size_t keyframe_bitmap_size; // Size in bytes
    // Keyframe positions list
    size_t *keyframe_positions;
    size_t keyframe_count;
    size_t keyframe_capacity;



    pthread_mutex_t mutex;           // Mutex for shared data
    pthread_mutex_t file_mutex;      // Mutex for file access
    pthread_cond_t data_written_cond;// Condition variable for data availability
    pthread_cond_t space_available_cond; // Condition variable for space in write buffer

    pthread_mutex_t io_mutex;        // Mutex for I/O request queue
    pthread_cond_t io_cond;          // Condition variable for I/O thread
    IORequest *io_request_head;      // Head of I/O request queue
    IORequest *io_request_tail;      // Tail of I/O request queue

    pthread_mutex_t read_slot_mutex[4]; // Mutexes for read slots
    pthread_cond_t read_slot_cond[4];   // Condition variables for read slots
    int read_slot_ready[4];             // Flags indicating if data is ready in read slots

    int shutdown;                       // Flag to indicate shutdown
    int algorithm_finished;             // Flag to indicate if the algorithm thread has finished writing data

    char file_name[256];                // File path for the buffer data on disk

    pthread_t io_thread_handle;         // Handle for the I/O thread
};

// Forward declarations
void *io_thread(void *arg);
void write_data_to_disk(Buffer *buffer);
void load_data_into_buffer(Buffer *buffer, int data_index, int slot);

// Function definitions

Buffer *buffer_create(DataType data_type) {
    Buffer *buffer = (Buffer *)malloc(sizeof(Buffer));
    if (!buffer) {
        perror("Failed to allocate Buffer");
        return NULL;
    }

    buffer->total_written_chunks = 0;
    buffer->write_index = 0;
    buffer->current_chunk_slot = 0;
    buffer->io_request_head = NULL;
    buffer->io_request_tail = NULL;
    buffer->shutdown = 0;  // Initialize shutdown flag
    buffer->algorithm_finished = 0; // Initialize algorithm_finished flag


    size_t total_elements = MAX_CHUNKS * CHUNK_SIZE;
    buffer->keyframe_bitmap_size = (total_elements + 7) / 8; // Round up to nearest byte
    buffer->keyframe_bitmap = calloc(buffer->keyframe_bitmap_size, sizeof(unsigned char));
    if (!buffer->keyframe_bitmap) {
        perror("Failed to allocate keyframe bitmap");
        free(buffer);
        return NULL;
    }


    // Initialize keyframe positions list
    buffer->keyframe_capacity = 1024; // Initial capacity
    buffer->keyframe_positions = malloc(buffer->keyframe_capacity * sizeof(size_t));
    if (!buffer->keyframe_positions) {
        perror("Failed to allocate keyframe positions array");
        // Free previously allocated resources
        free(buffer->keyframe_bitmap);
        free(buffer);
        return NULL;
    }
    buffer->keyframe_count = 0;

    strcpy(buffer->file_name, "buffer.bin");

    // Determine data size based on data_type
    switch (data_type) {
        case DATA_TYPE_INT:
            buffer->element_size = sizeof(int);
            break;
        default:
            buffer->element_size = sizeof(char); // Default to char
            break;
    }

    // Initialize read data buffers (4 slots)
    for (int i = 0; i < 4; ++i) {
        buffer->read_data[i] = malloc(CHUNK_SIZE * buffer->element_size);
        buffer->current_read_chunks[i] = -1; // No chunk loaded initially
        buffer->read_slot_ready[i] = 0;
        pthread_mutex_init(&buffer->read_slot_mutex[i], NULL);
        pthread_cond_init(&buffer->read_slot_cond[i], NULL);
    }

    // Initialize write buffer
    buffer->write_data = malloc(CHUNK_SIZE * buffer->element_size);

    // Initialize mutexes and condition variables
    pthread_mutex_init(&buffer->mutex, NULL);
    pthread_mutex_init(&buffer->file_mutex, NULL);
    pthread_cond_init(&buffer->data_written_cond, NULL);
    pthread_cond_init(&buffer->space_available_cond, NULL);
    pthread_mutex_init(&buffer->io_mutex, NULL);
    pthread_cond_init(&buffer->io_cond, NULL);

    // Start the I/O thread
    pthread_create(&buffer->io_thread_handle, NULL, io_thread, buffer);

    return buffer;
}

void buffer_destroy(Buffer *buffer) {
    if (!buffer) return;

    // Signal the I/O thread to shutdown
    pthread_mutex_lock(&buffer->io_mutex);
    buffer->shutdown = 1;
    pthread_cond_signal(&buffer->io_cond);  // Wake up the I/O thread if it's waiting
    pthread_mutex_unlock(&buffer->io_mutex);

    // Wait for the I/O thread to finish
    pthread_join(buffer->io_thread_handle, NULL);

    // Clean up resources
    pthread_mutex_destroy(&buffer->mutex);
    pthread_mutex_destroy(&buffer->file_mutex);
    pthread_cond_destroy(&buffer->data_written_cond);
    pthread_cond_destroy(&buffer->space_available_cond);
    pthread_mutex_destroy(&buffer->io_mutex);
    pthread_cond_destroy(&buffer->io_cond);
    for (int i = 0; i < 4; ++i) {
        pthread_mutex_destroy(&buffer->read_slot_mutex[i]);
        pthread_cond_destroy(&buffer->read_slot_cond[i]);
        free(buffer->read_data[i]);
    }
    free(buffer->write_data);

    if (buffer->keyframe_bitmap) {
        free(buffer->keyframe_bitmap);
    }

    if (buffer->keyframe_positions) {
        free(buffer->keyframe_positions);
    }

    free(buffer);
}

void buffer_write_data(Buffer *buffer, const void *data, size_t size) {
    pthread_mutex_lock(&buffer->mutex);

    while (size > 0) {
        // Calculate how much space is left in the write buffer
        size_t space_left = CHUNK_SIZE - buffer->write_index;
        size_t elements_to_write = (size < space_left) ? size : space_left;

        // Copy data into the write buffer
        memcpy((char *)buffer->write_data + buffer->write_index * buffer->element_size, data, elements_to_write * buffer->element_size);
        buffer->write_index += elements_to_write;

        // If write buffer is full, write to disk
        if (buffer->write_index == CHUNK_SIZE) {
            pthread_mutex_unlock(&buffer->mutex);
            write_data_to_disk(buffer);
            pthread_mutex_lock(&buffer->mutex);
        }

        // Update data pointers and sizes
        data = (const char *)data + elements_to_write * buffer->element_size;
        size -= elements_to_write;
    }

    // Signal that new data is available
    pthread_cond_broadcast(&buffer->data_written_cond);

    pthread_mutex_unlock(&buffer->mutex);
}

int buffer_read_data(Buffer *buffer, void *data, size_t position, size_t size) {
    size_t total_elements;
    size_t elements_read = 0;
    size_t elements_to_read;
    size_t write_buffer_start;
    size_t write_buffer_end;

    while (elements_read < size) {
        pthread_mutex_lock(&buffer->mutex);

        total_elements = buffer->total_written_chunks * CHUNK_SIZE + buffer->write_index;

        // Wait until the requested data is available
        while (position >= total_elements) {
            if (buffer->algorithm_finished || buffer->shutdown) {
                pthread_mutex_unlock(&buffer->mutex);
                printf("No more data available to read.\n");
                return -1; // No more data available
            }
            pthread_cond_wait(&buffer->data_written_cond, &buffer->mutex);
            total_elements = buffer->total_written_chunks * CHUNK_SIZE + buffer->write_index;
        }

        write_buffer_start = buffer->total_written_chunks * CHUNK_SIZE;
        write_buffer_end = write_buffer_start + buffer->write_index;

        if (position >= write_buffer_start) {
            // Read from write buffer
            elements_to_read = size - elements_read;
            size_t available_elements = write_buffer_end - position;
            if (elements_to_read > available_elements) {
                elements_to_read = available_elements;
            }

            memcpy(data, (char *)buffer->write_data + (position - write_buffer_start) * buffer->element_size,
                   elements_to_read * buffer->element_size);

            position += elements_to_read;
            data = (char *)data + elements_to_read * buffer->element_size;
            elements_read += elements_to_read;

            pthread_mutex_unlock(&buffer->mutex);
        } else {
            // Read from disk-loaded chunks
            int current_chunk = position / CHUNK_SIZE;
            int index_in_chunk = position % CHUNK_SIZE;
            int current_slot = -1;

            // Find which slot holds the current chunk
            for (int i = 0; i < 4; ++i) {
                if (buffer->current_read_chunks[i] == current_chunk) {
                    current_slot = i;
                    break;
                }
            }

            // If chunk is not loaded, load it
            if (current_slot == -1) {
                current_slot = SLOT_INDEX(buffer->current_chunk_slot, 1); // Use next slot
                buffer->current_read_chunks[current_slot] = current_chunk;
                load_data_into_buffer(buffer, current_chunk * CHUNK_SIZE, current_slot);
            }

            // Wait until the current chunk is loaded into memory
            pthread_mutex_lock(&buffer->read_slot_mutex[current_slot]);
            while (!buffer->read_slot_ready[current_slot]) {
                pthread_mutex_unlock(&buffer->mutex);
                pthread_cond_wait(&buffer->read_slot_cond[current_slot], &buffer->read_slot_mutex[current_slot]);
                pthread_mutex_lock(&buffer->mutex);
            }
            pthread_mutex_unlock(&buffer->read_slot_mutex[current_slot]);

            // Read data from the buffer
            size_t elements_in_chunk = CHUNK_SIZE - index_in_chunk;
            elements_to_read = size - elements_read;
            if (elements_to_read > elements_in_chunk) {
                elements_to_read = elements_in_chunk;
            }

            memcpy(data, (char *)buffer->read_data[current_slot] + index_in_chunk * buffer->element_size,
                   elements_to_read * buffer->element_size);

            position += elements_to_read;
            data = (char *)data + elements_to_read * buffer->element_size;
            elements_read += elements_to_read;

            pthread_mutex_unlock(&buffer->mutex);
        }
    }

    return 0; // Success
}

size_t buffer_get_total_elements(Buffer *buffer) {
    pthread_mutex_lock(&buffer->mutex);
    size_t total_elements = buffer->total_written_chunks * CHUNK_SIZE + buffer->write_index;
    pthread_mutex_unlock(&buffer->mutex);
    return total_elements;
}

void buffer_signal_algorithm_finished(Buffer *buffer) {
    pthread_mutex_lock(&buffer->mutex);
    buffer->algorithm_finished = 1;
    pthread_cond_broadcast(&buffer->data_written_cond);
    pthread_mutex_unlock(&buffer->mutex);
}

void buffer_set_shutdown(Buffer *buffer) {
    pthread_mutex_lock(&buffer->mutex);
    buffer->shutdown = 1;
    pthread_mutex_unlock(&buffer->mutex);
}

int buffer_get_shutdown(Buffer *buffer) {
    pthread_mutex_lock(&buffer->mutex);
    int shutdown = buffer->shutdown;
    pthread_mutex_unlock(&buffer->mutex);
    return shutdown;
}

void buffer_signal_all(Buffer *buffer) {
    pthread_mutex_lock(&buffer->mutex);
    pthread_cond_broadcast(&buffer->data_written_cond);
    pthread_cond_broadcast(&buffer->space_available_cond);
    pthread_mutex_unlock(&buffer->mutex);

    pthread_mutex_lock(&buffer->io_mutex);
    pthread_cond_broadcast(&buffer->io_cond);
    pthread_mutex_unlock(&buffer->io_mutex);

    for (int i = 0; i < 4; ++i) {
        pthread_mutex_lock(&buffer->read_slot_mutex[i]);
        pthread_cond_broadcast(&buffer->read_slot_cond[i]);
        pthread_mutex_unlock(&buffer->read_slot_mutex[i]);
    }
}

void buffer_mark_keyframe(Buffer *buffer, size_t position) {
    pthread_mutex_lock(&buffer->mutex);
    size_t byte_index = position / 8;
    size_t bit_index = position % 8;
    if (byte_index < buffer->keyframe_bitmap_size) {
        buffer->keyframe_bitmap[byte_index] |= (1 << bit_index);

        // Add position to keyframe_positions list
        if (buffer->keyframe_count == buffer->keyframe_capacity) {
            // Resize the array
            size_t new_capacity = buffer->keyframe_capacity * 2;
            size_t *new_array = realloc(buffer->keyframe_positions, new_capacity * sizeof(size_t));
            if (new_array) {
                buffer->keyframe_positions = new_array;
                buffer->keyframe_capacity = new_capacity;
            } else {
                // Handle allocation failure (optional: log an error)
                pthread_mutex_unlock(&buffer->mutex);
                return;
            }
        }
        buffer->keyframe_positions[buffer->keyframe_count++] = position;
    }
    pthread_mutex_unlock(&buffer->mutex);
}

int buffer_is_keyframe(Buffer *buffer, size_t position) {
    pthread_mutex_lock(&buffer->mutex);
    size_t byte_index = position / 8;
    size_t bit_index = position % 8;
    int is_keyframe = 0;
    if (byte_index < buffer->keyframe_bitmap_size) {
        is_keyframe = (buffer->keyframe_bitmap[byte_index] & (1 << bit_index)) != 0;
    }
    pthread_mutex_unlock(&buffer->mutex);
    return is_keyframe;
}


size_t buffer_get_keyframe_count(Buffer *buffer) {
    pthread_mutex_lock(&buffer->mutex);
    size_t count = buffer->keyframe_count;
    pthread_mutex_unlock(&buffer->mutex);
    return count;
}

size_t buffer_get_keyframe_position(Buffer *buffer, size_t index) {
    pthread_mutex_lock(&buffer->mutex);
    size_t position = 0;
    if (index < buffer->keyframe_count) {
        position = buffer->keyframe_positions[index];
    } else {
        // Handle out-of-bounds index (optional: set position to an invalid value)
        position = (size_t)-1;
    }
    pthread_mutex_unlock(&buffer->mutex);
    return position;
}



// The io_thread and other functions remain the same as in the previous code,
// ensuring they manage synchronization internally without exposing internals.

void write_data_to_disk(Buffer *buffer) {
    // Create a new IORequest
    IORequest *request = malloc(sizeof(IORequest));
    request->operation = IO_WRITE;
    request->size = buffer->write_index;
    request->offset = buffer->total_written_chunks * CHUNK_SIZE * buffer->element_size;
    // Allocate a buffer and copy data
    request->buffer = malloc(request->size * buffer->element_size);
    memcpy(request->buffer, buffer->write_data, request->size * buffer->element_size);
    request->next = NULL;

    // Enqueue the request
    pthread_mutex_lock(&buffer->io_mutex);
    if (buffer->io_request_tail == NULL) {
        buffer->io_request_head = request;
        buffer->io_request_tail = request;
    } else {
        buffer->io_request_tail->next = request;
        buffer->io_request_tail = request;
    }
    pthread_cond_signal(&buffer->io_cond);
    pthread_mutex_unlock(&buffer->io_mutex);
}

void load_data_into_buffer(Buffer *buffer, int data_index, int slot) {
    // Create a new IORequest
    IORequest *request = malloc(sizeof(IORequest));
    request->operation = IO_READ;
    request->size = CHUNK_SIZE;
    request->offset = data_index * buffer->element_size;
    request->buffer = buffer->read_data[slot]; // Buffer to read into
    request->slot = slot;
    request->next = NULL;

    // Set the slot as not ready
    pthread_mutex_lock(&buffer->read_slot_mutex[slot]);
    buffer->read_slot_ready[slot] = 0;
    pthread_mutex_unlock(&buffer->read_slot_mutex[slot]);

    // Enqueue the request
    pthread_mutex_lock(&buffer->io_mutex);
    if (buffer->io_request_tail == NULL) {
        buffer->io_request_head = request;
        buffer->io_request_tail = request;
    } else {
        buffer->io_request_tail->next = request;
        buffer->io_request_tail = request;
    }
    pthread_cond_signal(&buffer->io_cond);
    pthread_mutex_unlock(&buffer->io_mutex);
}

void *io_thread(void *arg) {
    Buffer *buffer = (Buffer *)arg;
    while (1) {
        pthread_mutex_lock(&buffer->io_mutex);
        // Wait until there is an I/O request or shutdown is signaled
        while (buffer->io_request_head == NULL && !buffer->shutdown) {
            pthread_cond_wait(&buffer->io_cond, &buffer->io_mutex);
        }
        // Check if shutdown is signaled and no pending I/O requests
        if (buffer->shutdown && buffer->io_request_head == NULL) {
            pthread_mutex_unlock(&buffer->io_mutex);
            break;  // Exit the loop to terminate the thread gracefully
        }

        // Dequeue the next I/O request
        IORequest *request = buffer->io_request_head;
        buffer->io_request_head = request->next;
        if (buffer->io_request_head == NULL) {
            buffer->io_request_tail = NULL;
        }
        pthread_mutex_unlock(&buffer->io_mutex);

        if (request->operation == IO_READ) {
            // Perform the read
            pthread_mutex_lock(&buffer->file_mutex);
            FILE *file = fopen(buffer->file_name, "rb");
            if (file == NULL) {
                if (errno == ENOENT) {
                    // File does not exist yet, fill buffer with zeros or appropriate default values
                    memset(request->buffer, 0, request->size * buffer->element_size);
                    // Optional: printf("File does not exist yet. Filling buffer with zeros.\n");
                } else {
                    perror("Error opening file for reading");
                }
            } else {
                if (fseek(file, request->offset, SEEK_SET) != 0) {
                    perror("Error seeking in file");
                } else {
                    size_t elements_read = fread(request->buffer, buffer->element_size, request->size, file);
                    if (elements_read != request->size) {
                        perror("Error reading data chunk from file");
                    }
                }
                fclose(file);
            }
            pthread_mutex_unlock(&buffer->file_mutex);
            // Signal that the data is ready
            int slot = request->slot;
            pthread_mutex_lock(&buffer->read_slot_mutex[slot]);
            buffer->read_slot_ready[slot] = 1;
            pthread_cond_signal(&buffer->read_slot_cond[slot]);
            pthread_mutex_unlock(&buffer->read_slot_mutex[slot]);
        } else if (request->operation == IO_WRITE) {
            // Perform the write
            pthread_mutex_lock(&buffer->file_mutex);
            FILE *file = fopen(buffer->file_name, "ab");
            if (file == NULL) {
                perror("Error opening file for writing");
            } else {
                size_t elements_written = fwrite(request->buffer, buffer->element_size, request->size, file);
                if (elements_written != request->size) {
                    perror("Error writing data chunk to file");
                } else {
                    fflush(file);
                }
                fclose(file);
            }
            pthread_mutex_unlock(&buffer->file_mutex);
            // Free the buffer allocated in the request
            free(request->buffer);
            // Update total_written_chunks if we wrote a full chunk
            pthread_mutex_lock(&buffer->mutex);
            if (request->size == CHUNK_SIZE) {
                buffer->write_index = 0;
                buffer->total_written_chunks++;
                pthread_cond_broadcast(&buffer->space_available_cond);
            }
            pthread_cond_broadcast(&buffer->data_written_cond);
            pthread_mutex_unlock(&buffer->mutex);
        }
        // Free the request
        free(request);
    }
    return NULL;
}

