#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "buffer.h"
#include "playback_controller.h"
#include "input_handler.h" // Include the input handler header

// Function prototypes
void *algorithm_thread(void *arg);
void *rendering_thread(void *arg);
int rendering_step(Buffer *buffer, size_t position);
int is_prime(int n);

int main() {
    // Create buffer
    Buffer *buffer = buffer_create(DATA_TYPE_INT);
    if (!buffer) {
        fprintf(stderr, "Failed to create buffer\n");
        return EXIT_FAILURE;
    }

    // Initialize playback controller
    PlaybackController playback;
    playback_init(&playback, buffer);

    // Start the algorithm thread
    pthread_t alg_thread;
    pthread_create(&alg_thread, NULL, algorithm_thread, buffer);

    // Start rendering thread
    pthread_t rendering_thread_handle;
    pthread_create(&rendering_thread_handle, NULL, rendering_thread, &playback);

    // Start user input thread
    pthread_t input_thread_handle;
    pthread_create(&input_thread_handle, NULL, input_thread, &playback);

    // Wait for rendering thread to finish
    pthread_join(rendering_thread_handle, NULL);

    // Wait for input thread to finish
    pthread_join(input_thread_handle, NULL);

    // Wait for algorithm thread to finish
    pthread_join(alg_thread, NULL);

    // Destroy buffer
    buffer_destroy(buffer);

    return 0;
}

void *algorithm_thread(void *arg) {
    Buffer *buffer = (Buffer *)arg;

    for (size_t i = 0; i < MAX_CHUNKS * CHUNK_SIZE; ++i) {
        // Check if shutdown flag is set
        if (buffer_get_shutdown(buffer)) {
            printf("Algorithm thread received shutdown signal.\n");
            break;
        }

        int data = (int)i;
        buffer_write_data(buffer, &data, 1);

        // Mark as keyframe if data is prime
        if (is_prime(data)) {
            buffer_mark_keyframe(buffer, i);
        }

        // Simulate some delay
        usleep(1000);
    }

    // Signal that the algorithm thread has finished
    buffer_signal_algorithm_finished(buffer);

    return NULL;
}

int rendering_step(Buffer *buffer, size_t position) {
    int data;
    int ret = buffer_read_data(buffer, &data, position, 1);
    if (ret == 0) {
        printf("Rendering data at position %lu: %d\n", position, data);
        return 0;
    } else {
        printf("No more data to render at position %lu.\n", position);
        return -1;
    }
}

void *rendering_thread(void *arg) {
    PlaybackController *controller = (PlaybackController *)arg;
    Buffer *buffer = controller->buffer;

    while (1) {
        // Check if shutdown flag is set
        if (buffer_get_shutdown(buffer)) {
            printf("Rendering thread received shutdown signal.\n");
            break;
        }

        pthread_mutex_lock(&controller->mutex);

        // Wait if playback is paused
        while (controller->paused) {
            pthread_cond_wait(&controller->cond, &controller->mutex);
        }

        // Get current playback state
        size_t position = controller->position;
        int direction = controller->direction;
        double speed = controller->speed;
        PlaybackMode mode = controller->mode;

        pthread_mutex_unlock(&controller->mutex);

        // Skip non-keyframes if in keyframe mode
        if (mode == PLAYBACK_MODE_KEYFRAME) {
            int is_keyframe = buffer_is_keyframe(buffer, position);
            while (!is_keyframe) {
                // Update position based on direction
                if (direction == PLAYBACK_FORWARD) {
                    position++;
                } else {
                    if (position > 0) {
                        position--;
                    } else {
                        // Reached the beginning
                        break;
                    }
                }

                // Check if position is beyond total elements
                size_t total_elements = buffer_get_total_elements(buffer);
                if (position >= total_elements) {
                    break; // No more data
                }

                is_keyframe = buffer_is_keyframe(buffer, position);
            }
        }

        // Check if we have reached the boundary
        int reached_boundary = 0;
        size_t total_elements = buffer_get_total_elements(buffer);
        if ((direction == PLAYBACK_FORWARD && position >= total_elements) ||
            (direction == PLAYBACK_REVERSE && position == 0)) {
            reached_boundary = 1;
        }

        if (reached_boundary) {
            // Pause at boundary and wait for direction change
            pthread_mutex_lock(&controller->mutex);
            controller->at_boundary = 1;
            while (controller->direction == direction) {
                printf("Playback reached boundary at position %lu. Waiting for direction change.\n", position);
                pthread_cond_wait(&controller->boundary_cond, &controller->mutex);
            }
            controller->at_boundary = 0;
            // Update direction after change
            direction = controller->direction;
            pthread_mutex_unlock(&controller->mutex);

            // Update position to move away from boundary
            if (direction == PLAYBACK_FORWARD && position < total_elements - 1) {
                position++;
            } else if (direction == PLAYBACK_REVERSE && position > 0) {
                position--;
            } else {
                // Cannot move further, exit the thread
                break;
            }
        }

        // Perform rendering step
        int ret = rendering_step(buffer, position);
        if (ret == -1) {
            // No more data to render
            break;
        }

        // Update playback position
        pthread_mutex_lock(&controller->mutex);
        controller->position = position;
        if (direction == PLAYBACK_FORWARD) {
            controller->position++;
        } else {
            if (controller->position > 0) {
                controller->position--;
            } else {
                // Reached the beginning, will handle in the next loop iteration
            }
        }
        pthread_mutex_unlock(&controller->mutex);

        // Adjust delay based on speed
        double delay = 50000 / speed;
        usleep((useconds_t)delay);

    }

    printf("Rendering thread has completed playback.\n");
    return NULL;
}

int is_prime(int n) {
    if (n <= 1) return 0;
    if (n <= 3) return 1;
    if (n % 2 == 0 || n % 3 == 0) return 0;
    for (int i = 5; i * i <= n; i += 6) {
        if (n % i == 0 || n % (i + 2) == 0) return 0;
    }
    return 1;
}
