#include "input_handler.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>
#include <stdlib.h>

// Function to configure terminal for non-canonical input
void configure_terminal(struct termios *original_termios) {
    struct termios new_termios;

    // Get current terminal attributes
    tcgetattr(STDIN_FILENO, original_termios);

    // Make a copy and modify it
    new_termios = *original_termios;

    // Disable canonical mode and echo
    new_termios.c_lflag &= ~(ICANON | ECHO);

    // Set minimum number of input bytes read
    new_termios.c_cc[VMIN] = 1;
    new_termios.c_cc[VTIME] = 0;

    // Apply the new settings
    tcsetattr(STDIN_FILENO, TCSANOW, &new_termios);
}

// Function to restore terminal settings
void restore_terminal(struct termios *original_termios) {
    // Restore the original terminal settings
    tcsetattr(STDIN_FILENO, TCSANOW, original_termios);
}

void *input_thread(void *arg) {
    PlaybackController *controller = (PlaybackController *)arg;
    struct termios original_termios;

    // Configure terminal
    configure_terminal(&original_termios);

    int running = 1;
    while (running) {
        char ch;
        int ret = read(STDIN_FILENO, &ch, 1);
        if (ret < 0) {
            perror("Error reading from stdin");
            break;
        }

        // Handle single-key commands
        switch (ch) {
            case 'p':  // Pause/Resume
                if (playback_get_paused(controller)) {
                    playback_resume(controller);
                    printf("Playback resumed.\n");
                } else {
                    playback_pause(controller);
                    printf("Playback paused.\n");
                }
                break;
            case 'f':  // Step forward
                pthread_mutex_lock(&controller->mutex);
                if (controller->paused) {
                    size_t total_elements = buffer_get_total_elements(controller->buffer);
                    if (controller->position < total_elements - 1) {
                        controller->position++;
                        printf("Stepped forward to position %lu.\n", controller->position);
                    } else {
                        printf("Already at the end of buffer.\n");
                    }
                } else {
                    printf("Cannot step forward while playback is running. Pause first.\n");
                }
                pthread_mutex_unlock(&controller->mutex);
                break;
            case 'b':  // Step backward
                pthread_mutex_lock(&controller->mutex);
                if (controller->paused) {
                    if (controller->position > 0) {
                        controller->position--;
                        printf("Stepped backward to position %lu.\n", controller->position);
                    } else {
                        printf("Already at the beginning of buffer.\n");
                    }
                } else {
                    printf("Cannot step backward while playback is running. Pause first.\n");
                }
                pthread_mutex_unlock(&controller->mutex);
                break;
            case 'r':  // Reverse playback direction
                playback_reverse(controller);
                printf("Playback direction reversed.\n");
                break;
            case 'k':  // Toggle keyframe mode
                pthread_mutex_lock(&controller->mutex);
                if (controller->mode == PLAYBACK_MODE_NORMAL) {
                    controller->mode = PLAYBACK_MODE_KEYFRAME;
                    printf("Switched to keyframe mode.\n");
                } else {
                    controller->mode = PLAYBACK_MODE_NORMAL;
                    printf("Switched to normal mode.\n");
                }
                pthread_mutex_unlock(&controller->mutex);
                break;
            case 's':  // Seek
                pthread_mutex_lock(&controller->mutex);
                if (controller->paused) {
                    pthread_mutex_unlock(&controller->mutex);
                    restore_terminal(&original_termios);
                    handle_seek(controller, 0);  // 0 indicates normal seek
                    configure_terminal(&original_termios);
                } else {
                    printf("Cannot seek while playback is running. Pause first.\n");
                    pthread_mutex_unlock(&controller->mutex);
                }
                break;
            case 'S':  // Seek keyframe
                pthread_mutex_lock(&controller->mutex);
                if (controller->paused) {
                    pthread_mutex_unlock(&controller->mutex);
                    restore_terminal(&original_termios);
                    handle_seek(controller, 1);  // 1 indicates keyframe seek
                    configure_terminal(&original_termios);
                } else {
                    printf("Cannot seek while playback is running. Pause first.\n");
                    pthread_mutex_unlock(&controller->mutex);
                }
                break;

            case 'q':  // Quit
                printf("Exiting program.\n");
                running = 0;
                playback_set_exit_flag(controller);
                // Set the shutdown flag in the buffer
                buffer_set_shutdown(controller->buffer);
                // Signal all condition variables to wake up threads
                playback_signal_all(controller);
                buffer_signal_all(controller->buffer);
                break;
            default:
                printf("Unknown command: %c\n", ch);
                break;
        }
    }

    // Restore terminal settings before exiting
    restore_terminal(&original_termios);

    return NULL;
}

void handle_seek(PlaybackController *controller, int keyframe_mode) {
    size_t position = 0;

    if (keyframe_mode) {
        // Seek to keyframe
        size_t keyframe_count = buffer_get_keyframe_count(controller->buffer);
        if (keyframe_count == 0) {
            printf("No keyframes available.\n");
            return;
        }

        printf("Available keyframes: 0 to %lu\n", keyframe_count - 1);
        printf("Enter keyframe index to seek to: ");
        fflush(stdout);

        size_t index;
        if (scanf("%lu", &index) != 1) {
            printf("Invalid input.\n");
            return;
        }

        if (index >= keyframe_count) {
            printf("Invalid keyframe index.\n");
            return;
        }

        position = buffer_get_keyframe_position(controller->buffer, index);
        if (position == (size_t)-1) {
            printf("Error retrieving keyframe position.\n");
            return;
        }
    } else {
        // Normal seek
        size_t total_elements = buffer_get_total_elements(controller->buffer);
        if (total_elements == 0) {
            printf("Buffer is empty.\n");
            return;
        }

        printf("Available positions: 0 to %lu\n", total_elements - 1);
        printf("Enter position to seek to: ");
        fflush(stdout);

        if (scanf("%lu", &position) != 1) {
            printf("Invalid input.\n");
            return;
        }

        if (position >= total_elements) {
            printf("Invalid position.\n");
            return;
        }
    }

    // Seek to the position
    pthread_mutex_lock(&controller->mutex);
    controller->position = position;
    printf("Seeked to position %lu.\n", position);
    pthread_mutex_unlock(&controller->mutex);
}
