#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include <termios.h>
#include "playback_controller.h"

// Function prototypes
void *input_thread(void *arg);
void configure_terminal(struct termios *original_termios);
void restore_terminal(struct termios *original_termios);
void handle_seek(PlaybackController *controller, int keyframe_mode);

#endif // INPUT_HANDLER_H
