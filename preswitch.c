/************************************/
/*                                  */
/*            EMI94.c               */
/*                                  */
/* Execute MARS Instruction ala     */
/* ICWS'94 Draft Standard.          */
/*                                  */
/* Last Update: November 8, 1995    */
/*                                  */
/************************************/

/* This ANSI C function is the benchmark MARS instruction   */
/* interpreter for the ICWS'94 Draft Standard.              */


/* The design philosophy of this function is to mirror the  */
/* standard as closely as possible, illuminate the meaning  */
/* of the standard, and provide the definitive answers to   */
/* questions of the "well, does the standard mean this or   */
/* that?" variety.  Although other, different implemen-     */
/* tations are definitely possible and encouraged; those    */
/* implementations should produce the same results as this  */
/* one does.                                                */


/* The function returns the state of the system.  What the  */
/* main program does with this information is not defined   */
/* by the standard.                                         */

enum SystemState {
        UNDEFINED,
        SUCCESS
};


typedef unsigned int Warrior;
typedef unsigned int Address;
extern void Queue(
                Warrior  W,
                Address  TaskPointer
                );


static Address Fold(
                Address  pointer,    /* The pointer to fold into the desired range.  */
                Address  limit,      /* The range limit.                             */
                Address  M           /* The size of Core.                            */
                ) {
        Address  result;
        result = pointer % limit;
        if ( result > (limit/2) ) {
                result += M - limit;
        };
        return(result);
}


/* Instructions are the principle data type.  Core is an    */
/* array of instructions, and there are three instruction   */
/* registers used by the MARS executive.                    */
enum Opcode {
        DAT,
        MOV,
        ADD,
        SUB,
        MUL,
        DIV,
        MOD,
        JMP,
        JMZ,
        JMN,
        DJN,
        CMP, /* aka SEQ */
        SNE,
        SLT,
        SPL,
        NOP,
};

enum Modifier {
        A,
        B,
        AB,
        BA,
        F,
        X,
        I
};

enum Mode {
        IMMEDIATE,
        DIRECT,
        A_INDIRECT,
        B_INDIRECT,
        A_DECREMENT,
        B_DECREMENT,
        A_INCREMENT,
        B_INCREMENT,
};

typedef struct Instruction {
        enum Opcode    Opcode;
        enum Modifier  Modifier;
        enum Mode      AMode;
        Address        ANumber;
        enum Mode      BMode;
        Address        BNumber;
} Instruction;


/* The function is passed which warrior is currently        */
/* executing, the address of the warrior's current task's   */
/* current instruction, a pointer to the Core, the size of  */
/* the Core, and the read and write limits.  It returns the */
/* system's state after attempting instruction execution.   */
enum SystemState EMI94(

                /* W indicates which warrior's code is executing.           */
                Warrior  W,

                /* PC is the address of this warrior's current task's       */
                /* current instruction.                                     */

                Address  PC,
                Instruction Core[],
                Address     M,
                ReadLimit,

                Address     WriteLimit


                ) {

        Instruction IR;
        Instruction IRA;
        Instruction IRB;

        Address     RPA;
        Address     WPA;
        Address     RPB;
        Address     WPB;
        Address     PIP;
        IR = Core[PC];

        if (IR.AMode == IMMEDIATE) {
                RPA = WPA = 0;
        } else {

                RPA = Fold(IR.ANumber, ReadLimit, M);
                WPA = Fold(IR.ANumber, WriteLimit, M);

                if (IR.AMode == A_INDIRECT
                                || IR.AMode == A_DECREMENT
                                || IR.AMode == A_INCREMENT) {
                        if (IR.AMode == A_DECREMENT) {
                                Core[((PC + WPA) % M)].ANumber =
                                        (Core[((PC + WPA) % M)].ANumber + M - 1) % M;
                        };

                        if (IR.AMode == A_INCREMENT) {
                                PIP = (PC + WPA) % M;
                        };


                        RPA = Fold(
                                        (RPA + Core[((PC + RPA) % M)].ANumber), ReadLimit, M
                                  );
                        WPA = Fold(
                                        (WPA + Core[((PC + WPA) % M)].ANumber), WriteLimit, M
                                  );
                };

                /* For instructions with B-indirection in the A-operand     */
                /* (B-number Indirect, B-number Predecrement,               */
                /* and B-number Postincrement A-modes):                     */

                if (IR.AMode == B_INDIRECT
                                || IR.AMode == B_DECREMENT
                                || IR.AMode == B_INCREMENT) {


                        if (IR.AMode == DECREMENT) {
                                Core[((PC + WPA) % M)].BNumber =
                                        (Core[((PC + WPA) % M)].BNumber + M - 1) % M;
                        };

                        if (IR.AMode == INCREMENT) {
                                PIP = (PC + WPA) % M;
                        };
                        RPA = Fold(
                                        (RPA + Core[((PC + RPA) % M)].BNumber), ReadLimit, M
                                  );
                        WPA = Fold(
                                        (WPA + Core[((PC + WPA) % M)].BNumber), WriteLimit, M
                                  );
                };
        };

        IRA = Core[((PC + RPA) % M)];


        if (IR.AMode == A_INCREMENT) {
                Core[PIP].ANumber = (Core[PIP].ANumber + 1) % M;
        }
        else if (IR.AMode == B_INCREMENT) {
                Core[PIP].BNumber = (Core[PIP].BNumber + 1) % M;
        };

        if (IR.BMode == IMMEDIATE) {
                RPB = WPB = 0;
        } else {
                RPB = Fold(IR.BNumber, ReadLimit, M);
                WPB = Fold(IR.BNumber, WriteLimit, M);
                if (IR.BMode == A_INDIRECT
                                || IR.BMode == A_DECREMENT
                                || IR.BMode == A_INCREMENT) {
                        if (IR.BMode == A_DECREMENT) {
                                Core[((PC + WPB) % M)].ANumber =
                                        (Core[((PC + WPB) % M)].ANumber + M - 1) % M
                                        ;
                        } else if (IR.BMode == A_INCREMENT) {
                                PIP = (PC + WPB) % M;
                        };
                        RPB = Fold(
                                        (RPB + Core[((PC + RPB) % M)].ANumber), ReadLimit, M
                                  );
                        WPB = Fold(
                                        (WPB + Core[((PC + WPB) % M)].ANumber), WriteLimit, M
                                  );
                };
                if (IR.BMode == B_INDIRECT
                                || IR.BMode == B_DECREMENT
                                || IR.BMode == B_INCREMENT) {
                        if (IR.BMode == B_DECREMENT) {
                                Core[((PC + WPB) % M)].BNumber =
                                        (Core[((PC + WPB) % M)].BNumber + M - 1) % M
                                        ;
                        } else if (IR.BMode == B_INCREMENT) {
                                PIP = (PC + WPB) % M;
                        };
                        RPB = Fold(
                                        (RPB + Core[((PC + RPB) % M)].BNumber), ReadLimit, M
                                  );
                        WPB = Fold(
                                        (WPB + Core[((PC + WPB) % M)].BNumber), WriteLimit, M
                                  );
                };
        };
        IRB = Core[((PC + RPB) % M)];
        if (IR.BMode == A_INCREMENT) {
                Core[PIP].ANumber = (Core[PIP].ANumber + 1) % M;
        }
        else if (IR.BMode == INCREMENT) {
                Core[PIP].BNumber = (Core[PIP].BNumber + 1) % M;
        };
