# Experiment 4
#
# idea - Test a technique when the datapoint modifies the key
#        as it goes through (experiment 4)
# 
# Data ( 1, 0 or 8) ->
#
#        -------------------------------------------------
#        | 0 | 1 | 8 | 1 | 1 | 8 | 0 | 1 | 8 | 0 | 1 | 8 | Key1 ->
#        -------------------------------------------------
#
# (1, 0 or 8) ->
#
#        -------------------------------------------------
#        | 1 | 0 | 1 | 1 | 1 | 1 | 8 | 0 | 1 | 1 | 1 | 1 | Key2 ->
#        -------------------------------------------------
#
# (1, 0 or 8) ->
#
#        -------------------------------------------------
#        | 1 | 0 | 1 | 8 | 8 | 8 | 1 | 0 | 1 | 1 | 8 | 1 | Key1 (modified) ->
#        -------------------------------------------------
#
# (1, 0 or 8) ->
#
#        -------------------------------------------------
#        | 8 | 1 | 0 | 8 | 8 | 0 | 1 | 1 | 8 | 0 | 1 | 1 | Key2 (modified) ->
#        -------------------------------------------------
#
# result (1, 0 or 8)
#
# Hypthoesis - Key scrambling without modifying the key itself will 
#              lead to data loss

# Conclusion - only 2 types of cell are recoverable, pattern recognition
#              in transmissions is possible

# Define cell types used in automata
use constant ONE => 1;
use constant THETA => 8;
use constant ZERO => 0;

# Create an array of all cell types
@a = (ONE, THETA, ZERO);
# Loop through all numbers 0 - 15 and use them as permutations
# by converting to binary "nibble" and passing to solve function
@key1a= (ONE, THETA, ZERO, ZERO, THETA, ONE);
@key2a= ( THETA, ONE, ONE, ZERO, ZERO, THETA, THETA);
@key3a= ( ONE, ONE, ZERO, ONE, THETA, ZERO);
@key4a= ( ZERO, ZERO, ZERO, ONE, THETA, ZERO);
@key5a= ( ZERO, ONE, ONE, ONE, THETA, ZERO);
@key6a= ( ONE, ZERO);
@key7a= ( THETA, ONE, THETA);
@key8a= ( ONE, ZERO, THETA, THETA, ONE);
@key10a= ( THETA, ONE, ONE, ZERO);
@key11a= ( ONE, ZERO, ZERO);
@key12a= (THETA, ONE, ONE, THETA, ZERO);
@key12a= (1,0,8,8,0,1,8);
@key14a= (1,8,8,8,8,0,1,1,0,1,0);
@key15a= (1,8,0,0,8,1,1);
@key16a= (8,1,8,0,0,8,1);
@key1b= (2,1,0,0,8,8,1,1,0);
@key2b= (2,0,8,0,1,0,0);
@key3b= (2,1,1,0,1,8,8,1,0);
@key4b= (2,0,1,1);
@key5b= (2,8,0,1,1,0,0,8,1);
@key6b= (2,8,1,0,1,0,8,1,0);
@key7b= (2,8,1,0,1,8,0,1,1);
@key8b= (2,1,1,0,8,1,8,1,0);
@key9b= (2,0,8,0,1,8,1,0,1,8);
@key10b= (2,8,0);
@key11b= (2,1,0,1,8);
@key12b= (2,8,0,8,1);
@key13b= (2,1,0,8,1,0,8);
@key14b= (2,1,8,8,1,0,0);
@key15b= (2,8,0,8,8,1,0,1);
@key16b= (2,8,1,1,0,8,0);

for my $i (0..15) {
        my @b = convert2nib($i);
        print @b, "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key1a, @key1b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key2a, @key2b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key3a, @key3b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key4a, @key4b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key5a, @key5b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key6a, @key6b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key7a, @key7b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key8a, @key8b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key9a, @key9b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key10a, @key10b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key11a, @key11b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key12a, @key12b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key13a, @key13b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key14a, @key14b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key15a, @key15b);
        }
        print "\n";
        foreach my $trinity (@a) {
                solve($trinity, @b, @key16a, @key16b);
        }
        print "\n";
        print "\n\n";
}

sub solve {#../
        my @k1 = (), @k2 = ();
        my $t = shift;
        my $a1 = shift;
        my $b1 = shift;
        my $a2 = shift;
        my $b2 = shift;
        my $i = 0;
        foreach (@_) {
                $i++;
                $_ == 2 and last;
                push @k1, $_;
        }

        for (;$i < scalar(@_); $i++) {
                push @k2, @_[$i];
        }

        print $t, " : ";
        # Use binary permutation to test (a's and b's are either 1 or 0)
        # Pass the trinity through 2 keys in order k1 k2 k1 k2
        $t = $a1? LDE($t, @k1) : RPS($t, @k1); 
        print $t, " : ";
        $t = $b1? LDE($t, @k2) : RPS($t, @k2); 
        print $t, " : ";
        $t = $a1? LDE($t, @k1) : RPS($t, @k1); 
        print $t, " : ";
        $t = $b2? LDE($t, @k2) : RPS($t, @k2); 
        print $t, "\n";
}#/..

sub LDE {#../
# LDE returns the same as RPS except combinations of the same trinity
# return a different one ( Learning creates discovery, experienece creates
# learning, discovery creates experience )
        #
        my $a = shift;
        my $i = -1;
        foreach $b (@_) {
                $i++;
                $a == ONE && $b == THETA and $a = ZERO and $_[$i] = ZERO and next;
                $a == ZERO && $b == THETA and $a = ONE and $_[$i] = ONE and next;
                $a == THETA && $b == ONE and $a = ZERO and $_[$i] = ZERO and next;
                $a == THETA && $b == ZERO and $a = ONE and $_[$i] = ONE and next;
                $a == ONE && $b == ZERO and $a = THETA and $_[$i] = THETA and next;
                $a == ZERO && $b == ONE and $a = THETA and $_[$i] = THETA and next;
                $a == ZERO && $b == ZERO  and $a = THETA and $_[$i] = THETA and next;
                $a == THETA && $b == THETA and $a = ONE and $_[$i] = ONE and next;
                $a == ONE && $b == ONE and $a = ZERO and $_[$i] = ZERO and next;
        }
        return $a;
}#/..

sub RPS {#../
# "Rock Paper Scissors"
        my $a = shift;
        my $i = -1;
        foreach $b (@_) {
                $i++;
                $a == $b and next;
                $a == ONE && $b == THETA and $a = ZERO and $_[$i] = ZERO and next;
                $a == ZERO && $b == THETA and $a = ONE and $_[$i] = ONE and next;
                $a == THETA && $b == ONE and $a = ZERO and $_[$i] = ZERO and next;
                $a == THETA && $b == ZERO and $a = ONE and $_[$i] = ONE and next;
                $a == ONE && $b == ZERO and $a = THETA and $_[$i] = THETA and next;
                $a == ZERO && $b == ONE and $a = THETA and $_[$i] = THETA and next;
        }
        return $a;
}#/..

sub convert2nib{#../
# convert an integer to binary nibble
        $n = shift;
        my @a; 
        my $i = 0;
        while ($n >= 1) {
                $i++;
                unshift(@a, $n%2);
                $n = int($n/2);
        }
        while ($i < 4) {
                unshift (@a, 0);
                $i++;
        }
        return @a;
}#/..
