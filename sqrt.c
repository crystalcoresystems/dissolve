#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

void bail(char *str);

int main(int argc, char **argv)
{
        char buff[100];
        int x;
        //process args
        if(argc != 2) 
                bail("error: incorrect usage\n");
        x = atoi(argv[1]);
        sprintf(buff,"%d", x);
        if( strcmp(buff, argv[1]) != 0 )
                bail("error: incorrect usage\n");
        //print sqrt
        printf(  "%i", (int)sqrt( (double)x )  ); 
        exit(0);
}

void bail(char *str)
//exit program with error message
{
        fprintf(stderr, "error: %s\n", str);
        exit(1);
}
