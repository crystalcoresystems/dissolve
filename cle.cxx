// Include necessary libraries
#include <iostream>
#include <vector>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <cairo.h>
#include <cairo-xlib.h>
#include <chrono>
#include <set>
#include <queue>
#include <map>

// Enum for directions
enum Direction { NONE = -1, NORTH = 0, EAST, SOUTH, WEST };
// Enum for instructions
enum Instruction {
    MOVE,
    TURN_RIGHT,
    TURN_LEFT,
    SPIN,
    CREATE,
    DESTROY,
    SWITCH
};

struct Active_Cell {
    std::vector<int>                            x;
    std::vector<int>                            y;
    std::vector<int>                            prev_x;
    std::vector<int>                            prev_y;
    std::vector<Direction>                      direction;
    std::vector<std::vector<Instruction>>       behavior;
    std::vector<std::map<std::pair<int, int>, int>> behavior_map;
};

struct Grid_Data {
    std::vector<std::vector<int>> grid1;
    std::vector<std::vector<int>> grid2;
    std::vector<std::vector<int>> prev_grid;
    int width;
    int height;
};
struct Window_Data {
    Display *display;
    Window window;
    cairo_surface_t *surface;
    cairo_t *cr;

    Window design_window;
    cairo_surface_t *design_surface;
    cairo_t *design_cr;

    int screen;
    int width;
    int height;
    int design_width;
    int design_height;
};

const int WIDTH                     = 1020;
const int HEIGHT                    = 820;
const int DESIGN_WIDTH              = 200;
const int DESIGN_HEIGHT             = 300;
const int CELL_SIZE                 = 8; 
const int INTERVAL                  = 70; 
const int NUMBER_OF_INSTRUCTIONS    = 7; 

struct Adjacent_Cells {
    int north;
    int east;
    int west;
    int south;
};

// instruction_to_string../
std::string instruction_to_string(Instruction instruction) {
    std::string instruction_text;

    switch (instruction) {
        case MOVE:
            instruction_text = "▲";
            break;
        case SPIN:
            instruction_text = "♻";
            break;
        case SWITCH:
            instruction_text = "⇆";
            break;
        case TURN_LEFT:
            instruction_text = "↰";
            break;
        case CREATE:
            instruction_text = "⊕";
            break;
        case DESTROY:
            instruction_text = "⊖";
            break;
        case TURN_RIGHT:
            instruction_text = "↱";
            break;
        default:
            instruction_text = "�";
            break;
    }

    return instruction_text;
}//..
// intialize_custom_instructions../
std::vector<Instruction> initialize_custom_instructions() {
    std::vector<Instruction> custom_instructions = {
        MOVE,           // 1
        TURN_LEFT,           // 3
        TURN_RIGHT,         // 4
        SPIN,           // 2
        SWITCH,      // 5
        CREATE,         // 6
        CREATE,         // 7
        CREATE,     // 8
        DESTROY         // 9
    };
    
    return custom_instructions;
}//..
// draw_instruction_slot../
void draw_instruction_slot(cairo_t *cr, int instruction_index, Instruction current_instruction, int x, int y) {
    int arrow_offset = 20;
    std::string instruction_text = instruction_to_string(current_instruction);

    // Get the size of the instruction text
    cairo_text_extents_t text_extents;
    cairo_select_font_face(cr, "Arial", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, 18);
    cairo_text_extents(cr, instruction_text.c_str(), &text_extents);

    // Draw the instruction text at the specified (x, y) position
    cairo_move_to(cr, x - text_extents.width / 2, y + text_extents.height / 2);
    cairo_show_text(cr, instruction_text.c_str());

    // Draw left arrow at (x - arrow_offset - text_extents.width / 2, y)
    cairo_move_to(cr, x - arrow_offset - text_extents.width / 2, y - 5);
    cairo_line_to(cr, x - arrow_offset - text_extents.width / 2, y + 5);
    cairo_line_to(cr, x - arrow_offset - text_extents.width / 2 - 5, y);
    cairo_close_path(cr);
    cairo_fill(cr);

    // Draw right arrow at (x + arrow_offset + text_extents.width / 2, y)
    cairo_move_to(cr, x + arrow_offset + text_extents.width / 2, y - 5);
    cairo_line_to(cr, x + arrow_offset + text_extents.width / 2, y + 5);
    cairo_line_to(cr, x + arrow_offset + text_extents.width / 2 + 5, y);
    cairo_close_path(cr);
    cairo_fill(cr);
}//..
// render_design_window ../
void render_design_window(Window_Data &window_data, const std::vector<Instruction> &custom_instructions) {
    // Clear the design window
    cairo_set_source_rgb(window_data.design_cr, 1, 1, 1); // Set background color to white
    cairo_paint(window_data.design_cr);

    cairo_set_source_rgb(window_data.design_cr, 0, 0, 0); // Set text and arrow color to black

    int instruction_width = window_data.design_width;
    int instruction_height = window_data.design_height / 10;

    for (int i = 0; i < 9; i++) {
        int instruction_index = i;
        int x = instruction_width / 2;
        int y = 10 + i * instruction_height + instruction_height / 2;

        draw_instruction_slot(window_data.design_cr, instruction_index, custom_instructions[instruction_index], x, y);
    }

    // Render the updated design window
    cairo_show_page(window_data.design_cr);
}//..
// update_instruction_on_click../
void update_instruction_on_click(std::vector<Instruction> &custom_instructions, int click_x, int click_y, int design_width, int design_height, cairo_t *cr) {
    int instruction_width = design_width;
    int instruction_height = design_height / 10;
    int arrow_offset = 20;
    int arrow_size = instruction_height / 2;

    for (int i = 0; i < 9; i++) {
        int instruction_index = i;
        int x = instruction_width / 2;
        int y = 10 + i * instruction_height + instruction_height / 2;

        std::string instruction_text = instruction_to_string(custom_instructions[instruction_index]);
        cairo_text_extents_t text_extents;
        cairo_text_extents(cr, instruction_text.c_str(), &text_extents);

        int left_arrow_x = x - arrow_offset - text_extents.width / 2;
        int right_arrow_x = x + arrow_offset + text_extents.width / 2;

        // Check if the click was on the left arrow
        if (click_x > left_arrow_x - arrow_size && click_x < left_arrow_x + arrow_size &&
            click_y > y - arrow_size / 2 && click_y < y + arrow_size / 2) {
            // Decrement the instruction value, wrapping around if necessary
            custom_instructions[instruction_index] = static_cast<Instruction>(
                (custom_instructions[instruction_index] - 1 + NUMBER_OF_INSTRUCTIONS) % NUMBER_OF_INSTRUCTIONS);
        }

        // Check if the click was on the right arrow
        if (click_x > right_arrow_x - arrow_size && click_x < right_arrow_x + arrow_size &&
            click_y > y - arrow_size / 2 && click_y < y + arrow_size / 2) {
            // Increment the instruction value, wrapping around if necessary
            custom_instructions[instruction_index] = static_cast<Instruction>(
                (custom_instructions[instruction_index] + 1) % NUMBER_OF_INSTRUCTIONS);
        }
    }
}//..
// init_cell_design_window../
Window_Data init_cell_design_window(Window_Data &window_data, int width, int height) {
    // Initialize the X11 window for cell design
    window_data.design_window = XCreateSimpleWindow(window_data.display,
                                                    RootWindow(window_data.display, window_data.screen),
                                                    0, 0, width, height, 1,
                                                    BlackPixel(window_data.display, window_data.screen),
                                                    WhitePixel(window_data.display, window_data.screen));

    XStoreName(window_data.display, window_data.design_window, " ");
    XSelectInput(window_data.display, window_data.design_window,
                 ButtonPressMask | ExposureMask | KeyPressMask);
    XMapWindow(window_data.display, window_data.design_window);

    // Initialize cairo surface and context for cell design window
    window_data.design_surface = cairo_xlib_surface_create(window_data.display, window_data.design_window,
                                                           DefaultVisual(window_data.display, window_data.screen),
                                                           width, height);

    window_data.design_cr = cairo_create(window_data.design_surface);


    window_data.design_width = width;
    window_data.design_height = height;

    return window_data;
}//..
// init_x11_cairo ../
/*
 * Initializes the X11 window and cairo surfaces for drawing.
 * Creates an X11 window and sets up cairo surfaces for rendering.
 * This function returns a Window_Data struct containing the X11 window,
 * display, screen, cairo surfaces, and contexts required for drawing.
 *
 * @param width  The width of the X11 window.
 * @param height The height of the X11 window.
 * @return       A Window_Data struct containing the initialized X11 window, display,
 *               screen, cairo surfaces, and contexts.
 */
Window_Data init_x11_cairo(int width, int height) {
    Window_Data window_data;

    // Initialize the X11 display and window
    window_data.display = XOpenDisplay(NULL);
    window_data.screen = DefaultScreen(window_data.display);
    window_data.window = XCreateSimpleWindow(window_data.display, RootWindow(window_data.display, window_data.screen),
            0, 0, width, height, 1,
            BlackPixel(window_data.display, window_data.screen),
            WhitePixel(window_data.display, window_data.screen));

    XStoreName(window_data.display, window_data.window, "Crystal Life Engine");
    
    XSelectInput(window_data.display, window_data.window, ButtonPressMask | ExposureMask | KeyPressMask);
    XMapWindow(window_data.display, window_data.window);

    // Initialize cairo surface and context
    window_data.surface = cairo_xlib_surface_create(window_data.display, window_data.window,
            DefaultVisual(window_data.display, window_data.screen),
            width, height);
    window_data.cr = cairo_create(window_data.surface);

    window_data.width = width;
    window_data.height = height;

    return window_data;
}//..
// init_grid ../
/*
 * Initializes a grid with the specified dimensions and cell size.
 * This function creates a Grid_Data struct containing two grids (grid1 and grid2)
 * and a previous grid (prev_grid) for storing the previous state of the cells.
 * All grids are initialized with the specified dimensions and zero values.
 *
 * @param width     The width of the grid in pixels.
 * @param height    The height of the grid in pixels.
 * @param cell_size The size of each grid cell in pixels.
 * @return          A Grid_Data struct containing the initialized grids and their dimensions.
 */
Grid_Data init_grid(int width, int height, int cell_size) {
    Grid_Data grid_data;
    int grid_width = width / cell_size;
    int grid_height = height / cell_size;

    // Resize the grid to the desired dimensions
    grid_data.grid1.resize(grid_height, std::vector<int>(grid_width, 0));
    grid_data.grid2.resize(grid_height, std::vector<int>(grid_width, 0));
    grid_data.prev_grid = std::vector<std::vector<int>>(grid_height, std::vector<int>(grid_width, 0));
    grid_data.height = grid_height;
    grid_data.width = grid_width;
    return grid_data;
}//..
// intialize_active_cell../
/*
 * Initializes an Active_Cell struct with the specified number of active cells
 * and their random positions on the grid. The active cells and surrounding cells
 * are filled with random non-zero values in both grid1 and grid2.
 *
 * @param grid_data A reference to the Grid_Data struct containing the grid information.
 * @return          An Active_Cell struct containing the initialized active cells and their properties.
 */
Active_Cell initialize_active_cell(Grid_Data &grid_data) {
    Active_Cell active_cells;
        int num_active_cells = 0; // Change this value to set the number of active cells

    for (int i = 0; i < num_active_cells; ++i) {
        int x = rand() % grid_data.width;
        int y = rand() % grid_data.height;

        active_cells.x.push_back(x);
        active_cells.y.push_back(y);
        active_cells.prev_x.push_back(-1);
        active_cells.prev_y.push_back(-1);
        active_cells.direction.push_back(static_cast<Direction>(rand() % 4));

        // Fill in the grid values for the active cell and the surrounding cells with random values
        for (int j = -1; j <= 1; ++j) {
            for (int k = -2; k <= 2; ++k) {
                int new_x = x + j;
                int new_y = y + k;
                if (new_x >= 0 && new_x < grid_data.width && new_y >= 0 && new_y < grid_data.height) {
                    grid_data.grid1[new_y][new_x] = 1 + (rand() % 3);
                    grid_data.grid2[new_y][new_x] = 1 + (rand() % 3);
                }
            }
        }
    }

    return active_cells;
}//..
// render_grid../
/*
 * Renders the grid and active cells on the given window. The function only
 * draws cells that have changed to or from zero. It also renders the active
 * cells with a different color.
 *
 * @param window_data  A reference to the Window_Data struct containing the window and Cairo context.
 * @param grid_data    A reference to the Grid_Data struct containing the grid information.
 * @param active_cell  A reference to the Active_Cell struct containing the active cells and their properties.
 * @param cell_size    The size of each cell in pixels.
 */
void render_grid(Window_Data &window_data, Grid_Data &grid_data, Active_Cell &active_cell, int cell_size) {
    // Iterate through the grid and draw each cell
    for (int y = 0; y < grid_data.height; ++y) {
        for (int x = 0; x < grid_data.width; ++x) {

            int cell_value =  grid_data.grid2[y][x] + (grid_data.grid1[y][x] << 2 );

            // Skip the cell if it hasn't changed to or from zero
            if (cell_value ==  grid_data.prev_grid[y][x])
                continue;

            // Choose the cell color based on its value
            if (cell_value >= 1 && cell_value <= 15) {
                // Ghoulish green-blue shades for values
                 //cairo_set_source_rgb(window_data.cr,(1/(double)cell_value), 1 - 4*(1/(double)cell_value), 1);
    cairo_set_source_rgb(window_data.cr, 0.13, 0.55, 0.13);
            } else if (cell_value > 3) {
                // White for values above 3
                cairo_set_source_rgb(window_data.cr, 1, 1, 1);
            } else {
                // Black for zero cells
    cairo_set_source_rgb(window_data.cr, 0.96, 0.87, 0.70);
            }

            // Draw the cell as a filled rectangle
            cairo_rectangle(window_data.cr, x * cell_size, y * cell_size, cell_size, cell_size);
            cairo_fill(window_data.cr);
        }
    }

    // Render the active cell
    for (size_t i = 0; i < active_cell.x.size(); ++i) {
        if (active_cell.x[i] == active_cell.prev_x[i] && active_cell.y[i] == active_cell.prev_y[i])
            continue;


        if(active_cell.prev_y[i] != -1 && grid_data.grid1[active_cell.prev_y[i]][active_cell.prev_x[i]] != 0) {
            int cell_value = (grid_data.grid2[active_cell.prev_y[i]][active_cell.prev_x[i]] << 2) +
                                grid_data.grid1[active_cell.prev_y[i]][active_cell.prev_x[i]];
            //cairo_set_source_rgb(window_data.cr,(1/(double)cell_value), 1 - 4*(1/(double)cell_value), 1);
    cairo_set_source_rgb(window_data.cr, 0.13, 0.55, 0.13);
            cairo_rectangle(window_data.cr, active_cell.prev_x[i] * cell_size, active_cell.prev_y[i] * cell_size, cell_size, cell_size);
            cairo_fill(window_data.cr);
        }

    cairo_set_source_rgb(window_data.cr, 0.13, 0.55, 0.13);
        cairo_rectangle(window_data.cr, active_cell.x[i] * cell_size, active_cell.y[i] * cell_size, cell_size, cell_size);
        cairo_fill(window_data.cr);
    }
}//..
// get_adjacent_cells../
/*
 * Retrieves the adjacent cells (north, south, east, and west) of a given cell in the grid.
 * The function takes into account the grid's boundaries and updates the adjacent cells accordingly.
 *
 * @param grid    A reference to the 2D vector representing the grid.
 * @param x       The x-coordinate of the cell in question.
 * @param y       The y-coordinate of the cell in question.
 * @param width   The width of the grid.
 * @param height  The height of the grid.
 * @return        An Adjacent_Cells struct containing the values of the adjacent cells.
 */
Adjacent_Cells get_adjacent_cells(const std::vector<std::vector<int>>& grid, int x, int y, int width, int height) {
    Adjacent_Cells adjacent_cells;

    // Find the adjacent cells
    adjacent_cells.north = (y > 0) ? grid[y - 1][x] : 0;
    adjacent_cells.south = (y < height - 1) ? grid[y + 1][x] : 0;
    adjacent_cells.west = (x > 0) ? grid[y][x - 1] : 0;
    adjacent_cells.east = (x < width - 1) ? grid[y][x + 1] : 0;

    // Update the adjacent cells based on the conditions
    if (adjacent_cells.north == 0 && adjacent_cells.south == 0) {
        adjacent_cells.north = 0;
        adjacent_cells.south = 0;
    } else if (adjacent_cells.north == 0) {
        int temp_y = y + 1;
        while (temp_y < height && grid[temp_y][x] != 0) {
            temp_y++;
        }
        adjacent_cells.north = (temp_y < 0) ? grid[temp_y - 1][x] : 0;
    } else if (adjacent_cells.south == 0) {
        int temp_y = y - 1;
        while (temp_y >= 0 && grid[temp_y][x] != 0) {
            temp_y--;
        }
        adjacent_cells.south = (temp_y >= 0) ? grid[temp_y + 1][x] : 0;
    }

    if (adjacent_cells.east == 0 && adjacent_cells.west == 0) {
        adjacent_cells.east = 0;
        adjacent_cells.west = 0;
    } else if (adjacent_cells.east == 0) {
        int temp_x = x - 1;
        while (temp_x < width && grid[y][temp_x] != 0) {
            temp_x--;
        }
        adjacent_cells.east = (temp_x >= 0) ? grid[y][temp_x + 1] : 0;
    } else if (adjacent_cells.west == 0) {
        int temp_x = x + 1;
        while (temp_x >= 0 && grid[y][temp_x] < width) {
            temp_x++;
        }
        adjacent_cells.west = (temp_x < width) ? grid[y][temp_x - 1] : 0;
    }

    return adjacent_cells;
}//..
// rps_solve../
/*
 * Determines the odd move out or the value played for a tie in a Rock, Paper, Scissors-like game.
 *
 * @param input   The input representing the player's move (1: Rock, 2: Paper, 3: Scissors).
 * @param mech    The input representing the other player's move
 * @return        An integer representing the odd move out or the value played for a tie.
 */
int rps_solve(int input, int mech)
{
    int tmp, tmp2, tmp3;

    tmp = input ^ mech;
    tmp3 = (tmp >> 1) | (tmp << 1) | tmp;
    tmp2 = tmp ^ tmp3;
    tmp2 = tmp2 ^ tmp3;
    tmp = ~tmp3 & input;
    return (tmp ^ tmp2) & 3;
}//..
// iterate_grid../
/*
 * Iterates through the grid and updates cell values based on the adjacent cells
 * using the rules of a Rock, Paper, Scissors-like game.
 *
 * @param old_grid   A reference to the grid of integers representing the current cell states.
 * @param width      The width of the grid.
 * @param height     The height of the grid.
 */
void iterate_grid(std::vector<std::vector<int>>& old_grid, int width, int height) {

    // Create a new grid with the same dimensions as the old grid
    std::vector<std::vector<int>> new_grid(height, std::vector<int>(width, 0));

    // Iterate over each cell in the old grid
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            if(old_grid[y][x] == 0) {
                new_grid[y][x] = 0;
                continue;
            }
            // Get the updated adjacent cells for the current cell
            Adjacent_Cells adjacent_cells = get_adjacent_cells(old_grid, x, y, width, height);
            // Solve the cells together using rps_solve()
            int sol1 = rps_solve(adjacent_cells.north, adjacent_cells.south);
            int sol2 = rps_solve(adjacent_cells.east, adjacent_cells.west);

            // Update the new grid with the solved value
            new_grid[y][x] = rps_solve(sol1, sol2);
        }
    }

    // Set the old grid equal to the new grid
    old_grid = new_grid;
}//..
// get_instruction_from_grid_values../
Instruction get_instruction_from_grid_values(const Active_Cell &active_cell, int i, int grid1_value, int grid2_value) {
    std::pair<int, int> grid_values = std::make_pair(grid1_value, grid2_value);
    int instruction_index = active_cell.behavior_map[i].at(grid_values);
    return active_cell.behavior[i][instruction_index];
}//..
// create_new_active_cell../
/*
 * Creates a new active cell at the specified location (x, y) and initializes
 * its properties, including direction. Also, sets the values of the surrounding
 * cells in the grid to random non-zero values.
 *
 * @param active_cell    A reference to an Active_Cell structure containing the
 *                       information about the active cell, such as its position
 *                       and direction.
 * @param grid_data      A reference to a Grid_Data structure containing information
 *                       about the grid, including the cell values.
 * @param x              The x-coordinate of the new active cell.
 * @param y              The y-coordinate of the new active cell.
 */
void create_new_active_cell(Active_Cell &active_cell, Grid_Data &grid_data, int x, int y,
                            const std::vector<Instruction> &instructions = {}) {

    // Add a new active cell at the specified location
    active_cell.x.push_back(x);
    active_cell.y.push_back(y);
    active_cell.prev_x.push_back(-1);
    active_cell.prev_y.push_back(-1);
    active_cell.direction.push_back(static_cast<Direction>(rand() % 4));

    std::vector<Instruction> default_behavior = {
        MOVE,           //
        SPIN,     //
        SPIN,         //
        SWITCH,         //
        TURN_LEFT,         //
        CREATE,        //
        CREATE,        //
        TURN_RIGHT,           //
        DESTROY      //
    };

    std::map<std::pair<int, int>, int> default_behavior_map = {
        {{1, 1}, 0},
        {{1, 2}, 1},
        {{1, 3}, 2},
        {{2, 1}, 3},
        {{2, 2}, 4},
        {{2, 3}, 5},
        {{3, 1}, 6},
        {{3, 2}, 7},
        {{3, 3}, 8}
    };



    // If instructions are provided, use them; otherwise, use the default behavior
    std::vector<Instruction> behavior_to_use = instructions.empty() ? default_behavior : instructions;

    // Initialize behavior and behavior_map
    active_cell.behavior.push_back(behavior_to_use);
    active_cell.behavior_map.push_back(default_behavior_map);


srand(777);
int size = 0;

// Draw the larger square
for (int i = -1; i <= size; ++i) {
    for (int j = -1; j <= size; ++j) {
        int new_x = x + i;
        int new_y = y + j;

        if (new_x >= 0 && new_x < grid_data.width && new_y >= 0 && new_y < grid_data.height) {
            if (grid_data.grid1[new_y][new_x] == 0) {
                grid_data.grid1[new_y][new_x] = 1 + rand() % 3;
                grid_data.grid2[new_y][new_x] = 1 + rand() % 3;
            }
        }
    }
}

}//..
// create_new_active_cell2../
/*
 * Creates a new active cell at the specified location (x, y) and initializes
 * its properties, including direction. Also, sets the values of the surrounding
 * cells in the grid to random non-zero values.
 *
 * @param active_cell    A reference to an Active_Cell structure containing the
 *                       information about the active cell, such as its position
 *                       and direction.
 * @param grid_data      A reference to a Grid_Data structure containing information
 *                       about the grid, including the cell values.
 * @param x              The x-coordinate of the new active cell.
 * @param y              The y-coordinate of the new active cell.
 */
void create_new_active_cell2(Active_Cell &active_cell, Grid_Data &grid_data, int x, int y,
                            const std::vector<Instruction> &instructions = {}) {

    // Add a new active cell at the specified location
    active_cell.x.push_back(x);
    active_cell.y.push_back(y);
    active_cell.prev_x.push_back(-1);
    active_cell.prev_y.push_back(-1);
    active_cell.direction.push_back(static_cast<Direction>(rand() % 4));

    std::vector<Instruction> default_behavior = {
        MOVE,           //
        SPIN,     //
        SPIN,         //
        SWITCH,         //
        TURN_LEFT,         //
        CREATE,        //
        CREATE,        //
        TURN_RIGHT,           //
        DESTROY      //
    };

    std::map<std::pair<int, int>, int> default_behavior_map = {
        {{1, 1}, 0},
        {{1, 2}, 1},
        {{1, 3}, 2},
        {{2, 1}, 3},
        {{2, 2}, 4},
        {{2, 3}, 5},
        {{3, 1}, 6},
        {{3, 2}, 7},
        {{3, 3}, 8}
    };



    // If instructions are provided, use them; otherwise, use the default behavior
    std::vector<Instruction> behavior_to_use = instructions.empty() ? default_behavior : instructions;

    // Initialize behavior and behavior_map
    active_cell.behavior.push_back(behavior_to_use);
    active_cell.behavior_map.push_back(default_behavior_map);
}//..
// destroy_cell_at_index../
void destroy_cell_at_index(Active_Cell &active_cell, size_t index) {
    active_cell.x.erase(active_cell.x.begin() + index);
    active_cell.y.erase(active_cell.y.begin() + index);
    active_cell.prev_x.erase(active_cell.prev_x.begin() + index);
    active_cell.prev_y.erase(active_cell.prev_y.begin() + index);
    active_cell.direction.erase(active_cell.direction.begin() + index);
    active_cell.behavior.erase(active_cell.behavior.begin() + index);
    active_cell.behavior_map.erase(active_cell.behavior_map.begin() + index);
}//..
// process_active_cell../
/*
 * Processes the active cell by performing actions based on the cell values
 * and updates the grid accordingly. Actions include moving, turning, creating,
 * and destroying cells in the grid.
 *
 * @param window_data    A reference to a Window_Data structure containing information
 *                       about the rendering context.
 * @param grid_data      A reference to a Grid_Data structure containing information
 *                       about the grid, including the cell values.
 * @param active_cell    A reference to an Active_Cell structure containing the
 *                       information about the active cell, such as its position
 *                       and direction.
 */
void process_active_cell(Window_Data &window_data, Grid_Data &grid_data, Active_Cell &active_cell) {
    // Helper function: reproduce
    auto reproduce = [&](Active_Cell &active_cell, Grid_Data &grid_data, const std::vector<size_t> &cells_to_reproduce) {
        int x = active_cell.x[cells_to_reproduce[0]];
        int y = active_cell.y[cells_to_reproduce[0]];

        // Define the dx, dy values for each direction (north, east, south, west)
        std::vector<int> dx = {0, 1, 0, -1};
        std::vector<int> dy = {-1, 0, 1, 0};

        for (int dir = 0; dir < 4; dir++) {
            int new_x = x;
            int new_y = y;

            new_x += dx[dir];
            new_y += dy[dir];
            while (new_x >= 0 && new_x < grid_data.width &&
                    new_y >= 0 && new_y < grid_data.height &&
                    grid_data.grid1[new_y][new_x] != 0) {
                new_x += dx[dir];
                new_y += dy[dir];
            }

            new_x -= dx[dir];
            new_y -= dy[dir];
            if (!(new_x == x && new_y == y)) {
                // Generate offspring instructions by crossover
                std::vector<Instruction> offspring_instructions;
                size_t num_instructions = active_cell.behavior[0].size();
                for (size_t instr_idx = 0; instr_idx < num_instructions; ++instr_idx) {
                    size_t parent_idx = rand() % cells_to_reproduce.size();
                    offspring_instructions.push_back(active_cell.behavior[cells_to_reproduce[parent_idx]][instr_idx]);
                }

                create_new_active_cell2(active_cell, grid_data, new_x, new_y, offspring_instructions);
            }
        }
    };

    std::set<size_t>                    cells_to_destroy;
    std::set<std::pair<int, int>>       cells_to_reproduce;
    std::set<size_t>                    marked_for_reproduction;
    std::map<std::pair<int, int>, std::vector<size_t>> cells_by_coordinates;
    std::vector<std::vector<int>> cell_counts(grid_data.height, std::vector<int>(grid_data.width, 0));

    for (size_t i = 0; i < active_cell.x.size(); ++i) {
        int x = active_cell.x[i];
        int y = active_cell.y[i];
        cell_counts[y][x]++;
    }

    for (size_t i = 0; i < active_cell.x.size(); ++i) {
        int x = active_cell.x[i];
        int y = active_cell.y[i];

        active_cell.prev_x[i] = x;
        active_cell.prev_y[i] = y;

        int grid1_value = grid_data.grid1[y][x];
        int grid2_value = grid_data.grid2[y][x];

            /// Check for Destroyed Cell
         if (grid1_value == 0) {
            cells_to_destroy.insert(i);
            continue;
         } 

        // Check for Partner Cell
        if (cell_counts[y][x] > 1) {
            std::pair<int, int> coord = std::make_pair(x, y);

            // Add the cells into the list corresponding to the coordinates in the cells_to_reproduce map
            cells_by_coordinates[coord].push_back(i);
            cells_to_destroy.insert(i);
            continue;
        }

        // Get the instruction based on the grid1_value and grid2_value
        Instruction instruction = get_instruction_from_grid_values(active_cell, i, grid1_value, grid2_value);

        Direction direction = active_cell.direction[i];

        // Act on the instruction
        switch (instruction) {
            case MOVE: {
                           // Calculate the new position based on the direction
                           int new_x = active_cell.x[i];
                           int new_y = active_cell.y[i];

                           switch (active_cell.direction[i]) {
                               case NORTH:
                                   new_y -= 1;
                                   break;
                               case EAST:
                                   new_x += 1;
                                   break;
                               case SOUTH:
                                   new_y += 1;
                                   break;
                               case WEST:
                                   new_x -= 1;
                                   break;
                           }

                           // Check if the new position is within bounds and not a 0 value cell
                           if (new_x >= 0 && new_x < grid_data.width &&
                                   new_y >= 0 && new_y < grid_data.height &&
                                   grid_data.grid1[new_y][new_x] != 0) {
                               active_cell.x[i] = new_x;
                               active_cell.y[i] = new_y;
                           }
                           break;
                       }
            case TURN_LEFT:
                       // Turn the active cell right (modulus is used to cycle through the directions)
                       active_cell.direction[i] = static_cast<Direction>((active_cell.direction[i] - 1));
                       if(active_cell.direction[i] == NONE)
                           active_cell.direction[i] = WEST;
                       break;
            case TURN_RIGHT:
                       // Turn the active cell right (modulus is used to cycle through the directions)
                       active_cell.direction[i] = static_cast<Direction>((active_cell.direction[i] + 1) % 4);
                       break;
            case SPIN:
                       // Turn the active cell right (modulus is used to cycle through the directions)
                       active_cell.direction[i] = static_cast<Direction>((active_cell.direction[i] + 2) % 4);
                       break;
            case SWITCH: {
                             // Iterate through the behavior vector of the active cell
                             for (size_t j = 0; j < active_cell.behavior[i].size(); ++j) {
                                 // Switch CREATE to DESTROY and vice versa
                                 if (active_cell.behavior[i][j] == CREATE) {
                                     active_cell.behavior[i][j] = DESTROY;
                                 } else if (active_cell.behavior[i][j] == DESTROY) {
                                     active_cell.behavior[i][j] = CREATE;
                                 }
                             }
                             break;
                         }
            case CREATE: {
                             int new_x = active_cell.x[i];
                             int new_y = active_cell.y[i];
                             int dx = 0;
                             int dy = 0;

                             switch (active_cell.direction[i]) {
                                 case NORTH:
                                     dy = -1;
                                     break;
                                 case EAST:
                                     dx = 1;
                                     break;
                                 case SOUTH:
                                     dy = 1;
                                     break;
                                 case WEST:
                                     dx = -1;
                                     break;
                             }

                             while (new_x >= 0 && new_x < grid_data.width && new_y >= 0 && new_y < grid_data.height) {
                                 if (grid_data.grid1[new_y][new_x] == 0) {
                                     grid_data.grid1[new_y][new_x] = grid_data.grid1[new_y-dy][new_x-dx];
                                     grid_data.grid2[new_y][new_x] = grid_data.grid2[new_y-dy][new_x-dx];
                                     break;
                                 }
                                 new_x += dx;
                                 new_y += dy;
                             }
                             break;
                         }
            case DESTROY: {
                              int new_x = active_cell.x[i];
                              int new_y = active_cell.y[i];
                              int dx = 0;
                              int dy = 0;
                              int prev_x = new_x;
                              int prev_y = new_y;

                              switch (active_cell.direction[i]) {
                                  case NORTH:
                                      dy = -1;
                                      break;
                                  case EAST:
                                      dx = 1;
                                      break;
                                  case SOUTH:
                                      dy = 1;
                                      break;
                                  case WEST:
                                      dx = -1;
                                      break;
                              }

                              while (new_x >= 0 && new_x < grid_data.width && new_y >= 0 && new_y < grid_data.height) {
                                  if (grid_data.grid1[new_y][new_x] == 0) {
                                      if(!(active_cell.x[i] == prev_x && active_cell.y[i] == prev_y)) {
                                          grid_data.grid1[prev_y][prev_x] = 0;
                                          grid_data.grid2[prev_y][prev_x] = 0;
                                      }
                                      break;
                                  }
                                  prev_x = new_x;
                                  prev_y = new_y;
                                  new_x += dx;
                                  new_y += dy;
                              }
                              if(new_x == grid_data.width || new_y == grid_data.height || new_x == -1 || new_y == -1) {
                                  grid_data.grid1[prev_y][prev_x] = 0;
                                  grid_data.grid2[prev_y][prev_x] = 0;
                              }

                              break;
                          }
        }
    }


    // Iterate through cells_by_coordinates and call reproduce() with the list of cells
    for (const auto &coord_cells : cells_by_coordinates) {
        const auto &coordinates = coord_cells.first;
        const auto &active_cells = coord_cells.second;
        reproduce(active_cell, grid_data, active_cells);
    }

    for (auto it = cells_to_destroy.rbegin(); it != cells_to_destroy.rend(); ++it) {
        //cairo_set_source_rgb(window_data.cr,(1/(double)5), 1 - 4*(1/(double)5), 1);
    cairo_set_source_rgb(window_data.cr, 0.13, 0.55, 0.13);
        cairo_rectangle(window_data.cr, active_cell.x[*it] * CELL_SIZE, active_cell.y[*it] * CELL_SIZE, CELL_SIZE, CELL_SIZE);
        cairo_fill(window_data.cr);
        destroy_cell_at_index(active_cell, *it);
    }
    // Redraw the grid and active cell after adding the new active cell
    //render_grid(window_data, grid_data, active_cell, CELL_SIZE);

}//..
// destroy_cells_bfs../
/*
 * Performs a breadth-first search (BFS) algorithm to destroy cells in the grid,
 * starting from the specified location (start_x, start_y). During the process,
 * active cells at the destroyed locations are removed, and the destroyed cells
 * are drawn as black rectangles on the window.
 *
 * @param window_data    A reference to a Window_Data structure containing information
 *                       about the window and the Cairo context used for drawing.
 * @param grid_data      A reference to a Grid_Data structure containing information
 *                       about the grid, including the cell values.
 * @param active_cell    A reference to an Active_Cell structure containing the
 *                       information about the active cells, such as their positions
 *                       and directions.
 * @param start_x        The x-coordinate of the starting location for the BFS.
 * @param start_y        The y-coordinate of the starting location for the BFS.
 */
void destroy_cells_bfs(Window_Data& window_data, Grid_Data &grid_data, Active_Cell &active_cell, int start_x, int start_y) {
    std::queue<std::pair<int, int>> queue;
    std::set<std::pair<int, int>> visited;

    queue.push({start_x, start_y});
    visited.insert({start_x, start_y});

    while (!queue.empty()) {
        auto current = queue.front();
        queue.pop();

        int x = current.first;
        int y = current.second;

        // Remove the active cell if it's present at the current location
        for (size_t i = 0; i < active_cell.x.size(); ++i) {
            if (active_cell.x[i] == x && active_cell.y[i] == y) {
                destroy_cell_at_index(active_cell, i);
                --i;
            }
        }

        // Destroy the CA cells
        grid_data.grid1[y][x] = 0;
        grid_data.grid2[y][x] = 0;

        // Draw the destroyed cell as a black rectangle
    cairo_set_source_rgb(window_data.cr, 0.96, 0.87, 0.70);
        cairo_rectangle(window_data.cr, x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE, CELL_SIZE);
        cairo_fill(window_data.cr);

        // Iterate through the neighboring cells
        int dx[] = {-1, 1, 0, 0};
        int dy[] = {0, 0, -1, 1};

        for (int i = 0; i < 4; ++i) {
            int new_x = x + dx[i];
            int new_y = y + dy[i];

            // Check if the new position is within bounds and not visited
            if (new_x >= 0 && new_x < grid_data.width &&
                    new_y >= 0 && new_y < grid_data.height &&
                    visited.find({new_x, new_y}) == visited.end()) {

                // Check if the neighboring cell is not empty (value != 0)
                if (grid_data.grid1[new_y][new_x] != 0) {
                    queue.push({new_x, new_y});
                    visited.insert({new_x, new_y});
                }
            }
        }
    }
}//..
// run_simulation../
/*
 * Executes the main event loop for the cellular automata simulation. It handles
 * user input, updates the grid and active cells, and redraws the grid at a
 * specified time interval. The simulation exits when the user presses a key or
 * closes the window.
 *
 * @param window_data    A reference to a Window_Data structure containing information
 *                       about the window and the Cairo context used for drawing.
 * @param grid_data      A reference to a Grid_Data structure containing information
 *                       about the grid, including the cell values.
 * @param active_cell    A reference to an Active_Cell structure containing the
 *                       information about the active cells, such as their positions
 *                       and directions.
 * @param cell_size      The size of each cell in the grid, used for rendering and
 *                       converting screen coordinates to grid coordinates.
 */
void run_simulation(Window_Data &window_data, Grid_Data &grid_data, Active_Cell &active_cell, int cell_size) {

    // Set the desired time interval between iterations (e.g., 100 milliseconds)
    const std::chrono::milliseconds time_interval(INTERVAL);

    // Main event loop
    XEvent event;
    bool running = true;
    auto next_frame_time = std::chrono::steady_clock::now() + time_interval;


    std::vector<Instruction> custom_instructions = initialize_custom_instructions();
    render_design_window(window_data, custom_instructions); // Pass custom_instructions here

    while (running) {
        while (XPending(window_data.display)) {
            XNextEvent(window_data.display, &event);

            // Check which window the event occurred in
            if (event.xany.window == window_data.design_window) {
                // Handle mouse clicks in the design window
                if (event.type == ButtonPress) {
                    update_instruction_on_click( custom_instructions, event.xbutton.x, event.xbutton.y, window_data.design_width,
                            window_data.design_height, window_data.design_cr);
                }
                render_design_window(window_data, custom_instructions);
                break;
            }

            switch (event.type) {
                case Expose:
                    // Redraw the grid and active cell
    cairo_set_source_rgb(window_data.cr, 0.96, 0.87, 0.70);
    cairo_paint(window_data.cr);
                    render_grid(window_data, grid_data, active_cell, cell_size);
                    break;

                case KeyPress:
                    // Exit the program when a key is pressed
                //    running = false;
                    break;

                case DestroyNotify:
                    // Exit the program when the window is closed
                    running = false;
                    break;

                case ButtonPress:
                    // Handle left mouse click
                    if (event.xbutton.button == Button1) {
                        int grid_x = event.xbutton.x / cell_size;
                        int grid_y = event.xbutton.y / cell_size;

                        // Call the function to create a new active cell
                        create_new_active_cell(active_cell, grid_data, grid_x, grid_y, custom_instructions);

                        // Redraw the grid and active cell after adding the new active cell
                        render_grid(window_data, grid_data, active_cell, cell_size);
                    }
                    // Handle right mouse click
                    else if (event.xbutton.button == Button3) {
                        int grid_x = event.xbutton.x / cell_size;
                        int grid_y = event.xbutton.y / cell_size;

                        // Call the function to destroy cells using BFS
                        destroy_cells_bfs(window_data, grid_data, active_cell, grid_x, grid_y);
                    }
                    break;
            }
        }

        // Check if it's time for the next frame
        auto now = std::chrono::steady_clock::now();
        if (now >= next_frame_time) {
            // Set the time for the next frame
            next_frame_time = now + time_interval;


            // Save the current grid state to the previous grid
            for (int y = 0; y < grid_data.height; ++y) {
                for (int x = 0; x < grid_data.width; ++x) {
                    grid_data.prev_grid[y][x] = (grid_data.grid1[y][x] << 2) + grid_data.grid2[y][x];
                }
            }
            // Update the active cell and cellular automata
            //update_active_cell(active_cell, grid_data, resource_data);
            iterate_grid(grid_data.grid1, grid_data.width, grid_data.height);
            iterate_grid(grid_data.grid2, grid_data.width, grid_data.height);

            process_active_cell(window_data, grid_data, active_cell);
            // Redraw the grid and active cell
            render_grid(window_data, grid_data, active_cell, cell_size);

        }
    }
}//..

/*
 * The main function of the cellular automata simulation program.
 * Initializes the grid, active cell, and window data, sets up the
 * X11 and Cairo context, and runs the simulation. It also cleans up
 * resources before exiting.
 */
int main() {
    // Initialize the grid, active cell, and window data
    Grid_Data grid_data = init_grid(WIDTH, HEIGHT, CELL_SIZE);
    Grid_Data resource_data = init_grid(WIDTH, HEIGHT, CELL_SIZE);
    Active_Cell active_cell = initialize_active_cell(grid_data);

    // Set up the X11 and cairo context
    Window_Data window_data = init_x11_cairo(WIDTH, HEIGHT);
    window_data = init_cell_design_window(window_data, DESIGN_WIDTH, DESIGN_HEIGHT);

    // Run the simulation
    run_simulation(window_data, grid_data, active_cell, CELL_SIZE);

    // Clean up resources
    // ...
    cairo_surface_destroy(window_data.surface);
    cairo_destroy(window_data.cr);
    XDestroyWindow(window_data.display, window_data.window);
    XCloseDisplay(window_data.display);

    return 0;
}
