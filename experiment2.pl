# Experiment 2
#
# Idea - Verify that a scrambling "lock" works with more than one operation
#        on each trinity
#
# Hypothesis - further scrambling will return the same amount of data 5/9
#              as long as each key uses the same number of scrambles
#
# Conclusion - The hypothesis is correct - when both are odd or both are even
#              the same data is retrieved. 
#              When one is even and the other is odd - 2/3 of data is retrieved
#
#              However with this experiment I have notices that the center
#              pattern can be read to determine the outcome, therefore anyone
#              who can see all locked messages can infer the result

# Define cell types used in automata
use constant ONE => 1;
use constant THETA => 8;
use constant ZERO => 0;

# Create an array of all cell types
@a = (ONE, THETA, ZERO);
# Loop through all numbers 0 - 15 and use them as permutations
# by converting to binary "nibble" and passing to solve function
for my $i (0..15) {
        my @b = convert2nib($i);
        print @b, "\n";
        foreach my $trinity (@a) {

                # Solve all combinations of cell types
                solve($trinity, @b, ZERO, ZERO);  #1
                solve($trinity, @b, ZERO, ONE);   #2
                solve($trinity, @b, ZERO, THETA); #3
                solve($trinity, @b, ONE, ZERO);   #4
                solve($trinity, @b, ONE, ONE);    #5
                solve($trinity, @b, ONE, THETA);  #6
                solve($trinity, @b, THETA, ZERO); #7
                solve($trinity, @b, THETA, ONE);  #8
                solve($trinity, @b, THETA, THETA);#9
        }
        print "\n\n";
}

sub solve {#../
        my ($t, $a1, $b1, $a2, $b2, $k1, $k2) = @_;
        print $t, " : ";
        # Use binary permutation to test (a's and b's are either 1 or 0)
        # Pass the trinity through 2 keys in order k1 k2 k1 k2
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        print $t, " : ";
        $t = $b1? LDE($t, $k2) : RPS($t, $k2); 
        $t = $b1? LDE($t, $k2) : RPS($t, $k2); 
        $t = $b1? LDE($t, $k2) : RPS($t, $k2); 
        print $t, " : ";
        $t = $a2? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        $t = $a1? LDE($t, $k1) : RPS($t, $k1); 
        print $t, " : ";
        $t = $b2? LDE($t, $k2) : RPS($t, $k2); 
        $t = $b2? LDE($t, $k2) : RPS($t, $k2); 
        $t = $b2? LDE($t, $k2) : RPS($t, $k2); 
        print $t, "\n";
}#/..

sub LDE {#../
# LDE returns the same as RPS except combinations of the same trinity
# return a different one ( Learning creates discovery, experienece creates
# learning, discovery creates experience )
#
        my ($a, $b) = @_;
        $a == ONE && $b == THETA and return ZERO;
        $a == ZERO && $b == THETA and return ONE;
        $a == THETA && $b == ONE and return ZERO;
        $a == THETA && $b == ZERO and return ONE;
        $a == ONE && $b == ZERO and return THETA;
        $a == ZERO && $b == ONE and return THETA;
        $a == ZERO && $b == ZERO  and return THETA;
        $a == THETA && $b == THETA and return ONE;
        $a == ONE && $b == ONE and return ZERO;
}#/..

sub RPS {#../
# "Rock Paper Scissors"
        my ($a, $b) = @_;
        $a == $b and return $a;
        $a == ONE && $b == THETA and return ZERO;
        $a == ZERO && $b == THETA and return ONE;
        $a == THETA && $b == ONE and return ZERO;
        $a == THETA && $b == ZERO and return ONE;
        $a == ONE && $b == ZERO and return THETA;
        $a == ZERO && $b == ONE and return THETA;
}#/..

sub convert2nib{#../
# convert an integer to binary nibble
        $n = shift;
        my @a; 
        my $i = 0;
        while ($n >= 1) {
                $i++;
                unshift(@a, $n%2);
                $n = int($n/2);
        }
        while ($i < 4) {
                unshift (@a, 0);
                $i++;
        }
        return @a;
}#/..
